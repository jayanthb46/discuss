+++
title = "Introduction to T-cells"
date = 2023-02-19
draft = false
tags = ["T -cells"]
categories = []
+++

## Introduction to T-cells

T-cells are important members of white blood cells. They are part of Adaptive immune system.
T – cells are formed in bone marrow. The T-cells represent cells that migrate to thymus from bone marrow.

The T-cells when present in bone marrow they are called Progenitor T-cells. The transfer of T-cells from the bone marrow to Thymus is called chemotaxsis. 

In Thymus T-cells differentiate in to mature T-cells . 

T-cells can recognize antigens presented by specific class of molecules called Major Histocompatibility complex class 1 (MHC1) and  class 2 (MHC2).

In Thymus mature T-cells start expressing antigen binding pocket such as T-cell Receptor. All mature T-cell Receptors express T-cell Receptor in combination with cluster of Differentiation 4 (CD4) or cluster of Differentiation 8 (CD8), but never both


## Story of Thymus

In 1960 Thymus was considered as vestigial organ . John miller a Phd student in London while studying the onset of leukemia in inbred  mice accidentally identified the immunological role of Thymus .

Thymectamy was used as tool for studying onset of cancer inbread mice. Miller found a correlation between timing of Thymectomy and count of lymphocytes. Thymectomy performed on day1 resulted in less count of Lymphocytes.

Thymectomy performed on day 5 resulted in improved count of lymphocytes.

The Day one Thymectomy mice was suseptible to infections in normal environment compared to Day five Thymectomy mice.

The Day 1 Thymectomy mice was  able to control infections in disease free sterile environment.

This experiment by Miller proved that Thymus plays a major role in Immunity.

Similar experiment by Parrot on neonatal mice resulted in neutrophilia and reduced life span.

Additional experiments were performed in which bursa of Fabricus and Thymus was removed in chicken. After removal of Thymus birds survived only for few weeks.

This experiments concluded that certain cells that originate in Thymus is responsible for protection from infectioous disease. Hence the name T (Thymus) was used in the further studies





