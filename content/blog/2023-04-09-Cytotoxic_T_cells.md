+++
title = "Cytotoxic T-cells"
date = 2023-04-09
draft = false
tags = ["T -cells"]
categories = []
+++

## Cytotoxic T-cells

Cytotoxic T cells (CTL) mediate antigen-specific, MHC-restricted cytotoxicity and are important for killing intra-cytoplasmic parasites that are not accessible to secreted antibody or to phagocytes. Examples include all viruses, rickettsias (causes of Rocky Mountain spotted fever and typhus), some obligate intracellular bacteria (Chlamydia), and some protozoan parasites which export their proteins from macrophage vesicles to the cytoplasm (Toxoplasma gondii). The only way to eliminate these pathogens is to kill their host cells.


Effector CTL bind their targets first via nonspecific adhesion molecules such as LFA-1 (CD 11a/18), then with specific TCR. TCR-peptide-MHC-CD8 binding reorients the CTL cytoskeleton to focus release of effector molecules towards the target cell. CTL induce apoptosis in their targets. Cells undergoing apoptosis undergo chromatin condensation and membrane vesicle shedding. DNA is cut into pieces in multiples of 200bp, which look like steps on a ladder (and is called a "DNA ladder") when run on a gel that separates the DNA by size. Fragmented DNA can be detected in individual cells using the TUNEL assay.

Programming the target cell to die requires only about 5 minutes contact between CTL and target, although the targets may appear viable for much longer. CTL then dissociate to bind and kill other target cells presenting the same epitope.

### First Mechanism
 
During their maturation into effector CTL, CD8 cells synthesize cytotoxic molecules and store them in cytoplasmic granules for quick release upon target cell binding. Perforin has sequence homology with complement C9, and polymerizes to form pores in the target cell membrane through which water and salts can enter. At high perforin concentrations, this may be enough to destroy the target by osmotic lysis, but physiological levels of perforin are believed to be too low to induce osmotic lysis. Instead, perforin pores allow granzymes to enter the target cell. Granzymes are serine proteases which trigger the apoptotic cascade leading to DNA fragmentation and membrane-bound vesicle shedding. Apoptotic enzymes activated by granzymes can also destroy viruses or other cytoplasmic pathogens in the target cells so that the pathogens cannot infect nearby cells. Dead target cells are rapidly ingested by macrophages without activating macrophage expression of B7, preventing co-stimulation of any self-specific T cells which bind the self peptides on macrophage MHC.

Perforin polymerization must occur preferentially in the target cell plasma membrane, since the CTL is not lysed and one CTL can sequentially kill many infected targets. It is not understood how CTL are protected from perforin polymerization and granzyme entry, since the molecules are nonspecific and in allogeneic systems CTL can serve as target cells

### Second Mechanism

A second mechanism for CTL killing was discovered in perforin knock-out mice. Binding of CTL membrane Fas ligand (FasL) to target cell Fas induces target cell apoptosis. Since activated Th1 and Th2 cells also express FasL, they may also have cytotoxic activity. Because mice with defects in Fas or FasL have a higher incidence of lymphoproliferative disease and autoimmunity, this cytotoxic mechanism may be important for regulating immune responses.

### Regulatory Functions

CTL also regulate immune responses by releasing IFN-Gamma, TNF-alpha, and TNF-betta. IFN-Gamma inhibits viral replication, activates IL-1 and TAP expression by infected cells to promote antigen presentation, and recruits and activates macrophages as APC and effector cells. TNF-alpha and TNF-beeta act with IFN-Gamma to activate macrophages and to directly kill some target cells. Macrophages with surviving vesicular pathogens such as Toxoplasma gondii may also present pathogen peptide on Class I MHC because some pathogen proteins are exported to the cytoplasm. In these cases, CTL may play an important role in eliminating the parasite by killing host macrophages as well as activating other macrophages to engulf and kill the parasite. IFN-Gamma can also starve resident intracellular parasites by reducing tryptophan concentration. Natural Killer cells mediate early innate cytotoxic responses to viruses and tumor cells with perforin and granzymes, although recognition of targets is quite different.


### References

- Finlay, David, and Doreen A. Cantrell. "Metabolism, migration and memory in cytotoxic T cells." Nature Reviews Immunology 11.2 (2011): 109-117.

- Lau, Lisa L., et al. "Cytotoxic T-cell memory without antigen." Nature 369.6482 (1994): 648-652.

