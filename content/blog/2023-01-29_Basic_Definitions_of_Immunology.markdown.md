+++
title = "Basic Definitions of Immunology"
date = 2023-01-29
draft = false
tags = ["Research"]
categories = []
+++

## Basic Definitions of Immunology

I'm trying to write some basic immunology, so here are some notes to help us remember. 
Today's blog contains some important definitions of concepts frequently used in Immunology

### Basophils
A type of granulocyte (a type of white blood cell). They are responsible for inflammatory reactions during immune response.

### Classical complement pathway
The one of three complement pathways that can activate the complement system, which clears damaged cells and microbes from an organism by promoting inflammation.

### Cytokines
The substances such as IFN_gamma (interferon gamma), TNF, IL-4, IL-10 that are secreted by certain cells of the immune system and have an effect on other cells (eg. on macrophages).

### Danger-associated molecular pattern (DAMP)
 The host molecules that can initiate and perpetuate a noninfectious inflammatory response. Examples are purine metabolites eg. ATP, adenosine. ATP and adenosine are released in high concentration after severe disruption of the cell.

### Dendritic cells
The antigen-presenting cells of the mammalian immune system. They process antigen material and present it on their cell surface to the T cells of the immune system. Dendritic cells can produce cytokines that send T cells towards a Th1 or Th2 response. Note that monocytes can differentiate into dendritic cells.

### Histamine
An organic nitrogenous compound involved in local immune response; as part of an immune response to foreign pathogens, histamine is produced by basophils and by mast cells found in nearby connective tissues. Histamine increases permeabililty of capillaries to white blood cells.

### Interferon gamma
A soluble cytokine critical for innate and adaptive immunity against viral, some bacterial and protozoal infections. An important activator of macrophages.

### Lectins (also known as lectin receptors): 
Lectins are carbohydrate-binding proteins. Within the innate immune system, lectins such as MBL (mannose binding lectin) help mediate defense against invading microbes. Other lectins likely are involved in modulating inflammatory processes. Here is a review I came across on the role of a type of lectins called galectins in the animal immune system.

### Lectin pathway
The type of cascade reaction in the complement system, that after activation, produces activated complement proteins further down the cascade. In contrast to the classical complement pathway, the lectin pathway does not recognise an antibody bound to its target, but rather starts with MBL (mannose binding protein) or ficulin bound to certain sugars.

### Lymphocytes
A subtype of white blood cell. There are many types including CD4+ T cells, CD8+ T cells, NK cells, NKT cells, B cells, etc.

### Macrophages
A type of white blood cell that engulfs and digests foreign substances, cancer cells, microbes, etc. Macrophages break down these substances and present the smaller proteins to the T lymphocytes.

### Mast cells
A type of granulocyte (a type of white blood cell). Contains many granules rich in histamine and heparin. Involved in wound healing, response to pathogens, etc.

### Monoclonal Antibodies
The Antibodies made by identical immune cells that are all clones of the same parent cell, and that all bind to the same epitope of the antigen.

### Mononuclear phagocyte system
Mononuclear phagocyte system (MGS) (also known as macrophage system): part of the immune system that consists of phagocytic cells located in reticular connective tissue. The cells are mostly macrophages and monocytes and they accumulate in the lymph nodes and in the spleen.

### Spleen
Spleen: Removes old red blood cells and holds a reserve of blood. Synthesises antibodies in its white pulp. Also has role in cell-mediated immunity.

### T cell
T cell (T lymphocyte): a type of lymphocyte that plays a key role in cell-mediated immunity. They hunt down and destroy cells that are infected with microbes or that have become cancerous.

### Th1 response
Th1 T cells tend to generate immune responses against intracellular parasites such as bacteria and viruses.

### Th2 response
The Th2 T cells generate immune responses against helminths and other extracellular parasites. Dendritic cells (which sense helminth antigens) are thought to play a dominant role in initiation of Th2 response. To do this they are thought to somehow cause granulocytes (like basophils, eosinophils, mast cells) to produce Th2-associated cytokines like IL4. The interleukin 4 (IL4) is a cytokine that induces differentiation of naive helper T cells (Th0 cells) to Th2 cells.

### References

- https://www.ncbi.nlm.nih.gov/books/NBK10779/
- Marshall, Jean S., et al. "An introduction to immunology and immunopathology." Allergy, Asthma & Clinical Immunology 14.2 (2018): 1-10.