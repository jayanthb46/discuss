+++
title = "Radar Plots"
date = 2022-12-11
draft = false
tags = ["R tutoial"]
categories = []
+++

## Radar Plots

Radar plots visualize several variables using a radial layout. This plot is most suitable for visualizing and comparing the properties associated with individual objects

```R
# read table from text connection
df <- read.csv('whiskies.txt', header=T)
```


```R
head(df)
```


<table class="dataframe">
<caption>A data.frame: 6 × 17</caption>
<thead>
	<tr><th></th><th scope=col>RowID</th><th scope=col>Distillery</th><th scope=col>Body</th><th scope=col>Sweetness</th><th scope=col>Smoky</th><th scope=col>Medicinal</th><th scope=col>Tobacco</th><th scope=col>Honey</th><th scope=col>Spicy</th><th scope=col>Winey</th><th scope=col>Nutty</th><th scope=col>Malty</th><th scope=col>Fruity</th><th scope=col>Floral</th><th scope=col>Postcode</th><th scope=col>Latitude</th><th scope=col>Longitude</th></tr>
	<tr><th></th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>1</th><td>1</td><td>Aberfeldy  </td><td>2</td><td>2</td><td>2</td><td>0</td><td>0</td><td>2</td><td>1</td><td>2</td><td>2</td><td>2</td><td>2</td><td>2</td><td> 	PH15 2EB</td><td>286580</td><td>749680</td></tr>
	<tr><th scope=row>2</th><td>2</td><td>Aberlour   </td><td>3</td><td>3</td><td>1</td><td>0</td><td>0</td><td>4</td><td>3</td><td>2</td><td>2</td><td>3</td><td>3</td><td>2</td><td> 	AB38 9PJ</td><td>326340</td><td>842570</td></tr>
	<tr><th scope=row>3</th><td>3</td><td>AnCnoc     </td><td>1</td><td>3</td><td>2</td><td>0</td><td>0</td><td>2</td><td>0</td><td>0</td><td>2</td><td>2</td><td>3</td><td>2</td><td> 	AB5 5LI </td><td>352960</td><td>839320</td></tr>
	<tr><th scope=row>4</th><td>4</td><td>Ardbeg     </td><td>4</td><td>1</td><td>4</td><td>4</td><td>0</td><td>0</td><td>2</td><td>0</td><td>1</td><td>2</td><td>1</td><td>0</td><td> 	PA42 7EB</td><td>141560</td><td>646220</td></tr>
	<tr><th scope=row>5</th><td>5</td><td>Ardmore    </td><td>2</td><td>2</td><td>2</td><td>0</td><td>0</td><td>1</td><td>1</td><td>1</td><td>2</td><td>3</td><td>1</td><td>1</td><td> 	AB54 4NH</td><td>355350</td><td>829140</td></tr>
	<tr><th scope=row>6</th><td>6</td><td>ArranIsleOf</td><td>2</td><td>3</td><td>1</td><td>1</td><td>0</td><td>1</td><td>1</td><td>1</td><td>0</td><td>1</td><td>1</td><td>2</td><td> KA27 8HJ </td><td>194050</td><td>649950</td></tr>
</tbody>
</table>




```R
features <- c("Sweetness", "Honey", "Fruity",
              "Winey", "Spicy", "Nutty", "Malty",
              "Floral", "Tobacco", "Medicinal",
              "Smoky", "Body")
df <- df[, c("Distillery", features)]
```


```R
head(df)
```


<table class="dataframe">
<caption>A data.frame: 6 × 13</caption>
<thead>
	<tr><th></th><th scope=col>Distillery</th><th scope=col>Sweetness</th><th scope=col>Honey</th><th scope=col>Fruity</th><th scope=col>Winey</th><th scope=col>Spicy</th><th scope=col>Nutty</th><th scope=col>Malty</th><th scope=col>Floral</th><th scope=col>Tobacco</th><th scope=col>Medicinal</th><th scope=col>Smoky</th><th scope=col>Body</th></tr>
	<tr><th></th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>1</th><td>Aberfeldy  </td><td>2</td><td>2</td><td>2</td><td>2</td><td>1</td><td>2</td><td>2</td><td>2</td><td>0</td><td>0</td><td>2</td><td>2</td></tr>
	<tr><th scope=row>2</th><td>Aberlour   </td><td>3</td><td>4</td><td>3</td><td>2</td><td>3</td><td>2</td><td>3</td><td>2</td><td>0</td><td>0</td><td>1</td><td>3</td></tr>
	<tr><th scope=row>3</th><td>AnCnoc     </td><td>3</td><td>2</td><td>3</td><td>0</td><td>0</td><td>2</td><td>2</td><td>2</td><td>0</td><td>0</td><td>2</td><td>1</td></tr>
	<tr><th scope=row>4</th><td>Ardbeg     </td><td>1</td><td>0</td><td>1</td><td>0</td><td>2</td><td>1</td><td>2</td><td>0</td><td>0</td><td>4</td><td>4</td><td>4</td></tr>
	<tr><th scope=row>5</th><td>Ardmore    </td><td>2</td><td>1</td><td>1</td><td>1</td><td>1</td><td>2</td><td>3</td><td>1</td><td>0</td><td>0</td><td>2</td><td>2</td></tr>
	<tr><th scope=row>6</th><td>ArranIsleOf</td><td>3</td><td>1</td><td>1</td><td>1</td><td>1</td><td>0</td><td>1</td><td>2</td><td>0</td><td>1</td><td>1</td><td>2</td></tr>
</tbody>
</table>




```R
library(ggiraphExtra)
library(ggplot2)
# select a random sample of whiskeys
i <- sample(seq_len(nrow(df)), 6)
# select some whiskeys I know
my.whiskeys <- c("Aultmore", "Loch Lomond", "Lagavulin", "Tomatin", "Laphroig", "Macallan")
plot.df <- df[which(df$Distillery %in% my.whiskeys),]
mycolor <- "#1c6193"
p <- ggRadar(plot.df, aes(group = Distillery), 
     rescale = FALSE, legend.position = "none",
     size = 1, interactive = FALSE, use.label = TRUE) +
     facet_wrap(~Distillery) + 
     scale_y_discrete(breaks = NULL) + # don't show ticks 
    theme(axis.text.x = element_text(size = 10)) + # larger label sizes
    # adjust colors of radar charts to uniform colors
    scale_fill_manual(values = rep(mycolor, nrow(plot.df))) +
    scale_color_manual(values = rep(mycolor, nrow(plot.df))) +
    ggtitle("Whiskey Tasting Characteristics")
print(p)
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/_posts/images/Dec2022/output_5_0.png)
    

