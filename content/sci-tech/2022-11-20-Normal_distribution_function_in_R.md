+++
title = "Normal Distribution Functions in R"
date = 2022-11-20
draft = false
tags = ["R tutoial"]
categories = []
+++

## Normal Distribution Functions in R

R is a great tool for working with Normal distributions. Here, I'll discuss which functions are available for dealing with the normal distribution: dnorm, pnorm, qnorm, and rnorm.

## Introduction

Every distribution has four associated functions whose prefix indicates the type of function and the suffix indicates the distribution. To exemplify the use of these functions, I will limit myself to the normal (Gaussian) distribution. The four normal distribution functions are:

* **d**norm: **density function** of the normal distribution
* **p**norm: **cumulative density function** of the normal distribution
* **q**norm: **quantile function** of the normal distribution
* **r**norm: **random sampling** from the normal distribution


## The probability density function: dnorm
The probability density function (PDF, in short: density) indicates the probability of observing a measurement with a specific value and thus the integral over the density is always 1. 

Using the density, it is possible to determine the probabilities of events.It can also be used to measure distribution of eventsUsing the density, it is possible to determine the probabilities of events. For example, you may wonder: *What is the likelihood that a person has an height of exactly 140?*. In this case, you would need to retrieve the density of the height distribution at value 140. The height distribution can be modeled with a mean of 100 and a standard deviation of 15. The corresponding density is:

```R
sample.range <- 50:150
iq.mean <- 100
iq.sd <- 15
iq.dist <- dnorm(sample.range, mean = iq.mean, sd = iq.sd)
iq.df <- data.frame("height" = sample.range, "Density" = iq.dist)
library(ggplot2)
ggplot(iq.df, aes(x = height, y = Density)) + geom_point()
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/_posts/images/Dec2022/output_6_0.png)
    



```R
head(iq.df)
```


<table class="dataframe">
<caption>A data.frame: 6 × 2</caption>
<thead>
	<tr><th></th><th scope=col>height</th><th scope=col>Density</th></tr>
	<tr><th></th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>1</th><td>50</td><td>0.0001028186</td></tr>
	<tr><th scope=row>2</th><td>51</td><td>0.0001281199</td></tr>
	<tr><th scope=row>3</th><td>52</td><td>0.0001589392</td></tr>
	<tr><th scope=row>4</th><td>53</td><td>0.0001962978</td></tr>
	<tr><th scope=row>5</th><td>54</td><td>0.0002413624</td></tr>
	<tr><th scope=row>6</th><td>55</td><td>0.0002954566</td></tr>
</tbody>
</table>




```R
pp <- function(x) {
    print(paste0(round(x * 100, 3), "%"))
}
# likelihood of IQ == 140?
pp(iq.df$Density[iq.df$height == 140])
# likelihood of IQ >= 140?
pp(sum(iq.df$Density[iq.df$height >= 140]))
# likelihood of 50 < IQ <= 90?
pp(sum(iq.df$Density[iq.df$height <= 90]))
```

    [1] "0.076%"
    [1] "0.384%"
    [1] "26.284%"
    

## The cumulative density function: pnorm
The cumulative density (CDF) function is a monotonically increasing function as it integrates over densities. To get an intuition of the CDF, let's create a plot for the height data.

```R
cdf <- pnorm(sample.range, iq.mean, iq.sd)
iq.df <- cbind(iq.df, "CDF_LowerTail" = cdf)

```


```R
head(iq.df)
```


<table class="dataframe">
<caption>A data.frame: 6 × 3</caption>
<thead>
	<tr><th></th><th scope=col>height</th><th scope=col>Density</th><th scope=col>CDF_LowerTail</th></tr>
	<tr><th></th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>1</th><td>50</td><td>0.0001028186</td><td>0.0004290603</td></tr>
	<tr><th scope=row>2</th><td>51</td><td>0.0001281199</td><td>0.0005441087</td></tr>
	<tr><th scope=row>3</th><td>52</td><td>0.0001589392</td><td>0.0006871379</td></tr>
	<tr><th scope=row>4</th><td>53</td><td>0.0001962978</td><td>0.0008641652</td></tr>
	<tr><th scope=row>5</th><td>54</td><td>0.0002413624</td><td>0.0010823005</td></tr>
	<tr><th scope=row>6</th><td>55</td><td>0.0002954566</td><td>0.0013498980</td></tr>
</tbody>
</table>




```R
ggplot(iq.df, aes(x = height, y =  CDF_LowerTail)) + geom_point()
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/_posts/images/Dec2022/output_13_0.png)
    



```R
pp(iq.df$CDF_LowerTail[iq.df$height == 90])
# set lower.tail to FALSE to obtain P[X >= x]
cdf <- pnorm(sample.range, iq.mean, iq.sd, lower.tail = FALSE)
iq.df <- cbind(iq.df, "CDF_UpperTail" = cdf)
# Probability for IQ >= 140? same value as before using dnorm!
pp(iq.df$CDF_UpperTail[iq.df$height == 140])
```

    [1] "25.249%"
    [1] "0.383%"
    
Note that the results from *pnorm* are the same as those obtained from manually summing up the probabilities obtained via *dnorm*. Moreover, by setting `lower.tail = FALSE`, `dnorm` can be used to directly compute p-values, which measure how the likelihood of an observation that is at least as extreme as the obtained one.

To remember that `pnorm` does not provide the PDF but the CDF, just imagine that the function carries a *p* in its name such that *pnorm* is lexicographically close to *qnorm*, which provides the inverse of the CDF. 
## The quantile function: qnorm
The quantile function is simply the inverse of the cumulative density function (iCDF). Thus, the quantile function maps from probabilities to values

```R
# input to qnorm is a vector of probabilities
prob.range <- seq(0, 1, 0.001)
icdf.df <- data.frame("Probability" = prob.range, "height" = qnorm(prob.range, iq.mean, iq.sd))
ggplot(icdf.df, aes(x = Probability, y = height)) + geom_point()
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/_posts/images/Dec2022/output_18_0.png)
    



```R
# what is the 25th IQ percentile?
print(icdf.df$height[icdf.df$Probability == 0.25])
# what is the 75 IQ percentile?
print(icdf.df$height[icdf.df$Probability == 0.75])
```

    [1] 89.88265
    [1] 110.1173
    

## The random sampling function: rnorm
When you want to draw random samples from the normal distribution, you can use `rnorm`. For example, we could use `rnorm` to simulate random samples from the IQ distribution.

```R
# fix random seed for reproducibility
set.seed(1)
# law of large numbers: mean will approach expected value for large N
n.samples <- c(100, 1000, 10000)
my.df <- do.call(rbind, lapply(n.samples, function(x) data.frame("SampleSize" = x, "height" = rnorm(x, iq.mean, iq.sd))))
# show one facet per random sample of a given size
ggplot() + geom_histogram(data = my.df, aes(x = height)) + facet_wrap(.~SampleSize, scales = "free_y")
# note: we can also implement our own sampler using the densities
my.sample <- sample(iq.df$height, 100, prob = iq.df$Density, replace = TRUE)
my.sample.df <- data.frame("height" = my.sample)
ggplot(my.sample.df, aes(x = height)) + geom_histogram()
```

    `stat_bin()` using `bins = 30`. Pick better value with `binwidth`.
    
    `stat_bin()` using `bins = 30`. Pick better value with `binwidth`.
    
    


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/_posts/images/Dec2022/output_22_1.png)
    



    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/_posts/images/Dec2022/output_22_2.png)
    

Note that we called `set.seed` in order to ensure that the random number generator always generates the same sequence of numbers for reproducibility.

## Summary
Of the four functions dealing with distributions, `dnorm` is the most important one. This is because the values from `pnorm`, `qnorm`, and `rnorm` are based on `dnorm`. Still, `pnorm`, `qnorm`, and `rnorm` are very useful convenience functions when dealing with the normal distribution. If you would like to learn about the corresponding functions for the other distributions, you can simply call `?distribtuion` to obtain more information.


##References

- https://www.statology.org/plot-normal-distribution-r/