+++
title = "Histogram in R"
date = 2021-08-22
draft = false
tags = ["R Tutorial"]
categories = []
+++


Histogram are frequently used in data analyses for visualizing the data. Histogram helps in identifying the frequency of the data and underlying distribution of the data. 

Histogram can be used for visualizing continous data. Histogram can be plotted by binning continous variables in to groups (i.e x-axis) and frequency of the group in the y-axis.

hist() function in R provides the implementation of histogram

```R
hist(iris$Petal.Length)
```


![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/Aug22_2021/output_1_0.png)



```R
hist(iris$Petal.Length, col="blue", xlab="Petal Length", main="Colored histogram")
```


![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/Aug22_2021/output_2_0.png)



```R
hist(iris$Petal.Length, breaks=50, col="gray", xlab="Petal Length", main="Colored histogram")
```


![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/Aug22_2021/output_3_0.png)

In statistics, the histogram is used to evaluate the distribution of the data. In order to show the distribution of the data we first will show density (or probably) instead of frequency, by using function freq=FALSE. Secondly, we will use the function curve() to show normal distribution line.

```R
# add a normal distribution line in histogram
hist(iris$Petal.Length, freq=FALSE, col="gray", xlab="Petal Length", main="Colored histogram")
curve(dnorm(x, mean=mean(iris$Petal.Length), sd=sd(iris$Petal.Length)), add=TRUE, col="red") #line
```


![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/Aug22_2021/output_5_0.png)


### References
- https://r-graphics.org/
