+++
title = "Cbind in R: Joining Columns in Data frame"
date = 2021-07-18
draft = false
tags = ["R Tutorial"]
categories = []
+++

## Cbind in R : Joining Columns in Data frame

### Introduction
The cbind in combination with rbind is used to add data to dataframe in R. It is one of the extensively used function to add columns to data frame. cbind function will be used to merge vectors and matrices to data frame.

cbind stands for column bind.

The cbind() is a built-in R function that takes a sequence of vector, matrix, or data-frame as arguments and combines them by columns. 

cbind works on vectors and matrices which are atomic in nature. List Expressions are not allowed.

The Syntax of cbind

cbind(a1, a2, ..., deparse.level = 1)

Parameters

The cbind() function takes a1,  a2, which takes vectors, matrices, and/or data frames.

deparse.level: for non-matrix, 0 constructs no labels, 1 or 2 constructs labels from the argument names.

### Implementing cbind in R
The cbind() function accepts first argument as data frame followed by series of arguments such as vector/matrix or data frame and combines them by columns to create new data frame

### Combining data frame and vector
In this example we have created new data frame (df) and vector(c4) and merged data frame(newDf).

```R
df <- data.frame(c1 = c(1, 2, 3, 4),
 c2 = c(5, 6, 7, 8),
 c3 = c(9, 10, 11, 12))
```


```R
head(df)
```


<table class="dataframe">
<caption>A data.frame: 4 × 3</caption>
<thead>
	<tr><th></th><th scope=col>c1</th><th scope=col>c2</th><th scope=col>c3</th></tr>
	<tr><th></th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>1</th><td>1</td><td>5</td><td> 9</td></tr>
	<tr><th scope=row>2</th><td>2</td><td>6</td><td>10</td></tr>
	<tr><th scope=row>3</th><td>3</td><td>7</td><td>11</td></tr>
	<tr><th scope=row>4</th><td>4</td><td>8</td><td>12</td></tr>
</tbody>
</table>




```R
c4 <- c(18, 19, 20, 21)
cat("After adding a column using cbind()", "\n")
newDf <- cbind(df, c4)
newDf
```

    After adding a column using cbind() 



<table class="dataframe">
<caption>A data.frame: 4 × 4</caption>
<thead>
	<tr><th scope=col>c1</th><th scope=col>c2</th><th scope=col>c3</th><th scope=col>c4</th></tr>
	<tr><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>1</td><td>5</td><td> 9</td><td>18</td></tr>
	<tr><td>2</td><td>6</td><td>10</td><td>19</td></tr>
	<tr><td>3</td><td>7</td><td>11</td><td>20</td></tr>
	<tr><td>4</td><td>8</td><td>12</td><td>21</td></tr>
</tbody>
</table>


In this example, we created a data frame using three vectors and then add a fourth vector as a column to the original data frame using the cbind() function. In the final data frame, we can see that the c4 column is added.

# Combining multiple data frame
To combine two data frames by columns, use the cbind() function. The cbind() data frame function is just a wrapper for data.frame(…, check.names = FALSE). This means that it will split matrix columns in data frame arguments, and convert character columns to factors unless stringsAsFactors = FALSE is specified.

The data frame method will be used if at least one argument is a data frame and the rest are vectors or matrices. 

Let’s create a second data frame and use the cbind() function to merge the second data frame to the first data frame.

```R
df <- data.frame(c1 = c(1, 2, 3, 4),
 c2 = c(5, 6, 7, 8),
 c3 = c(9, 10, 11, 12))
df

df2 <- data.frame(c4 = c(18, 19, 20, 21),
 c5 = c(29, 46, 47, 37))
df2
cat("After adding another data frame using cbind()", "\n")
newDf <- cbind(df, df2)
newDf
```


<table class="dataframe">
<caption>A data.frame: 4 × 3</caption>
<thead>
	<tr><th scope=col>c1</th><th scope=col>c2</th><th scope=col>c3</th></tr>
	<tr><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>1</td><td>5</td><td> 9</td></tr>
	<tr><td>2</td><td>6</td><td>10</td></tr>
	<tr><td>3</td><td>7</td><td>11</td></tr>
	<tr><td>4</td><td>8</td><td>12</td></tr>
</tbody>
</table>




<table class="dataframe">
<caption>A data.frame: 4 × 2</caption>
<thead>
	<tr><th scope=col>c4</th><th scope=col>c5</th></tr>
	<tr><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>18</td><td>29</td></tr>
	<tr><td>19</td><td>46</td></tr>
	<tr><td>20</td><td>47</td></tr>
	<tr><td>21</td><td>37</td></tr>
</tbody>
</table>



    After adding another data frame using cbind() 



<table class="dataframe">
<caption>A data.frame: 4 × 5</caption>
<thead>
	<tr><th></th><th>c2</th><th>c3</th><th >c4</th><th>c5</th></tr>
	<tr><th>&lt;dbl&gt;</th><th>&lt;dbl&gt;</th><th>&lt;dbl&gt;</th><th >&lt;dbl&gt;</th><th>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>1</td><td>5</td><td> 9</td><td>18</td><td>29</td></tr>
	<tr><td>2</td><td>6</td><td>10</td><td>19</td><td>46</td></tr>
	<tr><td>3</td><td>7</td><td>11</td><td>20</td><td>47</td></tr>
	<tr><td>4</td><td>8</td><td>12</td><td>21</td><td>37</td></tr>
</tbody>
</table>


You can see that both data frame’s row count is the same, and that is why it merges perfectly, but if the row counts of both columns are different, it will give us the error. The deparse.level value defines how the column names are generated. The default value of deparse.level is 1.

```R
df <- data.frame(c1 = c(1, 2, 3, 4),
 c2 = c(5, 6, 7, 8),
 c3 = c(9, 10, 11, 12))

df2 <- data.frame(c4 = c(18, 19, 20, 21, 6),
 c5 = c(29, 46, 47, 37, 10))
cat("After adding another data frame using cbind()", "\n")
newDf <- cbind(df, df2)
newDf
```

    After adding another data frame using cbind() 



    Error in data.frame(..., check.names = FALSE): arguments imply differing number of rows: 4, 5
    Traceback:


    1. cbind(df, df2)

    2. cbind(deparse.level, ...)

    3. data.frame(..., check.names = FALSE)

    4. stop(gettextf("arguments imply differing number of rows: %s", 
     .     paste(unique(nrows), collapse = ", ")), domain = NA)

We got this error: arguments imply differing number of rows: 4, 5

If the number of rows of the two data frames differs, the cbind() function cannot be used.


### cbind() function in R with Multiple Columns
The cbind() function can also be applied to multiple columns and data objects. Define a data frame using three columns and add the two columns to that data frame using the cbind() function and see the output.

```R
df <- data.frame(c1 = c(1, 2, 3, 4),
 c2 = c(5, 6, 7, 8),
 c3 = c(9, 10, 11, 12))

c4 <- c(18, 19, 20, 21)
c5 <- c(29, 46, 47, 37)
cat("After adding multiple columns using cbind()", "\n")
newDf <- cbind(df, c4, c5)
newDf
```

### References

- https://www.rdocumentation.org/packages/base/versions/3.6.2/topics/cbind
