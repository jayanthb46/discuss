+++
title = "Reading CSV files in R"
date = 2022-07-31
draft = false
tags = ["R tutoial"]
categories = []
+++

### Reading CSV files in R

#### using base R
Comma separated value files (CSVs) can be imported using read.csv, which is a wrapper arround read.table. It provides separator(sep = ",") specify the nature of separation in file.

```R
csv_path = 'https://gist.githubusercontent.com/netj/8836201/raw/6f9306ad21398ea43cba4f7d537619d0e07d5ae3/iris.csv'

df <- read.csv(csv_path)
```


```R
head(df)
```
<table>
<caption>A data.frame: 6 × 5</caption>
<thead>
	<tr><th></th><th scope=col>sepal.length</th><th scope=col>sepal.width</th><th scope=col>petal.length</th><th scope=col>petal.width</th><th scope=col>variety</th></tr>
	<tr><th></th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;chr&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>1</th><td>5.1</td><td>3.5</td><td>1.4</td><td>0.2</td><td>Setosa</td></tr>
	<tr><th scope=row>2</th><td>4.9</td><td>3.0</td><td>1.4</td><td>0.2</td><td>Setosa</td></tr>
	<tr><th scope=row>3</th><td>4.7</td><td>3.2</td><td>1.3</td><td>0.2</td><td>Setosa</td></tr>
	<tr><th scope=row>4</th><td>4.6</td><td>3.1</td><td>1.5</td><td>0.2</td><td>Setosa</td></tr>
	<tr><th scope=row>5</th><td>5.0</td><td>3.6</td><td>1.4</td><td>0.2</td><td>Setosa</td></tr>
	<tr><th scope=row>6</th><td>5.4</td><td>3.9</td><td>1.7</td><td>0.4</td><td>Setosa</td></tr>
</tbody>
</table>





#### Notes
- Unlike read.table, read.csv defaults to header = TRUE, and uses the first row as column names.
- All these functions will convert strings to factor class by default unless either as.is = TRUE or stringsAsFactors = FALSE.
- The read.csv2 variant defaults to sep = ";" and dec = "," for use on data from countries where the comma is used as a decimal point and the semicolon as a field separator.
### Importing using packages
The readr package's read_csv function offers much faster performance, a progress bar for large files, and more
popular default options than standard read.csv, including `stringsAsFactors = FALSE`.

```R
library(readr)
```


```R
df <- read_csv(csv_path)
```

    Parsed with column specification:
    cols(
      sepal.length = [32mcol_double()[39m,
      sepal.width = [32mcol_double()[39m,
      petal.length = [32mcol_double()[39m,
      petal.width = [32mcol_double()[39m,
      variety = [31mcol_character()[39m
    )
    
    


```R
library(data.table)
dt <- fread(csv_path)
```
#### Notes
- fread does not have all same options as read.table. One missing argument is na.comment, which may lead to unwanted behaviors if the source file contains #.
- fread uses only " for quote parameter.
- fread uses few (5) lines to guess variables types.

```R
files = list.files(pattern="*.csv")
data_list = lapply(files, read.table, header = TRUE)
```

### Exporting .csv files
Exporting using base R
Data can be written to a CSV file using write.csv():

```R
write.csv(mtcars, "mtcars.csv")
```


```R
library(readr)
write_csv(mtcars, "mtcars.csv")
```

### References
- https://www.tutorialspoint.com/r/r_csv_files.htm
