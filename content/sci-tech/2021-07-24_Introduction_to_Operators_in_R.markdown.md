+++
title = "Introduction to Operators in R"
date = 2021-07-24
draft = false
tags = ["R Tutorial"]
categories = []
+++

# Introduction to Operators in R
There are four forms of the operator in R: [, [[, $, and @. The fourth form is also known as the slot operator and is used to extract content from objects built with the S4 object system, also known as a formally defined object in R.The first form, [, can be used to extract content from vectors, lists, or data frames.

The second and third forms, [[ and $ extract content from a single object.

The $ operator uses a name to perform the extraction as in RObject$Name. Therefore it enables one to extract items from a list based on their names.
# List and Operators
To create a list in R, use the list() function.

```R
listA <- list("Apple", "Mango", "strawberry", "Grapes")

```


```R
print(listA)
```

    [[1]]
    [1] "Apple"
    
    [[2]]
    [1] "Mango"
    
    [[3]]
    [1] "strawberry"
    
    [[4]]
    [1] "Grapes"
    

To access a variable in the R dataset, use the dollar sign “$”. The $ operator can be used to select a variable/column, assign new values to a variable/column, or add a new variable/column in an R object.

```R
listA <- list("India" = "New Delhi", "Germany" = "Berlin")
print(listA)

print("After adding a variable to a list")

listA$France <- "Paris"
print(listA)
```

    $India
    [1] "New Delhi"
    
    $Germany
    [1] "Berlin"
    
    [1] "After adding a variable to a list"
    $India
    [1] "New Delhi"
    
    $Germany
    [1] "Berlin"
    
    $France
    [1] "Paris"
    

In the above example we have used '$' operator with '<-'(Assignment) operator to add new combination (France and paris) to listA

```R
print(listA$India)
```

    [1] "New Delhi"

In this example, we have selected the 'India' variable with '$' operator, which prints 'New Delhi' as its value

```R
print(listA[['India']])
```

    [1] "New Delhi"

Even double brackets([[]]) operator can be used to extract value simillar to '$' operator
# Data frame and operators
To add a new column in a data frame, use the $ operator. To select a column in a data frame, you can use the $ sign.

To create a data frame in R, use the data.frame() function.

```R
temp <- data.frame(name = c("Carrot", "Mango", "Brinjal", "Grapes", "Apple"),price = c(18, 10, 15, 7, 12),stringsAsFactors = FALSE)
```


```R
head(temp)
```


<table class="dataframe">
<caption>A data.frame: 5 × 2</caption>
<thead>
	<tr><th></th><th scope=col>name</th><th scope=col>price</th></tr>
	<tr><th></th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>1</th><td>Carrot </td><td>18</td></tr>
	<tr><th scope=row>2</th><td>Mango  </td><td>10</td></tr>
	<tr><th scope=row>3</th><td>Brinjal</td><td>15</td></tr>
	<tr><th scope=row>4</th><td>Grapes </td><td> 7</td></tr>
	<tr><th scope=row>5</th><td>Apple  </td><td>12</td></tr>
</tbody>
</table>


Let’s add a new column in the R data frame using $ sign.

```R
temp$category <- c("Vegetable", "Fruit", "Vegetable", "Fruit", "Fruit")
```


```R
head(temp)
```


<table class="dataframe">
<caption>A data.frame: 5 × 3</caption>
<thead>
	<tr><th></th><th scope=col>name</th><th scope=col>price</th><th scope=col>category</th></tr>
	<tr><th></th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;chr&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>1</th><td>Carrot </td><td>18</td><td>Vegetable</td></tr>
	<tr><th scope=row>2</th><td>Mango  </td><td>10</td><td>Fruit    </td></tr>
	<tr><th scope=row>3</th><td>Brinjal</td><td>15</td><td>Vegetable</td></tr>
	<tr><th scope=row>4</th><td>Grapes </td><td> 7</td><td>Fruit    </td></tr>
	<tr><th scope=row>5</th><td>Apple  </td><td>12</td><td>Fruit    </td></tr>
</tbody>
</table>


You can select the data frame column using the $ operator.

To select and print the values, of a column in a data frame, use the $ operator simillar to list operations discussed above with respect to $ operator 

```R
print(temp$name)
```

    [1] "Carrot"  "Mango"   "Brinjal" "Grapes"  "Apple"  


# References
- https://www.marsja.se/how-to-use-dollar-sign-in-r-examples-list-dataframe/

```R

```
