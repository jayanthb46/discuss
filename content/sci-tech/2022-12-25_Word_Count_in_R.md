+++
title = "Word Count in R"
date = 2022-12-25
draft = false
tags = ["R tutoial"]
categories = []
+++

## Word Count in R

```R
readLines("Test.txt",encoding="UTF-8")
```

    Warning message in readLines("Test.txt", encoding = "UTF-8"):
    "incomplete final line found on 'Test.txt'"
    


'The p value is calculated for a particular sample mean. Here we assume that we obtained a sample mean, x and want to find its p value. It is the probability that we would obtain a given sample mean that is greater than the absolute value of its Z-score or less than the negative of the absolute value of its Z-score.'



```R
sentences<-scan("Test.txt","character",sep="\n");
```


```R
sentences
```


'The p value is calculated for a particular sample mean. Here we assume that we obtained a sample mean, x and want to find its p value. It is the probability that we would obtain a given sample mean that is greater than the absolute value of its Z-score or less than the negative of the absolute value of its Z-score.'



```R
sentences<-gsub("\\.","",sentences)
sentences<-gsub("\\,","",sentences)
```


```R
sentences
```


'The p value is calculated for a particular sample mean Here we assume that we obtained a sample mean x and want to find its p value It is the probability that we would obtain a given sample mean that is greater than the absolute value of its Z-score or less than the negative of the absolute value of its Z-score'



```R
words<-strsplit(sentences," ")
```


```R
words
```


<ol>
	<li><style>
.list-inline {list-style: none; margin:0; padding: 0}
.list-inline>li {display: inline-block}
.list-inline>li:not(:last-child)::after {content: "\00b7"; padding: 0 .5ex}
</style>
<ol class=list-inline><li>'The'</li><li>'p'</li><li>'value'</li><li>'is'</li><li>'calculated'</li><li>'for'</li><li>'a'</li><li>'particular'</li><li>'sample'</li><li>'mean'</li><li>'Here'</li><li>'we'</li><li>'assume'</li><li>'that'</li><li>'we'</li><li>'obtained'</li><li>'a'</li><li>'sample'</li><li>'mean'</li><li>'x'</li><li>'and'</li><li>'want'</li><li>'to'</li><li>'find'</li><li>'its'</li><li>'p'</li><li>'value'</li><li>'It'</li><li>'is'</li><li>'the'</li><li>'probability'</li><li>'that'</li><li>'we'</li><li>'would'</li><li>'obtain'</li><li>'a'</li><li>'given'</li><li>'sample'</li><li>'mean'</li><li>'that'</li><li>'is'</li><li>'greater'</li><li>'than'</li><li>'the'</li><li>'absolute'</li><li>'value'</li><li>'of'</li><li>'its'</li><li>'Z-score'</li><li>'or'</li><li>'less'</li><li>'than'</li><li>'the'</li><li>'negative'</li><li>'of'</li><li>'the'</li><li>'absolute'</li><li>'value'</li><li>'of'</li><li>'its'</li><li>'Z-score'</li></ol>
</li>
</ol>




```R
words.freq<-table(unlist(words));
```


```R
words.freq
```


    
              a    absolute         and      assume  calculated        find 
              3           2           1           1           1           1 
            for       given     greater        Here          is          It 
              1           1           1           1           3           1 
            its        less        mean    negative      obtain    obtained 
              3           1           3           1           1           1 
             of          or           p  particular probability      sample 
              3           1           2           1           1           3 
           than        that         the         The          to       value 
              2           3           4           1           1           4 
           want          we       would           x     Z-score 
              1           3           1           1           2 



```R
cbind(names(words.freq),as.integer(words.freq))
```


<table class="dataframe">
<caption>A matrix: 35 × 2 of type chr</caption>
<tbody>
	<tr><td>a          </td><td>3</td></tr>
	<tr><td>absolute   </td><td>2</td></tr>
	<tr><td>and        </td><td>1</td></tr>
	<tr><td>assume     </td><td>1</td></tr>
	<tr><td>calculated </td><td>1</td></tr>
	<tr><td>find       </td><td>1</td></tr>
	<tr><td>for        </td><td>1</td></tr>
	<tr><td>given      </td><td>1</td></tr>
	<tr><td>greater    </td><td>1</td></tr>
	<tr><td>Here       </td><td>1</td></tr>
	<tr><td>is         </td><td>3</td></tr>
	<tr><td>It         </td><td>1</td></tr>
	<tr><td>its        </td><td>3</td></tr>
	<tr><td>less       </td><td>1</td></tr>
	<tr><td>mean       </td><td>3</td></tr>
	<tr><td>negative   </td><td>1</td></tr>
	<tr><td>obtain     </td><td>1</td></tr>
	<tr><td>obtained   </td><td>1</td></tr>
	<tr><td>of         </td><td>3</td></tr>
	<tr><td>or         </td><td>1</td></tr>
	<tr><td>p          </td><td>2</td></tr>
	<tr><td>particular </td><td>1</td></tr>
	<tr><td>probability</td><td>1</td></tr>
	<tr><td>sample     </td><td>3</td></tr>
	<tr><td>than       </td><td>2</td></tr>
	<tr><td>that       </td><td>3</td></tr>
	<tr><td>the        </td><td>4</td></tr>
	<tr><td>The        </td><td>1</td></tr>
	<tr><td>to         </td><td>1</td></tr>
	<tr><td>value      </td><td>4</td></tr>
	<tr><td>want       </td><td>1</td></tr>
	<tr><td>we         </td><td>3</td></tr>
	<tr><td>would      </td><td>1</td></tr>
	<tr><td>x          </td><td>1</td></tr>
	<tr><td>Z-score    </td><td>2</td></tr>
</tbody>
</table>

### References

- https://www.tutorialspoint.com/r/r_strings.htm
