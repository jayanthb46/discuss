+++
title = "CHECKSUM brief Discussion"
date = 2021-02-14
draft = false
tags = ["Plotting Word cloud in R"]
categories = ['File operations']
+++

### Date : February 14 2021

## CHECKSUM brief Discussion

Checksum is hash signature for the file.It is used to verify the integrity of the file. The checksum signature depends on file type , size , time stamp at which file was created. Therefore verifying the checksum signature ensures there was no alterations to the file. 

Verifying a checksum ensures there was no corruption or manipulation during the download and the file was downloaded completely and correctly. A common application for checksum verification is to verify a large download like an .iso disk image.

MD5 and SHA1 are the 2 popular methods used to create checksum.


### MD5 Signature

Message Digest 5(MD5) 

Most operating system distributions come with a tool to perform an MD5 hash. Here are examples on how to it in Windows, Mac, and Linux.

#### Mac terminal
md5 [file-to-hash]

#### Windows command prompt
certutil -hashfile [file-to-hash] md5

#### Linux shell
md5sum [file-to-hash]


### SHA Signature

Secure hash Algorithm (SHA) are a family of cryptographic hash function similar to MD5. Predomiant algorithms used for SHA are SHA-1, SHA-2 and SHA-3. 
The SHA-2 family consists of six hash functions with hash values that are 224, 256, 384 or 512 bits: SHA-224, SHA-256, SHA-384, SHA-512, SHA-512/224, SHA-512/256. SHA-256 and SHA-512 are novel hash functions computed with 32-bit and 64-bit words, respectively.

The most operating systems provide implementation of SHA1, SHA256, and SHA512. The SHA512 is generally regarded as strong hash function.

#### Mac terminal
- shasum -a 1 [file-to-hash]
- shasum -a 256 [file-to-hash]
- shasum -a 512 [file-to-hash]

#### Windows command prompt
- certutil -hashfile [file-to-hash] sha1
- certutil -hashfile [file-to-hash] sha256
- certutil -hashfile [file-to-hash] sha512

#### Linux shell
- sha1sum [file-to-hash]
- sha256sum [file-to-hash]
- sha512sum [file-to-hash]


### References

- https://en.wikipedia.org/wiki/MD5
- https://en.wikipedia.org/wiki/Secure_Hash_Algorithms
