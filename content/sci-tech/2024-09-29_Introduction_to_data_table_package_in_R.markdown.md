+++
title = "Introduction to data.table package in R"
date = 2024-09-29
draft = false
tags = ["R"]
categories = []
+++


## Introduction to data.table package in R.
To manage tabular data, R offers an alternative to the built-in data.frame called data.table. Because of its succinct syntax and quick processing of huge data, it's very popular.

In other words, you really type less code and get far faster. Data scientists prefer this package, which is among the most downloaded in R. In terms of speed, it is arguably one of the best developments for the R programming language.

Despite having a slightly different syntax than a standard R data.frame, data.table is nevertheless very user-friendly. It seems simple and logical that you wouldn't want to return to the original R data.frame syntax once you understand it.

The data.table package is authored by Matt Dowle and Arun Srinivasan.

### Install data.table


```R
# Install from CRAN
install.packages('data.table')

# Install Dev version from Gitlab
install.packages("data.table", repos="https://Rdatatable.gitlab.io/data.table")
data.table::update.dev.pkg()
```

### Loading data.table and importing Data


```R
library(data.table)
```
The data.tables version of read is called `fread()`, short for fast `read.CSV()`. As in `read.csv()` is compatible with both locally stored files and files hosted online. It is also 20 times faster than `read.csv()`. Now let's import the csv file containing the USAffairs dataset.

```R
affairs <- fread("https://vincentarelbundock.github.io/Rdatasets/csv/AER/Affairs.csv")
```


```R
head(affairs)
```


<table class="dataframe">
<caption>A data.table: 6 × 10</caption>
<thead>
	<tr><th scope=col>rownames</th><th scope=col>affairs</th><th scope=col>gender</th><th scope=col>age</th><th scope=col>yearsmarried</th><th scope=col>children</th><th scope=col>religiousness</th><th scope=col>education</th><th scope=col>occupation</th><th scope=col>rating</th></tr>
	<tr><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th></tr>
</thead>
<tbody>
	<tr><td> 4</td><td>0</td><td>male  </td><td>37</td><td>10.00</td><td>no </td><td>3</td><td>18</td><td>7</td><td>4</td></tr>
	<tr><td> 5</td><td>0</td><td>female</td><td>27</td><td> 4.00</td><td>no </td><td>4</td><td>14</td><td>6</td><td>4</td></tr>
	<tr><td>11</td><td>0</td><td>female</td><td>32</td><td>15.00</td><td>yes</td><td>1</td><td>12</td><td>1</td><td>4</td></tr>
	<tr><td>16</td><td>0</td><td>male  </td><td>57</td><td>15.00</td><td>yes</td><td>5</td><td>18</td><td>6</td><td>5</td></tr>
	<tr><td>23</td><td>0</td><td>male  </td><td>22</td><td> 0.75</td><td>no </td><td>2</td><td>17</td><td>6</td><td>3</td></tr>
	<tr><td>29</td><td>0</td><td>female</td><td>32</td><td> 1.50</td><td>no </td><td>2</td><td>17</td><td>5</td><td>5</td></tr>
</tbody>
</table>




```R
class(affairs)
```


'data.table' 'data.frame'


The imported data is stored as `data.table`. The `data.table` inherits from 'data.frame'.
The data.table is where the imported data is immediately saved. The output above illustrates how the data.table is a data.frame in and of itself because it derives from a data.frame class.

Thus functions that take in a data.frame will also execute flawlessly on a data.table. The speed of the read.csv() function was adequate because the dataset we imported was tiny.

However, when you import a large dataset (millions of rows), the speed increase becomes noticeable. To experience the speed of fread(), execute the code below. A 1 million row CSV file is created and loaded back to memory. The duration of both `fread()` and `read.csv()` were presented below. 

The system time statistics indicate `fread()` is faster than `read.csv()`.

```R
# Create a large .csv file
set.seed(100)
m <- data.frame(matrix(runif(10000000), nrow=1000000))
write.csv(m, 'm2.csv', row.names = F)

# Time taken by read.csv to import
system.time({m_df <- read.csv('m2.csv')})
#>   user  system elapsed 
#> 39.798   1.326  43.003 

# Time taken by fread to import
system.time({m_dt <- fread('m2.csv')})
#>  user  system elapsed 
#> 1.735   0.097   1.877
```


       user  system elapsed 
     18.752   0.279  19.029 



       user  system elapsed 
      0.244   0.012   0.067 

use setDT() to convert dataframe to data.table in place. 
Load Iris data set as data frame then use `setDT()` function to convert it to `data.table()`.

```R
data(iris)
head(iris)
```


<table class="dataframe">
<caption>A data.frame: 6 × 5</caption>
<thead>
	<tr><th></th><th scope=col>Sepal.Length</th><th scope=col>Sepal.Width</th><th scope=col>Petal.Length</th><th scope=col>Petal.Width</th><th scope=col>Species</th></tr>
	<tr><th></th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;fct&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>1</th><td>5.1</td><td>3.5</td><td>1.4</td><td>0.2</td><td>setosa</td></tr>
	<tr><th scope=row>2</th><td>4.9</td><td>3.0</td><td>1.4</td><td>0.2</td><td>setosa</td></tr>
	<tr><th scope=row>3</th><td>4.7</td><td>3.2</td><td>1.3</td><td>0.2</td><td>setosa</td></tr>
	<tr><th scope=row>4</th><td>4.6</td><td>3.1</td><td>1.5</td><td>0.2</td><td>setosa</td></tr>
	<tr><th scope=row>5</th><td>5.0</td><td>3.6</td><td>1.4</td><td>0.2</td><td>setosa</td></tr>
	<tr><th scope=row>6</th><td>5.4</td><td>3.9</td><td>1.7</td><td>0.4</td><td>setosa</td></tr>
</tbody>
</table>




```R
class(iris)
```


'data.frame'



```R
iris_dt <- setDT(iris)
class(iris_dt)
```

'data.table' 'data.frame'



### Filtering rows based on conditions


```R
f <- iris[iris$Sepal.Length < 6 & iris$Sepal.Width < 4,]
```


```R
head(f)
```


<table class="dataframe">
<caption>A data.table: 6 × 5</caption>
<thead>
	<tr><th scope=col>Sepal.Length</th><th scope=col>Sepal.Width</th><th scope=col>Petal.Length</th><th scope=col>Petal.Width</th><th scope=col>Species</th></tr>
	<tr><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;fct&gt;</th></tr>
</thead>
<tbody>
	<tr><td>5.1</td><td>3.5</td><td>1.4</td><td>0.2</td><td>setosa</td></tr>
	<tr><td>4.9</td><td>3.0</td><td>1.4</td><td>0.2</td><td>setosa</td></tr>
	<tr><td>4.7</td><td>3.2</td><td>1.3</td><td>0.2</td><td>setosa</td></tr>
	<tr><td>4.6</td><td>3.1</td><td>1.5</td><td>0.2</td><td>setosa</td></tr>
	<tr><td>5.0</td><td>3.6</td><td>1.4</td><td>0.2</td><td>setosa</td></tr>
	<tr><td>5.4</td><td>3.9</td><td>1.7</td><td>0.4</td><td>setosa</td></tr>
</tbody>
</table>




```R
f1 <- iris_dt[Sepal.Length < 6 & Sepal.Width < 4, ]
```


```R
head(f1)
```


<table class="dataframe">
<caption>A data.table: 6 × 5</caption>
<thead>
	<tr><th scope=col>Sepal.Length</th><th scope=col>Sepal.Width</th><th scope=col>Petal.Length</th><th scope=col>Petal.Width</th><th scope=col>Species</th></tr>
	<tr><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;fct&gt;</th></tr>
</thead>
<tbody>
	<tr><td>5.1</td><td>3.5</td><td>1.4</td><td>0.2</td><td>setosa</td></tr>
	<tr><td>4.9</td><td>3.0</td><td>1.4</td><td>0.2</td><td>setosa</td></tr>
	<tr><td>4.7</td><td>3.2</td><td>1.3</td><td>0.2</td><td>setosa</td></tr>
	<tr><td>4.6</td><td>3.1</td><td>1.5</td><td>0.2</td><td>setosa</td></tr>
	<tr><td>5.0</td><td>3.6</td><td>1.4</td><td>0.2</td><td>setosa</td></tr>
	<tr><td>5.4</td><td>3.9</td><td>1.7</td><td>0.4</td><td>setosa</td></tr>
</tbody>
</table>


Let's see how to subset columns now. 

```R
head(iris_dt[, 1])
```


<table class="dataframe">
<caption>A data.table: 6 × 1</caption>
<thead>
	<tr><th scope=col>Sepal.Length</th></tr>
	<tr><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>5.1</td></tr>
	<tr><td>4.9</td></tr>
	<tr><td>4.7</td></tr>
	<tr><td>4.6</td></tr>
	<tr><td>5.0</td></tr>
	<tr><td>5.4</td></tr>
</tbody>
</table>



### Drop columns in data.table


```R
drop_cols <- c("Petal.Length", "Petal.Width")
f2 <- iris_dt[, !drop_cols, with=FALSE]
```


```R
head(f2)
```


<table class="dataframe">
<caption>A data.table: 6 × 3</caption>
<thead>
	<tr><th scope=col>Sepal.Length</th><th scope=col>Sepal.Width</th><th scope=col>Species</th></tr>
	<tr><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;fct&gt;</th></tr>
</thead>
<tbody>
	<tr><td>5.1</td><td>3.5</td><td>setosa</td></tr>
	<tr><td>4.9</td><td>3.0</td><td>setosa</td></tr>
	<tr><td>4.7</td><td>3.2</td><td>setosa</td></tr>
	<tr><td>4.6</td><td>3.1</td><td>setosa</td></tr>
	<tr><td>5.0</td><td>3.6</td><td>setosa</td></tr>
	<tr><td>5.4</td><td>3.9</td><td>setosa</td></tr>
</tbody>
</table>



### How to rename columns


```R
setnames(iris_dt, 'Sepal.Length', 'SepalLength')
colnames(iris_dt)
```


'SepalLength' 'Sepal.Width' 'Petal.Length' 'Petal.Width' 'Species'



### Math operations on existing columns
You can always create a new column as you do with a data.frame, but, data.table lets you create column from within square brackets



```R
# data.frame syntax (works on data.table)
affairs$gender_age <- affairs$gender + affairs$children

# data.table syntax
iris_dt[, Petal := Petal.Length+Petal.Width]
head(iris_dt)
```


<table class="dataframe">
<caption>A data.table: 6 × 6</caption>
<thead>
	<tr><th scope=col>SepalLength</th><th scope=col>Sepal.Width</th><th scope=col>Petal.Length</th><th scope=col>Petal.Width</th><th scope=col>Species</th><th scope=col>Petal</th></tr>
	<tr><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;fct&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>5.1</td><td>3.5</td><td>1.4</td><td>0.2</td><td>setosa</td><td>1.6</td></tr>
	<tr><td>4.9</td><td>3.0</td><td>1.4</td><td>0.2</td><td>setosa</td><td>1.6</td></tr>
	<tr><td>4.7</td><td>3.2</td><td>1.3</td><td>0.2</td><td>setosa</td><td>1.5</td></tr>
	<tr><td>4.6</td><td>3.1</td><td>1.5</td><td>0.2</td><td>setosa</td><td>1.7</td></tr>
	<tr><td>5.0</td><td>3.6</td><td>1.4</td><td>0.2</td><td>setosa</td><td>1.6</td></tr>
	<tr><td>5.4</td><td>3.9</td><td>1.7</td><td>0.4</td><td>setosa</td><td>2.1</td></tr>
</tbody>
</table>



 ### .N and .I  operations
.N contains the number of rows present. So the following will get the number of rows for each unique value of `Species`

```R
iris_dt[, .N, by=Species]
```


<table class="dataframe">
<caption>A data.table: 3 × 2</caption>
<thead>
	<tr><th scope=col>Species</th><th scope=col>N</th></tr>
	<tr><th scope=col>&lt;fct&gt;</th><th scope=col>&lt;int&gt;</th></tr>
</thead>
<tbody>
	<tr><td>setosa    </td><td>50</td></tr>
	<tr><td>versicolor</td><td>50</td></tr>
	<tr><td>virginica </td><td>50</td></tr>
</tbody>
</table>


Now, how to create row numbers of items? It can be done using .I variable, short for ‘index’ (I guess)

```R
iris_dt[, .I]
```

prints row numbers


```R
iris_dt[SepalLength < 6, .I]
```

```R
iris_dt[, .I[SepalLength < 6]]
```
```R
iris_dt[, which(SepalLength < 6)]
```

The above 3 functions prints row numbers whose value less than 6 in SepalLength column.


### Chaining in data.table
Data.Table offers unique features there makes it even more powerful and truly a swiss army knife for data manipulation. First lets understand what chaining is.

Using chaining, you can do multiple datatable operatations one after the other without having to store intermediate results

```R
dt1 <- iris_dt[, .(mean_SepalLength=mean(SepalLength),
                     mean_SepalWidth=mean(Sepal.Width),
                     mean_PetalLength=mean(Petal.Length),
                     mean_PetalWidth=mean(Petal.Width)), by=Species][order(Species), ]
```


```R
dt1
```


<table class="dataframe">
<caption>A data.table: 3 × 5</caption>
<thead>
	<tr><th scope=col>Species</th><th scope=col>mean_SepalLength</th><th scope=col>mean_SepalWidth</th><th scope=col>mean_PetalLength</th><th scope=col>mean_PetalWidth</th></tr>
	<tr><th scope=col>&lt;fct&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>setosa    </td><td>5.006</td><td>3.428</td><td>1.462</td><td>0.246</td></tr>
	<tr><td>versicolor</td><td>5.936</td><td>2.770</td><td>4.260</td><td>1.326</td></tr>
	<tr><td>virginica </td><td>6.588</td><td>2.974</td><td>5.552</td><td>2.026</td></tr>
</tbody>
</table>




```R
output <- iris_dt[, lapply(.SD, mean), by=Species, .SDcols=c("SepalLength","Sepal.Width")]
output
```


<table class="dataframe">
<caption>A data.table: 3 × 3</caption>
<thead>
	<tr><th scope=col>Species</th><th scope=col>SepalLength</th><th scope=col>Sepal.Width</th></tr>
	<tr><th scope=col>&lt;fct&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>setosa    </td><td>5.006</td><td>3.428</td></tr>
	<tr><td>versicolor</td><td>5.936</td><td>2.770</td></tr>
	<tr><td>virginica </td><td>6.588</td><td>2.974</td></tr>
</tbody>
</table>




```R
setkey(iris_dt, "SepalLength","Sepal.Width")
```


```R
dt1 <- iris_dt[, .(mean_SepalLength=mean(SepalLength),
                     mean_SepalWidth=mean(Sepal.Width),
                     mean_PetalLength=mean(Petal.Length),
                     mean_PetalWidth=mean(Petal.Width)), keyby=Species][order(Species), ]
```


```R
dt1
```


<table class="dataframe">
<caption>A data.table: 3 × 5</caption>
<thead>
	<tr><th scope=col>Species</th><th scope=col>mean_SepalLength</th><th scope=col>mean_SepalWidth</th><th scope=col>mean_PetalLength</th><th scope=col>mean_PetalWidth</th></tr>
	<tr><th scope=col>&lt;fct&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>setosa    </td><td>5.006</td><td>3.428</td><td>1.462</td><td>0.246</td></tr>
	<tr><td>versicolor</td><td>5.936</td><td>2.770</td><td>4.260</td><td>1.326</td></tr>
	<tr><td>virginica </td><td>6.588</td><td>2.974</td><td>5.552</td><td>2.026</td></tr>
</tbody>
</table>


### Conclusion
In R, tabular data can be worked with using the data.table package. It offers the effective data.table object, a significantly better alternative of the built-in data.frame. It is incredibly quick and has a simple, clear syntax. This tutorial guide is an excellent place to start if you are familiar with the R language but haven't used the `data.table` package yet.


### References
- https://cran.r-project.org/web/packages/data.table/vignettes/datatable-intro.html
- https://cran.r-project.org/web/packages/data.table/data.table.pdf