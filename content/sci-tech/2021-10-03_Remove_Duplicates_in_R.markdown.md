+++
title = "Remove Duplicates in R"
date = 2021-10-03
draft = false
tags = ["R Tutorial"]
categories = []
+++



### Removing Duplicates in R


```R
library(dplyr)
```

    
    Attaching package: ‘dplyr’
    
    
    The following objects are masked from ‘package:stats’:
    
        filter, lag
    
    
    The following objects are masked from ‘package:base’:
    
        intersect, setdiff, setequal, union
    
    



```R
data <- data.frame(Column1 = c('P1', 'P1', 'P2', 'P3', 'P1', 'P1', 'P3', 'P4', 'P2', 'P4'), Column2 = c(5, 5, 3, 5, 2, 3, 4, 7, 10, 14))
```


```R
data
```


<table class="dataframe">
<caption>A data.frame: 10 × 2</caption>
<thead>
	<tr><th scope=col>Column1</th><th scope=col>Column2</th></tr>
	<tr><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>P1</td><td> 5</td></tr>
	<tr><td>P1</td><td> 5</td></tr>
	<tr><td>P2</td><td> 3</td></tr>
	<tr><td>P3</td><td> 5</td></tr>
	<tr><td>P1</td><td> 2</td></tr>
	<tr><td>P1</td><td> 3</td></tr>
	<tr><td>P3</td><td> 4</td></tr>
	<tr><td>P4</td><td> 7</td></tr>
	<tr><td>P2</td><td>10</td></tr>
	<tr><td>P4</td><td>14</td></tr>
</tbody>
</table>


If we want to delete duplicate rows or values from a certain column, we can use the distinct function.

Let’s remove duplicate rows from Column2.

```R
distinct(data)
```


<table class="dataframe">
<caption>A data.frame: 9 × 2</caption>
<thead>
	<tr><th scope=col>Column1</th><th scope=col>Column2</th></tr>
	<tr><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>P1</td><td> 5</td></tr>
	<tr><td>P2</td><td> 3</td></tr>
	<tr><td>P3</td><td> 5</td></tr>
	<tr><td>P1</td><td> 2</td></tr>
	<tr><td>P1</td><td> 3</td></tr>
	<tr><td>P3</td><td> 4</td></tr>
	<tr><td>P4</td><td> 7</td></tr>
	<tr><td>P2</td><td>10</td></tr>
	<tr><td>P4</td><td>14</td></tr>
</tbody>
</table>




```R
distinct(data, Column2, .keep_all=TRUE)
```


<table class="dataframe">
<caption>A data.frame: 7 × 2</caption>
<thead>
	<tr><th scope=col>Column1</th><th scope=col>Column2</th></tr>
	<tr><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>P1</td><td> 5</td></tr>
	<tr><td>P2</td><td> 3</td></tr>
	<tr><td>P1</td><td> 2</td></tr>
	<tr><td>P3</td><td> 4</td></tr>
	<tr><td>P4</td><td> 7</td></tr>
	<tr><td>P2</td><td>10</td></tr>
	<tr><td>P4</td><td>14</td></tr>
</tbody>
</table>




```R
unique(data)
```


<table class="dataframe">
<caption>A data.frame: 9 × 2</caption>
<thead>
	<tr><th></th><th scope=col>Column1</th><th scope=col>Column2</th></tr>
	<tr><th></th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>1</th><td>P1</td><td> 5</td></tr>
	<tr><th scope=row>3</th><td>P2</td><td> 3</td></tr>
	<tr><th scope=row>4</th><td>P3</td><td> 5</td></tr>
	<tr><th scope=row>5</th><td>P1</td><td> 2</td></tr>
	<tr><th scope=row>6</th><td>P1</td><td> 3</td></tr>
	<tr><th scope=row>7</th><td>P3</td><td> 4</td></tr>
	<tr><th scope=row>8</th><td>P4</td><td> 7</td></tr>
	<tr><th scope=row>9</th><td>P2</td><td>10</td></tr>
	<tr><th scope=row>10</th><td>P4</td><td>14</td></tr>
</tbody>
</table>


The duplicated function is also very handy to remove repeated rows from a data frame.

```R
duplicated(data)
```


<style>
.list-inline {list-style: none; margin:0; padding: 0}
.list-inline>li {display: inline-block}
.list-inline>li:not(:last-child)::after {content: "\00b7"; padding: 0 .5ex}
</style>
<ol class=list-inline><li>FALSE</li><li>TRUE</li><li>FALSE</li><li>FALSE</li><li>FALSE</li><li>FALSE</li><li>FALSE</li><li>FALSE</li><li>FALSE</li><li>FALSE</li></ol>




```R
data[!duplicated(data),]
```


<table class="dataframe">
<caption>A data.frame: 9 × 2</caption>
<thead>
	<tr><th></th><th scope=col>Column1</th><th scope=col>Column2</th></tr>
	<tr><th></th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>1</th><td>P1</td><td> 5</td></tr>
	<tr><th scope=row>3</th><td>P2</td><td> 3</td></tr>
	<tr><th scope=row>4</th><td>P3</td><td> 5</td></tr>
	<tr><th scope=row>5</th><td>P1</td><td> 2</td></tr>
	<tr><th scope=row>6</th><td>P1</td><td> 3</td></tr>
	<tr><th scope=row>7</th><td>P3</td><td> 4</td></tr>
	<tr><th scope=row>8</th><td>P4</td><td> 7</td></tr>
	<tr><th scope=row>9</th><td>P2</td><td>10</td></tr>
	<tr><th scope=row>10</th><td>P4</td><td>14</td></tr>
</tbody>
</table>




```R

```
