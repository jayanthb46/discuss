+++
title = "Pandas vs. PyArrow file reading speed comparison"
date = 2024-08-11
draft = false
tags = ["Python"]
categories = []
+++

# Pandas vs. PyArrow file reading speed comparison
## August, 2024

pyarrow is a Python library that provides a way to interact with Apache Arrow, an open-source project designed for high-performance data interchange and analytics. Apache Arrow provides a standardized columnar memory format optimized for fast data processing and interchange between systems.

Arrow's columnar format is designed to support efficient analytics on large datasets by allowing operations to be performed on individual columns rather than rows.

pyarrow allows for seamless data interchange between different systems and frameworks that support the Arrow format, such as Apache Spark, Dask, and pandas.

pyarrow supports reading and writing Parquet and Feather file formats. Parquet is a columnar storage file format that is efficient for querying and analyzing large datasets, while Feather is designed for fast, lightweight data interchange.

pandas is a powerful and widely-used Python library for data manipulation and analysis. It provides data structures and functions needed to work with structured data seamlessly. 

---


```python
import time
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pyarrow.parquet as pq
import os
import sys
import matplotlib as mpl
mpl.rcParams['figure.dpi']=125
```

## Create CSV files of various sizes

In this we create 11 csv files with varying file sizes. 

```python
for i in range(1,11):
    a = np.random.normal(size=(int(6500*i), int(1e2)))
    df = pd.DataFrame(a, columns=["C" + str(i) for i in range(100)])
    fname = "test"+str(i)+".csv"
    df.to_csv(fname)
    print(f"Size of file with {6500*i} rows: {round(os.path.getsize(fname)/(1024*1024),3)} MB")
```

    Size of file with 6500 rows: 12.2 MB
    Size of file with 13000 rows: 24.402 MB
    Size of file with 19500 rows: 36.608 MB
    Size of file with 26000 rows: 48.815 MB
    Size of file with 32500 rows: 61.019 MB
    Size of file with 39000 rows: 73.231 MB
    Size of file with 45500 rows: 85.432 MB
    Size of file with 52000 rows: 97.638 MB
    Size of file with 58500 rows: 109.847 MB
    Size of file with 65000 rows: 122.056 MB


## Create Parquet (compressed) files from the same CSV files

Here we convert above created csv files to parquet file format.


```python
for i in range(1,11):
    fname = "test"+str(i)+".csv"
    parquet_name = "test"+str(i)+"_parquet.zip"
    df = pd.read_csv(fname)
    df.to_parquet(parquet_name,compression="gzip")
    print(f"Created {parquet_name}")
```

    Created test1_parquet.zip
    Created test2_parquet.zip
    Created test3_parquet.zip
    Created test4_parquet.zip
    Created test5_parquet.zip
    Created test6_parquet.zip
    Created test7_parquet.zip
    Created test8_parquet.zip
    Created test9_parquet.zip
    Created test10_parquet.zip


---

## Reading speed of CSV (in Pandas) and Parquet files (with `PyArrow`)

Here we measure the time required to read CSV and Parquet files using pandas and PyArrow. The box plot is generated to show significant differences in time scales between pyarrow and pandas.


```python
t_read_pd,t_read_arrow = [],[]

for i in range(1,11):
    fname = "test"+str(i)+".csv"
    parquet_name = "test"+str(i)+"_parquet.zip"
    t1 = time.time()
    df1 = pd.read_csv(fname)
    t2 = time.time()
    delta_t = round((t2 - t1), 3)
    t_read_pd.append(delta_t)
    t1 = time.time()
    df2 = pq.read_table(parquet_name)
    t2 = time.time()
    delta_t = round((t2 - t1), 3)
    t_read_arrow.append(delta_t)
    print(f"Done for file # {i}")
```

    Done for file # 1
    Done for file # 2
    Done for file # 3
    Done for file # 4
    Done for file # 5
    Done for file # 6
    Done for file # 7
    Done for file # 8
    Done for file # 9
    Done for file # 10



```python
print(t_read_pd)
```

    [0.082, 0.162, 0.23, 0.306, 0.402, 0.508, 0.515, 0.572, 0.643, 0.715]



```python
print(t_read_arrow)
```

    [0.018, 0.017, 0.024, 0.029, 0.036, 0.042, 0.041, 0.046, 0.05, 0.057]



```python
fig = plt.figure(figsize =(10, 7))
ax = fig.add_axes([0,0,1,1])

data = [t_read_pd, t_read_arrow]

# Creating plot
bp = ax.boxplot(data)
ax.set_xticklabels(['pandas', 'pyarrow'])

# Adding title 
plt.title("box plot describing time difference between pandas and pyarrow")
plt.ylabel('Time in seconds')
plt.xlabel('Methods')

```
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/Aug_11_2024/output_11_1.png)

The box plot shows significant differences between pandas and pyarrow. The plot clearly indicates Pyarrow is faster than pandas.
    

```python
t_read_pd = np.array(t_read_pd)
t_read_arrow = np.array(t_read_arrow)
```


```python
data = [t_read_pd, t_read_arrow]
```


    
---

## What's the order of read time? Seconds, milliseconds?


```python
t1 = time.time()
df1 = pd.read_csv("test10.csv", usecols=["C1", "C99"])
t2 = time.time()
delta_t = round((t2 - t1), 3)
print(
    "Time taken to read 2 columns of a 100 MB (6500 rows) CSV file with Pandas:",
    delta_t,
    "seconds",
)
```

- Time taken to read 2 columns of a 100 MB (6500 rows) CSV file with Pandas: 0.348 seconds


- The reading speed of the 100 MB CSV file with `pd.read_csv()` is about 1.114 seconds.


```python
df1.head()
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>C1</th>
      <th>C99</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0.779346</td>
      <td>-1.149560</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1.012497</td>
      <td>0.654147</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1.862528</td>
      <td>0.675613</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0.596877</td>
      <td>0.517626</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1.130894</td>
      <td>-0.483014</td>
    </tr>
  </tbody>
</table>
</div>




```python
t1 = time.time()
df2 = pq.read_table("test10_parquet.zip", columns=["C1", "C99"])
t2 = time.time()
delta_t = round((t2 - t1), 3)
print(
    "Time taken to read 2 columns of the identical 6500 rows zipped parquet file with PyArrow:",
    delta_t,
    "seconds",
)
```

- Time taken to read 2 columns of the identical 6500 rows zipped parquet file with PyArrow: 0.005 seconds


- The reading speed of the same file (in the parquet gzipped version) with `pq.read_table()` is about 0.026 seconds


```python
# Convert from PyArrow table to dataframe
df3 = df2.to_pandas()
df3.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>C1</th>
      <th>C99</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>0.779346</td>
      <td>-1.149560</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1.012497</td>
      <td>0.654147</td>
    </tr>
    <tr>
      <th>2</th>
      <td>1.862528</td>
      <td>0.675613</td>
    </tr>
    <tr>
      <th>3</th>
      <td>0.596877</td>
      <td>0.517626</td>
    </tr>
    <tr>
      <th>4</th>
      <td>1.130894</td>
      <td>-0.483014</td>
    </tr>
  </tbody>
</table>
</div>



#### The dataframes `df1` and `df3` are the same, as expected.

---

## Reading a small number of columns is much faster with Arrow


```python
all_cols = ["C" + str(i) for i in range(100)]
t_pandas, t_arrow = [], []
for i in range(2, 100, 2):
    cols = list(np.random.choice(all_cols, size=i))
    t1 = time.time()
    df1 = pd.read_csv("test10.csv", usecols=cols)
    t2 = time.time()
    delta_t_pandas = round((t2 - t1), 3)
    t_pandas.append(delta_t_pandas)
    t1 = time.time()
    df2 = pq.read_table("test10_parquet.zip", columns=cols)
    t2 = time.time()
    delta_t_arrow = round((t2 - t1), 3)
    t_arrow.append(delta_t_arrow)
    print(f"Done for {i} columns")
```

    Done for 2 columns
    Done for 4 columns
    Done for 6 columns
    Done for 8 columns
    Done for 10 columns
    Done for 12 columns
    Done for 14 columns
    Done for 16 columns
    Done for 18 columns
    Done for 20 columns
    Done for 22 columns
    Done for 24 columns
    Done for 26 columns
    Done for 28 columns
    Done for 30 columns
    Done for 32 columns
    Done for 34 columns
    Done for 36 columns
    Done for 38 columns
    Done for 40 columns
    Done for 42 columns
    Done for 44 columns
    Done for 46 columns
    Done for 48 columns
    Done for 50 columns
    Done for 52 columns
    Done for 54 columns
    Done for 56 columns
    Done for 58 columns
    Done for 60 columns
    Done for 62 columns
    Done for 64 columns
    Done for 66 columns
    Done for 68 columns
    Done for 70 columns
    Done for 72 columns
    Done for 74 columns
    Done for 76 columns
    Done for 78 columns
    Done for 80 columns
    Done for 82 columns
    Done for 84 columns
    Done for 86 columns
    Done for 88 columns
    Done for 90 columns
    Done for 92 columns
    Done for 94 columns
    Done for 96 columns
    Done for 98 columns



```python
t_pandas = np.array(t_pandas)
t_arrow = np.array(t_arrow)
```

![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/Aug_11_2024/download.png)
The box plot shows significant differences between pandas and pyarrow. The plot clearly indicates Pyarrow is faster than pandas.

## References

- https://arrow.apache.org/docs/python/index.html
- https://pandas.pydata.org/docs/
