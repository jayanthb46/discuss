+++
title = "K-Medoids clustering in R"
date = 2024-09-15
draft = false
tags = ["R"]
categories = []
+++

## K-Medoids clustering in R.
## 15th September, 2024

### Introduction

K-Medoids clustering is a method used in data analysis for identifying clusters or groups within a dataset based on their similarity. This algorithm selects k points (k being the number of desired clusters) from the dataset and then assigns other data points to these points chosen according to their proximity. In this way, it attempts to divide the dataset into separate cluster groups while minimizing the overall distance between data points within each group and reducing overlap between different groups. The resulting clusters provide insights into the underlying structure and patterns of the data set. This technique has been widely applied in various fields such as finance, biology, psychology, and marketing.

### The concept of Partition Around Medoids (PAM) algorithm

Partition Around Medoids (PAM) is a variation of the K-Means++ algorithm designed specifically for detecting medoid centers - the centroid of clusters that contains the highest proportion of data points. PAM extends the traditional K-Means algorithm by adding a partitioning step before performing the final centroid assignment. Here's how it works:

1. Divide the entire dataset into subsets containing approximately equal numbers of samples. Each of these smaller datasets represents potential clusters.
2. Perform K-Means+ on each subset separately to calculate initial centroid locations. Use mean as the distance metric during this process.
3. Calculate partial sum of squared differences between data points and their nearest centroids at every partition point. This value indicates how well each centroid matches its corresponding data points.
4. Assign each data point to the closest centroid based on its distance measurement and total sum of squared differences calculated above. Centroids closer to fewer data points contribute significantly less to overall cluster quality than those associated with many data points.
5. Repeat steps 2–4 until either convergence is reached (no further improvements occur upon updating centroid positions), or a specified maximum number of iterations has been exceeded.
6. Finally, select the top 'k'-1 clusters based on the percentage of data points contained within them. This will yield the optimal set of partitions around medoids.


**Note** 
Note that, in practice, you should get similar results most of the time, using either euclidean or Manhattan distance metric. If your data contains outliers, Manhattan distance should give more robust results, whereas euclidean would be influenced by unusual values.

### Computing PAM in R


```R
# load the data
data("USArrests")
df<- scale(USArrests) # Scale the data
head(df,5) # look at the first five rows of the data
```


<table class="dataframe">
<caption>A matrix: 5 × 4 of type dbl</caption>
<thead>
	<tr><th></th><th scope=col>Murder</th><th scope=col>Assault</th><th scope=col>UrbanPop</th><th scope=col>Rape</th></tr>
</thead>
<tbody>
	<tr><th scope=row>Alabama</th><td>1.24256408</td><td>0.7828393</td><td>-0.5209066</td><td>-0.003416473</td></tr>
	<tr><th scope=row>Alaska</th><td>0.50786248</td><td>1.1068225</td><td>-1.2117642</td><td> 2.484202941</td></tr>
	<tr><th scope=row>Arizona</th><td>0.07163341</td><td>1.4788032</td><td> 0.9989801</td><td> 1.042878388</td></tr>
	<tr><th scope=row>Arkansas</th><td>0.23234938</td><td>0.2308680</td><td>-1.0735927</td><td>-0.184916602</td></tr>
	<tr><th scope=row>California</th><td>0.27826823</td><td>1.2628144</td><td> 1.7589234</td><td> 2.067820292</td></tr>
</tbody>
</table>




```R
# load the required packages
library(cluster)
library(factoextra)
library(fpc)
library(ggplot2) 
```

    Loading required package: ggplot2
    
    Welcome! Want to learn more? See two factoextra-related books at https://goo.gl/ve3WBa
    


The function `pam()` [cluster package] and `pamk()` [fpc package] can be used to compute PAM.

Now lets determine the possible number of clusters in a dataset. As we can see using the function `fviz_nbclust()` generates the plot. From the plot, the suggested number of clusters is 2. In the next section, we’ll classify the observations into 2 clusters.


```R
fviz_nbclust(df, pam, method = "silhouette")+
  theme_classic()
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/T1/output_7_0.png)
    


Computing PAM clustering

The R code below computes PAM algorithm with k = 2


```R
pam.res <- pam(df, 2)
print(pam.res)
```

    Medoids:
               ID     Murder    Assault   UrbanPop       Rape
    New Mexico 31  0.8292944  1.3708088  0.3081225  1.1603196
    Nebraska   27 -0.8008247 -0.8250772 -0.2445636 -0.5052109
    Clustering vector:
           Alabama         Alaska        Arizona       Arkansas     California 
                 1              1              1              2              1 
          Colorado    Connecticut       Delaware        Florida        Georgia 
                 1              2              2              1              1 
            Hawaii          Idaho       Illinois        Indiana           Iowa 
                 2              2              1              2              2 
            Kansas       Kentucky      Louisiana          Maine       Maryland 
                 2              2              1              2              1 
     Massachusetts       Michigan      Minnesota    Mississippi       Missouri 
                 2              1              2              1              1 
           Montana       Nebraska         Nevada  New Hampshire     New Jersey 
                 2              2              1              2              2 
        New Mexico       New York North Carolina   North Dakota           Ohio 
                 1              1              1              2              2 
          Oklahoma         Oregon   Pennsylvania   Rhode Island South Carolina 
                 2              2              2              2              1 
      South Dakota      Tennessee          Texas           Utah        Vermont 
                 2              1              1              2              2 
          Virginia     Washington  West Virginia      Wisconsin        Wyoming 
                 2              2              2              2              2 
    Objective function:
       build     swap 
    1.441358 1.368969 
    
    Available components:
     [1] "medoids"    "id.med"     "clustering" "objective"  "isolation" 
     [6] "clusinfo"   "silinfo"    "diss"       "call"       "data"      


The printed output shows:

- the cluster medoids: a matrix, which rows are the medoids and columns are variables
- the clustering vector: A vector of integers (from 1:k) indicating the cluster to which each point is allocated

If you want to add the point classifications to the original data, use the following code;


```R
dd <- cbind(USArrests, cluster = pam.res$cluster)
head(dd, n = 3)
```


<table class="dataframe">
<caption>A data.frame: 3 × 5</caption>
<thead>
	<tr><th></th><th scope=col>Murder</th><th scope=col>Assault</th><th scope=col>UrbanPop</th><th scope=col>Rape</th><th scope=col>cluster</th></tr>
	<tr><th></th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;int&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>Alabama</th><td>13.2</td><td>236</td><td>58</td><td>21.2</td><td>1</td></tr>
	<tr><th scope=row>Alaska</th><td>10.0</td><td>263</td><td>48</td><td>44.5</td><td>1</td></tr>
	<tr><th scope=row>Arizona</th><td> 8.1</td><td>294</td><td>80</td><td>31.0</td><td>1</td></tr>
</tbody>
</table>



Accessing to the results of the pam() function

The function pam() returns an object of class pam which components include:

- medoids: Objects that represent clusters
- clustering: a vector containing the cluster number of each object

These components can be accessed as follow;


```R
# Cluster medoids: New Mexico, Nebraska
pam.res$medoids
```


<table class="dataframe">
<caption>A matrix: 2 × 4 of type dbl</caption>
<thead>
	<tr><th></th><th scope=col>Murder</th><th scope=col>Assault</th><th scope=col>UrbanPop</th><th scope=col>Rape</th></tr>
</thead>
<tbody>
	<tr><th scope=row>New Mexico</th><td> 0.8292944</td><td> 1.3708088</td><td> 0.3081225</td><td> 1.1603196</td></tr>
	<tr><th scope=row>Nebraska</th><td>-0.8008247</td><td>-0.8250772</td><td>-0.2445636</td><td>-0.5052109</td></tr>
</tbody>
</table>




```R
# cluster numbers
head(pam.res$clustering)
```


<style>
.dl-inline {width: auto; margin:0; padding: 0}
.dl-inline>dt, .dl-inline>dd {float: none; width: auto; display: inline-block}
.dl-inline>dt::after {content: ":\0020"; padding-right: .5ex}
.dl-inline>dt:not(:first-of-type) {padding-left: .5ex}
</style><dl class=dl-inline><dt>Alabama</dt><dd>1</dd><dt>Alaska</dt><dd>1</dd><dt>Arizona</dt><dd>1</dd><dt>Arkansas</dt><dd>2</dd><dt>California</dt><dd>1</dd><dt>Colorado</dt><dd>1</dd></dl>



Visualizing PAM clusters

To visualize the partitioning results, we’ll use the function `fviz_cluster()` [factoextra package]. It draws a scatter plot of data points colored by cluster numbers. If the data contains more than 2 variables, the Principal Component Analysis (PCA) algorithm is used to reduce the dimensionality of the data. In this case, the first two principal dimensions are used to plot the data.


```R
fviz_cluster(pam.res, 
             palette = c("#00AFBB", "#FC4E07"), # color palette
             ellipse.type = "t", # Concentration ellipse
             repel = TRUE, # Avoid label overplotting (slow)
             ggtheme = theme_classic()
             )
```

    Warning message in grid.Call.graphics(C_polygon, x$x, x$y, index):
    “semi-transparency is not supported on this device: reported only once per page”



    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/T1/output_16_1.png)
    


### Summary

- The K-medoids algorithm, PAM, is a robust alternative to k-means for partitioning a data set into clusters of observation.

- In k-medoids method, each cluster is represented by a selected object within the cluster. The selected objects are named medoids and corresponds to the most centrally located points within the cluster.

- The PAM algorithm requires the user to know the data and to indicate the appropriate number of clusters to be produced. This can be estimated using the function `fviz_nbclust` [in factoextra R package].

- The R function `pam()` [cluster package] can be used to `compute PAM algorithm`. The simplified format is `pam(x, k)`, where `“x” is the data and k is the number of clusters to be generated`.

- After, performing PAM clustering, the R function `fviz_cluster()` [factoextra package] can be used to visualize the results. The format is `fviz_cluster(pam.res)`, where pam.res is the PAM results.

### References

- Kaufman, Leonard, and Peter Rousseeuw. 1990. Finding Groups in Data: An Introduction to Cluster Analysis