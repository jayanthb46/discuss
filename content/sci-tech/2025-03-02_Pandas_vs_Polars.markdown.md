+++
title = "Polars vs. Pandas file reading speed comparison"
date = 2025-03-02
draft = false
tags = ["Python"]
categories = []
+++

## Polars vs. Pandas file reading speed comparison

### Introduction to Polars and pandas

### Polars

Polars is a DataFrame library written in Rust and exposed through a Python interface. It's known for its speed and efficiency. Here's a breakdown of key aspects:

### Key Features and Advantages:

- Speed:
Polars is built on Apache Arrow, which enables efficient in-memory data representation.
It leverages Rust's performance and concurrency.
It utilizes query optimization techniques, such as lazy evaluation, to minimize computations.
- Lazy Evaluation:
Polars supports lazy evaluation, where operations are not executed immediately. Instead, a query plan is built and optimized before execution. This allows for significant performance gains.
- Memory Efficiency:
Its columnar data storage and efficient memory management contribute to reduced memory usage.
- Expressive API:
Polars offers a clear and consistent API for data manipulation, similar to Pandas but with a focus on performance.
- Parallelism:
Polars takes advantage of all available cores on your system.
- Arrow Backend:
Using Apache arrow as its memory model provides interoperability with other arrow compatible tools.

### Pandas

pandas is a dataframe library implemented in cython. It is considered as the corner stone of python libraries.

### Key Features and Capabilities:

- Data Cleaning:
Pandas provides tools for handling missing data (NaN), filtering data, and cleaning messy datasets.
- Data Transformation:
It allows you to reshape, merge, and join datasets, as well as perform various data transformations.
- Data Analysis:
Pandas offers functions for calculating descriptive statistics, grouping data, and performing other data analysis tasks.
- Data Input/Output:
It supports reading and writing data from various file formats, including CSV, Excel, SQL databases, and more.
- Time Series Analysis:
Pandas has robust capabilities for working with time series data, including date range generation, frequency conversion, and time-based indexing.
- Indexing and Selection:
Pandas has very flexible methods for selecting data from within the dataframes, allowing for selection by label, location, or boolean indexing.


This blog aims to compare performance of reading/loading large file formats using Pandas and Polars.
In this blog, I use a dataset contained 892 rows with 7 variables from titanic at [here](https://github.com/alicevillar/titanic-kaggle/blob/main/titanic.csv)

Due to limitation of computational resources we are not attempting big datasets for comparison. Results from small titanic dataset can be interpolated for big dataset.

- If you convert *csv to *parquet and/or *feather, file sizes are reduced to 82kb for *parquet and 32kb for *feather.
- The overall comparison of *csv, *parquet, and *feather formats can be summarized [here](https://twitter.com/levikul09/status/1644629913440501763/photo/1)


### Task 1 Import pandas and polars check version


```python
import pandas as pd
import polars as pl
import time
DATA_DIR = '/home/ab/Downloads'

print(f'Pandas version: {pd.__version__}')
print(f'Polars version: {pl.__version__}')
```

    Pandas version: 2.1.4
    Polars version: 1.23.0


- Indeed, Pandas version 2 has been up-to-date using either pyarrow or NumPy engines.
- We can choose these backends via a function `dtype='pyarrow'` or a default as `dtype_backend=numpy_nullable`
`pd_feather = pd.read_feather(f"{DATA_DIR}/titanic.feather", dtype_backend='pyarrow')`

## Check how is fast to read a large file with *CSV format using Pandas and Polars
### Polars with Lazily reading


```python
%time
pl_csv = (
    pl.scan_csv(f"{DATA_DIR}/titanic.csv")
 
)

```

    CPU times: user 1e+03 ns, sys: 0 ns, total: 1e+03 ns
    Wall time: 3.34 µs


### Polars with fully reading


```python
%time
pl_csv = (
    pl.read_csv(f"{DATA_DIR}/titanic.csv")
)
pl_csv.describe()
```

    CPU times: user 1 µs, sys: 1 µs, total: 2 µs
    Wall time: 3.58 µs





<div><style>
.dataframe > thead > tr,
.dataframe > tbody > tr {
  text-align: right;
  white-space: pre-wrap;
}
</style>
<small>shape: (9, 13)</small><table border="1" class="dataframe"><thead><tr><th>statistic</th><th>PassengerId</th><th>Survived</th><th>Pclass</th><th>Name</th><th>Sex</th><th>Age</th><th>SibSp</th><th>Parch</th><th>Ticket</th><th>Fare</th><th>Cabin</th><th>Embarked</th></tr><tr><td>str</td><td>f64</td><td>f64</td><td>f64</td><td>str</td><td>str</td><td>f64</td><td>f64</td><td>f64</td><td>str</td><td>f64</td><td>str</td><td>str</td></tr></thead><tbody><tr><td>&quot;count&quot;</td><td>891.0</td><td>891.0</td><td>891.0</td><td>&quot;891&quot;</td><td>&quot;891&quot;</td><td>714.0</td><td>891.0</td><td>891.0</td><td>&quot;891&quot;</td><td>891.0</td><td>&quot;204&quot;</td><td>&quot;889&quot;</td></tr><tr><td>&quot;null_count&quot;</td><td>0.0</td><td>0.0</td><td>0.0</td><td>&quot;0&quot;</td><td>&quot;0&quot;</td><td>177.0</td><td>0.0</td><td>0.0</td><td>&quot;0&quot;</td><td>0.0</td><td>&quot;687&quot;</td><td>&quot;2&quot;</td></tr><tr><td>&quot;mean&quot;</td><td>446.0</td><td>0.383838</td><td>2.308642</td><td>null</td><td>null</td><td>29.699118</td><td>0.523008</td><td>0.381594</td><td>null</td><td>32.204208</td><td>null</td><td>null</td></tr><tr><td>&quot;std&quot;</td><td>257.353842</td><td>0.486592</td><td>0.836071</td><td>null</td><td>null</td><td>14.526497</td><td>1.102743</td><td>0.806057</td><td>null</td><td>49.693429</td><td>null</td><td>null</td></tr><tr><td>&quot;min&quot;</td><td>1.0</td><td>0.0</td><td>1.0</td><td>&quot;Abbing, Mr. Anthony&quot;</td><td>&quot;female&quot;</td><td>0.42</td><td>0.0</td><td>0.0</td><td>&quot;110152&quot;</td><td>0.0</td><td>&quot;A10&quot;</td><td>&quot;C&quot;</td></tr><tr><td>&quot;25%&quot;</td><td>224.0</td><td>0.0</td><td>2.0</td><td>null</td><td>null</td><td>20.0</td><td>0.0</td><td>0.0</td><td>null</td><td>7.925</td><td>null</td><td>null</td></tr><tr><td>&quot;50%&quot;</td><td>446.0</td><td>0.0</td><td>3.0</td><td>null</td><td>null</td><td>28.0</td><td>0.0</td><td>0.0</td><td>null</td><td>14.4542</td><td>null</td><td>null</td></tr><tr><td>&quot;75%&quot;</td><td>669.0</td><td>1.0</td><td>3.0</td><td>null</td><td>null</td><td>38.0</td><td>1.0</td><td>0.0</td><td>null</td><td>31.0</td><td>null</td><td>null</td></tr><tr><td>&quot;max&quot;</td><td>891.0</td><td>1.0</td><td>3.0</td><td>&quot;van Melkebeke, Mr. Philemon&quot;</td><td>&quot;male&quot;</td><td>80.0</td><td>8.0</td><td>6.0</td><td>&quot;WE/P 5735&quot;</td><td>512.3292</td><td>&quot;T&quot;</td><td>&quot;S&quot;</td></tr></tbody></table></div>


### Pandas reading csv

```python
%time
pd_csv = (
    pd.read_csv(f"{DATA_DIR}/titanic.csv") #defaults as NumPy backed
    
)
```

    CPU times: user 2 µs, sys: 1 µs, total: 3 µs
    Wall time: 4.29 µs




```python
%time
pd_csv = (
    pd.read_csv(f"{DATA_DIR}/titanic.csv", dtype_backend='pyarrow')
    
)
```

    CPU times: user 3 µs, sys: 0 ns, total: 3 µs
    Wall time: 5.96 µs



```python

pd_csv.describe()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PassengerId</th>
      <th>Survived</th>
      <th>Pclass</th>
      <th>Age</th>
      <th>SibSp</th>
      <th>Parch</th>
      <th>Fare</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>891.000000</td>
      <td>891.000000</td>
      <td>891.000000</td>
      <td>714.000000</td>
      <td>891.000000</td>
      <td>891.000000</td>
      <td>891.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>446.000000</td>
      <td>0.383838</td>
      <td>2.308642</td>
      <td>29.699118</td>
      <td>0.523008</td>
      <td>0.381594</td>
      <td>32.204208</td>
    </tr>
    <tr>
      <th>std</th>
      <td>257.353842</td>
      <td>0.486592</td>
      <td>0.836071</td>
      <td>14.526497</td>
      <td>1.102743</td>
      <td>0.806057</td>
      <td>49.693429</td>
    </tr>
    <tr>
      <th>min</th>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>0.420000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>223.500000</td>
      <td>0.000000</td>
      <td>2.000000</td>
      <td>20.125000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>7.910400</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>446.000000</td>
      <td>0.000000</td>
      <td>3.000000</td>
      <td>28.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>14.454200</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>668.500000</td>
      <td>1.000000</td>
      <td>3.000000</td>
      <td>38.000000</td>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>31.000000</td>
    </tr>
    <tr>
      <th>max</th>
      <td>891.000000</td>
      <td>1.000000</td>
      <td>3.000000</td>
      <td>80.000000</td>
      <td>8.000000</td>
      <td>6.000000</td>
      <td>512.329200</td>
    </tr>
  </tbody>
</table>
</div>



- This task, Pandas takes 6 micro seconds using pyarrow backend and 4.2 micro seconds using numpy backend, 
- while Polars with lazily and fully reading needs 3.34 micro-seconds and 3.58 micro seconds, respectively.
- Polars wins Pandas in the reading task using *csv format

## Check to read a *parquet file
- Convert *csv to *parquet format, this process takes 5.25 micro seconds
- Now, the parquet file size is 82 kb


```python
%time
parquet = (
    pl.scan_csv(f"{DATA_DIR}/titanic.csv")
    .sink_parquet(f"{DATA_DIR}/titanic.parquet")
)
```

    CPU times: user 2 µs, sys: 1 µs, total: 3 µs
    Wall time: 5.25 µs



```python
%time
parquet = (
    pl.scan_parquet(f"{DATA_DIR}/titanic.parquet")
)
```

    CPU times: user 1 µs, sys: 1 µs, total: 2 µs
    Wall time: 3.58 µs



```python
%time
pl_parquet = (
    pl.read_parquet(f"{DATA_DIR}/titanic.parquet")
)
```

    CPU times: user 1 µs, sys: 0 ns, total: 1 µs
    Wall time: 2.86 µs



```python
pl_parquet.describe()
```

<div>
<table border="1" class="dataframe"><thead><tr><th>statistic</th><th>PassengerId</th><th>Survived</th><th>Pclass</th><th>Name</th><th>Sex</th><th>Age</th><th>SibSp</th><th>Parch</th><th>Ticket</th><th>Fare</th><th>Cabin</th><th>Embarked</th></tr><tr><td>str</td><td>f64</td><td>f64</td><td>f64</td><td>str</td><td>str</td><td>f64</td><td>f64</td><td>f64</td><td>str</td><td>f64</td><td>str</td><td>str</td></tr></thead><tbody><tr><td>&quot;count&quot;</td><td>891.0</td><td>891.0</td><td>891.0</td><td>&quot;891&quot;</td><td>&quot;891&quot;</td><td>714.0</td><td>891.0</td><td>891.0</td><td>&quot;891&quot;</td><td>891.0</td><td>&quot;204&quot;</td><td>&quot;889&quot;</td></tr><tr><td>&quot;null_count&quot;</td><td>0.0</td><td>0.0</td><td>0.0</td><td>&quot;0&quot;</td><td>&quot;0&quot;</td><td>177.0</td><td>0.0</td><td>0.0</td><td>&quot;0&quot;</td><td>0.0</td><td>&quot;687&quot;</td><td>&quot;2&quot;</td></tr><tr><td>&quot;mean&quot;</td><td>446.0</td><td>0.383838</td><td>2.308642</td><td>null</td><td>null</td><td>29.699118</td><td>0.523008</td><td>0.381594</td><td>null</td><td>32.204208</td><td>null</td><td>null</td></tr><tr><td>&quot;std&quot;</td><td>257.353842</td><td>0.486592</td><td>0.836071</td><td>null</td><td>null</td><td>14.526497</td><td>1.102743</td><td>0.806057</td><td>null</td><td>49.693429</td><td>null</td><td>null</td></tr><tr><td>&quot;min&quot;</td><td>1.0</td><td>0.0</td><td>1.0</td><td>&quot;Abbing, Mr. Anthony&quot;</td><td>&quot;female&quot;</td><td>0.42</td><td>0.0</td><td>0.0</td><td>&quot;110152&quot;</td><td>0.0</td><td>&quot;A10&quot;</td><td>&quot;C&quot;</td></tr><tr><td>&quot;25%&quot;</td><td>224.0</td><td>0.0</td><td>2.0</td><td>null</td><td>null</td><td>20.0</td><td>0.0</td><td>0.0</td><td>null</td><td>7.925</td><td>null</td><td>null</td></tr><tr><td>&quot;50%&quot;</td><td>446.0</td><td>0.0</td><td>3.0</td><td>null</td><td>null</td><td>28.0</td><td>0.0</td><td>0.0</td><td>null</td><td>14.4542</td><td>null</td><td>null</td></tr><tr><td>&quot;75%&quot;</td><td>669.0</td><td>1.0</td><td>3.0</td><td>null</td><td>null</td><td>38.0</td><td>1.0</td><td>0.0</td><td>null</td><td>31.0</td><td>null</td><td>null</td></tr><tr><td>&quot;max&quot;</td><td>891.0</td><td>1.0</td><td>3.0</td><td>&quot;van Melkebeke, Mr. Philemon&quot;</td><td>&quot;male&quot;</td><td>80.0</td><td>8.0</td><td>6.0</td><td>&quot;WE/P 5735&quot;</td><td>512.3292</td><td>&quot;T&quot;</td><td>&quot;S&quot;</td></tr></tbody></table></div>




```python
%time
pd_parquet = (
    pd.read_parquet(f"{DATA_DIR}/titanic.parquet") # for NumPy backend
)
```

    CPU times: user 1 µs, sys: 0 ns, total: 1 µs
    Wall time: 3.58 µs



```python
pd_feather = pd.read_parquet(f"{DATA_DIR}/titanic.parquet", dtype_backend='pyarrow')
```


```python
pd_parquet.describe()
```




<div>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PassengerId</th>
      <th>Survived</th>
      <th>Pclass</th>
      <th>Age</th>
      <th>SibSp</th>
      <th>Parch</th>
      <th>Fare</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>891.000000</td>
      <td>891.000000</td>
      <td>891.000000</td>
      <td>714.000000</td>
      <td>891.000000</td>
      <td>891.000000</td>
      <td>891.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>446.000000</td>
      <td>0.383838</td>
      <td>2.308642</td>
      <td>29.699118</td>
      <td>0.523008</td>
      <td>0.381594</td>
      <td>32.204208</td>
    </tr>
    <tr>
      <th>std</th>
      <td>257.353842</td>
      <td>0.486592</td>
      <td>0.836071</td>
      <td>14.526497</td>
      <td>1.102743</td>
      <td>0.806057</td>
      <td>49.693429</td>
    </tr>
    <tr>
      <th>min</th>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>0.420000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>223.500000</td>
      <td>0.000000</td>
      <td>2.000000</td>
      <td>20.125000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>7.910400</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>446.000000</td>
      <td>0.000000</td>
      <td>3.000000</td>
      <td>28.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>14.454200</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>668.500000</td>
      <td>1.000000</td>
      <td>3.000000</td>
      <td>38.000000</td>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>31.000000</td>
    </tr>
    <tr>
      <th>max</th>
      <td>891.000000</td>
      <td>1.000000</td>
      <td>3.000000</td>
      <td>80.000000</td>
      <td>8.000000</td>
      <td>6.000000</td>
      <td>512.329200</td>
    </tr>
  </tbody>
</table>
</div>



- This task, Pandas takes 4 minutes and 41.0 seconds using NumPy backend, 2.6 seconds with PyArrow backend, and 8.4 seconds to describe, 
- while Polars with lazily, fully reading and describing needs 8.82 micro-seconds, 1.5 seconds and 8.8 seconds, respectively.
# ==> Polars wins Pandas in the reading using *parquet format 

## Check to read a large *feather file

### converting csv to feather.

```python
%time
parquet = (
    pl.scan_csv(f"{DATA_DIR}/titanic.csv")
    .sink_ipc(f"{DATA_DIR}/titanic.feather")
)
```

    CPU times: user 1 µs, sys: 0 ns, total: 1 µs
    Wall time: 3.34 µs

- Now, the parquet file size is 82kb.


```python
%time
feather = (
    pl.scan_ipc(f"{DATA_DIR}/titanic.feather")
)
```

    CPU times: user 1e+03 ns, sys: 1e+03 ns, total: 2 µs
    Wall time: 4.05 µs



```python
%time
pl_feather = (
    pl.read_ipc(f"{DATA_DIR}/titanic.feather")
)
```

    CPU times: user 1 µs, sys: 1 µs, total: 2 µs
    Wall time: 5.01 µs


    Could not memory_map compressed IPC file, defaulting to normal read. Toggle off 'memory_map' to silence this warning.



```python
pl_feather.describe()
```

<div>
<table border="1" class="dataframe"><thead><tr><th>statistic</th><th>PassengerId</th><th>Survived</th><th>Pclass</th><th>Name</th><th>Sex</th><th>Age</th><th>SibSp</th><th>Parch</th><th>Ticket</th><th>Fare</th><th>Cabin</th><th>Embarked</th></tr><tr><td>str</td><td>f64</td><td>f64</td><td>f64</td><td>str</td><td>str</td><td>f64</td><td>f64</td><td>f64</td><td>str</td><td>f64</td><td>str</td><td>str</td></tr></thead><tbody><tr><td>&quot;count&quot;</td><td>891.0</td><td>891.0</td><td>891.0</td><td>&quot;891&quot;</td><td>&quot;891&quot;</td><td>714.0</td><td>891.0</td><td>891.0</td><td>&quot;891&quot;</td><td>891.0</td><td>&quot;204&quot;</td><td>&quot;889&quot;</td></tr><tr><td>&quot;null_count&quot;</td><td>0.0</td><td>0.0</td><td>0.0</td><td>&quot;0&quot;</td><td>&quot;0&quot;</td><td>177.0</td><td>0.0</td><td>0.0</td><td>&quot;0&quot;</td><td>0.0</td><td>&quot;687&quot;</td><td>&quot;2&quot;</td></tr><tr><td>&quot;mean&quot;</td><td>446.0</td><td>0.383838</td><td>2.308642</td><td>null</td><td>null</td><td>29.699118</td><td>0.523008</td><td>0.381594</td><td>null</td><td>32.204208</td><td>null</td><td>null</td></tr><tr><td>&quot;std&quot;</td><td>257.353842</td><td>0.486592</td><td>0.836071</td><td>null</td><td>null</td><td>14.526497</td><td>1.102743</td><td>0.806057</td><td>null</td><td>49.693429</td><td>null</td><td>null</td></tr><tr><td>&quot;min&quot;</td><td>1.0</td><td>0.0</td><td>1.0</td><td>&quot;Abbing, Mr. Anthony&quot;</td><td>&quot;female&quot;</td><td>0.42</td><td>0.0</td><td>0.0</td><td>&quot;110152&quot;</td><td>0.0</td><td>&quot;A10&quot;</td><td>&quot;C&quot;</td></tr><tr><td>&quot;25%&quot;</td><td>224.0</td><td>0.0</td><td>2.0</td><td>null</td><td>null</td><td>20.0</td><td>0.0</td><td>0.0</td><td>null</td><td>7.925</td><td>null</td><td>null</td></tr><tr><td>&quot;50%&quot;</td><td>446.0</td><td>0.0</td><td>3.0</td><td>null</td><td>null</td><td>28.0</td><td>0.0</td><td>0.0</td><td>null</td><td>14.4542</td><td>null</td><td>null</td></tr><tr><td>&quot;75%&quot;</td><td>669.0</td><td>1.0</td><td>3.0</td><td>null</td><td>null</td><td>38.0</td><td>1.0</td><td>0.0</td><td>null</td><td>31.0</td><td>null</td><td>null</td></tr><tr><td>&quot;max&quot;</td><td>891.0</td><td>1.0</td><td>3.0</td><td>&quot;van Melkebeke, Mr. Philemon&quot;</td><td>&quot;male&quot;</td><td>80.0</td><td>8.0</td><td>6.0</td><td>&quot;WE/P 5735&quot;</td><td>512.3292</td><td>&quot;T&quot;</td><td>&quot;S&quot;</td></tr></tbody></table></div>




```python
d = pd_csv = (
    pd.read_csv(f"{DATA_DIR}/titanic.csv") #defaults as NumPy backed
    
)
d.to_feather("pandas_titanic.feather")
```


```python
%time
pd_feather = pd.read_feather(f"{DATA_DIR}/pandas_titanic.feather") # for Numpy backend
```

    CPU times: user 4 µs, sys: 2 µs, total: 6 µs
    Wall time: 13.4 µs



```python
%time
pd_feather = pd.read_feather(f"{DATA_DIR}/pandas_titanic.feather", dtype_backend='pyarrow')
```

    CPU times: user 7 µs, sys: 3 µs, total: 10 µs
    Wall time: 16.5 µs



```python
pd_feather.describe()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PassengerId</th>
      <th>Survived</th>
      <th>Pclass</th>
      <th>Age</th>
      <th>SibSp</th>
      <th>Parch</th>
      <th>Fare</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>count</th>
      <td>891.000000</td>
      <td>891.000000</td>
      <td>891.000000</td>
      <td>714.000000</td>
      <td>891.000000</td>
      <td>891.000000</td>
      <td>891.000000</td>
    </tr>
    <tr>
      <th>mean</th>
      <td>446.000000</td>
      <td>0.383838</td>
      <td>2.308642</td>
      <td>29.699118</td>
      <td>0.523008</td>
      <td>0.381594</td>
      <td>32.204208</td>
    </tr>
    <tr>
      <th>std</th>
      <td>257.353842</td>
      <td>0.486592</td>
      <td>0.836071</td>
      <td>14.526497</td>
      <td>1.102743</td>
      <td>0.806057</td>
      <td>49.693429</td>
    </tr>
    <tr>
      <th>min</th>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>1.000000</td>
      <td>0.420000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
    </tr>
    <tr>
      <th>25%</th>
      <td>223.500000</td>
      <td>0.000000</td>
      <td>2.000000</td>
      <td>20.125000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>7.910400</td>
    </tr>
    <tr>
      <th>50%</th>
      <td>446.000000</td>
      <td>0.000000</td>
      <td>3.000000</td>
      <td>28.000000</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>14.454200</td>
    </tr>
    <tr>
      <th>75%</th>
      <td>668.500000</td>
      <td>1.000000</td>
      <td>3.000000</td>
      <td>38.000000</td>
      <td>1.000000</td>
      <td>0.000000</td>
      <td>31.000000</td>
    </tr>
    <tr>
      <th>max</th>
      <td>891.000000</td>
      <td>1.000000</td>
      <td>3.000000</td>
      <td>80.000000</td>
      <td>8.000000</td>
      <td>6.000000</td>
      <td>512.329200</td>
    </tr>
  </tbody>
</table>
</div>



- This task, Pandas takes 13.4 micro seconds using NumPy backend, 16.5 micro seconds using PyArrow backend, and 11.7 micro seconds to describe the file, 
- while Polars with lazily, fully reading and describing needs 4 micro-seconds, 5 micro-seconds and 11.9 micro seconds, respectively.
- ==> Polars wins Pandas in the reading task using *feather format

- In summary, Polars wins all experiments in reading and describing a large data using *csv, *parquet, and *feather formats.
- Pandas with PyArrow backend to read *parquet and *feather is much faster than that of NumPy backend, while Pandas using either PyArrow or NumPy engines does not improve performance to read a large *csv file
- Congratulations Polars (written in Rust and Arrow2)!!!!

### References
- https://docs.pola.rs/user-guide/io/multiple/
- https://blog.jetbrains.com/pycharm/2024/07/polars-vs-pandas/