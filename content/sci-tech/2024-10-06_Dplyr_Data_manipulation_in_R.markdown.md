+++
title = "Dplyr Data manipulation in R"
date = 2024-10-06
draft = false
tags = ["R"]
categories = []
+++


## Dplyr Data manipulation in R
Data manipulation in R using the package dplyr is a powerful and flexible way to transform, summarize, and analyze data frames. With its intuitive syntax and array of functions, dplyr makes it easy for users to modify their datasets according to specific criteria or requirements.Here's an overview of some common data manipulation techniques in R with dplyr.Here we use hflights data set for Analysis.

```R
# Load the dplyr package
library(dplyr)

# Load the hflights package
library(hflights)
```

    
    Attaching package: ‘dplyr’
    
    
    The following objects are masked from ‘package:stats’:
    
        filter, lag
    
    
    The following objects are masked from ‘package:base’:
    
        intersect, setdiff, setequal, union
    
    

Summary of the data

```R
# Call both head() and summary() on hflights
head(hflights)
```


<table class="dataframe">
<caption>A data.frame: 6 × 21</caption>
<thead>
	<tr><th></th><th scope=col>Year</th><th scope=col>Month</th><th scope=col>DayofMonth</th><th scope=col>DayOfWeek</th><th scope=col>DepTime</th><th scope=col>ArrTime</th><th scope=col>UniqueCarrier</th><th scope=col>FlightNum</th><th scope=col>TailNum</th><th scope=col>ActualElapsedTime</th><th scope=col>⋯</th><th scope=col>ArrDelay</th><th scope=col>DepDelay</th><th scope=col>Origin</th><th scope=col>Dest</th><th scope=col>Distance</th><th scope=col>TaxiIn</th><th scope=col>TaxiOut</th><th scope=col>Cancelled</th><th scope=col>CancellationCode</th><th scope=col>Diverted</th></tr>
	<tr><th></th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>⋯</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>5424</th><td>2011</td><td>1</td><td>1</td><td>6</td><td>1400</td><td>1500</td><td>AA</td><td>428</td><td>N576AA</td><td>60</td><td>⋯</td><td>-10</td><td> 0</td><td>IAH</td><td>DFW</td><td>224</td><td>7</td><td>13</td><td>0</td><td></td><td>0</td></tr>
	<tr><th scope=row>5425</th><td>2011</td><td>1</td><td>2</td><td>7</td><td>1401</td><td>1501</td><td>AA</td><td>428</td><td>N557AA</td><td>60</td><td>⋯</td><td> -9</td><td> 1</td><td>IAH</td><td>DFW</td><td>224</td><td>6</td><td> 9</td><td>0</td><td></td><td>0</td></tr>
	<tr><th scope=row>5426</th><td>2011</td><td>1</td><td>3</td><td>1</td><td>1352</td><td>1502</td><td>AA</td><td>428</td><td>N541AA</td><td>70</td><td>⋯</td><td> -8</td><td>-8</td><td>IAH</td><td>DFW</td><td>224</td><td>5</td><td>17</td><td>0</td><td></td><td>0</td></tr>
	<tr><th scope=row>5427</th><td>2011</td><td>1</td><td>4</td><td>2</td><td>1403</td><td>1513</td><td>AA</td><td>428</td><td>N403AA</td><td>70</td><td>⋯</td><td>  3</td><td> 3</td><td>IAH</td><td>DFW</td><td>224</td><td>9</td><td>22</td><td>0</td><td></td><td>0</td></tr>
	<tr><th scope=row>5428</th><td>2011</td><td>1</td><td>5</td><td>3</td><td>1405</td><td>1507</td><td>AA</td><td>428</td><td>N492AA</td><td>62</td><td>⋯</td><td> -3</td><td> 5</td><td>IAH</td><td>DFW</td><td>224</td><td>9</td><td> 9</td><td>0</td><td></td><td>0</td></tr>
	<tr><th scope=row>5429</th><td>2011</td><td>1</td><td>6</td><td>4</td><td>1359</td><td>1503</td><td>AA</td><td>428</td><td>N262AA</td><td>64</td><td>⋯</td><td> -7</td><td>-1</td><td>IAH</td><td>DFW</td><td>224</td><td>6</td><td>13</td><td>0</td><td></td><td>0</td></tr>
</tbody>
</table>




```R
glimpse(hflights)
```

    Rows: 227,496
    Columns: 21
    $ Year              [3m[90m<int>[39m[23m 2011, 2011, 2011, 2011, 2011, 2011, 2011, 2011, 2011…
    $ Month             [3m[90m<int>[39m[23m 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1…
    $ DayofMonth        [3m[90m<int>[39m[23m 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 1…
    $ DayOfWeek         [3m[90m<int>[39m[23m 6, 7, 1, 2, 3, 4, 5, 6, 7, 1, 2, 3, 4, 5, 6, 7, 1, 2…
    $ DepTime           [3m[90m<int>[39m[23m 1400, 1401, 1352, 1403, 1405, 1359, 1359, 1355, 1443…
    $ ArrTime           [3m[90m<int>[39m[23m 1500, 1501, 1502, 1513, 1507, 1503, 1509, 1454, 1554…
    $ UniqueCarrier     [3m[90m<chr>[39m[23m "AA", "AA", "AA", "AA", "AA", "AA", "AA", "AA", "AA"…
    $ FlightNum         [3m[90m<int>[39m[23m 428, 428, 428, 428, 428, 428, 428, 428, 428, 428, 42…
    $ TailNum           [3m[90m<chr>[39m[23m "N576AA", "N557AA", "N541AA", "N403AA", "N492AA", "N…
    $ ActualElapsedTime [3m[90m<int>[39m[23m 60, 60, 70, 70, 62, 64, 70, 59, 71, 70, 70, 56, 63, …
    $ AirTime           [3m[90m<int>[39m[23m 40, 45, 48, 39, 44, 45, 43, 40, 41, 45, 42, 41, 44, …
    $ ArrDelay          [3m[90m<int>[39m[23m -10, -9, -8, 3, -3, -7, -1, -16, 44, 43, 29, 5, -9, …
    $ DepDelay          [3m[90m<int>[39m[23m 0, 1, -8, 3, 5, -1, -1, -5, 43, 43, 29, 19, -2, -3, …
    $ Origin            [3m[90m<chr>[39m[23m "IAH", "IAH", "IAH", "IAH", "IAH", "IAH", "IAH", "IA…
    $ Dest              [3m[90m<chr>[39m[23m "DFW", "DFW", "DFW", "DFW", "DFW", "DFW", "DFW", "DF…
    $ Distance          [3m[90m<int>[39m[23m 224, 224, 224, 224, 224, 224, 224, 224, 224, 224, 22…
    $ TaxiIn            [3m[90m<int>[39m[23m 7, 6, 5, 9, 9, 6, 12, 7, 8, 6, 8, 4, 6, 5, 6, 12, 8,…
    $ TaxiOut           [3m[90m<int>[39m[23m 13, 9, 17, 22, 9, 13, 15, 12, 22, 19, 20, 11, 13, 15…
    $ Cancelled         [3m[90m<int>[39m[23m 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0…
    $ CancellationCode  [3m[90m<chr>[39m[23m "", "", "", "", "", "", "", "", "", "", "", "", "", …
    $ Diverted          [3m[90m<int>[39m[23m 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0…



```R

```
Here's an overview of some common data manipulation techniques in R with dplyr:

Filtering data: Using the filter() function, we can select specific rows based on specified conditions like values within certain ranges, equalities, differences between variables, or missing values. For example, selecting all rows where 'DepTime' column has values greater than 1400 would look like this:

```R
filtered_data = filter(hflights, DepTime > 1400)
```


```R
head(filtered_data)
```


<table class="dataframe">
<caption>A data.frame: 6 × 21</caption>
<thead>
	<tr><th></th><th scope=col>Year</th><th scope=col>Month</th><th scope=col>DayofMonth</th><th scope=col>DayOfWeek</th><th scope=col>DepTime</th><th scope=col>ArrTime</th><th scope=col>UniqueCarrier</th><th scope=col>FlightNum</th><th scope=col>TailNum</th><th scope=col>ActualElapsedTime</th><th scope=col>⋯</th><th scope=col>ArrDelay</th><th scope=col>DepDelay</th><th scope=col>Origin</th><th scope=col>Dest</th><th scope=col>Distance</th><th scope=col>TaxiIn</th><th scope=col>TaxiOut</th><th scope=col>Cancelled</th><th scope=col>CancellationCode</th><th scope=col>Diverted</th></tr>
	<tr><th></th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>⋯</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>1</th><td>2011</td><td>1</td><td> 2</td><td>7</td><td>1401</td><td>1501</td><td>AA</td><td>428</td><td>N557AA</td><td>60</td><td>⋯</td><td>-9</td><td> 1</td><td>IAH</td><td>DFW</td><td>224</td><td>6</td><td> 9</td><td>0</td><td></td><td>0</td></tr>
	<tr><th scope=row>2</th><td>2011</td><td>1</td><td> 4</td><td>2</td><td>1403</td><td>1513</td><td>AA</td><td>428</td><td>N403AA</td><td>70</td><td>⋯</td><td> 3</td><td> 3</td><td>IAH</td><td>DFW</td><td>224</td><td>9</td><td>22</td><td>0</td><td></td><td>0</td></tr>
	<tr><th scope=row>3</th><td>2011</td><td>1</td><td> 5</td><td>3</td><td>1405</td><td>1507</td><td>AA</td><td>428</td><td>N492AA</td><td>62</td><td>⋯</td><td>-3</td><td> 5</td><td>IAH</td><td>DFW</td><td>224</td><td>9</td><td> 9</td><td>0</td><td></td><td>0</td></tr>
	<tr><th scope=row>4</th><td>2011</td><td>1</td><td> 9</td><td>7</td><td>1443</td><td>1554</td><td>AA</td><td>428</td><td>N476AA</td><td>71</td><td>⋯</td><td>44</td><td>43</td><td>IAH</td><td>DFW</td><td>224</td><td>8</td><td>22</td><td>0</td><td></td><td>0</td></tr>
	<tr><th scope=row>5</th><td>2011</td><td>1</td><td>10</td><td>1</td><td>1443</td><td>1553</td><td>AA</td><td>428</td><td>N504AA</td><td>70</td><td>⋯</td><td>43</td><td>43</td><td>IAH</td><td>DFW</td><td>224</td><td>6</td><td>19</td><td>0</td><td></td><td>0</td></tr>
	<tr><th scope=row>6</th><td>2011</td><td>1</td><td>11</td><td>2</td><td>1429</td><td>1539</td><td>AA</td><td>428</td><td>N565AA</td><td>70</td><td>⋯</td><td>29</td><td>29</td><td>IAH</td><td>DFW</td><td>224</td><td>8</td><td>20</td><td>0</td><td></td><td>0</td></tr>
</tbody>
</table>




```R
nrow(filtered_data)
```


115907

Summarizing data: Functions such as summarise() provide quick ways to calculate statistics across one or multiple columns at once.

```R
mean_values = summarise(filtered_data, mean(Distance), median(DepTime))
```


```R
mean_values
```


<table class="dataframe">
<caption>A data.frame: 1 × 2</caption>
<thead>
	<tr><th scope=col>mean(Distance)</th><th scope=col>median(DepTime)</th></tr>
	<tr><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;int&gt;</th></tr>
</thead>
<tbody>
	<tr><td>762.3938</td><td>1756</td></tr>
</tbody>
</table>




```R
grouped_data = group_by(hflights, UniqueCarrier) %>% summarise(avg_distance = mean(Distance))
```


```R
head(grouped_data)
```


<table class="dataframe">
<caption>A tibble: 6 × 2</caption>
<thead>
	<tr><th scope=col>UniqueCarrier</th><th scope=col>avg_distance</th></tr>
	<tr><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>AA</td><td> 483.8212</td></tr>
	<tr><td>AS</td><td>1874.0000</td></tr>
	<tr><td>B6</td><td>1428.0000</td></tr>
	<tr><td>CO</td><td>1098.0549</td></tr>
	<tr><td>DL</td><td> 723.2832</td></tr>
	<tr><td>EV</td><td> 775.6815</td></tr>
</tbody>
</table>



### Renaming variables: 
Assigning new variable names using rename() function:


```R
grouped_data <- tbl_df(grouped_data)
colnames(grouped_data)
renamed_columns = rename(grouped_data, Average.Distance = avg_distance)
```

    Warning message:
    “[1m[22m`tbl_df()` was deprecated in dplyr 1.0.0.
    [36mℹ[39m Please use `tibble::as_tibble()` instead.”



<style>
.list-inline {list-style: none; margin:0; padding: 0}
.list-inline>li {display: inline-block}
.list-inline>li:not(:last-child)::after {content: "\00b7"; padding: 0 .5ex}
</style>
<ol class=list-inline><li>'UniqueCarrier'</li><li>'avg_distance'</li></ol>




```R
colnames(renamed_columns)
```


<style>
.list-inline {list-style: none; margin:0; padding: 0}
.list-inline>li {display: inline-block}
.list-inline>li:not(:last-child)::after {content: "\00b7"; padding: 0 .5ex}
</style>
<ol class=list-inline><li>'UniqueCarrier'</li><li>'Average.Distance'</li></ol>


Sorting data: We can sort data either ascendingly (using arrange() function) or descendingly (using desc() function):
sorted_data = arrange(hflights, Distance)
sorted_data = arrange(hflights, desc(Distance))


```R
head(sorted_data)
```


<table class="dataframe">
<caption>A data.frame: 6 × 21</caption>
<thead>
	<tr><th></th><th scope=col>Year</th><th scope=col>Month</th><th scope=col>DayofMonth</th><th scope=col>DayOfWeek</th><th scope=col>DepTime</th><th scope=col>ArrTime</th><th scope=col>UniqueCarrier</th><th scope=col>FlightNum</th><th scope=col>TailNum</th><th scope=col>ActualElapsedTime</th><th scope=col>⋯</th><th scope=col>ArrDelay</th><th scope=col>DepDelay</th><th scope=col>Origin</th><th scope=col>Dest</th><th scope=col>Distance</th><th scope=col>TaxiIn</th><th scope=col>TaxiOut</th><th scope=col>Cancelled</th><th scope=col>CancellationCode</th><th scope=col>Diverted</th></tr>
	<tr><th></th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>⋯</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>1</th><td>2011</td><td>1</td><td>31</td><td>1</td><td> 924</td><td>1413</td><td>CO</td><td>1</td><td>N69063</td><td>529</td><td>⋯</td><td> 23</td><td> -1</td><td>IAH</td><td>HNL</td><td>3904</td><td> 6</td><td>31</td><td>0</td><td></td><td>0</td></tr>
	<tr><th scope=row>2</th><td>2011</td><td>1</td><td>30</td><td>7</td><td> 925</td><td>1410</td><td>CO</td><td>1</td><td>N76064</td><td>525</td><td>⋯</td><td> 20</td><td>  0</td><td>IAH</td><td>HNL</td><td>3904</td><td>13</td><td>19</td><td>0</td><td></td><td>0</td></tr>
	<tr><th scope=row>3</th><td>2011</td><td>1</td><td>29</td><td>6</td><td>1045</td><td>1445</td><td>CO</td><td>1</td><td>N69063</td><td>480</td><td>⋯</td><td> 55</td><td> 80</td><td>IAH</td><td>HNL</td><td>3904</td><td> 4</td><td>17</td><td>0</td><td></td><td>0</td></tr>
	<tr><th scope=row>4</th><td>2011</td><td>1</td><td>28</td><td>5</td><td>1516</td><td>1916</td><td>CO</td><td>1</td><td>N77066</td><td>480</td><td>⋯</td><td>326</td><td>351</td><td>IAH</td><td>HNL</td><td>3904</td><td> 7</td><td>10</td><td>0</td><td></td><td>0</td></tr>
	<tr><th scope=row>5</th><td>2011</td><td>1</td><td>27</td><td>4</td><td> 950</td><td>1344</td><td>CO</td><td>1</td><td>N76055</td><td>474</td><td>⋯</td><td> -6</td><td> 25</td><td>IAH</td><td>HNL</td><td>3904</td><td> 4</td><td>15</td><td>0</td><td></td><td>0</td></tr>
	<tr><th scope=row>6</th><td>2011</td><td>1</td><td>26</td><td>3</td><td> 944</td><td>1350</td><td>CO</td><td>1</td><td>N76065</td><td>486</td><td>⋯</td><td>  0</td><td> 19</td><td>IAH</td><td>HNL</td><td>3904</td><td> 5</td><td>10</td><td>0</td><td></td><td>0</td></tr>
</tbody>
</table>



### Converting to a tibble


```R
# convert to a tibble
hflights<- tbl_df(hflights)
str(hflights)
```

    Warning message:
    “[1m[22m`tbl_df()` was deprecated in dplyr 1.0.0.
    [36mℹ[39m Please use `tibble::as_tibble()` instead.”


    tibble [227,496 × 21] (S3: tbl_df/tbl/data.frame)
     $ Year             : int [1:227496] 2011 2011 2011 2011 2011 2011 2011 2011 2011 2011 ...
     $ Month            : int [1:227496] 1 1 1 1 1 1 1 1 1 1 ...
     $ DayofMonth       : int [1:227496] 1 2 3 4 5 6 7 8 9 10 ...
     $ DayOfWeek        : int [1:227496] 6 7 1 2 3 4 5 6 7 1 ...
     $ DepTime          : int [1:227496] 1400 1401 1352 1403 1405 1359 1359 1355 1443 1443 ...
     $ ArrTime          : int [1:227496] 1500 1501 1502 1513 1507 1503 1509 1454 1554 1553 ...
     $ UniqueCarrier    : chr [1:227496] "AA" "AA" "AA" "AA" ...
     $ FlightNum        : int [1:227496] 428 428 428 428 428 428 428 428 428 428 ...
     $ TailNum          : chr [1:227496] "N576AA" "N557AA" "N541AA" "N403AA" ...
     $ ActualElapsedTime: int [1:227496] 60 60 70 70 62 64 70 59 71 70 ...
     $ AirTime          : int [1:227496] 40 45 48 39 44 45 43 40 41 45 ...
     $ ArrDelay         : int [1:227496] -10 -9 -8 3 -3 -7 -1 -16 44 43 ...
     $ DepDelay         : int [1:227496] 0 1 -8 3 5 -1 -1 -5 43 43 ...
     $ Origin           : chr [1:227496] "IAH" "IAH" "IAH" "IAH" ...
     $ Dest             : chr [1:227496] "DFW" "DFW" "DFW" "DFW" ...
     $ Distance         : int [1:227496] 224 224 224 224 224 224 224 224 224 224 ...
     $ TaxiIn           : int [1:227496] 7 6 5 9 9 6 12 7 8 6 ...
     $ TaxiOut          : int [1:227496] 13 9 17 22 9 13 15 12 22 19 ...
     $ Cancelled        : int [1:227496] 0 0 0 0 0 0 0 0 0 0 ...
     $ CancellationCode : chr [1:227496] "" "" "" "" ...
     $ Diverted         : int [1:227496] 0 0 0 0 0 0 0 0 0 0 ...



```R
# Create the carriers, containing only the UniqueCarrier variable of hflights
carriers <- hflights$UniqueCarrier
```

### Rename or Translate column using lookup table.


```R
abc <- c("AA" = "American", "AS" = "Alaska", "B6" = "JetBlue", "CO" = "Continental", 
         "DL" = "Delta", "OO" = "SkyWest", "UA" = "United", "US" = "US_Airways", 
         "WN" = "Southwest", "EV" = "Atlantic_Southeast", "F9" = "Frontier", 
         "FL" = "AirTran", "MQ" = "American_Eagle", "XE" = "ExpressJet", "YV" = "Mesa")
```


```R
hflights %>% select(UniqueCarrier) %>% head()
```


<table class="dataframe">
<caption>A tibble: 6 × 1</caption>
<thead>
	<tr><th scope=col>UniqueCarrier</th></tr>
	<tr><th scope=col>&lt;chr&gt;</th></tr>
</thead>
<tbody>
	<tr><td>AA</td></tr>
	<tr><td>AA</td></tr>
	<tr><td>AA</td></tr>
	<tr><td>AA</td></tr>
	<tr><td>AA</td></tr>
	<tr><td>AA</td></tr>
</tbody>
</table>




```R
# Use abc to translate the UniqueCarrier column of hflights
hflights$UniqueCarrier <- abc[hflights$UniqueCarrier]
```


```R
hflights %>% select(UniqueCarrier) %>% head()
```


<table class="dataframe">
<caption>A tibble: 6 × 1</caption>
<thead>
	<tr><th scope=col>UniqueCarrier</th></tr>
	<tr><th scope=col>&lt;chr&gt;</th></tr>
</thead>
<tbody>
	<tr><td>American</td></tr>
	<tr><td>American</td></tr>
	<tr><td>American</td></tr>
	<tr><td>American</td></tr>
	<tr><td>American</td></tr>
	<tr><td>American</td></tr>
</tbody>
</table>




```R
d1 <- sorted_data %>% mutate(X2 = recode(UniqueCarrier, !!!abc))
```


```R
h2 <- d1 %>%
  select('UniqueCarrier', X2)
```


```R
head(h2)
```


<table class="dataframe">
<caption>A data.frame: 6 × 2</caption>
<thead>
	<tr><th></th><th scope=col>UniqueCarrier</th><th scope=col>X2</th></tr>
	<tr><th></th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>1</th><td>CO</td><td>Continental</td></tr>
	<tr><th scope=row>2</th><td>CO</td><td>Continental</td></tr>
	<tr><th scope=row>3</th><td>CO</td><td>Continental</td></tr>
	<tr><th scope=row>4</th><td>CO</td><td>Continental</td></tr>
	<tr><th scope=row>5</th><td>CO</td><td>Continental</td></tr>
	<tr><th scope=row>6</th><td>CO</td><td>Continental</td></tr>
</tbody>
</table>




```R

```

### Missing value Analysis


```R
# one column with missing label
table(hflights$CancellationCode)
```


    
                A      B      C      D 
    224523   1202   1652    118      1 



```R
hflights <- hflights %>%
  mutate(
    CancellationCode = ifelse(CancellationCode == "", "E", CancellationCode)
  )
```


```R
table(hflights$CancellationCode)
```


    
         A      B      C      D      E 
      1202   1652    118      1 224523 



```R
head(hflights$Code)
```

    Warning message:
    “Unknown or uninitialised column: `Code`.”



    NULL



```R
# Build the lookup table: lut
lut <- c("A" = "carrier",
         "B" = "weather",
         "C" = "FFA",
         "D" = "security",
         "E" = "not cancelled")
```


```R
# Use the lookup table to create a vector of code labels. Assign the vector to the CancellationCode column of hflights
hflights$Code <- lut[hflights$CancellationCode]
```


```R
head(hflights$Code)
```


<style>
.dl-inline {width: auto; margin:0; padding: 0}
.dl-inline>dt, .dl-inline>dd {float: none; width: auto; display: inline-block}
.dl-inline>dt::after {content: ":\0020"; padding-right: .5ex}
.dl-inline>dt:not(:first-of-type) {padding-left: .5ex}
</style><dl class=dl-inline><dt>E</dt><dd>'not cancelled'</dd><dt>E</dt><dd>'not cancelled'</dd><dt>E</dt><dd>'not cancelled'</dd><dt>E</dt><dd>'not cancelled'</dd><dt>E</dt><dd>'not cancelled'</dd><dt>E</dt><dd>'not cancelled'</dd></dl>


### References

- https://dplyr.tidyverse.org/articles/dplyr.html