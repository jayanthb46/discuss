+++
title = "Duckdb in R"
date = 2024-10-13
draft = false
tags = ["R"]
categories = []
+++

## Duckdb in R.
## 13th october, 2024

### Introduction

DuckDB is a database system written in C++ programming language. It provides efficient storage and manipulation of data stored within it through its SQL-like interface. Users can connect to databases from within their R scripts and perform various operations such as inserting, updating, deleting, and querying records. This makes working with large datasets easier and more convenient for users who prefer analyzing data within R rather than switching between different software environments like spreadsheets or database management tools. Additionally, since DuckDB operates entirely within R, there's no need to worry about compatibility issues arising due to differences in platform requirements or versions. Overall, incorporating DuckDB into R opens up new possibilities for handling and visualizing data effectively while staying within one's preferred environment.


### Loading packages

```R
library(duckdb)
library(dplyr)
data("flights", package = "nycflights13")
```

    Loading required package: DBI
    
    
    Attaching package: ‘dplyr’
    
    
    The following objects are masked from ‘package:stats’:
    
        filter, lag
    
    
    The following objects are masked from ‘package:base’:
    
        intersect, setdiff, setequal, union
    
    


### Loading data to Duckdb

```R
head(flights)
```


<table class="dataframe">
<caption>A tibble: 6 × 19</caption>
<thead>
	<tr><th >year</th><th >month</th><th >day</th><th >dep_time</th><th >sched_dep_time</th><th >dep_delay</th><th >arr_time</th><th >sched_arr_time</th><th >arr_delay</th><th >carrier</th><th >flight</th><th >tailnum</th><th >origin</th><th >dest</th><th >air_time</th><th >distance</th><th >hour</th><th >minute</th><th >time_hour</th></tr>
	
</thead>
<tbody>
	<tr><td>2013</td><td>1</td><td>1</td><td>517</td><td>515</td><td> 2</td><td> 830</td><td> 819</td><td> 11</td><td>UA</td><td>1545</td><td>N14228</td><td>EWR</td><td>IAH</td><td>227</td><td>1400</td><td>5</td><td>15</td><td>2013-01-01 05:00:00</td></tr>
	<tr><td>2013</td><td>1</td><td>1</td><td>533</td><td>529</td><td> 4</td><td> 850</td><td> 830</td><td> 20</td><td>UA</td><td>1714</td><td>N24211</td><td>LGA</td><td>IAH</td><td>227</td><td>1416</td><td>5</td><td>29</td><td>2013-01-01 05:00:00</td></tr>
	<tr><td>2013</td><td>1</td><td>1</td><td>542</td><td>540</td><td> 2</td><td> 923</td><td> 850</td><td> 33</td><td>AA</td><td>1141</td><td>N619AA</td><td>JFK</td><td>MIA</td><td>160</td><td>1089</td><td>5</td><td>40</td><td>2013-01-01 05:00:00</td></tr>
	<tr><td>2013</td><td>1</td><td>1</td><td>544</td><td>545</td><td>-1</td><td>1004</td><td>1022</td><td>-18</td><td>B6</td><td> 725</td><td>N804JB</td><td>JFK</td><td>BQN</td><td>183</td><td>1576</td><td>5</td><td>45</td><td>2013-01-01 05:00:00</td></tr>
	<tr><td>2013</td><td>1</td><td>1</td><td>554</td><td>600</td><td>-6</td><td> 812</td><td> 837</td><td>-25</td><td>DL</td><td> 461</td><td>N668DN</td><td>LGA</td><td>ATL</td><td>116</td><td> 762</td><td>6</td><td> 0</td><td>2013-01-01 06:00:00</td></tr>
	<tr><td>2013</td><td>1</td><td>1</td><td>554</td><td>558</td><td>-4</td><td> 740</td><td> 728</td><td> 12</td><td>UA</td><td>1696</td><td>N39463</td><td>EWR</td><td>ORD</td><td>150</td><td> 719</td><td>5</td><td>58</td><td>2013-01-01 05:00:00</td></tr>
</tbody>
</table>




```R
con <- dbConnect(duckdb())
```


```R
duckdb_register(con, "flights", flights)
```

### Query Duckdb in R.


```R
dbGetQuery(con,
  "SELECT origin, COUNT(*) AS n FROM flights GROUP BY origin")
```


<table class="dataframe">
<caption>A data.frame: 3 × 2</caption>
<thead>
	<tr><th >origin</th><th >n</th></tr>
	<tr><th >&lt;chr&gt;</th><th >&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>JFK</td><td>111279</td></tr>
	<tr><td>EWR</td><td>120835</td></tr>
	<tr><td>LGA</td><td>104662</td></tr>
</tbody>
</table>





### SQL in R.


```R
dbGetQuery(con,
"SELECT origin, dest
FROM (SELECT origin, dest, n
  FROM (SELECT origin, dest, n, RANK() OVER (
      PARTITION BY origin ORDER BY n DESC) AS h
    FROM (SELECT origin, dest, COUNT(*) AS n
      FROM flights
      GROUP BY origin, dest
    ) AS curly
  ) AS moe
  WHERE (h <= 3)
) AS shemp ORDER BY origin;")


```


<table class="dataframe">
<caption>A data.frame: 9 × 2</caption>
<thead>
	<tr><th >origin</th><th >dest</th></tr>
</thead>
<tbody>
	<tr><td>EWR</td><td>ORD</td></tr>
	<tr><td>EWR</td><td>BOS</td></tr>
	<tr><td>EWR</td><td>SFO</td></tr>
	<tr><td>JFK</td><td>LAX</td></tr>
	<tr><td>JFK</td><td>SFO</td></tr>
	<tr><td>JFK</td><td>BOS</td></tr>
	<tr><td>LGA</td><td>ATL</td></tr>
	<tr><td>LGA</td><td>ORD</td></tr>
	<tr><td>LGA</td><td>CLT</td></tr>
</tbody>
</table>

### pipe operator on Duckdb


```R
flights %>%
group_by(origin) %>%
count(dest, sort = TRUE) %>%
slice_head(n = 3) %>%
select(origin, dest)
```


<table class="dataframe">
<caption>A grouped_df: 9 × 2</caption>
<thead>
	<tr><th >origin</th><th >dest</th></tr>
</thead>
<tbody>
	<tr><td>EWR</td><td>ORD</td></tr>
	<tr><td>EWR</td><td>BOS</td></tr>
	<tr><td>EWR</td><td>SFO</td></tr>
	<tr><td>JFK</td><td>LAX</td></tr>
	<tr><td>JFK</td><td>SFO</td></tr>
	<tr><td>JFK</td><td>BOS</td></tr>
	<tr><td>LGA</td><td>ATL</td></tr>
	<tr><td>LGA</td><td>ORD</td></tr>
	<tr><td>LGA</td><td>CLT</td></tr>
</tbody>
</table>



```R
tbl(con, "flights") %>%
group_by(origin) %>%
count(dest, sort = TRUE, name = "N") %>%
slice_max(order_by = N, n = 3) %>%
select(origin, dest)
```

   [Source:   SQL [9 x 2]
   [Database: DuckDB v1.1.0 [ab@Linux 6.8.0-45-generic:R 4.3.3/:memory:]

    LGA    ATL  
    LGA    ORD  
    LGA    CLT  
    JFK    LAX  
    JFK    SFO  
    JFK    BOS  
    EWR    ORD  
    EWR    BOS  
    EWR    SFO  


### References
- https://colab.research.google.com/drive/1eOA2FYHqEfZWLYssbUxdIpSL3PFxWVjk?usp=sharing#scrollTo=cf49_HQa2o8h
- https://duckdb.org/docs/