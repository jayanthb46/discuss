+++
title = "Tidy Text in R"
date = 2024-09-08
draft = false
tags = ["R"]
categories = []
+++

## TidyText package in R.

The tidy text format allows you to organize structured data efficiently and easily analyze textual information without losing meaningful content. 
Using R's built-in functionality and packages like 'tidytext', one can transform raw text data into a more manageable and standardized form suitable 
for statistical analysis or visualization purposes.



```R
library(tidytext)
library(magrittr)
library(tidyverse)
```


```R
# Read the text file
df<- scan("/home/ab/Desktop/text-analysis-master/data/text_data_for_analysis.txt", what = "character", sep = "\n")
```


```R
df
```


<style>
.list-inline {list-style: none; margin:0; padding: 0}
.list-inline>li {display: inline-block}
.list-inline>li:not(:last-child)::after {content: "\00b7"; padding: 0 .5ex}
</style>
<ol class=list-inline><li>'As a term, data analytics predominantly refers to an assortment of applications, from basic business'</li><li>'intelligence (BI), reporting and online analytical processing (OLAP) to various forms of advanced'</li><li>'analytics. In that sense, it\'s similar in nature to business analytics, another umbrella term for'</li><li>'approaches to analyzing data -- with the difference that the latter is oriented to business uses, while'</li><li>'data analytics has a broader focus. The expansive view of the term isn\'t universal, though: In some'</li><li>'cases, people use data analytics specifically to mean advanced analytics, treating BI as a separate'</li><li>'category. Data analytics initiatives can help businesses increase revenues, improve operational'</li><li>'efficiency, optimize marketing campaigns and customer service efforts, respond more quickly to'</li><li>'emerging market trends and gain a competitive edge over rivals -- all with the ultimate goal of'</li><li>'boosting business performance. Depending on the particular application, the data that\'s analyzed'</li><li>'can consist of either historical records or new information that has been processed for real-time'</li><li>'analytics uses. In addition, it can come from a mix of internal systems and external data sources. At'</li><li>'a high level, data analytics methodologies include exploratory data analysis (EDA), which aims to find'</li><li>'patterns and relationships in data, and confirmatory data analysis (CDA), which applies statistical'</li><li>'techniques to determine whether hypotheses about a data set are true or false. EDA is often'</li><li>'compared to detective work, while CDA is akin to the work of a judge or jury during a court trial -- a'</li><li>'distinction first drawn by statistician John W. Tukey in his 1977 book Exploratory Data Analysis. Data'</li><li>'analytics can also be separated into quantitative data analysis and qualitative data analysis. The'</li><li>'former involves analysis of numerical data with quantifiable variables that can be compared or'</li><li>'measured statistically. The qualitative approach is more interpretive -- it focuses on understanding'</li><li>'the content of non-numerical data like text, images, audio and video, including common phrases,'</li><li>'themes and points of view.'</li></ol>




```R
# basic eda
str(df)
typeof(df) # character vector
```

     chr [1:22] "As a term, data analytics predominantly refers to an assortment of applications, from basic business" ...



'character'



```R
# convert all words to lowercase
df<- tolower(df)
```


```R
## extract all words only
df<- strsplit(df, "\\W") # where \\W is a regex to match any non-word character.  
head(df)
```


<ol>
	<li><style>
.list-inline {list-style: none; margin:0; padding: 0}
.list-inline>li {display: inline-block}
.list-inline>li:not(:last-child)::after {content: "\00b7"; padding: 0 .5ex}
</style>
<ol class=list-inline><li>'as'</li><li>'a'</li><li>'term'</li><li>''</li><li>'data'</li><li>'analytics'</li><li>'predominantly'</li><li>'refers'</li><li>'to'</li><li>'an'</li><li>'assortment'</li><li>'of'</li><li>'applications'</li><li>''</li><li>'from'</li><li>'basic'</li><li>'business'</li></ol>
</li>
	<li><style>
.list-inline {list-style: none; margin:0; padding: 0}
.list-inline>li {display: inline-block}
.list-inline>li:not(:last-child)::after {content: "\00b7"; padding: 0 .5ex}
</style>
<ol class=list-inline><li>'intelligence'</li><li>''</li><li>'bi'</li><li>''</li><li>''</li><li>'reporting'</li><li>'and'</li><li>'online'</li><li>'analytical'</li><li>'processing'</li><li>''</li><li>'olap'</li><li>''</li><li>'to'</li><li>'various'</li><li>'forms'</li><li>'of'</li><li>'advanced'</li></ol>
</li>
	<li><style>
.list-inline {list-style: none; margin:0; padding: 0}
.list-inline>li {display: inline-block}
.list-inline>li:not(:last-child)::after {content: "\00b7"; padding: 0 .5ex}
</style>
<ol class=list-inline><li>'analytics'</li><li>''</li><li>'in'</li><li>'that'</li><li>'sense'</li><li>''</li><li>'it'</li><li>'s'</li><li>'similar'</li><li>'in'</li><li>'nature'</li><li>'to'</li><li>'business'</li><li>'analytics'</li><li>''</li><li>'another'</li><li>'umbrella'</li><li>'term'</li><li>'for'</li></ol>
</li>
	<li><style>
.list-inline {list-style: none; margin:0; padding: 0}
.list-inline>li {display: inline-block}
.list-inline>li:not(:last-child)::after {content: "\00b7"; padding: 0 .5ex}
</style>
<ol class=list-inline><li>'approaches'</li><li>'to'</li><li>'analyzing'</li><li>'data'</li><li>''</li><li>''</li><li>''</li><li>'with'</li><li>'the'</li><li>'difference'</li><li>'that'</li><li>'the'</li><li>'latter'</li><li>'is'</li><li>'oriented'</li><li>'to'</li><li>'business'</li><li>'uses'</li><li>''</li><li>'while'</li></ol>
</li>
	<li><style>
.list-inline {list-style: none; margin:0; padding: 0}
.list-inline>li {display: inline-block}
.list-inline>li:not(:last-child)::after {content: "\00b7"; padding: 0 .5ex}
</style>
<ol class=list-inline><li>'data'</li><li>'analytics'</li><li>'has'</li><li>'a'</li><li>'broader'</li><li>'focus'</li><li>''</li><li>'the'</li><li>'expansive'</li><li>'view'</li><li>'of'</li><li>'the'</li><li>'term'</li><li>'isn'</li><li>'t'</li><li>'universal'</li><li>''</li><li>'though'</li><li>''</li><li>'in'</li><li>'some'</li></ol>
</li>
	<li><style>
.list-inline {list-style: none; margin:0; padding: 0}
.list-inline>li {display: inline-block}
.list-inline>li:not(:last-child)::after {content: "\00b7"; padding: 0 .5ex}
</style>
<ol class=list-inline><li>'cases'</li><li>''</li><li>'people'</li><li>'use'</li><li>'data'</li><li>'analytics'</li><li>'specifically'</li><li>'to'</li><li>'mean'</li><li>'advanced'</li><li>'analytics'</li><li>''</li><li>'treating'</li><li>'bi'</li><li>'as'</li><li>'a'</li><li>'separate'</li></ol>
</li>
</ol>




```R
df.words<- unlist(df)
```


```R
notblanks <- which(df.words!="") 
```


```R
head(notblanks)
```


<style>
.list-inline {list-style: none; margin:0; padding: 0}
.list-inline>li {display: inline-block}
.list-inline>li:not(:last-child)::after {content: "\00b7"; padding: 0 .5ex}
</style>
<ol class=list-inline><li>1</li><li>2</li><li>3</li><li>5</li><li>6</li><li>7</li></ol>




```R
df.wordsonly<-df.words[notblanks]
```


```R
length(unique(df.wordsonly))
```


194



```R
# Accessing and understanding word data
df.wordsonly.freq<- table(df.wordsonly)
df.wordsonly.freq_sorted<- sort(df.wordsonly.freq, decreasing = TRUE)
df.wordsonly.freq_sorted[c(1:10)]
```


    df.wordsonly
         data       the        to         a analytics        of       and  analysis 
           18        11        11        10        10        10         9         6 
           in       can 
            6         5 



```R
df1 <- as.data.frame(df.wordsonly.freq_sorted)
```


```R
length(df.wordsonly)
```


320



```R
head(df1)
```


<table class="dataframe">
<caption>A data.frame: 6 × 2</caption>
<thead>
	<tr><th></th><th scope=col>df.wordsonly</th><th scope=col>Freq</th></tr>
	<tr><th></th><th scope=col>&lt;fct&gt;</th><th scope=col>&lt;int&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>1</th><td>data     </td><td>18</td></tr>
	<tr><th scope=row>2</th><td>the      </td><td>11</td></tr>
	<tr><th scope=row>3</th><td>to       </td><td>11</td></tr>
	<tr><th scope=row>4</th><td>a        </td><td>10</td></tr>
	<tr><th scope=row>5</th><td>analytics</td><td>10</td></tr>
	<tr><th scope=row>6</th><td>of       </td><td>10</td></tr>
</tbody>
</table>