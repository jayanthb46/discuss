+++
title = "unlist Function in R"
date = 2022-04-03
draft = false
tags = ["R Tutorial"]
categories = []
+++

## unlist Function in R

unlist() function in R converts a list data structure to vector in R. The basic code for unlist() is described below.

### Unlist List of Vectors in R.


```R
my_list <- list(l1 = c(1, 7, 3, 5),                 # First list element
                l2 = c('A', 'A', 'A'),                    # Second list element
                l3 = c('AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH'))   # Third list element
```


```R
my_list
```


<dl>
	<dt>$l1</dt>
		<dd><style>


<ol class=list-inline><li>'A'</li><li>'A'</li><li>'A'</li></ol>
</dd>
	<dt>$l3</dt>
		<dd><style>
.list-inline {list-style: none; margin:0; padding: 0}
.list-inline>li {display: inline-block}
.list-inline>li:not(:last-child)::after {content: "\00b7"; padding: 0 .5ex}
</style>
<ol class=list-inline><li>'AB'</li><li>'AC'</li><li>'AD'</li><li>'AE'</li><li>'AF'</li><li>'AG'</li><li>'AH'</li></ol>
</dd>
</dl>




```R
unlist(my_list)
```


<style>

</style><dl class=dl-inline><dt>l11</dt><dd>'1'</dd><dt>l12</dt><dd>'7'</dd><dt>l13</dt><dd>'3'</dd><dt>l14</dt><dd>'5'</dd><dt>l21</dt><dd>'A'</dd><dt>l22</dt><dd>'A'</dd><dt>l23</dt><dd>'A'</dd><dt>l31</dt><dd>'AB'</dd><dt>l32</dt><dd>'AC'</dd><dt>l33</dt><dd>'AD'</dd><dt>l34</dt><dd>'AE'</dd><dt>l35</dt><dd>'AF'</dd><dt>l36</dt><dd>'AG'</dd><dt>l37</dt><dd>'AH'</dd></dl>




```R
unlist(my_list, use.names=FALSE)
```


<style>
.list-inline {list-style: none; margin:0; padding: 0}
.list-inline>li {display: inline-block}
.list-inline>li:not(:last-child)::after {content: "\00b7"; padding: 0 .5ex}
</style>
<ol class=list-inline><li>'1'</li><li>'7'</li><li>'3'</li><li>'5'</li><li>'A'</li><li>'A'</li><li>'A'</li><li>'AB'</li><li>'AC'</li><li>'AD'</li><li>'AE'</li><li>'AF'</li><li>'AG'</li><li>'AH'</li></ol>




```R
 df <- data.frame(x1 = c(5, 1, 2),x2 = c(7, 5, 7))
```


```R
df
```


<table>
<caption>A data.frame: 3 × 2</caption>
<thead>
	<tr><th scope=col>x1</th><th scope=col>x2</th></tr>
	<tr><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>5</td><td>7</td></tr>
	<tr><td>1</td><td>5</td></tr>
	<tr><td>2</td><td>7</td></tr>
</tbody>
</table>




```R
unlist(df)
```


<style>
</style><dl class=dl-inline><dt>x11</dt><dd>5</dd><dt>x12</dt><dd>1</dd><dt>x13</dt><dd>2</dd><dt>x21</dt><dd>7</dd><dt>x22</dt><dd>5</dd><dt>x23</dt><dd>7</dd></dl>


### References
https://rdrr.io/r/base/unlist.html
