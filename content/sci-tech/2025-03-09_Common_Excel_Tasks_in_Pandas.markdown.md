+++
title = "Common Excel Tasks in Pandas"
date = 2025-03-09
draft = false
tags = ["Python"]
categories = []
+++


## Common Excel Tasks in Pandas
### Introduction


In Pandas, working with Excel data is a common task, and many Excel operations can be performed using this Python library. Here are some common tasks you can do with Excel files using Pandas:
In this article, I'll focus on some Excel tasks related to data selection and how to map them to pandas. 



### Getting Set Up

Import the pandas and numpy modules.


```python
import pandas as pd
import numpy as np
```

Load in the Excel data that represents a year's worth of sales.


```python
df = pd.read_excel('https://github.com/chris1610/pbpython/blob/master/data/sample-salesv3.xlsx?raw=true')
```

Take a quick look at the data types to make sure everything came through as expected.


```python
df.dtypes
```




    account number      int64
    name               object
    sku                object
    quantity            int64
    unit price        float64
    ext price         float64
    date               object
    dtype: object



You'll notice that our date column is showing up as a generic `object`. We are going to convert it to datetime object to make some selections a little easier.


```python
df['date'] = pd.to_datetime(df['date'])
```


```python
df.head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>account number</th>
      <th>name</th>
      <th>sku</th>
      <th>quantity</th>
      <th>unit price</th>
      <th>ext price</th>
      <th>date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>740150</td>
      <td>Barton LLC</td>
      <td>B1-20000</td>
      <td>39</td>
      <td>86.69</td>
      <td>3380.91</td>
      <td>2014-01-01 07:21:51</td>
    </tr>
    <tr>
      <th>1</th>
      <td>714466</td>
      <td>Trantow-Barrows</td>
      <td>S2-77896</td>
      <td>-1</td>
      <td>63.16</td>
      <td>-63.16</td>
      <td>2014-01-01 10:00:47</td>
    </tr>
    <tr>
      <th>2</th>
      <td>218895</td>
      <td>Kulas Inc</td>
      <td>B1-69924</td>
      <td>23</td>
      <td>90.70</td>
      <td>2086.10</td>
      <td>2014-01-01 13:24:58</td>
    </tr>
    <tr>
      <th>3</th>
      <td>307599</td>
      <td>Kassulke, Ondricka and Metz</td>
      <td>S1-65481</td>
      <td>41</td>
      <td>21.05</td>
      <td>863.05</td>
      <td>2014-01-01 15:05:22</td>
    </tr>
    <tr>
      <th>4</th>
      <td>412290</td>
      <td>Jerde-Hilpert</td>
      <td>S2-34077</td>
      <td>6</td>
      <td>83.21</td>
      <td>499.26</td>
      <td>2014-01-01 23:26:55</td>
    </tr>
  </tbody>
</table>
</div>




```python
df.dtypes
```




    account number             int64
    name                      object
    sku                       object
    quantity                   int64
    unit price               float64
    ext price                float64
    date              datetime64[ns]
    dtype: object



The date is now a datetime object which will be useful in future steps.

### Filtering the data

Similar to the autofilter function in Excel, you can use pandas to filter and select certain subsets of data.

For instance, if we want to just see a specific account number, we can easily do that with pandas.

Note, I am going to use the `head` function to show the top results. This is purely for the purposes of keeping the article shorter.


```python
df[df["account number"]==307599].head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>account number</th>
      <th>name</th>
      <th>sku</th>
      <th>quantity</th>
      <th>unit price</th>
      <th>ext price</th>
      <th>date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>3</th>
      <td>307599</td>
      <td>Kassulke, Ondricka and Metz</td>
      <td>S1-65481</td>
      <td>41</td>
      <td>21.05</td>
      <td>863.05</td>
      <td>2014-01-01 15:05:22</td>
    </tr>
    <tr>
      <th>13</th>
      <td>307599</td>
      <td>Kassulke, Ondricka and Metz</td>
      <td>S2-10342</td>
      <td>17</td>
      <td>12.44</td>
      <td>211.48</td>
      <td>2014-01-04 07:53:01</td>
    </tr>
    <tr>
      <th>34</th>
      <td>307599</td>
      <td>Kassulke, Ondricka and Metz</td>
      <td>S2-78676</td>
      <td>35</td>
      <td>33.04</td>
      <td>1156.40</td>
      <td>2014-01-10 05:26:31</td>
    </tr>
    <tr>
      <th>58</th>
      <td>307599</td>
      <td>Kassulke, Ondricka and Metz</td>
      <td>B1-20000</td>
      <td>22</td>
      <td>37.87</td>
      <td>833.14</td>
      <td>2014-01-15 16:22:22</td>
    </tr>
    <tr>
      <th>70</th>
      <td>307599</td>
      <td>Kassulke, Ondricka and Metz</td>
      <td>S2-10342</td>
      <td>44</td>
      <td>96.79</td>
      <td>4258.76</td>
      <td>2014-01-18 06:32:31</td>
    </tr>
  </tbody>
</table>
</div>



You could also do the filtering based on numeric values.


```python
df[df["quantity"] > 22].head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>account number</th>
      <th>name</th>
      <th>sku</th>
      <th>quantity</th>
      <th>unit price</th>
      <th>ext price</th>
      <th>date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>740150</td>
      <td>Barton LLC</td>
      <td>B1-20000</td>
      <td>39</td>
      <td>86.69</td>
      <td>3380.91</td>
      <td>2014-01-01 07:21:51</td>
    </tr>
    <tr>
      <th>2</th>
      <td>218895</td>
      <td>Kulas Inc</td>
      <td>B1-69924</td>
      <td>23</td>
      <td>90.70</td>
      <td>2086.10</td>
      <td>2014-01-01 13:24:58</td>
    </tr>
    <tr>
      <th>3</th>
      <td>307599</td>
      <td>Kassulke, Ondricka and Metz</td>
      <td>S1-65481</td>
      <td>41</td>
      <td>21.05</td>
      <td>863.05</td>
      <td>2014-01-01 15:05:22</td>
    </tr>
    <tr>
      <th>14</th>
      <td>737550</td>
      <td>Fritsch, Russel and Anderson</td>
      <td>B1-53102</td>
      <td>23</td>
      <td>71.56</td>
      <td>1645.88</td>
      <td>2014-01-04 08:57:48</td>
    </tr>
    <tr>
      <th>15</th>
      <td>239344</td>
      <td>Stokes LLC</td>
      <td>S1-06532</td>
      <td>34</td>
      <td>71.51</td>
      <td>2431.34</td>
      <td>2014-01-04 11:34:58</td>
    </tr>
  </tbody>
</table>
</div>



If we want to do more complex filtering, we can use `map` to filter. In this example, let's look for items with sku's that start with B1.


```python
df[df["sku"].map(lambda x: x.startswith('B1'))].head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>account number</th>
      <th>name</th>
      <th>sku</th>
      <th>quantity</th>
      <th>unit price</th>
      <th>ext price</th>
      <th>date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>740150</td>
      <td>Barton LLC</td>
      <td>B1-20000</td>
      <td>39</td>
      <td>86.69</td>
      <td>3380.91</td>
      <td>2014-01-01 07:21:51</td>
    </tr>
    <tr>
      <th>2</th>
      <td>218895</td>
      <td>Kulas Inc</td>
      <td>B1-69924</td>
      <td>23</td>
      <td>90.70</td>
      <td>2086.10</td>
      <td>2014-01-01 13:24:58</td>
    </tr>
    <tr>
      <th>6</th>
      <td>218895</td>
      <td>Kulas Inc</td>
      <td>B1-65551</td>
      <td>2</td>
      <td>31.10</td>
      <td>62.20</td>
      <td>2014-01-02 10:57:23</td>
    </tr>
    <tr>
      <th>14</th>
      <td>737550</td>
      <td>Fritsch, Russel and Anderson</td>
      <td>B1-53102</td>
      <td>23</td>
      <td>71.56</td>
      <td>1645.88</td>
      <td>2014-01-04 08:57:48</td>
    </tr>
    <tr>
      <th>17</th>
      <td>239344</td>
      <td>Stokes LLC</td>
      <td>B1-50809</td>
      <td>14</td>
      <td>16.23</td>
      <td>227.22</td>
      <td>2014-01-04 22:14:32</td>
    </tr>
  </tbody>
</table>
</div>



It's easy to chain two statements together using the &.


```python
df[df["sku"].map(lambda x: x.startswith('B1')) & (df["quantity"] > 22)].head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>account number</th>
      <th>name</th>
      <th>sku</th>
      <th>quantity</th>
      <th>unit price</th>
      <th>ext price</th>
      <th>date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>740150</td>
      <td>Barton LLC</td>
      <td>B1-20000</td>
      <td>39</td>
      <td>86.69</td>
      <td>3380.91</td>
      <td>2014-01-01 07:21:51</td>
    </tr>
    <tr>
      <th>2</th>
      <td>218895</td>
      <td>Kulas Inc</td>
      <td>B1-69924</td>
      <td>23</td>
      <td>90.70</td>
      <td>2086.10</td>
      <td>2014-01-01 13:24:58</td>
    </tr>
    <tr>
      <th>14</th>
      <td>737550</td>
      <td>Fritsch, Russel and Anderson</td>
      <td>B1-53102</td>
      <td>23</td>
      <td>71.56</td>
      <td>1645.88</td>
      <td>2014-01-04 08:57:48</td>
    </tr>
    <tr>
      <th>26</th>
      <td>737550</td>
      <td>Fritsch, Russel and Anderson</td>
      <td>B1-53636</td>
      <td>42</td>
      <td>42.06</td>
      <td>1766.52</td>
      <td>2014-01-08 00:02:11</td>
    </tr>
    <tr>
      <th>31</th>
      <td>714466</td>
      <td>Trantow-Barrows</td>
      <td>B1-33087</td>
      <td>32</td>
      <td>19.56</td>
      <td>625.92</td>
      <td>2014-01-09 10:16:32</td>
    </tr>
  </tbody>
</table>
</div>



Another useful function that pandas supports is called `isin`. It allows us to define a list of values we want to look for.

In this case, we look for all records that include two specific account numbers.


```python
df[df["account number"].isin([714466,218895])].head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>account number</th>
      <th>name</th>
      <th>sku</th>
      <th>quantity</th>
      <th>unit price</th>
      <th>ext price</th>
      <th>date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1</th>
      <td>714466</td>
      <td>Trantow-Barrows</td>
      <td>S2-77896</td>
      <td>-1</td>
      <td>63.16</td>
      <td>-63.16</td>
      <td>2014-01-01 10:00:47</td>
    </tr>
    <tr>
      <th>2</th>
      <td>218895</td>
      <td>Kulas Inc</td>
      <td>B1-69924</td>
      <td>23</td>
      <td>90.70</td>
      <td>2086.10</td>
      <td>2014-01-01 13:24:58</td>
    </tr>
    <tr>
      <th>5</th>
      <td>714466</td>
      <td>Trantow-Barrows</td>
      <td>S2-77896</td>
      <td>17</td>
      <td>87.63</td>
      <td>1489.71</td>
      <td>2014-01-02 10:07:15</td>
    </tr>
    <tr>
      <th>6</th>
      <td>218895</td>
      <td>Kulas Inc</td>
      <td>B1-65551</td>
      <td>2</td>
      <td>31.10</td>
      <td>62.20</td>
      <td>2014-01-02 10:57:23</td>
    </tr>
    <tr>
      <th>8</th>
      <td>714466</td>
      <td>Trantow-Barrows</td>
      <td>S1-50961</td>
      <td>22</td>
      <td>84.09</td>
      <td>1849.98</td>
      <td>2014-01-03 11:29:02</td>
    </tr>
  </tbody>
</table>
</div>



Pandas supports another function called `query` which allows you to efficiently select subsets of data. It does require the installation of [numexpr](https://github.com/pydata/numexpr) so make sure you have it installed before trying this step.

If you would like to get a list of customers by name, you can do that with a query, similar to the python syntax shown above.


```python
df.query('name == ["Kulas Inc","Barton LLC"]').head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>account number</th>
      <th>name</th>
      <th>sku</th>
      <th>quantity</th>
      <th>unit price</th>
      <th>ext price</th>
      <th>date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>740150</td>
      <td>Barton LLC</td>
      <td>B1-20000</td>
      <td>39</td>
      <td>86.69</td>
      <td>3380.91</td>
      <td>2014-01-01 07:21:51</td>
    </tr>
    <tr>
      <th>2</th>
      <td>218895</td>
      <td>Kulas Inc</td>
      <td>B1-69924</td>
      <td>23</td>
      <td>90.70</td>
      <td>2086.10</td>
      <td>2014-01-01 13:24:58</td>
    </tr>
    <tr>
      <th>6</th>
      <td>218895</td>
      <td>Kulas Inc</td>
      <td>B1-65551</td>
      <td>2</td>
      <td>31.10</td>
      <td>62.20</td>
      <td>2014-01-02 10:57:23</td>
    </tr>
    <tr>
      <th>33</th>
      <td>218895</td>
      <td>Kulas Inc</td>
      <td>S1-06532</td>
      <td>3</td>
      <td>22.36</td>
      <td>67.08</td>
      <td>2014-01-09 23:58:27</td>
    </tr>
    <tr>
      <th>36</th>
      <td>218895</td>
      <td>Kulas Inc</td>
      <td>S2-34077</td>
      <td>16</td>
      <td>73.04</td>
      <td>1168.64</td>
      <td>2014-01-10 12:07:30</td>
    </tr>
  </tbody>
</table>
</div>



The query function allows you do more than just this simple example but for the purposes of this discussion, I'm showing it so you are aware that it is out there for you.

### Working with Dates

Using pandas, you can do complex filtering on dates. Before doing anything with dates, I encourage you to sort by the date column to make sure the results return what you are expecting.


```python
df = df.sort_values(by='date')
df.head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>account number</th>
      <th>name</th>
      <th>sku</th>
      <th>quantity</th>
      <th>unit price</th>
      <th>ext price</th>
      <th>date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>740150</td>
      <td>Barton LLC</td>
      <td>B1-20000</td>
      <td>39</td>
      <td>86.69</td>
      <td>3380.91</td>
      <td>2014-01-01 07:21:51</td>
    </tr>
    <tr>
      <th>1</th>
      <td>714466</td>
      <td>Trantow-Barrows</td>
      <td>S2-77896</td>
      <td>-1</td>
      <td>63.16</td>
      <td>-63.16</td>
      <td>2014-01-01 10:00:47</td>
    </tr>
    <tr>
      <th>2</th>
      <td>218895</td>
      <td>Kulas Inc</td>
      <td>B1-69924</td>
      <td>23</td>
      <td>90.70</td>
      <td>2086.10</td>
      <td>2014-01-01 13:24:58</td>
    </tr>
    <tr>
      <th>3</th>
      <td>307599</td>
      <td>Kassulke, Ondricka and Metz</td>
      <td>S1-65481</td>
      <td>41</td>
      <td>21.05</td>
      <td>863.05</td>
      <td>2014-01-01 15:05:22</td>
    </tr>
    <tr>
      <th>4</th>
      <td>412290</td>
      <td>Jerde-Hilpert</td>
      <td>S2-34077</td>
      <td>6</td>
      <td>83.21</td>
      <td>499.26</td>
      <td>2014-01-01 23:26:55</td>
    </tr>
  </tbody>
</table>
</div>



The python filtering syntax shown before works with dates.


```python
df[df['date'] >='20140905'].head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>account number</th>
      <th>name</th>
      <th>sku</th>
      <th>quantity</th>
      <th>unit price</th>
      <th>ext price</th>
      <th>date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1042</th>
      <td>163416</td>
      <td>Purdy-Kunde</td>
      <td>B1-38851</td>
      <td>41</td>
      <td>98.69</td>
      <td>4046.29</td>
      <td>2014-09-05 01:52:32</td>
    </tr>
    <tr>
      <th>1043</th>
      <td>714466</td>
      <td>Trantow-Barrows</td>
      <td>S1-30248</td>
      <td>1</td>
      <td>37.16</td>
      <td>37.16</td>
      <td>2014-09-05 06:17:19</td>
    </tr>
    <tr>
      <th>1044</th>
      <td>729833</td>
      <td>Koepp Ltd</td>
      <td>S1-65481</td>
      <td>48</td>
      <td>16.04</td>
      <td>769.92</td>
      <td>2014-09-05 08:54:41</td>
    </tr>
    <tr>
      <th>1045</th>
      <td>729833</td>
      <td>Koepp Ltd</td>
      <td>S2-11481</td>
      <td>6</td>
      <td>26.50</td>
      <td>159.00</td>
      <td>2014-09-05 16:33:15</td>
    </tr>
    <tr>
      <th>1046</th>
      <td>737550</td>
      <td>Fritsch, Russel and Anderson</td>
      <td>B1-33364</td>
      <td>4</td>
      <td>76.44</td>
      <td>305.76</td>
      <td>2014-09-06 08:59:08</td>
    </tr>
  </tbody>
</table>
</div>



One of the really nice features of pandas is that it understands dates so will allow us to do partial filtering. If we want to only look for data more recent than a specific month, we can do so.


```python
df[df['date'] >='2014-03'].head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>account number</th>
      <th>name</th>
      <th>sku</th>
      <th>quantity</th>
      <th>unit price</th>
      <th>ext price</th>
      <th>date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>242</th>
      <td>163416</td>
      <td>Purdy-Kunde</td>
      <td>S1-30248</td>
      <td>19</td>
      <td>65.03</td>
      <td>1235.57</td>
      <td>2014-03-01 16:07:40</td>
    </tr>
    <tr>
      <th>243</th>
      <td>527099</td>
      <td>Sanford and Sons</td>
      <td>S2-82423</td>
      <td>3</td>
      <td>76.21</td>
      <td>228.63</td>
      <td>2014-03-01 17:18:01</td>
    </tr>
    <tr>
      <th>244</th>
      <td>527099</td>
      <td>Sanford and Sons</td>
      <td>B1-50809</td>
      <td>8</td>
      <td>70.78</td>
      <td>566.24</td>
      <td>2014-03-01 18:53:09</td>
    </tr>
    <tr>
      <th>245</th>
      <td>737550</td>
      <td>Fritsch, Russel and Anderson</td>
      <td>B1-50809</td>
      <td>20</td>
      <td>50.11</td>
      <td>1002.20</td>
      <td>2014-03-01 23:47:17</td>
    </tr>
    <tr>
      <th>246</th>
      <td>688981</td>
      <td>Keeling LLC</td>
      <td>B1-86481</td>
      <td>-1</td>
      <td>97.16</td>
      <td>-97.16</td>
      <td>2014-03-02 01:46:44</td>
    </tr>
  </tbody>
</table>
</div>



Of course, you can chain the criteria.


```python
df[(df['date'] >='20140701') & (df['date'] <= '20140715')].head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>account number</th>
      <th>name</th>
      <th>sku</th>
      <th>quantity</th>
      <th>unit price</th>
      <th>ext price</th>
      <th>date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>778</th>
      <td>737550</td>
      <td>Fritsch, Russel and Anderson</td>
      <td>S1-65481</td>
      <td>35</td>
      <td>70.51</td>
      <td>2467.85</td>
      <td>2014-07-01 00:21:58</td>
    </tr>
    <tr>
      <th>779</th>
      <td>218895</td>
      <td>Kulas Inc</td>
      <td>S1-30248</td>
      <td>9</td>
      <td>16.56</td>
      <td>149.04</td>
      <td>2014-07-01 00:52:38</td>
    </tr>
    <tr>
      <th>780</th>
      <td>163416</td>
      <td>Purdy-Kunde</td>
      <td>S2-82423</td>
      <td>44</td>
      <td>68.27</td>
      <td>3003.88</td>
      <td>2014-07-01 08:15:52</td>
    </tr>
    <tr>
      <th>781</th>
      <td>672390</td>
      <td>Kuhn-Gusikowski</td>
      <td>B1-04202</td>
      <td>48</td>
      <td>99.39</td>
      <td>4770.72</td>
      <td>2014-07-01 11:12:13</td>
    </tr>
    <tr>
      <th>782</th>
      <td>642753</td>
      <td>Pollich LLC</td>
      <td>S2-23246</td>
      <td>1</td>
      <td>51.29</td>
      <td>51.29</td>
      <td>2014-07-02 04:02:39</td>
    </tr>
  </tbody>
</table>
</div>



Because pandas understands date columns, you can express the date value in multiple formats and it will give you the results you expect.


```python
df[df['date'] >= 'Oct-2014'].head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>account number</th>
      <th>name</th>
      <th>sku</th>
      <th>quantity</th>
      <th>unit price</th>
      <th>ext price</th>
      <th>date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1141</th>
      <td>307599</td>
      <td>Kassulke, Ondricka and Metz</td>
      <td>B1-50809</td>
      <td>25</td>
      <td>56.63</td>
      <td>1415.75</td>
      <td>2014-10-01 10:56:32</td>
    </tr>
    <tr>
      <th>1142</th>
      <td>737550</td>
      <td>Fritsch, Russel and Anderson</td>
      <td>S2-82423</td>
      <td>38</td>
      <td>45.17</td>
      <td>1716.46</td>
      <td>2014-10-01 16:17:24</td>
    </tr>
    <tr>
      <th>1143</th>
      <td>737550</td>
      <td>Fritsch, Russel and Anderson</td>
      <td>S1-47412</td>
      <td>6</td>
      <td>68.68</td>
      <td>412.08</td>
      <td>2014-10-01 22:28:49</td>
    </tr>
    <tr>
      <th>1144</th>
      <td>146832</td>
      <td>Kiehn-Spinka</td>
      <td>S2-11481</td>
      <td>13</td>
      <td>18.80</td>
      <td>244.40</td>
      <td>2014-10-02 00:31:01</td>
    </tr>
    <tr>
      <th>1145</th>
      <td>424914</td>
      <td>White-Trantow</td>
      <td>B1-53102</td>
      <td>9</td>
      <td>94.47</td>
      <td>850.23</td>
      <td>2014-10-02 02:48:26</td>
    </tr>
  </tbody>
</table>
</div>




```python
df[df['date'] >= '10-10-2014'].head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>account number</th>
      <th>name</th>
      <th>sku</th>
      <th>quantity</th>
      <th>unit price</th>
      <th>ext price</th>
      <th>date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>1174</th>
      <td>257198</td>
      <td>Cronin, Oberbrunner and Spencer</td>
      <td>S2-34077</td>
      <td>13</td>
      <td>12.24</td>
      <td>159.12</td>
      <td>2014-10-10 02:59:06</td>
    </tr>
    <tr>
      <th>1175</th>
      <td>740150</td>
      <td>Barton LLC</td>
      <td>S1-65481</td>
      <td>28</td>
      <td>53.00</td>
      <td>1484.00</td>
      <td>2014-10-10 15:08:53</td>
    </tr>
    <tr>
      <th>1176</th>
      <td>146832</td>
      <td>Kiehn-Spinka</td>
      <td>S1-27722</td>
      <td>15</td>
      <td>64.39</td>
      <td>965.85</td>
      <td>2014-10-10 18:24:01</td>
    </tr>
    <tr>
      <th>1177</th>
      <td>257198</td>
      <td>Cronin, Oberbrunner and Spencer</td>
      <td>S2-16558</td>
      <td>3</td>
      <td>35.34</td>
      <td>106.02</td>
      <td>2014-10-11 01:48:13</td>
    </tr>
    <tr>
      <th>1178</th>
      <td>737550</td>
      <td>Fritsch, Russel and Anderson</td>
      <td>B1-53636</td>
      <td>10</td>
      <td>56.95</td>
      <td>569.50</td>
      <td>2014-10-11 10:25:53</td>
    </tr>
  </tbody>
</table>
</div>



When working with time series data, if we convert the data to use the date as at the index, we can do some more filtering.

Set the new index using `set_index`.


```python
df2 = df.set_index(['date'])
df2.head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>account number</th>
      <th>name</th>
      <th>sku</th>
      <th>quantity</th>
      <th>unit price</th>
      <th>ext price</th>
    </tr>
    <tr>
      <th>date</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2014-01-01 07:21:51</th>
      <td>740150</td>
      <td>Barton LLC</td>
      <td>B1-20000</td>
      <td>39</td>
      <td>86.69</td>
      <td>3380.91</td>
    </tr>
    <tr>
      <th>2014-01-01 10:00:47</th>
      <td>714466</td>
      <td>Trantow-Barrows</td>
      <td>S2-77896</td>
      <td>-1</td>
      <td>63.16</td>
      <td>-63.16</td>
    </tr>
    <tr>
      <th>2014-01-01 13:24:58</th>
      <td>218895</td>
      <td>Kulas Inc</td>
      <td>B1-69924</td>
      <td>23</td>
      <td>90.70</td>
      <td>2086.10</td>
    </tr>
    <tr>
      <th>2014-01-01 15:05:22</th>
      <td>307599</td>
      <td>Kassulke, Ondricka and Metz</td>
      <td>S1-65481</td>
      <td>41</td>
      <td>21.05</td>
      <td>863.05</td>
    </tr>
    <tr>
      <th>2014-01-01 23:26:55</th>
      <td>412290</td>
      <td>Jerde-Hilpert</td>
      <td>S2-34077</td>
      <td>6</td>
      <td>83.21</td>
      <td>499.26</td>
    </tr>
  </tbody>
</table>
</div>



We can slice the data to get a range.


```python
df2["20140101":"20140201"].head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>account number</th>
      <th>name</th>
      <th>sku</th>
      <th>quantity</th>
      <th>unit price</th>
      <th>ext price</th>
    </tr>
    <tr>
      <th>date</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2014-01-01 07:21:51</th>
      <td>740150</td>
      <td>Barton LLC</td>
      <td>B1-20000</td>
      <td>39</td>
      <td>86.69</td>
      <td>3380.91</td>
    </tr>
    <tr>
      <th>2014-01-01 10:00:47</th>
      <td>714466</td>
      <td>Trantow-Barrows</td>
      <td>S2-77896</td>
      <td>-1</td>
      <td>63.16</td>
      <td>-63.16</td>
    </tr>
    <tr>
      <th>2014-01-01 13:24:58</th>
      <td>218895</td>
      <td>Kulas Inc</td>
      <td>B1-69924</td>
      <td>23</td>
      <td>90.70</td>
      <td>2086.10</td>
    </tr>
    <tr>
      <th>2014-01-01 15:05:22</th>
      <td>307599</td>
      <td>Kassulke, Ondricka and Metz</td>
      <td>S1-65481</td>
      <td>41</td>
      <td>21.05</td>
      <td>863.05</td>
    </tr>
    <tr>
      <th>2014-01-01 23:26:55</th>
      <td>412290</td>
      <td>Jerde-Hilpert</td>
      <td>S2-34077</td>
      <td>6</td>
      <td>83.21</td>
      <td>499.26</td>
    </tr>
  </tbody>
</table>
</div>



Once again, we can use various date representations to remove any ambiguity around date naming conventions.


```python
df2["2014-Jan-1":"2014-Feb-1"].head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>account number</th>
      <th>name</th>
      <th>sku</th>
      <th>quantity</th>
      <th>unit price</th>
      <th>ext price</th>
    </tr>
    <tr>
      <th>date</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2014-01-01 07:21:51</th>
      <td>740150</td>
      <td>Barton LLC</td>
      <td>B1-20000</td>
      <td>39</td>
      <td>86.69</td>
      <td>3380.91</td>
    </tr>
    <tr>
      <th>2014-01-01 10:00:47</th>
      <td>714466</td>
      <td>Trantow-Barrows</td>
      <td>S2-77896</td>
      <td>-1</td>
      <td>63.16</td>
      <td>-63.16</td>
    </tr>
    <tr>
      <th>2014-01-01 13:24:58</th>
      <td>218895</td>
      <td>Kulas Inc</td>
      <td>B1-69924</td>
      <td>23</td>
      <td>90.70</td>
      <td>2086.10</td>
    </tr>
    <tr>
      <th>2014-01-01 15:05:22</th>
      <td>307599</td>
      <td>Kassulke, Ondricka and Metz</td>
      <td>S1-65481</td>
      <td>41</td>
      <td>21.05</td>
      <td>863.05</td>
    </tr>
    <tr>
      <th>2014-01-01 23:26:55</th>
      <td>412290</td>
      <td>Jerde-Hilpert</td>
      <td>S2-34077</td>
      <td>6</td>
      <td>83.21</td>
      <td>499.26</td>
    </tr>
  </tbody>
</table>
</div>




```python
df2["2014-Jan-1":"2014-Feb-1"].tail()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>account number</th>
      <th>name</th>
      <th>sku</th>
      <th>quantity</th>
      <th>unit price</th>
      <th>ext price</th>
    </tr>
    <tr>
      <th>date</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2014-01-31 22:51:18</th>
      <td>383080</td>
      <td>Will LLC</td>
      <td>B1-05914</td>
      <td>43</td>
      <td>80.17</td>
      <td>3447.31</td>
    </tr>
    <tr>
      <th>2014-02-01 09:04:59</th>
      <td>383080</td>
      <td>Will LLC</td>
      <td>B1-20000</td>
      <td>7</td>
      <td>33.69</td>
      <td>235.83</td>
    </tr>
    <tr>
      <th>2014-02-01 11:51:46</th>
      <td>412290</td>
      <td>Jerde-Hilpert</td>
      <td>S1-27722</td>
      <td>11</td>
      <td>21.12</td>
      <td>232.32</td>
    </tr>
    <tr>
      <th>2014-02-01 17:24:32</th>
      <td>412290</td>
      <td>Jerde-Hilpert</td>
      <td>B1-86481</td>
      <td>3</td>
      <td>35.99</td>
      <td>107.97</td>
    </tr>
    <tr>
      <th>2014-02-01 19:56:48</th>
      <td>412290</td>
      <td>Jerde-Hilpert</td>
      <td>B1-20000</td>
      <td>23</td>
      <td>78.90</td>
      <td>1814.70</td>
    </tr>
  </tbody>
</table>
</div>




```python
df2["2014"].head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>account number</th>
      <th>name</th>
      <th>sku</th>
      <th>quantity</th>
      <th>unit price</th>
      <th>ext price</th>
    </tr>
    <tr>
      <th>date</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2014-01-01 07:21:51</th>
      <td>740150</td>
      <td>Barton LLC</td>
      <td>B1-20000</td>
      <td>39</td>
      <td>86.69</td>
      <td>3380.91</td>
    </tr>
    <tr>
      <th>2014-01-01 10:00:47</th>
      <td>714466</td>
      <td>Trantow-Barrows</td>
      <td>S2-77896</td>
      <td>-1</td>
      <td>63.16</td>
      <td>-63.16</td>
    </tr>
    <tr>
      <th>2014-01-01 13:24:58</th>
      <td>218895</td>
      <td>Kulas Inc</td>
      <td>B1-69924</td>
      <td>23</td>
      <td>90.70</td>
      <td>2086.10</td>
    </tr>
    <tr>
      <th>2014-01-01 15:05:22</th>
      <td>307599</td>
      <td>Kassulke, Ondricka and Metz</td>
      <td>S1-65481</td>
      <td>41</td>
      <td>21.05</td>
      <td>863.05</td>
    </tr>
    <tr>
      <th>2014-01-01 23:26:55</th>
      <td>412290</td>
      <td>Jerde-Hilpert</td>
      <td>S2-34077</td>
      <td>6</td>
      <td>83.21</td>
      <td>499.26</td>
    </tr>
  </tbody>
</table>
</div>




```python
df2["2014-Dec"].head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>account number</th>
      <th>name</th>
      <th>sku</th>
      <th>quantity</th>
      <th>unit price</th>
      <th>ext price</th>
    </tr>
    <tr>
      <th>date</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2014-12-01 20:15:34</th>
      <td>714466</td>
      <td>Trantow-Barrows</td>
      <td>S1-82801</td>
      <td>3</td>
      <td>77.97</td>
      <td>233.91</td>
    </tr>
    <tr>
      <th>2014-12-02 20:00:04</th>
      <td>146832</td>
      <td>Kiehn-Spinka</td>
      <td>S2-23246</td>
      <td>37</td>
      <td>57.81</td>
      <td>2138.97</td>
    </tr>
    <tr>
      <th>2014-12-03 04:43:53</th>
      <td>218895</td>
      <td>Kulas Inc</td>
      <td>S2-77896</td>
      <td>30</td>
      <td>77.44</td>
      <td>2323.20</td>
    </tr>
    <tr>
      <th>2014-12-03 06:05:43</th>
      <td>141962</td>
      <td>Herman LLC</td>
      <td>B1-53102</td>
      <td>20</td>
      <td>26.12</td>
      <td>522.40</td>
    </tr>
    <tr>
      <th>2014-12-03 14:17:34</th>
      <td>642753</td>
      <td>Pollich LLC</td>
      <td>B1-53636</td>
      <td>19</td>
      <td>71.21</td>
      <td>1352.99</td>
    </tr>
  </tbody>
</table>
</div>



### Additional String Functions

Pandas has support for vectorized string functions as well. If we want to identify all the skus that contain a certain value, we can use `str.contains`. In this case, we know that the sku is always represented in the same way, so B1 only shows up in the front of the sku.


```python
df[df['sku'].str.contains('B1')].head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>account number</th>
      <th>name</th>
      <th>sku</th>
      <th>quantity</th>
      <th>unit price</th>
      <th>ext price</th>
      <th>date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>740150</td>
      <td>Barton LLC</td>
      <td>B1-20000</td>
      <td>39</td>
      <td>86.69</td>
      <td>3380.91</td>
      <td>2014-01-01 07:21:51</td>
    </tr>
    <tr>
      <th>2</th>
      <td>218895</td>
      <td>Kulas Inc</td>
      <td>B1-69924</td>
      <td>23</td>
      <td>90.70</td>
      <td>2086.10</td>
      <td>2014-01-01 13:24:58</td>
    </tr>
    <tr>
      <th>6</th>
      <td>218895</td>
      <td>Kulas Inc</td>
      <td>B1-65551</td>
      <td>2</td>
      <td>31.10</td>
      <td>62.20</td>
      <td>2014-01-02 10:57:23</td>
    </tr>
    <tr>
      <th>14</th>
      <td>737550</td>
      <td>Fritsch, Russel and Anderson</td>
      <td>B1-53102</td>
      <td>23</td>
      <td>71.56</td>
      <td>1645.88</td>
      <td>2014-01-04 08:57:48</td>
    </tr>
    <tr>
      <th>17</th>
      <td>239344</td>
      <td>Stokes LLC</td>
      <td>B1-50809</td>
      <td>14</td>
      <td>16.23</td>
      <td>227.22</td>
      <td>2014-01-04 22:14:32</td>
    </tr>
  </tbody>
</table>
</div>



We can string queries together and use sort to control how the data is ordered.

A common need I have in Excel is to understand all the unique items in a column. For instance, maybe I only want to know when customers purchased in this time period. The unique function makes this trivial.


```python
df[(df['sku'].str.contains('B1-531')) & (df['quantity']>40)].sort_values(by=['quantity','name'],ascending=[0,1])
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>account number</th>
      <th>name</th>
      <th>sku</th>
      <th>quantity</th>
      <th>unit price</th>
      <th>ext price</th>
      <th>date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>684</th>
      <td>642753</td>
      <td>Pollich LLC</td>
      <td>B1-53102</td>
      <td>46</td>
      <td>26.07</td>
      <td>1199.22</td>
      <td>2014-06-08 19:33:33</td>
    </tr>
    <tr>
      <th>792</th>
      <td>688981</td>
      <td>Keeling LLC</td>
      <td>B1-53102</td>
      <td>45</td>
      <td>41.19</td>
      <td>1853.55</td>
      <td>2014-07-04 21:42:22</td>
    </tr>
    <tr>
      <th>176</th>
      <td>383080</td>
      <td>Will LLC</td>
      <td>B1-53102</td>
      <td>45</td>
      <td>89.22</td>
      <td>4014.90</td>
      <td>2014-02-11 04:14:09</td>
    </tr>
    <tr>
      <th>1213</th>
      <td>604255</td>
      <td>Halvorson, Crona and Champlin</td>
      <td>B1-53102</td>
      <td>41</td>
      <td>55.05</td>
      <td>2257.05</td>
      <td>2014-10-18 19:27:01</td>
    </tr>
    <tr>
      <th>1215</th>
      <td>307599</td>
      <td>Kassulke, Ondricka and Metz</td>
      <td>B1-53102</td>
      <td>41</td>
      <td>93.70</td>
      <td>3841.70</td>
      <td>2014-10-18 23:25:10</td>
    </tr>
    <tr>
      <th>1128</th>
      <td>714466</td>
      <td>Trantow-Barrows</td>
      <td>B1-53102</td>
      <td>41</td>
      <td>55.68</td>
      <td>2282.88</td>
      <td>2014-09-27 10:42:48</td>
    </tr>
    <tr>
      <th>1001</th>
      <td>424914</td>
      <td>White-Trantow</td>
      <td>B1-53102</td>
      <td>41</td>
      <td>81.25</td>
      <td>3331.25</td>
      <td>2014-08-26 11:44:30</td>
    </tr>
  </tbody>
</table>
</div>



### Bonus Task

I frequently find myself trying to get a list of unique items in a long list within Excel. It is a multi-step process to do this in Excel but is fairly simple in pandas. We just use the `unique` function on a column to get the list.


```python
df["name"].unique()
```




    array(['Barton LLC', 'Trantow-Barrows', 'Kulas Inc',
           'Kassulke, Ondricka and Metz', 'Jerde-Hilpert', 'Koepp Ltd',
           'Fritsch, Russel and Anderson', 'Kiehn-Spinka', 'Keeling LLC',
           'Frami, Hills and Schmidt', 'Stokes LLC', 'Kuhn-Gusikowski',
           'Herman LLC', 'White-Trantow', 'Sanford and Sons', 'Pollich LLC',
           'Will LLC', 'Cronin, Oberbrunner and Spencer',
           'Halvorson, Crona and Champlin', 'Purdy-Kunde'], dtype=object)



If we wanted to include the account number, we could use `drop_duplicates`.


```python
df.drop_duplicates(subset=["account number","name"]).head()
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>account number</th>
      <th>name</th>
      <th>sku</th>
      <th>quantity</th>
      <th>unit price</th>
      <th>ext price</th>
      <th>date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>740150</td>
      <td>Barton LLC</td>
      <td>B1-20000</td>
      <td>39</td>
      <td>86.69</td>
      <td>3380.91</td>
      <td>2014-01-01 07:21:51</td>
    </tr>
    <tr>
      <th>1</th>
      <td>714466</td>
      <td>Trantow-Barrows</td>
      <td>S2-77896</td>
      <td>-1</td>
      <td>63.16</td>
      <td>-63.16</td>
      <td>2014-01-01 10:00:47</td>
    </tr>
    <tr>
      <th>2</th>
      <td>218895</td>
      <td>Kulas Inc</td>
      <td>B1-69924</td>
      <td>23</td>
      <td>90.70</td>
      <td>2086.10</td>
      <td>2014-01-01 13:24:58</td>
    </tr>
    <tr>
      <th>3</th>
      <td>307599</td>
      <td>Kassulke, Ondricka and Metz</td>
      <td>S1-65481</td>
      <td>41</td>
      <td>21.05</td>
      <td>863.05</td>
      <td>2014-01-01 15:05:22</td>
    </tr>
    <tr>
      <th>4</th>
      <td>412290</td>
      <td>Jerde-Hilpert</td>
      <td>S2-34077</td>
      <td>6</td>
      <td>83.21</td>
      <td>499.26</td>
      <td>2014-01-01 23:26:55</td>
    </tr>
  </tbody>
</table>
</div>



We are obviously pulling in more data than we need and getting some non-useful information, so select only the first and second columns using `ix`.


```python
df.drop_duplicates(subset=["account number","name"]).iloc[:,[0,1]]
```




<div>

<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>account number</th>
      <th>name</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>740150</td>
      <td>Barton LLC</td>
    </tr>
    <tr>
      <th>1</th>
      <td>714466</td>
      <td>Trantow-Barrows</td>
    </tr>
    <tr>
      <th>2</th>
      <td>218895</td>
      <td>Kulas Inc</td>
    </tr>
    <tr>
      <th>3</th>
      <td>307599</td>
      <td>Kassulke, Ondricka and Metz</td>
    </tr>
    <tr>
      <th>4</th>
      <td>412290</td>
      <td>Jerde-Hilpert</td>
    </tr>
    <tr>
      <th>7</th>
      <td>729833</td>
      <td>Koepp Ltd</td>
    </tr>
    <tr>
      <th>9</th>
      <td>737550</td>
      <td>Fritsch, Russel and Anderson</td>
    </tr>
    <tr>
      <th>10</th>
      <td>146832</td>
      <td>Kiehn-Spinka</td>
    </tr>
    <tr>
      <th>11</th>
      <td>688981</td>
      <td>Keeling LLC</td>
    </tr>
    <tr>
      <th>12</th>
      <td>786968</td>
      <td>Frami, Hills and Schmidt</td>
    </tr>
    <tr>
      <th>15</th>
      <td>239344</td>
      <td>Stokes LLC</td>
    </tr>
    <tr>
      <th>16</th>
      <td>672390</td>
      <td>Kuhn-Gusikowski</td>
    </tr>
    <tr>
      <th>18</th>
      <td>141962</td>
      <td>Herman LLC</td>
    </tr>
    <tr>
      <th>20</th>
      <td>424914</td>
      <td>White-Trantow</td>
    </tr>
    <tr>
      <th>21</th>
      <td>527099</td>
      <td>Sanford and Sons</td>
    </tr>
    <tr>
      <th>30</th>
      <td>642753</td>
      <td>Pollich LLC</td>
    </tr>
    <tr>
      <th>37</th>
      <td>383080</td>
      <td>Will LLC</td>
    </tr>
    <tr>
      <th>51</th>
      <td>257198</td>
      <td>Cronin, Oberbrunner and Spencer</td>
    </tr>
    <tr>
      <th>67</th>
      <td>604255</td>
      <td>Halvorson, Crona and Champlin</td>
    </tr>
    <tr>
      <th>106</th>
      <td>163416</td>
      <td>Purdy-Kunde</td>
    </tr>
  </tbody>
</table>
</div>



I hope you found this useful. I encourage you to try and apply these ideas to some of your own repetitive Excel tasks and streamline your work flow.

