+++
title = "String Manipulation in R"
date = 2022-07-16
draft = false
tags = ["R tutoial"]
categories = []
+++

# String manipulation in R

### Printing Strings in R
`print` and `cat` are the two predominant ways of printing strings in R. 

```R
print("Hello World")
```

    [1] "Hello World"
    


```R
print("Hello world\n")
```

    [1] "Hello world\n"
    


```R
cat("Hello world")
```

    Hello world
`cat`can interprete special characters such as '\n' . `print` fails to iterprete special characters

```R
cat("Hello world\n")
```

    Hello world
    
`cat` accepts one or more character vectors as argument and prits them to console.Default delimiter is space if length greater than one.

```R
cat(c("hello", "world", "\n"))
```

    hello world 
    


```R
cat("hello world")
```

    hello world

### String manipulation with `stringi` package in R


```R
library('stringi')
```

#### Count Patterns in String


```R
stri_count_fixed("Time after Time", "Ti")
```


2



```R
length(gregexpr("T","Time after Time")[[1]])
# [1] 3
length(gregexpr("Ti","Time after time")[[1]])
# [1] 2
length(gregexpr("Time","Time after Time")[[1]])
# [1] 1
```


2



1



2



```R
stri_count_fixed(c("Time after Time","Timing","Timed","Times of India"), c("Ti"))
```


<ol class=list-inline><li>2</li><li>1</li><li>1</li><li>1</li></ol>


counting with regex provides flexibility of identifying multiple patterns in the string

```R
stri_count_regex(c("Time after Time","Timing","Timed","times of India"), c("^T"))
```



<ol class=list-inline><li>1</li><li>1</li><li>1</li><li>0</li></ol>




```R
stri_count_regex(c("Time after Time","Timing","Timed","times of India"), c("^T|t"))
```



<ol class=list-inline><li>2</li><li>1</li><li>1</li><li>1</li></ol>




```R
stri_count_regex("babbaba, ab , bc","^b")
```


1


#### Duplicating Strings


```R
stri_dup("abc",5)
```


'abcabcabcabcabc'



```R
paste0(rep("abc",10),collapse = "")
```


'abcabcabcabcabcabcabcabcabcabc'


#### Spliting string based on pattern


```R
stri_split_fixed(c("To me not to me", "This is an interesting answer.")," ")
```


<ol>
<li>
<ol class=list-inline><li>'To'</li><li>'me'</li><li>'not'</li><li>'to'</li><li>'me'</li></ol>
</li>
<li>
<ol class=list-inline><li>'This'</li><li>'is'</li><li>'an'</li><li>'interesting'</li><li>'answer.'</li></ol>
</li>
</ol>




```R
stri_split_fixed("Busses, run in the road::roads.",c(" ", ",", "s",":"))
```


<ol>
<li>
<ol class=list-inline><li>'Busses,'</li><li>'run'</li><li>'in'</li><li>'the'</li><li>'road::roads.'</li></ol>
</li>
	<li>
<ol class=list-inline><li>'Busses'</li><li>' run in the road::roads.'</li></ol>
</li>
	<li>
<ol class=list-inline><li>'Bu'</li><li>''</li><li>'e'</li><li>', run in the road::road'</li><li>'.'</li></ol>
</li>
	<li>
<ol class=list-inline><li>'Busses, run in the road'</li><li>''</li><li>'roads.'</li></ol>
</li>
</ol>



### References
- https://www.jstatsoft.org/article/view/v103i02
