+++
title = "Introduction to GGPlot Barplot"
date = 2022-09-18
draft = false
tags = ["R tutoial"]
categories = []
+++

## Introduction to GGPlot Barplot 
Bar Graphs are most commonly used graphs for visualization. Bar Graph typically represents the association of X and Y variables in the form of bar height. The bar chart is used for categorical numerical comparison data which is separate and distinct. For example comparison of price of four items in specific time. The bar graph would not be better choice when comparing price of 4 different items over time.

```R
df1 <- read.csv('https://raw.githubusercontent.com/balakuntlaJayanth/balakuntlaJayanth.github.io/main/Images/ctl_trt.txt', sep='\t', check.name=FALSE)
```


```R
head(df1)
```


<table class="dataframe">
<caption>A data.frame: 3 × 2</caption>
<thead>
	<tr><th></th><th col>group</th><th col>weight</th></tr>
	<tr><th></th><th col>&lt;chr&gt;</th><th col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><th row>1</th><td>ctrl</td><td>5.032</td></tr>
	<tr><th row>2</th><td>trt1</td><td>4.661</td></tr>
	<tr><th row>3</th><td>trt2</td><td>5.526</td></tr>
</tbody>
</table>




```R
library(tidyverse)
library(dplyr)
library(ggplot2)
library(tidyr)
library(scales)  # for percentage scales
```

```R
df1 %>% ggplot(aes(x=group, y=weight)) + geom_bar(stat="identity")
```
    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/balakuntlaJayanth.github.io/main/Images/18092022/output_5_0.png)
    

If you explicitly specify stat = "identity" in geom_bar(), ggplot2 will skip the aggregation and we will provide the y values
### Special Variables
ggplot with double periods around them (..count.., ..density.., etc.) are called Special variables

```R
data(tips, package = "reshape2")
```

```R
head(tips)
```

<table class="dataframe">
<caption>A data.frame: 6 × 7</caption>
<thead>
	<tr><th></th><th col>total_bill</th><th col>tip</th><th col>sex</th><th col>smoker</th><th col>day</th><th col>time</th><th col>size</th></tr>
	<tr><th></th><th col>&lt;dbl&gt;</th><th col>&lt;dbl&gt;</th><th col>&lt;fct&gt;</th><th col>&lt;fct&gt;</th><th col>&lt;fct&gt;</th><th col>&lt;fct&gt;</th><th col>&lt;int&gt;</th></tr>
</thead>
<tbody>
	<tr><th row>1</th><td>16.99</td><td>1.01</td><td>Female</td><td>No</td><td>Sun</td><td>Dinner</td><td>2</td></tr>
	<tr><th row>2</th><td>10.34</td><td>1.66</td><td>Male  </td><td>No</td><td>Sun</td><td>Dinner</td><td>3</td></tr>
	<tr><th row>3</th><td>21.01</td><td>3.50</td><td>Male  </td><td>No</td><td>Sun</td><td>Dinner</td><td>3</td></tr>
	<tr><th row>4</th><td>23.68</td><td>3.31</td><td>Male  </td><td>No</td><td>Sun</td><td>Dinner</td><td>2</td></tr>
	<tr><th row>5</th><td>24.59</td><td>3.61</td><td>Female</td><td>No</td><td>Sun</td><td>Dinner</td><td>4</td></tr>
	<tr><th row>6</th><td>25.29</td><td>4.71</td><td>Male  </td><td>No</td><td>Sun</td><td>Dinner</td><td>4</td></tr>
</tbody>
</table>



#### Special Variable `..count..`


```R
ggplot(tips, aes(x = day)) +  
  geom_bar(aes(y = (..count..)/sum(..count..)))
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/balakuntlaJayanth.github.io/main/Images/18092022/output_12_0.png)
    



```R
ggplot(tips, aes(day)) + 
          geom_bar(aes(y = (..count..)/sum(..count..))) + 
          scale_y_continuous(labels=scales::percent) +
  ylab("relative frequencies")
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/balakuntlaJayanth.github.io/main/Images/18092022/output_13_0.png)
    


#### Special variable `..prop..`


```R
ggplot(tips, aes(day, group = sex)) + 
          geom_bar(aes(y = ..prop.. ), stat="count")
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/balakuntlaJayanth.github.io/main/Images/18092022/output_15_0.png)
    


### Colors in bar plot


```R
df1 %>% ggplot(aes(x=group, y=weight)) + geom_bar(stat='identity', fill='lightblue', colour = 'red')
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/balakuntlaJayanth.github.io/main/Images/18092022/output_17_0.png)
    

fill variable added to geom_bar for specifing the filling color (light blue) and color for border color red

```R
ggplot(tips, aes(day, group = sex)) + geom_bar(aes(y = ..prop.., fill = factor(..x..)), stat="count")
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/balakuntlaJayanth.github.io/main/Images/18092022/output_19_0.png)
    

passing special variable `..x..` to produce different random color for each bar.

```R
ggplot(tips, aes(day, group = sex)) + geom_bar(aes(y = ..prop.., fill = factor(..x..)), stat="count") + scale_fill_brewer(palette='Pastel1')
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/balakuntlaJayanth.github.io/main/Images/18092022/output_21_0.png)
    

you can use `scale_fill_brewer` to generate manual colors 
### Grouping Bar Graphs


```R
library(gcookbook)
```


```R
head(cabbage_exp)
```


<table class="dataframe">
<caption>A data.frame: 6 × 6</caption>
<thead>
	<tr><th></th><th col>Cultivar</th><th col>Date</th><th col>Weight</th><th col>sd</th><th col>n</th><th col>se</th></tr>
	<tr><th></th><th col>&lt;fct&gt;</th><th col>&lt;fct&gt;</th><th col>&lt;dbl&gt;</th><th col>&lt;dbl&gt;</th><th col>&lt;int&gt;</th><th col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><th row>1</th><td>c39</td><td>d16</td><td>3.18</td><td>0.9566144</td><td>10</td><td>0.30250803</td></tr>
	<tr><th row>2</th><td>c39</td><td>d20</td><td>2.80</td><td>0.2788867</td><td>10</td><td>0.08819171</td></tr>
	<tr><th row>3</th><td>c39</td><td>d21</td><td>2.74</td><td>0.9834181</td><td>10</td><td>0.31098410</td></tr>
	<tr><th row>4</th><td>c52</td><td>d16</td><td>2.26</td><td>0.4452215</td><td>10</td><td>0.14079141</td></tr>
	<tr><th row>5</th><td>c52</td><td>d20</td><td>3.11</td><td>0.7908505</td><td>10</td><td>0.25008887</td></tr>
	<tr><th row>6</th><td>c52</td><td>d21</td><td>1.47</td><td>0.2110819</td><td>10</td><td>0.06674995</td></tr>
</tbody>
</table>


In this example we will use cabbage data , which has 2 categorical variables cultivar and date and one continous variable weight

```R
ggplot(cabbage_exp, aes(x=Date, y=Weight, fill=Cultivar)) + geom_bar(stat = 'identity',position="dodge") 
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/balakuntlaJayanth.github.io/main/Images/18092022/output_27_0.png)
    

you can produce grouped bar charts by mapping the variable to `fill` and `dodge` variable.

```R

```


```R
data <- read.csv("https://osf.io/meyhp/?action=download")
```


```R
head(data)
```


<table class="dataframe">
<caption>A data.frame: 6 × 28</caption>
<thead>
	<tr><th></th><th col>X</th><th col>timestamp</th><th col>code</th><th col>i01</th><th col>i02r</th><th col>i03</th><th col>i04</th><th col>i05</th><th scol>i06r</th><th scope=col>i07</th><th col>...</th><th col>time_conversation</th><th col>presentation</th><th col>n_party</th><th col>clients</th><th col>extra_vignette</th><th scope=col>extra_description</th><th col>prop_na_per_row</th><th col>extra_mean</th><th col>extra_median</th><th col>client_freq</th></tr>
	<tr><th></th><th col>&lt;int&gt;</th><th col>&lt;chr&gt;</th><th col>&lt;chr&gt;</th><th col>&lt;int&gt;</th><th col>&lt;int&gt;</th><th col>&lt;int&gt;</th><th col>&lt;int&gt;</th><th col>&lt;int&gt;</th><th col>&lt;int&gt;</th><th col>&lt;int&gt;</th><th col>...</th><th col>&lt;dbl&gt;</th><th col>&lt;chr&gt;</th><th col>&lt;int&gt;</th><th scope=col>&lt;chr&gt;</th><th col>&lt;chr&gt;</th><th col>&lt;int&gt;</th><th col>&lt;dbl&gt;</th><th col>&lt;dbl&gt;</th><th col>&lt;dbl&gt;</th><th col>&lt;int&gt;</th></tr>
</thead>
<tbody>
	<tr><th row>1</th><td>1</td><td>11.03.2015 19:17:48</td><td>HSC</td><td>3</td><td>3</td><td>3</td><td>3</td><td>4</td><td>4</td><td>3</td><td>...</td><td>10</td><td>nein</td><td>20</td><td></td><td></td><td>NA</td><td>0.04347826</td><td>2.9</td><td>3.0</td><td>NA</td></tr>
	<tr><th row>2</th><td>2</td><td>11.03.2015 19:18:05</td><td>ERB</td><td>2</td><td>2</td><td>1</td><td>2</td><td>3</td><td>2</td><td>2</td><td>...</td><td>15</td><td>nein</td><td> 5</td><td></td><td></td><td>NA</td><td>0.04347826</td><td>2.1</td><td>2.0</td><td>NA</td></tr>
	<tr><th row>3</th><td>3</td><td>11.03.2015 19:18:09</td><td>ADP</td><td>3</td><td>4</td><td>1</td><td>4</td><td>4</td><td>1</td><td>3</td><td>...</td><td>15</td><td>nein</td><td> 3</td><td></td><td></td><td>NA</td><td>0.04347826</td><td>2.6</td><td>3.0</td><td>NA</td></tr>
	<tr><th row>4</th><td>4</td><td>11.03.2015 19:18:19</td><td>KHB</td><td>3</td><td>3</td><td>2</td><td>4</td><td>3</td><td>3</td><td>3</td><td>...</td><td> 5</td><td>nein</td><td>25</td><td></td><td></td><td>NA</td><td>0.04347826</td><td>2.9</td><td>3.0</td><td>NA</td></tr>
	<tr><th row>5</th><td>5</td><td>11.03.2015 19:18:19</td><td>PTG</td><td>4</td><td>3</td><td>1</td><td>4</td><td>4</td><td>3</td><td>4</td><td>...</td><td> 5</td><td>nein</td><td> 4</td><td></td><td></td><td>NA</td><td>0.04347826</td><td>3.2</td><td>3.5</td><td>NA</td></tr>
	<tr><th row>6</th><td>6</td><td>11.03.2015 19:18:23</td><td>ABL</td><td>3</td><td>2</td><td>1</td><td>4</td><td>2</td><td>3</td><td>4</td><td>...</td><td>20</td><td>ja  </td><td> 4</td><td></td><td></td><td>NA</td><td>0.04347826</td><td>2.8</td><td>3.0</td><td>NA</td></tr>
</tbody>
</table>


The data consists of results of a survey on extraversion and associated behavior.

Say, we would like to visualize the responsed to the extraversion items (there are 10 of them).



```R
data %>% 
  select(i01:i10) %>% 
  gather %>% 
  dplyr::count(key, value) %>% 
  ungroup -> data2
```


```R
head(data2)
```


<table class="dataframe">
<caption>A data.frame: 6 × 3</caption>
<thead>
	<tr><th></th><th col>key</th><th col>value</th><th col>n</th></tr>
	<tr><th></th><th col>&lt;chr&gt;</th><th col>&lt;int&gt;</th><th col>&lt;int&gt;</th></tr>
</thead>
<tbody>
	<tr><th row>1</th><td>i01 </td><td> 1</td><td>  7</td></tr>
	<tr><th row>2</th><td>i01 </td><td> 2</td><td> 39</td></tr>
	<tr><th row>3</th><td>i01 </td><td> 3</td><td>230</td></tr>
	<tr><th row>4</th><td>i01 </td><td> 4</td><td>223</td></tr>
	<tr><th row>5</th><td>i01 </td><td>NA</td><td>  2</td></tr>
	<tr><th row>6</th><td>i02r</td><td> 1</td><td> 18</td></tr>
</tbody>
</table>




```R
ggplot(data2) +
  geom_col(aes(x = key, fill = factor(value), y = n))
```
    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/balakuntlaJayanth.github.io/main/Images/18092022/output_35_0.png)
    

### References

- http://www.sthda.com/english/wiki/ggplot2-barplots-quick-start-guide-r-software-and-data-visualization
