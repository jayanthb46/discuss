+++
title = "Paste Function in R"
date = 2021-08-01
draft = false
tags = ["R Tutorial"]
categories = []
+++


### Paste Function in R

The paste() in R is an inbuilt function used to concatenate vectors by converting them into character.Important Functions of paste
    - concatenate vectors or strings
    - concatenate columns of DataframeThe syntax of the paste() function is,paste(Values, sep=" ", collapse=NULL)

Values can be one or more R objects that need to be concatenated

```R
paste("X", 9, 41, "Z")
```


'Hello 19 21 Mate'



```R
paste("X",9,41,"Z", sep=":")
```


'X:9:41:Z'



```R
paste(c("A", "B", "C"), 1:9, sep = "|")
```


<style>
.list-inline {list-style: none; margin:0; padding: 0}
.list-inline>li {display: inline-block}
.list-inline>li:not(:last-child)::after {content: "\00b7"; padding: 0 .5ex}
</style>
<ol class=list-inline><li>'A|1'</li><li>'B|2'</li><li>'C|3'</li><li>'A|4'</li><li>'B|5'</li><li>'C|6'</li><li>'A|7'</li><li>'B|8'</li><li>'C|9'</li></ol>




```R
paste(c("A", "B", "C"), 1:9, sep = "|", collapse=':')
```


'A|1:B|2:C|3:A|4:B|5:C|6:A|7:B|8:C|9'



```R
paste0("X", 9, 41, "Z")
```


'X941Z'



```R
paste0(c("A", "B", "C"), 1:9, sep = "|", collapse=':')
```


'A1|:B2|:C3|:A4|:B5|:C6|:A7|:B8|:C9|'


### References

https://www.rdocumentation.org/packages/base/versions/3.6.2/topics/paste
