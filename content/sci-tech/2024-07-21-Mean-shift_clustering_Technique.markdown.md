+++
title = "Mean-shift clustering Technique"
date = 2024-07-21
draft = false
tags = ["Python"]
categories = []
+++

# Mean-shift Clustering Technique

Mean shift clustering is a non-parametric clustering technique used in machine learning , bioinformatics and computer vision. It is particularly useful for applications where the number of clusters is not known a priori or the clusters are irregularly shaped by k-means and DB-scan clustering.
For each data point, a mean shift vector is computed based on the weighted average of its neighbors.
This vector points towards the direction of the maximum increase in the density function.
Mean shift clustering is based on KDE, which estimates the probability density function of a dataset's.
KDE assigns each data point a probability density based on its neighboring points.
Mean shift is a mode-seeking algorithm; it iteratively shifts each data point towards the mode (peak) of the underlying density function.
The mode corresponds to the center of a cluster in the data space.
Mean shift continues shifting points until they converge to local density maxima, where no further shifts are possible
After convergence, points that converge to the same mode are assigned to the same cluster.
The number of clusters is determined by the algorithm based on the data and the bandwidth parameter.

## Make the synthetic data


```python
import numpy as np
import matplotlib.pyplot as plt
%matplotlib inline
```


```python
from sklearn.cluster import MeanShift
from sklearn import metrics
from sklearn.datasets import make_blobs

# Generate sample data
centers = [[1, 1], [-1, -1], [1, -1]]
X, labels_true = make_blobs(n_samples=300, centers=centers, cluster_std=0.4,random_state=101)
```


```python
X.shape
```

    (300, 2)


```python
plt.figure(figsize=(8,5))
plt.scatter(X[:,0],X[:,1],edgecolors='k',c='orange',s=75)
plt.grid(True)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.show()
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/21062024/output_6_0.png)
    


## Clustering


```python
ms_model = MeanShift().fit(X)
cluster_centers = ms_model.cluster_centers_
labels = ms_model.labels_
n_clusters = len(cluster_centers)
labels = ms_model.labels_
```

#### Number of detected clusters and their centers


```python
print("Number of clusters detected by the algorithm:", n_clusters)
```

    Number of clusters detected by the algorithm: 3



```python
print("Cluster centers detected at:\n\n", cluster_centers)
```

    Cluster centers detected at:
    
     [[ 1.09954715 -1.09294823]
     [ 0.9931698   1.05074234]
     [-0.99420039 -0.96765319]]



```python
plt.figure(figsize=(8,5))
plt.scatter(X[:,0],X[:,1],edgecolors='k',c=ms_model.labels_,s=75)
plt.grid(True)
plt.xticks(fontsize=15)
plt.yticks(fontsize=15)
plt.show()
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/21062024/output_12_0.png)
    


#### Homogeneity

Homogeneity metric of a cluster labeling given a ground truth.

A clustering result satisfies homogeneity if all of its clusters contain only data points which are members of a single class. This metric is independent of the absolute values of the labels: a permutation of the class or cluster label values won’t change the score value in any way.


```python
print ("Homogeneity score:", metrics.homogeneity_score(labels_true,labels))
```

    Homogeneity score: 0.9405073022327173


#### Completeness

Completeness metric of a cluster labeling given a ground truth.

A clustering result satisfies completeness if all the data points that are members of a given class are elements of the same cluster. This metric is independent of the absolute values of the labels: a permutation of the class or cluster label values won’t change the score value in any way.


```python
print("Completeness score:",metrics.completeness_score(labels_true,labels))
```

    Completeness score: 0.9405073022327173


## Time complexity and model quality as the data size grows


```python
import time
from tqdm import tqdm 
```


```python
n_samples = [10,20,50,100,200,500,1000,2000,3000,5000,7500,10000]
centers = [[1, 1], [-1, -1], [1, -1]]
t_ms = []
homo_ms=[]
complete_ms=[]

for i in tqdm(n_samples):
    X,labels_true = make_blobs(n_samples=i, centers=centers, cluster_std=0.4,random_state=101)
    t1 = time.time()
    ms_model = MeanShift().fit(X)
    t2=time.time()
    t_ms.append(t2-t1)
    homo_ms.append(metrics.homogeneity_score(labels_true,ms_model.labels_))
    complete_ms.append(metrics.completeness_score(labels_true,ms_model.labels_))
```

    100%|█████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 12/12 [00:43<00:00,  3.61s/it]



```python
plt.figure(figsize=(8,5))
plt.title("Time complexity of Mean Shift\n",fontsize=20)
plt.scatter(n_samples,t_ms,edgecolors='k',c='green',s=100)
plt.plot(n_samples,t_ms,'k--',lw=3)
plt.grid(True)
plt.xticks(fontsize=15)
plt.xlabel("Number of samples",fontsize=15)
plt.yticks(fontsize=15)
plt.ylabel("Time taken for model (sec)",fontsize=15)
plt.show()
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/21062024/output_20_0.png)
    



```python
plt.figure(figsize=(8,5))
plt.title("Homogeneity score with data set size\n",fontsize=20)
plt.scatter(n_samples,homo_ms,edgecolors='k',c='green',s=100)
plt.plot(n_samples,homo_ms,'k--',lw=3)
plt.grid(True)
plt.xticks(fontsize=15)
plt.xlabel("Number of samples",fontsize=15)
plt.yticks(fontsize=15)
plt.ylabel("Homogeneity score",fontsize=15)
plt.show()
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/21062024/output_21_0.png)
    



```python
plt.figure(figsize=(8,5))
plt.title("Completeness score with data set size\n",fontsize=20)
plt.scatter(n_samples,complete_ms,edgecolors='k',c='green',s=100)
plt.plot(n_samples,complete_ms,'k--',lw=3)
plt.grid(True)
plt.xticks(fontsize=15)
plt.xlabel("Number of samples",fontsize=15)
plt.yticks(fontsize=15)
plt.ylabel("Completeness score",fontsize=15)
plt.show()
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/21062024/output_22_0.png)
    


## How well the cluster detection works in the presence of noise?

Create data sets with varying degree of noise std. dev and run the model to detect clusters.


```python
noise = [0.01,0.05,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0,1.25,1.5,1.75,2.0]
n_clusters = []
for i in noise:
    centers = [[1, 1], [-1, -1], [1, -1]]
    X, labels_true = make_blobs(n_samples=200, centers=centers, cluster_std=i,random_state=101)
    ms_model=MeanShift().fit(X)
    n_clusters.append(len(ms_model.cluster_centers_))
```


```python
print("Detected number of clusters:",n_clusters)
plt.figure(figsize=(8,5))
plt.title("Cluster detection with noisy data\n",fontsize=20)
plt.scatter(noise,n_clusters,edgecolors='k',c='green',s=100)
plt.grid(True)
plt.xticks(fontsize=15)
plt.xlabel("Noise std.dev",fontsize=15)
plt.yticks(fontsize=15)
plt.ylabel("Number of clusters detected",fontsize=15)
plt.show()
```

    Detected number of clusters: [3, 3, 3, 3, 3, 3, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1]



    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/21062024/output_25_1.png)
    


### Advantages:
- No need to specify the number of clusters: Mean shift automatically determines the number of clusters based on the data and bandwidth.
- Handles irregular shapes: Unlike k-means, mean shift can find clusters of arbitrary shape because it uses density information rather than distance.

## Limitations:
- Computational complexity: Mean shift can be computationally expensive, especially for large datasets, because it involves calculating distances and updating points iteratively.
- Bandwidth selection: The performance of mean shift clustering can be sensitive to the choice of bandwidth parameter. If chosen poorly, it can lead to over-smoothing or under-smoothing of the density estimate.

## Application Areas:
- Image Segmentation: Mean shift clustering is widely used in computer vision for segmenting images based on color or texture similarity.
- Object Tracking: It can be used to track objects in video sequences by clustering features that correspond to moving objects.
- Feature Space Clustering: In general machine learning applications, mean shift can be applied to cluster data in feature spaces where the number of clusters is not predefined.
- Mean shift clustering is a powerful technique but requires careful parameter tuning and understanding of its underlying assumptions for effective application.

## References
- https://scikit-learn.org/stable/modules/generated/sklearn.cluster.MeanShift.html