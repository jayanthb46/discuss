+++
title = "Adding p-value to plots in R"
date = 2021-09-19
draft = false
tags = ["R Tutorial"]
categories = []
+++

## Adding p-value to plots in R

### IntroductionIn 

Statistics comparison of two groups is the fundamental problem. Statisticians perform various statistical tests based on data to show statistically significant differences between different groups compared. 

Statistically significant results will be presented as a plot to show the difference between groups.

From the above discussion, we understood that there are 2 steps

- Statistical testing based on Null hypothesis

- Visualising the favored results by various types of plots

In today's blog, we discuss a package called ggsignif which integrates the above steps into single step

The R package, ggsignif provides a quick way to visualize such pairwise indicators as an annotation in a plot, for example showing if a difference is statistically significant. 

ggsignif favors the principles of the grammar of graphics and adds a new layer that can be added to plots made with the ggplot2 package.In today's blog, standard iris dataset is used to compare the difference between sepal length and petal length.
Difference between 2 groups(sepal length and petal length) are annotated on boxplot using ggsignif package.

#### Step 1

In step 1 we load iris dataset in to dataframe

```R
library(ggpubr)
library(reshape2)
```

    Loading required package: ggplot2
    
    Warning message:
    “package ‘ggplot2’ was built under R version 4.1.0”
    
#### Step 2    

In step 2 understanding data using `head` and `summary` functions in R.

```R
data('iris')
```


```R
head(iris)
```


<table class="dataframe">
<caption>A data.frame: 6 × 5</caption>
<thead>
	<tr><th></th><th scope=col>Sepal.Length</th><th scope=col>Sepal.Width</th><th scope=col>Petal.Length</th><th scope=col>Petal.Width</th><th scope=col>Species</th></tr>
	<tr><th></th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;fct&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>1</th><td>5.1</td><td>3.5</td><td>1.4</td><td>0.2</td><td>setosa</td></tr>
	<tr><th scope=row>2</th><td>4.9</td><td>3.0</td><td>1.4</td><td>0.2</td><td>setosa</td></tr>
	<tr><th scope=row>3</th><td>4.7</td><td>3.2</td><td>1.3</td><td>0.2</td><td>setosa</td></tr>
	<tr><th scope=row>4</th><td>4.6</td><td>3.1</td><td>1.5</td><td>0.2</td><td>setosa</td></tr>
	<tr><th scope=row>5</th><td>5.0</td><td>3.6</td><td>1.4</td><td>0.2</td><td>setosa</td></tr>
	<tr><th scope=row>6</th><td>5.4</td><td>3.9</td><td>1.7</td><td>0.4</td><td>setosa</td></tr>
</tbody>
</table>




```R
summary(iris)
```


      Sepal.Length    Sepal.Width     Petal.Length    Petal.Width
     Min.   :4.300   Min.   :2.000   Min.   :1.000   Min.   :0.100 
     1st Qu.:5.100   1st Qu.:2.800   1st Qu.:1.600   1st Qu.:0.300 
     Median :5.800   Median :3.000   Median :4.350   Median :1.300 
     Mean   :5.843   Mean   :3.057   Mean   :3.758   Mean   :1.199 
     3rd Qu.:6.400   3rd Qu.:3.300   3rd Qu.:5.100   3rd Qu.:1.800 
     Max.   :7.900   Max.   :4.400   Max.   :6.900   Max.   :2.500 
           Species  
     setosa    :50 
     versicolor:50 
     virginica :50 
                    
                    
#### Step 3                    

In step 3 data transformation for basic boxplot

```R
df1 <- melt(iris)
```

    Using Species as id variables
    



```R
head(df1)
```


<table class="dataframe">
<caption>A data.frame: 6 × 3</caption>
<thead>
	<tr><th></th><th scope=col>Species</th><th scope=col>variable</th><th scope=col>value</th></tr>
	<tr><th></th><th scope=col>&lt;fct&gt;</th><th scope=col>&lt;fct&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>1</th><td>setosa</td><td>Sepal.Length</td><td>5.1</td></tr>
	<tr><th scope=row>2</th><td>setosa</td><td>Sepal.Length</td><td>4.9</td></tr>
	<tr><th scope=row>3</th><td>setosa</td><td>Sepal.Length</td><td>4.7</td></tr>
	<tr><th scope=row>4</th><td>setosa</td><td>Sepal.Length</td><td>4.6</td></tr>
	<tr><th scope=row>5</th><td>setosa</td><td>Sepal.Length</td><td>5.0</td></tr>
	<tr><th scope=row>6</th><td>setosa</td><td>Sepal.Length</td><td>5.4</td></tr>
</tbody>
</table>




```R
tail(df1)
```


<table class="dataframe">
<caption>A data.frame: 6 × 3</caption>
<thead>
	<tr><th></th><th scope=col>Species</th><th scope=col>variable</th><th scope=col>value</th></tr>
	<tr><th></th><th scope=col>&lt;fct&gt;</th><th scope=col>&lt;fct&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>595</th><td>virginica</td><td>Petal.Width</td><td>2.5</td></tr>
	<tr><th scope=row>596</th><td>virginica</td><td>Petal.Width</td><td>2.3</td></tr>
	<tr><th scope=row>597</th><td>virginica</td><td>Petal.Width</td><td>1.9</td></tr>
	<tr><th scope=row>598</th><td>virginica</td><td>Petal.Width</td><td>2.0</td></tr>
	<tr><th scope=row>599</th><td>virginica</td><td>Petal.Width</td><td>2.3</td></tr>
	<tr><th scope=row>600</th><td>virginica</td><td>Petal.Width</td><td>1.8</td></tr>
</tbody>
</table>


#### Step 4

In step 4  plotting boxplot to visualize difference between groups

```R
bp <- ggplot(df1, aes(x=variable, y=value, group=variable)) + 
  geom_boxplot(aes(fill=Species)) + facet_wrap(~Species) + theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
```


```R
bp
```


![png](https://raw.githubusercontent.com/balakuntlaJayanth/balakuntlaJayanth.github.io/main/Images/Sep19_2021/output_16_0.png)

In step5 annotating plot with pvalue based on wilcox test between groups sepal length and petal length

```R
bp + geom_signif(comparisons = list(c("Sepal.Length","Petal.Length")),map_signif_level = TRUE)
```


![png](https://raw.githubusercontent.com/balakuntlaJayanth/balakuntlaJayanth.github.io/main/Images/Sep19_2021/output_18_0.png)

`**` annotations indicate the difference between sepal length and petal length are statistically significant between 2 groups.

Plots discussed below we use ttest and anova instead of default wilcox test.

```R
bp + geom_signif(comparisons = list(c("Sepal.Length","Petal.Length")),map_signif_level = TRUE, method= 'ttest')
```

    Warning message:
    “Ignoring unknown parameters: method”



![png](https://raw.githubusercontent.com/balakuntlaJayanth/balakuntlaJayanth.github.io/main/Images/Sep19_2021/output_20_1.png)



```R
bp + geom_signif(comparisons = list(c("Sepal.Length","Petal.Length")),map_signif_level = TRUE, method= 'anova')
```

    Warning message:
    “Ignoring unknown parameters: method”



![png](https://raw.githubusercontent.com/balakuntlaJayanth/balakuntlaJayanth.github.io/main/Images/Sep19_2021/output_22_1.png)

### References

- https://www.crumplab.com/psyc7709_2019/book/docs/how-to-annotate-a-graph-using-gg-signif.html