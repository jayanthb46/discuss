+++
title = "GGPlot in R part 1"
date = 2022-09-09
draft = false
tags = ["R tutoial"]
categories = []
+++

:::cell
``` {.r .cell-code}
library(tidyverse)
```

### GGPlot in R part 1

#### Introduction to GGPlot

GGPlot was designed by Hadley Wickham from RStudio. GGPlot is considered
to be one of the elegant libraries preferred fo data visualization.

This blog provides an brief overview of how the
[`ggplot2`](https://ggplot2.tidyverse.org/) package works.

GGPlot breaks down to visualization into series of layers . Each Layer
represents unique component of the plot .

Layers are added one above to finish the plot like a bricks for wall.
Here we consider `mpg` dataset for this tutorial.

#### mpg Dataset summary

::: cell
``` {.r .cell-code}
head(mpg)
```

::: {.cell-output .cell-output-stdout}
    # A tibble: 6 x 11
      manufacturer model displ  year   cyl trans      drv     cty   hwy fl    class 
      <chr>        <chr> <dbl> <int> <int> <chr>      <chr> <int> <int> <chr> <chr> 
    1 audi         a4      1.8  1999     4 auto(l5)   f        18    29 p     compa~
    2 audi         a4      1.8  1999     4 manual(m5) f        21    29 p     compa~
    3 audi         a4      2    2008     4 manual(m6) f        20    31 p     compa~
    4 audi         a4      2    2008     4 auto(av)   f        21    30 p     compa~
    5 audi         a4      2.8  1999     6 auto(l5)   f        16    26 p     compa~
    6 audi         a4      2.8  1999     6 manual(m5) f        18    26 p     compa~
:::
:::

::: cell
``` {.r .cell-code}
nrow(mpg)
```

::: {.cell-output .cell-output-stdout}
    [1] 234
:::

``` {.r .cell-code}
ncol(mpg)
```

::: {.cell-output .cell-output-stdout}
    [1] 11
:::
:::

`mpg` dataset has 11 columns 234 rows.

#### Layers in GGPlot

First layer of the plot is aesthetic mapping or aestetic referred as
`aes`. The `aes` is used to define the data points from columns in data
frame .

::: cell
``` {.r .cell-code}
ggplot(mpg, aes(displ, hwy))
```

::: cell-output-display
![](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/09_09_2022/unnamed-chunk-4-1.png)
:::
:::

Any box was created by `aes` to describe the boundaries of the plot.

#### Geom Second Layer

Geom Layer is the fundamental building block for data visualization
layered above aesthetics.

There are different types Geom like `geom_point()` which describe the
nature of the plot.

Example of Geom

::: cell
``` {.r .cell-code}
ggplot(mpg, aes(displ, hwy)) + geom_point()
```

::: cell-output-display
![](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/09_09_2022/unnamed-chunk-5-1.png)
:::
:::

In this example additional layer of `geom_point()` is added to initial
layer using `+` operator.

#### Properties of Geom

-   The Geom's can overlay-ed above one another to generate another
    Geom.

-   Geom names are associated with the type of plot specific Geom
    produces. For example `geom_point()` generates a scatter plot with
    point as the Geometric shape used to describe the relationship of
    variables.

#### References

-   https://rkabacoff.github.io/datavis/IntroGGPLOT.html
