+++
title = "Data Tables in R"
date = 2022-07-31
draft = false
tags = ["R tutoial"]
categories = []
+++

### Data Tables in R
Data.table is a package that extends the functionality of data frames from base R, particularly improving on their
performance and syntax
#### Creation of Data Table
A data.table is an enhanced version of the data.frame class from base R. As such, its class() attribute is the vector
"data.table" "data.frame" and functions that work on a data.frame will also work with a data.table. There are
many ways to create, load or coerce to a data.table.

```R
library(data.table)
```


```R
DT <- data.table(
x = letters[1:5],
y = 1:5,
z = (1:5) > 3
)
```


```R
DT
```


<table>
<caption>A data.table: 5 × 3</caption>
<thead>
	<tr><th scope=col>x</th><th scope=col>y</th><th scope=col>z</th></tr>
	<tr><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;lgl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>a</td><td>1</td><td>FALSE</td></tr>
	<tr><td>b</td><td>2</td><td>FALSE</td></tr>
	<tr><td>c</td><td>3</td><td>FALSE</td></tr>
	<tr><td>d</td><td>4</td><td> TRUE</td></tr>
	<tr><td>e</td><td>5</td><td> TRUE</td></tr>
</tbody>
</table>


Unlike data.frame, data.table will not coerce strings to factors:

```R
sapply(DT, class)
```


<style>
.dl-inline {width: auto; margin:0; padding: 0}
.dl-inline>dt, .dl-inline>dd {float: none; width: auto; display: inline-block}
.dl-inline>dt::after {content: ":\0020"; padding-right: .5ex}
.dl-inline>dt:not(:first-of-type) {padding-left: .5ex}
</style><dl class=dl-inline><dt>x</dt><dd>'character'</dd><dt>y</dt><dd>'integer'</dd><dt>z</dt><dd>'logical'</dd></dl>



#### Reading from a File


```R
dt <- fread("C:\\Users\\AB\\Desktop\\myfile.csv")
```


```R
dt
```


<table>
<caption>A data.table: 4 × 1</caption>
<thead>
	<tr><th scope=col>Nummer</th></tr>
	<tr><th scope=col>&lt;chr&gt;</th></tr>
</thead>
<tbody>
	<tr><td>072XXXXXX63</td></tr>
	<tr><td>072XXXXXX76</td></tr>
	<tr><td>07XXXXXX66 </td></tr>
	<tr><td>072XXXXXX4 </td></tr>
</tbody>
</table>



#### Modifying a Data Frame


```R
DF <- data.frame(x = letters[1:5], y = 1:5, z = (1:5) > 3)
# modification
setDT(DF)
```


```R
head(DF)
```


<table>
<caption>A data.table: 5 × 3</caption>
<thead>
	<tr><th scope=col>x</th><th scope=col>y</th><th scope=col>z</th></tr>
	<tr><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;lgl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>a</td><td>1</td><td>FALSE</td></tr>
	<tr><td>b</td><td>2</td><td>FALSE</td></tr>
	<tr><td>c</td><td>3</td><td>FALSE</td></tr>
	<tr><td>d</td><td>4</td><td> TRUE</td></tr>
	<tr><td>e</td><td>5</td><td> TRUE</td></tr>
</tbody>
</table>




```R
sapply(DF, class)
```


<style>
.dl-inline {width: auto; margin:0; padding: 0}
.dl-inline>dt, .dl-inline>dd {float: none; width: auto; display: inline-block}
.dl-inline>dt::after {content: ":\0020"; padding-right: .5ex}
.dl-inline>dt:not(:first-of-type) {padding-left: .5ex}
</style><dl class=dl-inline><dt>x</dt><dd>'character'</dd><dt>y</dt><dd>'integer'</dd><dt>z</dt><dd>'logical'</dd></dl>




```R
mat <- matrix(0, ncol = 10, nrow = 10)
```


```R
mat
```


<table>
<caption>A matrix: 10 × 10 of type dbl</caption>
<tbody>
	<tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
	<tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
	<tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
	<tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
	<tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
	<tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
	<tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
	<tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
	<tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
	<tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
</tbody>
</table>




```R
DT <- data.table(mat)
```


```R
DT
```


<table>
<caption>A data.table: 10 × 10</caption>
<thead>
	<tr><th scope=col>V1</th><th scope=col>V2</th><th scope=col>V3</th><th scope=col>V4</th><th scope=col>V5</th><th scope=col>V6</th><th scope=col>V7</th><th scope=col>V8</th><th scope=col>V9</th><th scope=col>V10</th></tr>
	<tr><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
	<tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
	<tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
	<tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
	<tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
	<tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
	<tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
	<tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
	<tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
	<tr><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td><td>0</td></tr>
</tbody>
</table>




```R
mtcars = data.table(mtcars) #
```


```R
head(mtcars)
```


<table>
<caption>A data.table: 6 × 11</caption>
<thead>
	<tr><th scope=col>mpg</th><th scope=col>cyl</th><th scope=col>disp</th><th scope=col>hp</th><th scope=col>drat</th><th scope=col>wt</th><th scope=col>qsec</th><th scope=col>vs</th><th scope=col>am</th><th scope=col>gear</th><th scope=col>carb</th></tr>
	<tr><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>21.0</td><td>6</td><td>160</td><td>110</td><td>3.90</td><td>2.620</td><td>16.46</td><td>0</td><td>1</td><td>4</td><td>4</td></tr>
	<tr><td>21.0</td><td>6</td><td>160</td><td>110</td><td>3.90</td><td>2.875</td><td>17.02</td><td>0</td><td>1</td><td>4</td><td>4</td></tr>
	<tr><td>22.8</td><td>4</td><td>108</td><td> 93</td><td>3.85</td><td>2.320</td><td>18.61</td><td>1</td><td>1</td><td>4</td><td>1</td></tr>
	<tr><td>21.4</td><td>6</td><td>258</td><td>110</td><td>3.08</td><td>3.215</td><td>19.44</td><td>1</td><td>0</td><td>3</td><td>1</td></tr>
	<tr><td>18.7</td><td>8</td><td>360</td><td>175</td><td>3.15</td><td>3.440</td><td>17.02</td><td>0</td><td>0</td><td>3</td><td>2</td></tr>
	<tr><td>18.1</td><td>6</td><td>225</td><td>105</td><td>2.76</td><td>3.460</td><td>20.22</td><td>1</td><td>0</td><td>3</td><td>1</td></tr>
</tbody>
</table>



#### Special Symbols in Data Table

##### .SD Symbol
.SD
.SD refers to the subset of the data.table for each group, excluding all columns used in by.
.SD along with lapply can be used to apply any function to multiple columns by group in a data.table
We will continue using the same built-in dataset, mtcars:

Mean of all columns in the dataset by number of cylinders, cyl:

```R
mtcars[ , lapply(.SD, mean), by = cyl]
```


<table>
<caption>A data.table: 3 × 11</caption>
<thead>
	<tr><th scope=col>cyl</th><th scope=col>mpg</th><th scope=col>disp</th><th scope=col>hp</th><th scope=col>drat</th><th scope=col>wt</th><th scope=col>qsec</th><th scope=col>vs</th><th scope=col>am</th><th scope=col>gear</th><th scope=col>carb</th></tr>
	<tr><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>6</td><td>19.74286</td><td>183.3143</td><td>122.28571</td><td>3.585714</td><td>3.117143</td><td>17.97714</td><td>0.5714286</td><td>0.4285714</td><td>3.857143</td><td>3.428571</td></tr>
	<tr><td>4</td><td>26.66364</td><td>105.1364</td><td> 82.63636</td><td>4.070909</td><td>2.285727</td><td>19.13727</td><td>0.9090909</td><td>0.7272727</td><td>4.090909</td><td>1.545455</td></tr>
	<tr><td>8</td><td>15.10000</td><td>353.1000</td><td>209.21429</td><td>3.229286</td><td>3.999214</td><td>16.77214</td><td>0.0000000</td><td>0.1428571</td><td>3.285714</td><td>3.500000</td></tr>
</tbody>
</table>


Apart from cyl, there are other categorical columns in the dataset such as vs, am, gear and carb. It doesn't really
make sense to take the mean of these columns. So let's exclude these columns. This is where .SDcols comes into
the picture.
#### .SDcols
.SDcols
.SDcols specifies the columns of the data.table that are included in .SD.
Mean of all columns (continuous columns) in the dataset by number of gears gear, and number of cylinders, cyl,
arranged by gear and cyl:

```R
cols_chosen <- c("mpg", "disp", "hp", "drat", "wt", "qsec")
```


```R
mtcars[order(gear, cyl), lapply(.SD, mean), by = .(gear, cyl), .SDcols = cols_chosen]
```


<table>
<caption>A data.table: 8 × 8</caption>
<thead>
	<tr><th scope=col>gear</th><th scope=col>cyl</th><th scope=col>mpg</th><th scope=col>disp</th><th scope=col>hp</th><th scope=col>drat</th><th scope=col>wt</th><th scope=col>qsec</th></tr>
	<tr><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>3</td><td>4</td><td>21.500</td><td>120.1000</td><td> 97.0000</td><td>3.700000</td><td>2.465000</td><td>20.0100</td></tr>
	<tr><td>3</td><td>6</td><td>19.750</td><td>241.5000</td><td>107.5000</td><td>2.920000</td><td>3.337500</td><td>19.8300</td></tr>
	<tr><td>3</td><td>8</td><td>15.050</td><td>357.6167</td><td>194.1667</td><td>3.120833</td><td>4.104083</td><td>17.1425</td></tr>
	<tr><td>4</td><td>4</td><td>26.925</td><td>102.6250</td><td> 76.0000</td><td>4.110000</td><td>2.378125</td><td>19.6125</td></tr>
	<tr><td>4</td><td>6</td><td>19.750</td><td>163.8000</td><td>116.5000</td><td>3.910000</td><td>3.093750</td><td>17.6700</td></tr>
	<tr><td>5</td><td>4</td><td>28.200</td><td>107.7000</td><td>102.0000</td><td>4.100000</td><td>1.826500</td><td>16.8000</td></tr>
	<tr><td>5</td><td>6</td><td>19.700</td><td>145.0000</td><td>175.0000</td><td>3.620000</td><td>2.770000</td><td>15.5000</td></tr>
	<tr><td>5</td><td>8</td><td>15.400</td><td>326.0000</td><td>299.5000</td><td>3.880000</td><td>3.370000</td><td>14.5500</td></tr>
</tbody>
</table>




```R
mtcars[ , lapply(.SD, mean), .SDcols = cols_chosen]
```


<table>
<caption>A data.table: 1 × 6</caption>
<thead>
	<tr><th scope=col>mpg</th><th scope=col>disp</th><th scope=col>hp</th><th scope=col>drat</th><th scope=col>wt</th><th scope=col>qsec</th></tr>
	<tr><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>20.09062</td><td>230.7219</td><td>146.6875</td><td>3.596563</td><td>3.21725</td><td>17.84875</td></tr>
</tbody>
</table>


Note:
It is not necessary to define cols_chosen beforehand. .SDcols can directly take column names
.SDcols can also directly take a vector of columnnumbers. In the above example this would be mtcars[ ,
lapply(.SD, mean), .SDcols = c(1,3:7)]
#### .N symbol
.N
.N is shorthand for the number of rows in a group.

```R
x <- mtcars[, .(count=.N), by=cyl]
```


```R
x
```


<table>
<caption>A data.table: 3 × 2</caption>
<thead>
	<tr><th scope=col>cyl</th><th scope=col>count</th></tr>
	<tr><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;int&gt;</th></tr>
</thead>
<tbody>
	<tr><td>6</td><td> 7</td></tr>
	<tr><td>4</td><td>11</td></tr>
	<tr><td>8</td><td>14</td></tr>
</tbody>
</table>


Editing entire columns
Use the := operator inside j to assign new columns:

```R
mtcars[, mpg_sq := mpg^2]
```
Remove columns by setting to NULL:

```R
mtcars[, mpg_sq := NULL]
```
Add multiple columns by using the := operator's multivariate format:

```R
mtcars[, `:=`(mpg_sq = mpg^2, wt_sqrt = sqrt(wt))]
```


```R
head(mtcars)
```


<table>
<caption>A data.table: 6 × 13</caption>
<thead>
	<tr><th scope=col>mpg</th><th scope=col>cyl</th><th scope=col>disp</th><th scope=col>hp</th><th scope=col>drat</th><th scope=col>wt</th><th scope=col>qsec</th><th scope=col>vs</th><th scope=col>am</th><th scope=col>gear</th><th scope=col>carb</th><th scope=col>mpg_sq</th><th scope=col>wt_sqrt</th></tr>
	<tr><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>21.0</td><td>6</td><td>160</td><td>110</td><td>3.90</td><td>2.620</td><td>16.46</td><td>0</td><td>1</td><td>4</td><td>4</td><td>441.00</td><td>1.618641</td></tr>
	<tr><td>21.0</td><td>6</td><td>160</td><td>110</td><td>3.90</td><td>2.875</td><td>17.02</td><td>0</td><td>1</td><td>4</td><td>4</td><td>441.00</td><td>1.695582</td></tr>
	<tr><td>22.8</td><td>4</td><td>108</td><td> 93</td><td>3.85</td><td>2.320</td><td>18.61</td><td>1</td><td>1</td><td>4</td><td>1</td><td>519.84</td><td>1.523155</td></tr>
	<tr><td>21.4</td><td>6</td><td>258</td><td>110</td><td>3.08</td><td>3.215</td><td>19.44</td><td>1</td><td>0</td><td>3</td><td>1</td><td>457.96</td><td>1.793042</td></tr>
	<tr><td>18.7</td><td>8</td><td>360</td><td>175</td><td>3.15</td><td>3.440</td><td>17.02</td><td>0</td><td>0</td><td>3</td><td>2</td><td>349.69</td><td>1.854724</td></tr>
	<tr><td>18.1</td><td>6</td><td>225</td><td>105</td><td>2.76</td><td>3.460</td><td>20.22</td><td>1</td><td>0</td><td>3</td><td>1</td><td>327.61</td><td>1.860108</td></tr>
</tbody>
</table>

#### References

