+++
title = "Overlay plots in R"
date = 2021-08-15
draft = false
tags = ["R Tutorial"]
categories = []
+++

## Overlay plots in R

### How to Overlay Line Plots
Let’s create a data set and overlay three-line plots in a single plot in R

```R
a1 = c(15, 41, 13, 17, 18)
b1 = c(17, 10, 12, 17, 21)
a2 = c(14, 31, 17, 19, 12)
b2 = c(17, 2, 18, 19, 22)
a3 = c(14, 16, 11, 14, 12)
b3 = c(13, 18, 19, 12, 14)
```


```R
plot(a1, b1, type='l', col='blue', lty = 1,pch=15,lwd=1)
```


![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/Aug_15_2021/output_4_0.png)



```R
plot(a1, b1, type='l', col='blue', lty = 1,pch=15,lwd=1)
lines(a2, b2, col='red', lty = 1,pch=19,lwd=1)
```


![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/Aug_15_2021/output_5_0.png)

Additional red line overlayed over blue line

```R
plot(a1, b1, type='l', col='blue', lty = 1,pch=15,lwd=1)
lines(a2, b2, col='red', lty = 1,pch=19,lwd=1)
lines(a3,b3, col='green',lty=1,pch=19,lwd=1)
```


![png](output_7_0.png)

Additional green line overlayed over blue and red lines

```R
plot(a1, b1, type='l', col='blue', lty = 1,pch=15,lwd=1)
lines(a2, b2, col='red', lty = 1,pch=19,lwd=1)
lines(a3,b3, col='green',lty=1,pch=19,lwd=1)
legend("bottomright", legend=c('Line 1', 'Line 2', 'Line 3'),col=c('blue', 'green', 'red'), lty=1,inset = c(0.1, 0.1))
```


![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/Aug_15_2021/output_9_0.png)

Legend overlayed over plot
### Beautiful overlayed plot


```R
# Create data:
a=c(1:5)
b=c(5,3,4,5,5)
c=c(4,5,4,3,1)
 
# Make a basic graph
plot( b~a , type="b" , bty="l" , xlab="value of a" , ylab="value of b" , col=rgb(0.2,0.4,0.1,0.7) , lwd=3 , pch=17 , ylim=c(1,5) )
lines(c ~a , col=rgb(0.8,0.4,0.1,0.7) , lwd=3 , pch=19 , type="b" )
 
# Add a legend
legend("bottomleft", 
  legend = c("Group 1", "Group 2"), 
  col = c(rgb(0.2,0.4,0.1,0.7), 
  rgb(0.8,0.4,0.1,0.7)), 
  pch = c(17,19), 
  bty = "n", 
  pt.cex = 2, 
  cex = 1.2, 
  text.col = "black", 
  horiz = F , 
  inset = c(0.1, 0.1))
```


![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/Aug_15_2021/output_12_0.png)


### Overlayed ggplot line plot


```R
library(ggplot2)
df1 <- data.frame(x = seq(2, 8, by = 2),
                  y = seq(30, 15, by = -5))
df2 <- data.frame(x = seq(2, 8, by = 2),
                  y = seq(12, 24, by = 4))

ggplot(df1, aes(x, log(y))) + 
   geom_line() +
   geom_line(data = df2, color = "red") # re-define data and overwrite top layer inheritance
```

    Warning message:
    “package ‘ggplot2’ was built under R version 4.1.0”



![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/Aug_15_2021/output_14_1.png)


### References
- https://www.r-graph-gallery.com/119-add-a-legend-to-a-plot.html