+++
title = "Molecular Descriptors calculation"
date = 2024-03-24
draft = false
tags = ["Chemistry"]
categories = []
+++

# Molecular Descriptors calculation

## Molecular Descriptors introduction

Molecular descriptors are a fundamental concept in cheminformatics, bioinformatics, and computational chemistry. They play a pivotal role in characterizing and quantifying the structural and physicochemical properties of molecules. These descriptors are crucial for various applications, including drug discovery, chemical informatics, predictive modeling, and property prediction.

### Definition of Molecular Descriptors:
Molecular descriptors, also known as chemical descriptors or molecular properties, are numerical or categorical representations of a molecule’s characteristics. These characteristics encompass a wide range of information, from the molecule’s size and shape to its chemical reactivity and electronic properties. Molecular descriptors provide a standardized and quantitative way to describe and compare molecules.

### Types of Molecular Descriptors:
Molecular descriptors can be categorized into several types based on the aspects of molecules they represent:

  - Constitutional Descriptors: These descriptors encode basic molecular information, such as the number of atoms, bonds, rings, and functional groups in a molecule. Examples include molecular weight, molecular formula, and the number of hydrogen bond acceptors and donors.

  - Topological Descriptors: Topological descriptors capture the connectivity of atoms in a molecule without considering bond lengths or angles. They are often used in graph-based representations of molecules. Examples include Wiener index, Randic index, and molecular branching indices.

  - Geometrical Descriptors: These descriptors focus on the three-dimensional (3D) geometry of molecules, including measures of molecular size, shape, and symmetry. Examples include molecular volume, surface area, and moments of inertia.

  - Electronic Descriptors: Electronic descriptors pertain to the electronic structure and properties of molecules. They include information about molecular orbitals, electron density, ionization potential, and electron affinity.

  - Quantum-Chemical Descriptors: These descriptors are derived from quantum-mechanical calculations and provide detailed information about a molecule's electronic and structural properties. Examples include HOMO (highest occupied molecular orbital) and LUMO (lowest unoccupied molecular orbital) energies.

  - Thermodynamic Descriptors: Thermodynamic descriptors relate to a molecule's thermodynamic stability and behavior under specific conditions. These include properties like Gibbs free energy, enthalpy, and entropy.

  - Spectroscopic Descriptors: Spectroscopic descriptors are derived from spectroscopic techniques such as nuclear magnetic  resonance (NMR) and infrared (IR) spectroscopy. They include parameters like chemical shifts and vibrational frequencies.

  - Hybrid Descriptors: Hybrid descriptors combine information from multiple sources or descriptor types to provide a more comprehensive representation of a molecule. For example, molecular docking scores, which combine geometric and energetic information, are hybrid descriptors used in molecular docking studies.

### Applications of Molecular Descriptors:

Molecular descriptors serve as valuable tools in various scientific and industrial applications:

  - Drug Discovery: In pharmaceutical research, molecular descriptors are used to predict the bioactivity, toxicity, and pharmacokinetic properties of potential drug candidates. Quantitative Structure-Activity Relationship (QSAR) models rely on molecular descriptors to correlate chemical structures with biological activities.

  - Cheminformatics: Molecular descriptors are essential for chemical database searching, compound library design, and virtual screening. They enable the identification of structurally similar compounds with desired properties.

  - Material Science: In materials research, descriptors aid in the design of new materials with specific properties, such as polymers, catalysts, and nanomaterials.

  - Environmental Chemistry: Molecular descriptors are employed to assess the environmental fate and toxicity of chemical compounds, aiding in risk assessment and regulatory compliance.

  - Computational Chemistry: Molecular descriptors are integral to molecular modeling, molecular dynamics simulations, and quantum  chemistry calculations. They provide input parameters and aid in the interpretation of simulation results.



### Available Molecular Descriptors software and tools:
There are several molecular descriptor calculation tools and software widely used in cheminformatics and computational chemistry. Some popular options include:

  - RDKit: An open-source toolkit for cheminformatics that provides a wide range of descriptor calculation functions. It is written in Python and offers a user-friendly interface.

  - Mordred : An open-source toolkit for cheminformatics that provides a wide range of descriptor calculation functions. It is written in Python and offers a user-friendly interface.

  - ChemPy: A Python library specifically designed for molecular descriptor calculation. It is lightweight and easy to integrate into various workflows.

  - PaDEL-Descriptor: A standalone software that calculates a comprehensive set of molecular descriptors. It offers a graphical user interface for ease of use.

  - CDK (Chemistry Development Kit): An open-source Java library for cheminformatics that includes descriptor calculation capabilities. It can be integrated into Java-based applications.

  - Dragon: A commercial software package known for its extensive set of molecular descriptors and advanced cheminformatics tools. It offers a user-friendly graphical interface.

  - MOE (Molecular Operating Environment): A comprehensive commercial software suite that includes molecular descriptor calculation among its many features. It is widely used in drug discovery and computational chemistry.

These tools and software packages cater to a range of user preferences, from open-source options like RDKit to comprehensive commercial solutions like MOE, providing flexibility and versatility for molecular descriptor calculations in various research contexts.

### Challenges and Advancements:

  - Despite their utility, molecular descriptors have limitations. They may not capture all relevant information about a molecule, and the choice of descriptors depends on the specific research question. Moreover, the sheer volume of possible descriptors for complex molecules can be overwhelming.

  - Advancements in machine learning and artificial intelligence have led to the development of descriptor selection techniques and automated feature engineering. These approaches help identify the most informative descriptors for a given prediction task, reducing dimensionality and improving model performance.

  - In conclusion, molecular descriptors are essential tools for characterizing and quantifying molecular properties. They find widespread use in drug discovery, materials science, computational chemistry, and various other fields. As computational methods and machine learning continue to advance, molecular descriptor-based approaches will remain indispensable for understanding and predicting the behavior of molecules in diverse applications.

# Calculating molecular descriptors using RDkit and Mordred 


```python
#!pip install rdkit-pypi
#!pip install mordred
```


```python
# https://www.rdkit.org/
#https://github.com/rdkit/rdkit
from rdkit.Chem import AllChem
from rdkit import Chem
from rdkit.Chem import Descriptors
from rdkit.ML.Descriptors import MoleculeDescriptors

# https://pandas.pydata.org
import pandas as pd

# https://numpy.org/doc/stable/release.html
import numpy as np

#https://github.com/mordred-descriptor/mordred
from mordred import Calculator, descriptors
```


```python
# import session_info
# session_info.show()
```


```python
url = "https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/01112020/Smiles_input_data.csv"
dataset = pd.read_csv(url)
dataset.shape
```




    (2904, 1)




```python
dataset.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>SMILES</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Cc1ccc(cc1)C(F)(F)F</td>
    </tr>
    <tr>
      <th>1</th>
      <td>OC(=O)CCCCl</td>
    </tr>
    <tr>
      <th>2</th>
      <td>CC(C)(Oc1ccc(CCNC(=O)c2ccc(Cl)cc2)cc1)C(=O)O</td>
    </tr>
    <tr>
      <th>3</th>
      <td>Nc1ccc(Cl)c(Cl)c1</td>
    </tr>
    <tr>
      <th>4</th>
      <td>C[C@@H](CCO)CCC=C(C)C</td>
    </tr>
  </tbody>
</table>
</div>



## 1. Generate canonical SMILES


```python
# There might be one or more valid SMILES that can represent one compound
# Thanks to Pat Walters for this information,checkout his excellent blog: https://www.blogger.com/profile/18223198920629617711
def canonical_smiles(smiles):
    mols = [Chem.MolFromSmiles(smi) for smi in smiles] 
    smiles = [Chem.MolToSmiles(mol) for mol in mols]
    return smiles
```


```python
from rdkit.Chem import Draw
from rdkit.Chem.Draw import IPythonConsole

Chem.MolFromSmiles('C=CCC')
```




    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/01112020/output_9_0.png)
    




```python
Chem.MolFromSmiles('CCC=C')
```




    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/01112020/output_10_0.png)
    




```python
canonical_smiles(['C=CCC'])
```




    ['C=CCC']




```python
canonical_smiles(['CCC=C'])
```




    ['C=CCC']




```python
# Canonical SMILES
Canon_SMILES = canonical_smiles(dataset.SMILES)
len(Canon_SMILES)
```




    2904




```python
# Put the smiles in the dataframe
dataset['SMILES'] = Canon_SMILES
dataset
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>SMILES</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Cc1ccc(C(F)(F)F)cc1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>O=C(O)CCCCl</td>
    </tr>
    <tr>
      <th>2</th>
      <td>CC(C)(Oc1ccc(CCNC(=O)c2ccc(Cl)cc2)cc1)C(=O)O</td>
    </tr>
    <tr>
      <th>3</th>
      <td>Nc1ccc(Cl)c(Cl)c1</td>
    </tr>
    <tr>
      <th>4</th>
      <td>CC(C)=CCC[C@@H](C)CCO</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
    </tr>
    <tr>
      <th>2899</th>
      <td>c1ccc(P(CCP(c2ccccc2)c2ccccc2)c2ccccc2)cc1</td>
    </tr>
    <tr>
      <th>2900</th>
      <td>Brc1cccc2sccc12</td>
    </tr>
    <tr>
      <th>2901</th>
      <td>CCOC(=O)N1c2ccccc2C=C[C@@H]1OCC</td>
    </tr>
    <tr>
      <th>2902</th>
      <td>c1ccc2sccc2c1</td>
    </tr>
    <tr>
      <th>2903</th>
      <td>Cc1cccc(C)c1O</td>
    </tr>
  </tbody>
</table>
<p>2904 rows × 1 columns</p>
</div>




```python
# Create a list for duplicate smiles
duplicates_smiles = dataset[dataset['SMILES'].duplicated()]['SMILES'].values
len(duplicates_smiles)
```




    31




```python
# Create a list for duplicate smiles
dataset[dataset['SMILES'].isin(duplicates_smiles)].sort_values(by=['SMILES'])
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>SMILES</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>218</th>
      <td>C1=CCCCCCC1</td>
    </tr>
    <tr>
      <th>2125</th>
      <td>C1=CCCCCCC1</td>
    </tr>
    <tr>
      <th>1901</th>
      <td>C1CC[C@H]2CCCC[C@@H]2C1</td>
    </tr>
    <tr>
      <th>552</th>
      <td>C1CC[C@H]2CCCC[C@@H]2C1</td>
    </tr>
    <tr>
      <th>554</th>
      <td>C=CN1CCCC1=O</td>
    </tr>
    <tr>
      <th>1808</th>
      <td>C=CN1CCCC1=O</td>
    </tr>
    <tr>
      <th>2203</th>
      <td>C=Cc1ccccc1</td>
    </tr>
    <tr>
      <th>1354</th>
      <td>C=Cc1ccccc1</td>
    </tr>
    <tr>
      <th>2341</th>
      <td>C=Cc1ccccc1</td>
    </tr>
    <tr>
      <th>1265</th>
      <td>C=Cc1ccncc1</td>
    </tr>
    <tr>
      <th>265</th>
      <td>C=Cc1ccncc1</td>
    </tr>
    <tr>
      <th>2400</th>
      <td>CC(C)(C)OC(=O)N1CCC(C(=O)O)CC1</td>
    </tr>
    <tr>
      <th>1621</th>
      <td>CC(C)(C)OC(=O)N1CCC(C(=O)O)CC1</td>
    </tr>
    <tr>
      <th>1184</th>
      <td>CC(C)C[C@@H](N)C(=O)O</td>
    </tr>
    <tr>
      <th>1759</th>
      <td>CC(C)C[C@@H](N)C(=O)O</td>
    </tr>
    <tr>
      <th>2324</th>
      <td>CC(C)I</td>
    </tr>
    <tr>
      <th>85</th>
      <td>CC(C)I</td>
    </tr>
    <tr>
      <th>2885</th>
      <td>CC(C)[C@@H](N)C(=O)O</td>
    </tr>
    <tr>
      <th>2408</th>
      <td>CC(C)[C@@H](N)C(=O)O</td>
    </tr>
    <tr>
      <th>21</th>
      <td>CC(C)[C@@H](N)C(=O)O</td>
    </tr>
    <tr>
      <th>1856</th>
      <td>CC(C)[C@@H](N)CO</td>
    </tr>
    <tr>
      <th>2813</th>
      <td>CC(C)[C@@H](N)CO</td>
    </tr>
    <tr>
      <th>1841</th>
      <td>CC(C)[C@@H]1CC[C@@H](C)C[C@H]1O</td>
    </tr>
    <tr>
      <th>1448</th>
      <td>CC(C)[C@@H]1CC[C@@H](C)C[C@H]1O</td>
    </tr>
    <tr>
      <th>1881</th>
      <td>CC1(C)[C@H]2CC=C(CO)[C@@H]1C2</td>
    </tr>
    <tr>
      <th>819</th>
      <td>CC1(C)[C@H]2CC=C(CO)[C@@H]1C2</td>
    </tr>
    <tr>
      <th>1635</th>
      <td>CCCCCCCC/C=C/CCCCCCCC(=O)O</td>
    </tr>
    <tr>
      <th>2310</th>
      <td>CCCCCCCC/C=C/CCCCCCCC(=O)O</td>
    </tr>
    <tr>
      <th>262</th>
      <td>CI</td>
    </tr>
    <tr>
      <th>853</th>
      <td>CI</td>
    </tr>
    <tr>
      <th>2061</th>
      <td>CO[C@H]1O[C@H](CO)[C@H](O)[C@H](O)[C@@H]1O</td>
    </tr>
    <tr>
      <th>1944</th>
      <td>CO[C@H]1O[C@H](CO)[C@H](O)[C@H](O)[C@@H]1O</td>
    </tr>
    <tr>
      <th>1918</th>
      <td>CSCC[C@@H](N)C(=O)O</td>
    </tr>
    <tr>
      <th>276</th>
      <td>CSCC[C@@H](N)C(=O)O</td>
    </tr>
    <tr>
      <th>11</th>
      <td>N[C@@H](CC(=O)O)C(=O)O</td>
    </tr>
    <tr>
      <th>1469</th>
      <td>N[C@@H](CC(=O)O)C(=O)O</td>
    </tr>
    <tr>
      <th>135</th>
      <td>N[C@@H](CCC(=O)O)C(=O)O</td>
    </tr>
    <tr>
      <th>475</th>
      <td>N[C@@H](CCC(=O)O)C(=O)O</td>
    </tr>
    <tr>
      <th>871</th>
      <td>N[C@@H](CO)C(=O)O</td>
    </tr>
    <tr>
      <th>1730</th>
      <td>N[C@@H](CO)C(=O)O</td>
    </tr>
    <tr>
      <th>1109</th>
      <td>N[C@@H](Cc1ccccc1)C(=O)O</td>
    </tr>
    <tr>
      <th>313</th>
      <td>N[C@@H](Cc1ccccc1)C(=O)O</td>
    </tr>
    <tr>
      <th>2540</th>
      <td>N[C@H]1CCCC[C@@H]1N</td>
    </tr>
    <tr>
      <th>2194</th>
      <td>N[C@H]1CCCC[C@@H]1N</td>
    </tr>
    <tr>
      <th>1361</th>
      <td>O=C(/C=C/c1ccccc1)c1ccccc1</td>
    </tr>
    <tr>
      <th>921</th>
      <td>O=C(/C=C/c1ccccc1)c1ccccc1</td>
    </tr>
    <tr>
      <th>643</th>
      <td>O=C(O)[C@H]1CCCN1</td>
    </tr>
    <tr>
      <th>254</th>
      <td>O=C(O)[C@H]1CCCN1</td>
    </tr>
    <tr>
      <th>1542</th>
      <td>O=C(O)[C@H]1CCCN1</td>
    </tr>
    <tr>
      <th>914</th>
      <td>O=C(O)c1ccccc1</td>
    </tr>
    <tr>
      <th>2011</th>
      <td>O=C(O)c1ccccc1</td>
    </tr>
    <tr>
      <th>2231</th>
      <td>O=C1C=CCCC1</td>
    </tr>
    <tr>
      <th>541</th>
      <td>O=C1C=CCCC1</td>
    </tr>
    <tr>
      <th>1183</th>
      <td>O=C1OC(=O)c2ncccc21</td>
    </tr>
    <tr>
      <th>2715</th>
      <td>O=C1OC(=O)c2ncccc21</td>
    </tr>
    <tr>
      <th>594</th>
      <td>OC[C@H]1O[C@H](O)[C@H](O)[C@H](O)[C@@H]1O</td>
    </tr>
    <tr>
      <th>2381</th>
      <td>OC[C@H]1O[C@H](O)[C@H](O)[C@H](O)[C@@H]1O</td>
    </tr>
    <tr>
      <th>1294</th>
      <td>c1cnc2[nH]ccc2c1</td>
    </tr>
    <tr>
      <th>1732</th>
      <td>c1cnc2[nH]ccc2c1</td>
    </tr>
  </tbody>
</table>
</div>



## 2.  Drop duplicate values


```python
dataset_new = dataset.drop_duplicates(subset=['SMILES'])
len(dataset_new)
```




    2873




```python
dataset_new
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>SMILES</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>Cc1ccc(C(F)(F)F)cc1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>O=C(O)CCCCl</td>
    </tr>
    <tr>
      <th>2</th>
      <td>CC(C)(Oc1ccc(CCNC(=O)c2ccc(Cl)cc2)cc1)C(=O)O</td>
    </tr>
    <tr>
      <th>3</th>
      <td>Nc1ccc(Cl)c(Cl)c1</td>
    </tr>
    <tr>
      <th>4</th>
      <td>CC(C)=CCC[C@@H](C)CCO</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
    </tr>
    <tr>
      <th>2899</th>
      <td>c1ccc(P(CCP(c2ccccc2)c2ccccc2)c2ccccc2)cc1</td>
    </tr>
    <tr>
      <th>2900</th>
      <td>Brc1cccc2sccc12</td>
    </tr>
    <tr>
      <th>2901</th>
      <td>CCOC(=O)N1c2ccccc2C=C[C@@H]1OCC</td>
    </tr>
    <tr>
      <th>2902</th>
      <td>c1ccc2sccc2c1</td>
    </tr>
    <tr>
      <th>2903</th>
      <td>Cc1cccc(C)c1O</td>
    </tr>
  </tbody>
</table>
<p>2873 rows × 1 columns</p>
</div>



## Calculate descriptors using RDkit

### a. General molecular descriptors-about 200 molecular descriptors


```python
list(Descriptors._descList)
```




```python
calc = MoleculeDescriptors.MolecularDescriptorCalculator([x[0] for x in Descriptors._descList])
```


```python
calc.GetDescriptorNames()
```



```python
def RDkit_descriptors(smiles):
    mols = [Chem.MolFromSmiles(i) for i in smiles] 
    calc = MoleculeDescriptors.MolecularDescriptorCalculator([x[0] for x in Descriptors._descList])
    desc_names = calc.GetDescriptorNames()
    
    Mol_descriptors =[]
    for mol in mols:
        # add hydrogens to molecules
        mol=Chem.AddHs(mol)
        # Calculate all 200 descriptors for each molecule
        descriptors = calc.CalcDescriptors(mol)
        Mol_descriptors.append(descriptors)
    return Mol_descriptors,desc_names 

# Function call
Mol_descriptors,desc_names = RDkit_descriptors(dataset_new['SMILES'])
```

```python
df_with_200_descriptors = pd.DataFrame(Mol_descriptors,columns=desc_names)
df_with_200_descriptors
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>MaxAbsEStateIndex</th>
      <th>MaxEStateIndex</th>
      <th>MinAbsEStateIndex</th>
      <th>MinEStateIndex</th>
      <th>qed</th>
      <th>SPS</th>
      <th>MolWt</th>
      <th>HeavyAtomMolWt</th>
      <th>ExactMolWt</th>
      <th>NumValenceElectrons</th>
      <th>...</th>
      <th>fr_sulfide</th>
      <th>fr_sulfonamd</th>
      <th>fr_sulfone</th>
      <th>fr_term_acetylene</th>
      <th>fr_tetrazole</th>
      <th>fr_thiazole</th>
      <th>fr_thiocyan</th>
      <th>fr_thiophene</th>
      <th>fr_unbrch_alkane</th>
      <th>fr_urea</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>12.550510</td>
      <td>12.550510</td>
      <td>1.008796</td>
      <td>-5.076351</td>
      <td>0.546828</td>
      <td>21.909091</td>
      <td>160.138</td>
      <td>153.082</td>
      <td>160.049985</td>
      <td>60</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>1</th>
      <td>10.676844</td>
      <td>10.676844</td>
      <td>1.840718</td>
      <td>-3.333333</td>
      <td>0.569323</td>
      <td>29.000000</td>
      <td>122.551</td>
      <td>115.495</td>
      <td>122.013457</td>
      <td>42</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2</th>
      <td>13.050084</td>
      <td>13.050084</td>
      <td>0.722809</td>
      <td>-4.111425</td>
      <td>0.790287</td>
      <td>24.520000</td>
      <td>361.825</td>
      <td>341.665</td>
      <td>361.108086</td>
      <td>132</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>3</th>
      <td>7.402685</td>
      <td>7.402685</td>
      <td>0.074321</td>
      <td>-0.449630</td>
      <td>0.582519</td>
      <td>16.888889</td>
      <td>162.019</td>
      <td>156.979</td>
      <td>160.979905</td>
      <td>48</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>4</th>
      <td>8.095237</td>
      <td>8.095237</td>
      <td>1.886963</td>
      <td>-4.484184</td>
      <td>0.606746</td>
      <td>50.909091</td>
      <td>156.269</td>
      <td>136.109</td>
      <td>156.151415</td>
      <td>66</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>2868</th>
      <td>9.505488</td>
      <td>9.505488</td>
      <td>0.973292</td>
      <td>-3.873136</td>
      <td>0.373065</td>
      <td>24.214286</td>
      <td>398.426</td>
      <td>374.234</td>
      <td>398.135324</td>
      <td>138</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2869</th>
      <td>7.651157</td>
      <td>7.651157</td>
      <td>0.017477</td>
      <td>-0.170718</td>
      <td>0.625891</td>
      <td>17.500000</td>
      <td>213.099</td>
      <td>208.059</td>
      <td>211.929533</td>
      <td>50</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2870</th>
      <td>13.017078</td>
      <td>13.017078</td>
      <td>0.325694</td>
      <td>-3.813937</td>
      <td>0.823664</td>
      <td>39.333333</td>
      <td>247.294</td>
      <td>230.158</td>
      <td>247.120843</td>
      <td>96</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2871</th>
      <td>7.592407</td>
      <td>7.592407</td>
      <td>0.030556</td>
      <td>-0.348333</td>
      <td>0.519376</td>
      <td>19.555556</td>
      <td>134.203</td>
      <td>128.155</td>
      <td>134.019021</td>
      <td>44</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>1</td>
      <td>0</td>
      <td>0</td>
    </tr>
    <tr>
      <th>2872</th>
      <td>7.522616</td>
      <td>7.522616</td>
      <td>0.821250</td>
      <td>-2.919676</td>
      <td>0.557704</td>
      <td>28.000000</td>
      <td>122.167</td>
      <td>112.087</td>
      <td>122.073165</td>
      <td>48</td>
      <td>...</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
      <td>0</td>
    </tr>
  </tbody>
</table>
<p>2873 rows × 210 columns</p>
</div>



### b. Fingerprints


```python
def morgan_fpts(data):
    Morgan_fpts = []
    for i in data:
        mol = Chem.MolFromSmiles(i) 
        fpts =  AllChem.GetMorganFingerprintAsBitVect(mol,2,2048)
        mfpts = np.array(fpts)
        Morgan_fpts.append(mfpts)  
    return np.array(Morgan_fpts)
```


```python
Morgan_fpts = morgan_fpts(dataset_new['SMILES'])
Morgan_fpts.shape
```




    (2873, 2048)




```python
Morgan_fingerprints = pd.DataFrame(Morgan_fpts,columns=['Col_{}'.format(i) for i in range(Morgan_fpts.shape[1])])
Morgan_fingerprints
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Col_0</th>
      <th>Col_1</th>
      <th>Col_2</th>
      <th>Col_3</th>
      <th>Col_4</th>
      <th>Col_5</th>
      <th>Col_6</th>
      <th>Col_7</th>
      <th>Col_8</th>
      <th>Col_9</th>
      <th>...</th>
      <th>Col_2038</th>
      <th>Col_2039</th>
      <th>Col_2040</th>
      <th>Col_2041</th>
      <th>Col_2042</th>
      <th>Col_2043</th>
      <th>Col_2044</th>
      <th>Col_2045</th>
      <th>Col_2046</th>
      <th>Col_2047</th>
    </tr>
  </thead>
</table>
<p>2873 rows × 2048 columns</p>
</div>



## Calculate descreptors using Mordred-1826 descriptors


```python
def All_Mordred_descriptors(data):
    calc = Calculator(descriptors, ignore_3D=False)
    mols = [Chem.MolFromSmiles(smi) for smi in data]
    
    # pandas df
    df = calc.pandas(mols)
    return df
```


```python
mordred_descriptors = All_Mordred_descriptors(dataset_new['SMILES'])
```



```python
mordred_descriptors.shape
```




    (2873, 1826)




```python
mordred_descriptors
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>ABC</th>
      <th>ABCGG</th>
      <th>nAcid</th>
      <th>nBase</th>
      <th>SpAbs_A</th>
      <th>SpMax_A</th>
      <th>SpDiam_A</th>
      <th>SpAD_A</th>
      <th>SpMAD_A</th>
      <th>LogEE_A</th>
      <th>SRW10</th>
      <th>TSRW10</th>
      <th>MW</th>
      <th>AMW</th>
      <th>WPath</th>
      <th>WPol</th>
      <th>Zagreb1</th>
      <th>Zagreb2</th>
      <th>mZagreb1</th>
      <th>mZagreb2</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>module 'numpy' has no attribute 'float'.\n`np....</td>
      <td>module 'numpy' has no attribute 'float'.\n`np....</td>
      <td>0</td>
      <td>0</td>
      <td>12.527341</td>
      <td>2.311476</td>
      <td>4.622953</td>
      <td>12.527341</td>
      <td>1.138849</td>
      <td>3.302522</td>
      <td>9.182249</td>
      <td>41.326257</td>
      <td>160.049985</td>
      <td>8.891666</td>
      <td>152</td>
      <td>13</td>
      <td>54.0</td>
      <td>59.0</td>
      <td>5.284722</td>
      <td>2.333333</td>
    </tr>
    <tr>
      <th>1</th>
      <td>1</td>
      <td>0</td>
      <td>7.727407</td>
      <td>1.931852</td>
      <td>3.863703</td>
      <td>7.727407</td>
      <td>1.103915</td>
      <td>2.752227</td>
      <td>7.321850</td>
      <td>31.336140</td>
      <td>122.013457</td>
      <td>8.715247</td>
      <td>52</td>
      <td>4</td>
      <td>24.0</td>
      <td>22.0</td>
      <td>3.861111</td>
      <td>1.833333</td>
    </tr>
