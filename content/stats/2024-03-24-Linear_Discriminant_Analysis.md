+++
title = "Linear Discriminant Analysis"
date = 2024-03-24
draft = false
tags = ["Linear Discriminant Analysis"]
categories = []
+++

## Introduction to Linear Discriminant Analysis (LDA)

Linear Discriminant Analysis is a method used for dimensionality reduction while preserving as much discriminatory information as possible to maintain class separability. It's important to note that despite its name, LDA is a classification algorithm, not a regression one.

LDA operates under the assumption that the data follows a Gaussian distribution and that each class shares the same covariance matrix. This assumption allows LDA to estimate the mean and covariance matrix for each class and subsequently determine the optimal linear decision boundary that best separates the classes.

### Key Concepts and Steps

- Mean Vectors: LDA begins by computing the mean vectors for each class in the dataset.

- Scatter Matrices: Next, LDA calculates the scatter matrices, which describe the spread and distribution of data within and between classes. These matrices are instrumental in determining the optimal decision boundary.

- Eigenvalue Decomposition: Through eigenvalue decomposition of the scatter matrices, LDA extracts the eigenvectors and eigenvalues. These eigenvectors represent the directions of maximum variance in the data.

- Projection: Finally, LDA projects the data onto a lower-dimensional subspace defined by the eigenvectors corresponding to the largest eigenvalues. This projection optimally separates the classes while minimizing overlap.


## LDA implementation in R


```R
#LOAD NECESSARY LIBRARIES
library(ggplot2)
library(MASS)
```


```R
attach(iris)
str(iris)
```

    'data.frame':	150 obs. of  5 variables:
     $ Sepal.Length: num  5.1 4.9 4.7 4.6 5 5.4 4.6 5 4.4 4.9 ...
     $ Sepal.Width : num  3.5 3 3.2 3.1 3.6 3.9 3.4 3.4 2.9 3.1 ...
     $ Petal.Length: num  1.4 1.4 1.3 1.5 1.4 1.7 1.4 1.5 1.4 1.5 ...
     $ Petal.Width : num  0.2 0.2 0.2 0.2 0.2 0.4 0.3 0.2 0.2 0.1 ...
     $ Species     : Factor w/ 3 levels "setosa","versicolor",..: 1 1 1 1 1 1 1 1 1 1 ...



```R
head(iris)
```


<table class="dataframe">
<caption>A data.frame: 6 × 5</caption>
<thead>
	<tr><th></th><th scope=col>Sepal.Length</th><th scope=col>Sepal.Width</th><th scope=col>Petal.Length</th><th scope=col>Petal.Width</th><th scope=col>Species</th></tr>
	<tr><th></th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;fct&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>1</th><td>5.1</td><td>3.5</td><td>1.4</td><td>0.2</td><td>setosa</td></tr>
	<tr><th scope=row>2</th><td>4.9</td><td>3.0</td><td>1.4</td><td>0.2</td><td>setosa</td></tr>
	<tr><th scope=row>3</th><td>4.7</td><td>3.2</td><td>1.3</td><td>0.2</td><td>setosa</td></tr>
	<tr><th scope=row>4</th><td>4.6</td><td>3.1</td><td>1.5</td><td>0.2</td><td>setosa</td></tr>
	<tr><th scope=row>5</th><td>5.0</td><td>3.6</td><td>1.4</td><td>0.2</td><td>setosa</td></tr>
	<tr><th scope=row>6</th><td>5.4</td><td>3.9</td><td>1.7</td><td>0.4</td><td>setosa</td></tr>
</tbody>
</table>


```R
nrow(iris)
```
150

Total 150 columns observed.


```R
ncol(iris)
```

5

Total 5 rows observed.


```R
names(iris)
```

<style>
.list-inline {list-style: none; margin:0; padding: 0}
.list-inline>li {display: inline-block}
.list-inline>li:not(:last-child)::after {content: "\00b7"; padding: 0 .5ex}
</style>
<ol class=list-inline><li>'Sepal.Length'</li><li>'Sepal.Width'</li><li>'Petal.Length'</li><li>'Petal.Width'</li><li>'Species'</li></ol>


```R
#Normalise data
iris[1:4] <- scale(iris[1:4])
```


```R
head(iris)
```


<table class="dataframe">
<caption>A data.frame: 6 × 5</caption>
<thead>
	<tr><th></th><th scope=col>Sepal.Length</th><th scope=col>Sepal.Width</th><th scope=col>Petal.Length</th><th scope=col>Petal.Width</th><th scope=col>Species</th></tr>
	<tr><th></th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;fct&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>1</th><td>-0.8976739</td><td> 1.01560199</td><td>-1.335752</td><td>-1.311052</td><td>setosa</td></tr>
	<tr><th scope=row>2</th><td>-1.1392005</td><td>-0.13153881</td><td>-1.335752</td><td>-1.311052</td><td>setosa</td></tr>
	<tr><th scope=row>3</th><td>-1.3807271</td><td> 0.32731751</td><td>-1.392399</td><td>-1.311052</td><td>setosa</td></tr>
	<tr><th scope=row>4</th><td>-1.5014904</td><td> 0.09788935</td><td>-1.279104</td><td>-1.311052</td><td>setosa</td></tr>
	<tr><th scope=row>5</th><td>-1.0184372</td><td> 1.24503015</td><td>-1.335752</td><td>-1.311052</td><td>setosa</td></tr>
	<tr><th scope=row>6</th><td>-0.5353840</td><td> 1.93331463</td><td>-1.165809</td><td>-1.048667</td><td>setosa</td></tr>
</tbody>
</table>




```R
apply(iris[1:4], 2, mean)
apply(iris[1:4], 2, sd)
```


<style>
.dl-inline {width: auto; margin:0; padding: 0}
.dl-inline>dt, .dl-inline>dd {float: none; width: auto; display: inline-block}
.dl-inline>dt::after {content: ":\0020"; padding-right: .5ex}
.dl-inline>dt:not(:first-of-type) {padding-left: .5ex}
</style><dl class=dl-inline><dt>Sepal.Length</dt><dd>-4.48431846126682e-16</dd><dt>Sepal.Width</dt><dd>2.03409428334532e-16</dd><dt>Petal.Length</dt><dd>-2.89532576155012e-17</dd><dt>Petal.Width</dt><dd>-3.66304931988711e-17</dd></dl>




<style>
.dl-inline {width: auto; margin:0; padding: 0}
.dl-inline>dt, .dl-inline>dd {float: none; width: auto; display: inline-block}
.dl-inline>dt::after {content: ":\0020"; padding-right: .5ex}
.dl-inline>dt:not(:first-of-type) {padding-left: .5ex}
</style><dl class=dl-inline><dt>Sepal.Length</dt><dd>1</dd><dt>Sepal.Width</dt><dd>1</dd><dt>Petal.Length</dt><dd>1</dd><dt>Petal.Width</dt><dd>1</dd></dl>




```R
#SPLIT DATA INTO TRAINING AND TESTING SET
set.seed(1)
sample <- sample(c(TRUE, FALSE), nrow(iris), replace=TRUE, prob=c(0.7,0.3))
train <- iris[sample, ]
test <- iris[!sample, ] 
```


```R
#FIT LDA MODEL
model <- lda(Species~., data=train)
```


```R
model
```


    Call:
    lda(Species ~ ., data = train)
    
    Prior probabilities of groups:
        setosa versicolor  virginica 
     0.3207547  0.3207547  0.3584906 
    
    Group means:
               Sepal.Length Sepal.Width Petal.Length Petal.Width
    setosa       -1.0397484   0.8131654   -1.2891006  -1.2570316
    versicolor    0.1820921  -0.6038909    0.3403524   0.2208153
    virginica     0.9582674  -0.1919146    1.0389776   1.1229172
    
    Coefficients of linear discriminants:
                        LD1        LD2
    Sepal.Length  0.7922820 -0.5294210
    Sepal.Width   0.5710586 -0.7130743
    Petal.Length -4.0762061  2.7305131
    Petal.Width  -2.0602181 -2.6326229
    
    Proportion of trace:
       LD1    LD2 
    0.9921 0.0079 



```R
#USE MODEL TO MAKE PREDICTIONS
predicted <- predict(model, test)
mean(predicted$class==test$Species)
```


1



```R
#sysfonts::font_add_google("Roboto Condensed")
# or add an arbitrary font with
#sysfonts::font_add("Roboto Condensed", regular = "RobotoCondensed-Regular.ttf")
```


```R
#VISUALIZE LINEAR DISCRIMINANTS
lda_plot <- cbind(train, predict(model)$x)
ggplot(lda_plot, aes(LD1, LD2)) +
  geom_point(aes(color = Species))
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/01112020/output_14_0.png)


### Applications of Linear Discriminant Analysis:

Linear Discriminant Analysis finds applications in various fields, including:

- **Pattern Recognition:** LDA is widely used for classifying patterns based on extracted features, making it valuable in image and speech recognition systems.

- **Biometrics:** In biometric systems, LDA aids in identifying individuals based on distinctive features while reducing computational complexity.

- **Finance:** LDA helps in portfolio optimization, credit scoring, and fraud detection by identifying patterns and anomalies in financial data.

- **Medical Diagnosis:** LDA assists in disease diagnosis by analyzing patient data and identifying key biomarkers indicative of specific conditions.

### Advantages of Linear Discriminant Analysis:

- **Dimensionality Reduction:** By projecting data onto a lower-dimensional subspace, LDA reduces the complexity of the problem while preserving discriminatory information.

- **Feature Extraction:** LDA automatically selects the most discriminative features, enhancing classification performance and interpretability.

- **Robustness to Overfitting:** LDA's simplicity and reliance on linear decision boundaries make it less prone to overfitting compared to more complex models.

- **Interpretability:** The linear nature of LDA facilitates interpretation, allowing practitioners to understand the underlying factors driving classification decisions.

### Conclusion:

Linear Discriminant Analysis stands as a powerful tool in the realm of classification and dimensionality reduction. Its ability to efficiently separate classes while minimizing information loss makes it indispensable in various real-world applications. As we continue to navigate the complexities of data analysis and machine learning, LDA remains a steadfast ally, illuminating the path towards insightful decision-making and discovery.


### References
- https://www.geeksforgeeks.org/ml-linear-discriminant-analysis/

## Reference
- https://www.geeksforgeeks.org/ml-linear-discriminant-analysis/