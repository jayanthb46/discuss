+++
title = "PCA VS TSNE"
date = 2021-07-04
draft = false
tags = ["PCA VS TSNE"]
categories = []
+++


## PCA VS TSNE  

### PCA

PCA is an unsupervised linear separation technique . It preserves the global geometry of the data. PCA transforms the data in higher dimension by  linear mapping of the data to a lower-dimensional space  in a manner such that variance of the data in the low-dimensional is maximized. 

PCA calculates the eigen vectors from covariance matrix.

### Data used for Analysis

we have used IRIS data set from kaggle. (https://www.kaggle.com/uciml/iris).

Breast cancer data set from wisconsin university available in kaggle (https://www.kaggle.com/uciml/breast-cancer-wisconsin-data)

### PCA implementation explaining multiple Steps


```python
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
```


```python
df1 = pd.read_csv('Iris.csv')
```


```python
df1.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Id</th>
      <th>SepalLengthCm</th>
      <th>SepalWidthCm</th>
      <th>PetalLengthCm</th>
      <th>PetalWidthCm</th>
      <th>Species</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>1</td>
      <td>5.1</td>
      <td>3.5</td>
      <td>1.4</td>
      <td>0.2</td>
      <td>Iris-setosa</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2</td>
      <td>4.9</td>
      <td>3.0</td>
      <td>1.4</td>
      <td>0.2</td>
      <td>Iris-setosa</td>
    </tr>
    <tr>
      <th>2</th>
      <td>3</td>
      <td>4.7</td>
      <td>3.2</td>
      <td>1.3</td>
      <td>0.2</td>
      <td>Iris-setosa</td>
    </tr>
    <tr>
      <th>3</th>
      <td>4</td>
      <td>4.6</td>
      <td>3.1</td>
      <td>1.5</td>
      <td>0.2</td>
      <td>Iris-setosa</td>
    </tr>
    <tr>
      <th>4</th>
      <td>5</td>
      <td>5.0</td>
      <td>3.6</td>
      <td>1.4</td>
      <td>0.2</td>
      <td>Iris-setosa</td>
    </tr>
  </tbody>
</table>
</div>




```python
df1 = df1.drop(columns=['Id'])
print(df1.shape)
df1.head(3)
```

    (150, 5)





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>SepalLengthCm</th>
      <th>SepalWidthCm</th>
      <th>PetalLengthCm</th>
      <th>PetalWidthCm</th>
      <th>Species</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>5.1</td>
      <td>3.5</td>
      <td>1.4</td>
      <td>0.2</td>
      <td>Iris-setosa</td>
    </tr>
    <tr>
      <th>1</th>
      <td>4.9</td>
      <td>3.0</td>
      <td>1.4</td>
      <td>0.2</td>
      <td>Iris-setosa</td>
    </tr>
    <tr>
      <th>2</th>
      <td>4.7</td>
      <td>3.2</td>
      <td>1.3</td>
      <td>0.2</td>
      <td>Iris-setosa</td>
    </tr>
  </tbody>
</table>
</div>




```python
#droping the target column
target = df1['Species']
data_x = df1.iloc[:,:-1]
```
#### Step 1: Normalize the data so that whole data will be in single scale

```python
from sklearn.preprocessing import StandardScaler
scale = StandardScaler()
data = scale.fit_transform(data_x.values)
data.shape
```




    (150, 4)


#### Step2 : Generate a Covariance matrix

```python
df_matrix = np.asmatrix(data)
print(df_matrix.shape)
```

    (150, 4)



```python
covar_matrix = np.cov(df_matrix.T)
print(covar_matrix.shape)
```

    (4, 4)

We got a covariance matrix of shape 4x4

#### Step3 : Compute eigen values and eigen vectors
Now, let us calculate the eigen values and eigen vectors of the covariance matrix. Eigen values are the indicators of the variance and Eigen vectors indicate the direction of the variance.

```python
eigvalues, eigvectors = np.linalg.eig(covar_matrix)
```
#### Step4 : Selecting components
Here we are selecting the top eigen vectors which represent component 1 and component 2 of the PCA.

```python
top2eig = eigvectors[:,0:2]
top2eig
```




    array([[ 0.52237162, -0.37231836],
           [-0.26335492, -0.92555649],
           [ 0.58125401, -0.02109478],
           [ 0.56561105, -0.06541577]])




```python
new_data = data_x.dot(top2eig)
#creating a new dataframe including target
new_df = pd.DataFrame(np.hstack((new_data,np.array(target).reshape(-1,1))),columns=['PC1','PC2','Species'])

new_df.head(10)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PC1</th>
      <th>PC2</th>
      <th>Species</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>2.66923</td>
      <td>-5.18089</td>
      <td>Iris-setosa</td>
    </tr>
    <tr>
      <th>1</th>
      <td>2.69643</td>
      <td>-4.64365</td>
      <td>Iris-setosa</td>
    </tr>
    <tr>
      <th>2</th>
      <td>2.48116</td>
      <td>-4.75218</td>
      <td>Iris-setosa</td>
    </tr>
    <tr>
      <th>3</th>
      <td>2.57151</td>
      <td>-4.62661</td>
      <td>Iris-setosa</td>
    </tr>
    <tr>
      <th>4</th>
      <td>2.59066</td>
      <td>-5.23621</td>
      <td>Iris-setosa</td>
    </tr>
    <tr>
      <th>5</th>
      <td>3.0081</td>
      <td>-5.68222</td>
      <td>Iris-setosa</td>
    </tr>
    <tr>
      <th>6</th>
      <td>2.49094</td>
      <td>-4.90871</td>
      <td>Iris-setosa</td>
    </tr>
    <tr>
      <th>7</th>
      <td>2.70145</td>
      <td>-5.05321</td>
      <td>Iris-setosa</td>
    </tr>
    <tr>
      <th>8</th>
      <td>2.46158</td>
      <td>-4.36493</td>
      <td>Iris-setosa</td>
    </tr>
    <tr>
      <th>9</th>
      <td>2.67166</td>
      <td>-4.73177</td>
      <td>Iris-setosa</td>
    </tr>
  </tbody>
</table>
</div>


Step5 : Plotting the data

```python
sns.scatterplot(new_df['PC1'],new_df['PC2'],hue=new_df['Species'])
plt.title('PCA-plot')
plt.show()
```


![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/04072021/output_20_0.png)


### PCA with Sklearn


```python
from sklearn.decomposition import PCA
pca  = PCA(n_components=2)
#here data is scaled data that we did earlier using standard scalar
pca_components = pca.fit_transform(data)
print(pca_components.shape)
```

    (150, 2)



```python
#creating a new dataframe including target
new_df_pca = pd.DataFrame(np.hstack((pca_components,np.array(target).reshape(-1,1))),columns=['1st_component','2nd_component','Species'])
new_df_pca.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>1st_component</th>
      <th>2nd_component</th>
      <th>Species</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>-2.26454</td>
      <td>0.505704</td>
      <td>Iris-setosa</td>
    </tr>
    <tr>
      <th>1</th>
      <td>-2.08643</td>
      <td>-0.655405</td>
      <td>Iris-setosa</td>
    </tr>
    <tr>
      <th>2</th>
      <td>-2.36795</td>
      <td>-0.318477</td>
      <td>Iris-setosa</td>
    </tr>
    <tr>
      <th>3</th>
      <td>-2.3042</td>
      <td>-0.575368</td>
      <td>Iris-setosa</td>
    </tr>
    <tr>
      <th>4</th>
      <td>-2.38878</td>
      <td>0.674767</td>
      <td>Iris-setosa</td>
    </tr>
  </tbody>
</table>
</div>




```python
sns.scatterplot(new_df_pca['1st_component'],new_df_pca['2nd_component'],hue=new_df_pca['Species'])
plt.title('Scatter-plot')
plt.show()
```


![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/04072021/output_24_0.png)


### TSNE 
t-Distributed Stochastic Neighbor Embedding(TSNE) is a supervised learning technique. 

It was introduced by Maaten and Hinton in 2008. It is a very useful dimensionality reduction technique and fairly better than the other techniques such as PCA. It has a very amazing ability to create two-dimensional maps from high dimension data.

TSNE is a non linear dimmension reduction technique to effectively map thousands of points in higher dimmension to lower dimensions based on probability that points are neighbours. TSNE technique will not preserve the global structure of the data. Advantage of this technique is that closely related samples are placed together irrespective of global geometry of the data.

TSNE is heavily used for visualization purpose and it is not used for qualitative analysis.

TSNE is an iterative algorithm and it depends on perplexity,number of iterations and learning rate.In bioinformatics TSNE is used in Single cell RNA seq data analysis.
### TSNE using sklearn


```python
from sklearn.manifold import TSNE

tsne = TSNE(n_components=2,perplexity=20,n_iter=1000)
tsne_result = tsne.fit_transform(data)

#creating a new dataframe including target
new_df_tsne = pd.DataFrame(np.hstack((tsne_result,np.array(target).reshape(-1,1))),columns=['1st_component','2nd_component','Species'])
new_df_tsne.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>1st_component</th>
      <th>2nd_component</th>
      <th>Species</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>6.76659</td>
      <td>31.082</td>
      <td>Iris-setosa</td>
    </tr>
    <tr>
      <th>1</th>
      <td>4.25417</td>
      <td>24.5466</td>
      <td>Iris-setosa</td>
    </tr>
    <tr>
      <th>2</th>
      <td>6.1137</td>
      <td>25.8241</td>
      <td>Iris-setosa</td>
    </tr>
    <tr>
      <th>3</th>
      <td>5.91717</td>
      <td>24.6729</td>
      <td>Iris-setosa</td>
    </tr>
    <tr>
      <th>4</th>
      <td>7.5741</td>
      <td>31.9241</td>
      <td>Iris-setosa</td>
    </tr>
  </tbody>
</table>
</div>




```python
sns.scatterplot(new_df_tsne['1st_component'],new_df_tsne['2nd_component'],hue=new_df_tsne['Species'])
plt.title('Scatter-plot')
plt.show()
```


![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/04072021/output_30_0.png)


#### Playing with Perplexity
Perplexity is a critical parameter that effects the visualization of groups in TSNE plot.
The Perplexity parameter can vary between 5 to 50. Higher the perplexity maximum separation of clusters.
Default value of the perplexity is 30.

Different perplexitys can capture different dimmensions of the data.

```python
tsne = TSNE(n_components=2,perplexity=30,n_iter=1000)
tsne_result = tsne.fit_transform(data)

#creating a new dataframe including target
new_df_tsne = pd.DataFrame(np.hstack((tsne_result,np.array(target).reshape(-1,1))),columns=['1st_component','2nd_component','Species'])
sns.scatterplot(new_df_tsne['1st_component'],new_df_tsne['2nd_component'],hue=new_df_tsne['Species'])
plt.title('Scatter-plot')
plt.show()
```


![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/04072021/output_33_0.png)



```python
tsne = TSNE(n_components=2,perplexity=40,n_iter=1000)
tsne_result = tsne.fit_transform(data)

#creating a new dataframe including target
new_df_tsne = pd.DataFrame(np.hstack((tsne_result,np.array(target).reshape(-1,1))),columns=['1st_component','2nd_component','Species'])
sns.scatterplot(new_df_tsne['1st_component'],new_df_tsne['2nd_component'],hue=new_df_tsne['Species'])
plt.title('Scatter-plot')
plt.show()
```


![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/04072021/output_34_0.png)



```python
tsne = TSNE(n_components=2,perplexity=50,n_iter=1000)
tsne_result = tsne.fit_transform(data)

#creating a new dataframe including target
new_df_tsne = pd.DataFrame(np.hstack((tsne_result,np.array(target).reshape(-1,1))),columns=['1st_component','2nd_component','Species'])
sns.scatterplot(new_df_tsne['1st_component'],new_df_tsne['2nd_component'],hue=new_df_tsne['Species'])
plt.title('Scatter-plot')
plt.show()
```


![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/04072021/output_35_0.png)



```python
tsne = TSNE(n_components=2,perplexity=100,n_iter=1000)
tsne_result = tsne.fit_transform(data)

#creating a new dataframe including target
new_df_tsne = pd.DataFrame(np.hstack((tsne_result,np.array(target).reshape(-1,1))),columns=['1st_component','2nd_component','Species'])
sns.scatterplot(new_df_tsne['1st_component'],new_df_tsne['2nd_component'],hue=new_df_tsne['Species'])
plt.title('Scatter-plot')
plt.show()
```


![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/04072021/output_36_0.png)

With Perplexity values ranging 20 to 50 for IRIS data set clustering pattern between setosa and versicolor remained largely identical . The perplexity value of 100 returned higher degree of separation between setosa and versicolor returned with multiple clusters due to inclusion of higher percentage of local interactions.
As discussed above higher the perplexity higher the clusters between groups.

### PCA vs TSNE comparison using Breast cancer data set

Breast cancer data set is a binary classification data set used to explain differences between PCA and TSNE.

```python
df2 = pd.read_csv('data.csv')
```


```python
df2.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>diagnosis</th>
      <th>radius_mean</th>
      <th>texture_mean</th>
      <th>perimeter_mean</th>
      <th>area_mean</th>
      <th>smoothness_mean</th>
      <th>compactness_mean</th>
      <th>concavity_mean</th>
      <th>concave points_mean</th>
      <th>...</th>
      <th>texture_worst</th>
      <th>perimeter_worst</th>
      <th>area_worst</th>
      <th>smoothness_worst</th>
      <th>compactness_worst</th>
      <th>concavity_worst</th>
      <th>concave points_worst</th>
      <th>symmetry_worst</th>
      <th>fractal_dimension_worst</th>
      <th>Unnamed: 32</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>842302</td>
      <td>M</td>
      <td>17.99</td>
      <td>10.38</td>
      <td>122.80</td>
      <td>1001.0</td>
      <td>0.11840</td>
      <td>0.27760</td>
      <td>0.3001</td>
      <td>0.14710</td>
      <td>...</td>
      <td>17.33</td>
      <td>184.60</td>
      <td>2019.0</td>
      <td>0.1622</td>
      <td>0.6656</td>
      <td>0.7119</td>
      <td>0.2654</td>
      <td>0.4601</td>
      <td>0.11890</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>1</th>
      <td>842517</td>
      <td>M</td>
      <td>20.57</td>
      <td>17.77</td>
      <td>132.90</td>
      <td>1326.0</td>
      <td>0.08474</td>
      <td>0.07864</td>
      <td>0.0869</td>
      <td>0.07017</td>
      <td>...</td>
      <td>23.41</td>
      <td>158.80</td>
      <td>1956.0</td>
      <td>0.1238</td>
      <td>0.1866</td>
      <td>0.2416</td>
      <td>0.1860</td>
      <td>0.2750</td>
      <td>0.08902</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2</th>
      <td>84300903</td>
      <td>M</td>
      <td>19.69</td>
      <td>21.25</td>
      <td>130.00</td>
      <td>1203.0</td>
      <td>0.10960</td>
      <td>0.15990</td>
      <td>0.1974</td>
      <td>0.12790</td>
      <td>...</td>
      <td>25.53</td>
      <td>152.50</td>
      <td>1709.0</td>
      <td>0.1444</td>
      <td>0.4245</td>
      <td>0.4504</td>
      <td>0.2430</td>
      <td>0.3613</td>
      <td>0.08758</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>3</th>
      <td>84348301</td>
      <td>M</td>
      <td>11.42</td>
      <td>20.38</td>
      <td>77.58</td>
      <td>386.1</td>
      <td>0.14250</td>
      <td>0.28390</td>
      <td>0.2414</td>
      <td>0.10520</td>
      <td>...</td>
      <td>26.50</td>
      <td>98.87</td>
      <td>567.7</td>
      <td>0.2098</td>
      <td>0.8663</td>
      <td>0.6869</td>
      <td>0.2575</td>
      <td>0.6638</td>
      <td>0.17300</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>4</th>
      <td>84358402</td>
      <td>M</td>
      <td>20.29</td>
      <td>14.34</td>
      <td>135.10</td>
      <td>1297.0</td>
      <td>0.10030</td>
      <td>0.13280</td>
      <td>0.1980</td>
      <td>0.10430</td>
      <td>...</td>
      <td>16.67</td>
      <td>152.20</td>
      <td>1575.0</td>
      <td>0.1374</td>
      <td>0.2050</td>
      <td>0.4000</td>
      <td>0.1625</td>
      <td>0.2364</td>
      <td>0.07678</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
<p>5 rows × 33 columns</p>
</div>




```python
data = df2.copy
```
From a quick look at the data, we can observe few things. Our target column, diagnosis is in a non-numeric format, therefore if we need to carry out any visualisations, we will probably need to convert the strings to numeric values. Second point, there exists Null values in the dataframe and we need to get rid of that. Furthermore, the id column is probably irrelevant in our visualisation endeavours so we can get rid of that as well.

```python
# Drop the id column
del df2['id']
# Convert the diagnosis column to numeric format
df2['diagnosis'] = df2['diagnosis'].factorize()[0]
# Fill all Null values with zero
df2 = df2.fillna(value=0)
# Store the diagnosis column in a target object and then drop it
target = df2['diagnosis']
data = df2.drop('diagnosis', axis=1)
```

### Normalizing data


```python
X = df2.values
```


```python
X
```




    array([[ 0.     , 17.99   , 10.38   , ...,  0.4601 ,  0.1189 ,  0.     ],
           [ 0.     , 20.57   , 17.77   , ...,  0.275  ,  0.08902,  0.     ],
           [ 0.     , 19.69   , 21.25   , ...,  0.3613 ,  0.08758,  0.     ],
           ...,
           [ 0.     , 16.6    , 28.08   , ...,  0.2218 ,  0.0782 ,  0.     ],
           [ 0.     , 20.6    , 29.33   , ...,  0.4087 ,  0.124  ,  0.     ],
           [ 1.     ,  7.76   , 24.54   , ...,  0.2871 ,  0.07039,  0.     ]])




```python
from sklearn.preprocessing import StandardScaler
X_std = StandardScaler().fit_transform(X)
```

#### Running PCA and TSNE on cancer data set


```python
pca = PCA(n_components=2)
pca_2d_std = pca.fit_transform(X_std)

# Invoke the TSNE method
tsne = TSNE(n_components=2, verbose=1, perplexity=40, n_iter=2000)
tsne_results_std = tsne.fit_transform(X_std)
```

    [t-SNE] Computing 121 nearest neighbors...
    [t-SNE] Indexed 569 samples in 0.005s...
    [t-SNE] Computed neighbors for 569 samples in 0.054s...
    [t-SNE] Computed conditional probabilities for sample 569 / 569
    [t-SNE] Mean sigma: 1.547354
    [t-SNE] KL divergence after 250 iterations with early exaggeration: 64.204811
    [t-SNE] KL divergence after 1850 iterations: 0.827410



```python
# Plot the TSNE and PCA visuals side-by-side
plt.figure(figsize = (16,11))
plt.subplot(121)
plt.scatter(pca_2d_std[:,0],pca_2d_std[:,1], c = target, 
            cmap = "RdYlGn", edgecolor = "None", alpha=0.35)
plt.colorbar()
plt.title('PCA Scatter Plot')
plt.subplot(122)
plt.scatter(tsne_results_std[:,0],tsne_results_std[:,1],  c = target, 
            cmap = "RdYlGn", edgecolor = "None", alpha=0.35)
plt.colorbar()
plt.title('TSNE Scatter Plot')
plt.show()
```


![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/04072021/output_50_0.png)

In TSNE we are able to achieve clear demarcation between Malignant and Tumor columns.

### References
1. YouTube. (2013, November 6). Visualizing Data Using t-SNE [Video File]. Retrieved from https://www.youtube.com/watch?v=RJVL80Gg3lA
2. L.J.P. van der Maaten and G.E. Hinton. Visualizing High-Dimensional Data Using t-SNE. Journal of Machine Learning Research 9(Nov):2579–2605, 2008.
