
+++
title = "Standard Error in R"
date = 2021-08-08
draft = false
tags = ["R Tutorial"]
categories = []
+++

## Introduction to Standard Error 
Standard error function is provided by plotrix library in R. The standard error is standard deviation divided by the square root of the sample size.Standard Deviation and Standard Error are measures of Dispersion. Standanda Deviation measures the distribution of entire population . standard Error measures the distribution of sample statistic. 

Standard Error is generally associated with statistics such as Mean and Median. If Standard Error is associated with Mean. Then it is called Standard Error of the Mean (SEM).

The Standard Error of the Mean represents how sample Mean deviates from population Mean. 

Higher the standard error the statistic associated with the  standard error are more diverse it becomes more likely that any given statistic is an inaccurate representation of the true population statistic.

The Standard error depends on sample size. Higher the sample size lower the Standard error. 

In other words, the larger your sample size, the closer your sample mean is to the actual population mean. 

### Standard Error in R.


```R
set.seed(1)

data <- rnorm(100)

print(data)
se <- sd(data) / sqrt(100)

cat("The Standard Error is: ", se)
```

      [1] -0.626453811  0.183643324 -0.835628612  1.595280802  0.329507772
      [6] -0.820468384  0.487429052  0.738324705  0.575781352 -0.305388387
     [11]  1.511781168  0.389843236 -0.621240581 -2.214699887  1.124930918
     [16] -0.044933609 -0.016190263  0.943836211  0.821221195  0.593901321
     [21]  0.918977372  0.782136301  0.074564983 -1.989351696  0.619825748
     [26] -0.056128740 -0.155795507 -1.470752384 -0.478150055  0.417941560
     [31]  1.358679552 -0.102787727  0.387671612 -0.053805041 -1.377059557
     [36] -0.414994563 -0.394289954 -0.059313397  1.100025372  0.763175748
     [41] -0.164523596 -0.253361680  0.696963375  0.556663199 -0.688755695
     [46] -0.707495157  0.364581962  0.768532925 -0.112346212  0.881107726
     [51]  0.398105880 -0.612026393  0.341119691 -1.129363096  1.433023702
     [56]  1.980399899 -0.367221476 -1.044134626  0.569719627 -0.135054604
     [61]  2.401617761 -0.039240003  0.689739362  0.028002159 -0.743273209
     [66]  0.188792300 -1.804958629  1.465554862  0.153253338  2.172611670
     [71]  0.475509529 -0.709946431  0.610726353 -0.934097632 -1.253633400
     [76]  0.291446236 -0.443291873  0.001105352  0.074341324 -0.589520946
     [81] -0.568668733 -0.135178615  1.178086997 -1.523566800  0.593946188
     [86]  0.332950371  1.063099837 -0.304183924  0.370018810  0.267098791
     [91] -0.542520031  1.207867806  1.160402616  0.700213650  1.586833455
     [96]  0.558486426 -1.276592208 -0.573265414 -1.224612615 -0.473400636
    The Standard Error is:  0.08981994
In the above example

`rnorm(100)` is used to generate 100 pseudo random numbers. 

The standard deviation is calculated by `sd` function in R. `sqrt(100)` is used to calculate the square root of sample size which is 100 in this example.
### Standard Error calculation using plotrix library 


```R
library("plotrix")

op <- std.error(c(11, 21, 19, 46))

print(op)

print(op)
```

    [1] 7.564996
    [1] 7.564996

### Reference

- https://www.statisticshowto.com/probability-and-statistics/statistics-definitions/what-is-the-standard-error-of-a-sample/
- https://jtr13.github.io/cc19/plotrix-for-complex-visualizations.html