+++
title = "Kruskal Wallis test in R"
date = 2024-03-10
draft = false
tags = ["R"]
categories = []
+++

## Kruskal-Wallis test in R

### Introduction

The Kruskal-Wallis test is a non-parametric statistical test used to determine if there are statistically significant differences between the medians of three or more independent groups. It’s particularly useful when the assumptions of one-way ANOVA are not met, such as when the data is not normally distributed or when dealing with outliers.

### Data

Data for the present article is based on the `penguins` dataset (an alternative to the well-known `iris` dataset), accessible via the `{palmerpenguins}` package:


```R
library(palmerpenguins)
```

    Warning message:
    "package 'palmerpenguins' was built under R version 4.3.3"
    

The original dataset contains data for 344 penguins of 3 different species (Adelie, Chinstrap and Gentoo).

It contains 8 variables, but we focus only on the flipper length and the species for this article, so we keep only those 2 variables:

```R
library(tidyverse)
```


```R
head(penguins)
```


<table class="dataframe">
<caption>A tibble: 6 × 8</caption>
<thead>
	<tr><th scope=col>species</th><th scope=col>island</th><th scope=col>bill_length_mm</th><th scope=col>bill_depth_mm</th><th scope=col>flipper_length_mm</th><th scope=col>body_mass_g</th><th scope=col>sex</th><th scope=col>year</th></tr>
	<tr><th scope=col>&lt;fct&gt;</th><th scope=col>&lt;fct&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;fct&gt;</th><th scope=col>&lt;int&gt;</th></tr>
</thead>
<tbody>
	<tr><td>Adelie</td><td>Torgersen</td><td>39.1</td><td>18.7</td><td>181</td><td>3750</td><td>male  </td><td>2007</td></tr>
	<tr><td>Adelie</td><td>Torgersen</td><td>39.5</td><td>17.4</td><td>186</td><td>3800</td><td>female</td><td>2007</td></tr>
	<tr><td>Adelie</td><td>Torgersen</td><td>40.3</td><td>18.0</td><td>195</td><td>3250</td><td>female</td><td>2007</td></tr>
	<tr><td>Adelie</td><td>Torgersen</td><td>  NA</td><td>  NA</td><td> NA</td><td>  NA</td><td>NA    </td><td>2007</td></tr>
	<tr><td>Adelie</td><td>Torgersen</td><td>36.7</td><td>19.3</td><td>193</td><td>3450</td><td>female</td><td>2007</td></tr>
	<tr><td>Adelie</td><td>Torgersen</td><td>39.3</td><td>20.6</td><td>190</td><td>3650</td><td>male  </td><td>2007</td></tr>
</tbody>
</table>

If you are unfamiliar with the pipe operator (`%>%`), you can also select variables with `penguins[, c("species", "flipper_length_mm")]`.


```R
dat <- penguins %>%
  select(species, flipper_length_mm)
```





```R
summary(dat)
```


          species    flipper_length_mm
     Adelie   :152   Min.   :172.0    
     Chinstrap: 68   1st Qu.:190.0    
     Gentoo   :124   Median :197.0    
                     Mean   :200.9    
                     3rd Qu.:213.0    
                     Max.   :231.0    
                     NA's   :2        



```R
library(doBy)
summaryBy(flipper_length_mm~species, data=dat,
          FUN = median,
          na.rm = TRUE)
```

    Warning message:
    "package 'doBy' was built under R version 4.3.3"
    
    Attaching package: 'doBy'
    
    
    The following object is masked from 'package:dplyr':
    
        order_by
    
    
    


<table class="dataframe">
<caption>A tibble: 3 × 2</caption>
<thead>
	<tr><th scope=col>species</th><th scope=col>flipper_length_mm.median</th></tr>
	<tr><th scope=col>&lt;fct&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>Adelie   </td><td>190</td></tr>
	<tr><td>Chinstrap</td><td>196</td></tr>
	<tr><td>Gentoo   </td><td>216</td></tr>
</tbody>
</table>




```R
ggplot(dat) +
  aes(x = species, y = flipper_length_mm, fill = species) +
  geom_boxplot() +
  theme(legend.position = "none")
```

  


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/01112020/output_6_1.png)
    
Based on the boxplots and the summary statistics, we already see that, in our sample penguins from the Adelie species seem to have the smallest flippers, while those from the Gentoo species seem to have the biggest flippers. However, only a sound statistical test will tell us the answer.

### Kruskal-Wallis test

### Aim and hypotheses

As mentioned earlier, the Kruskal-Wallis test allows to compare three or more groups. More precisely, it is used to compare three or more groups.

It can be seen as the extension to the Mann-Whitney test which allows to compare 2 groups under the non-normality assumption.

In the context of our example, we are going to use the Kruskal-Wallis test to help us answer the following question: "Is the length of the flippers different between the 3 species of penguins?".

The null and alternative hypotheses of the Kruskal-Wallis test are:

* H_0: The 3 species are equal in terms of flipper length
* H_1: At least one species is different from the other 2 species in terms of flipper length

Be careful that, as for the ANOVA, the alternative hypothesis is ***not*** that all species are different in terms of flipper length. The opposite of all species being equal ($H_0$) is that *at least* one species is different from the others ($H_1$).

In this sense, if the null hypothesis is rejected, it means that at least one species is different from the other 2, but not necessarily that all 3 species are different from each other. It could be that flipper length for the species Gentoo is different than for the species Chinstrap and Adelie, but flipper length is similar between Chinstrap and Adelie. Other types of test (known as post-hoc tests and covered later) must be performed to test whether all 3 species differ.

### Assumptions

First, the Kruskal-Wallis test compares several groups in terms of a quantitative variable. So there must be one quantitative dependent variable (which corresponds to the measurements to which the question relates) and one qualitative independent variable (with at least 2 levels which will determine the groups to compare).

Note that in theory, Kruskal-Wallis test can also be used for only two groups. However, in practice we use the Mann-Whitney test for two groups and Kruskal-Wallis for three or more groups.

Second, remember that the Kruskal-Wallis test is a nonparametric test, so the **normality assumption is not required**. However, the **independence assumption still holds**.

This means that the data, collected from a representative and randomly selected portion of the total population, should be independent between groups and within each group. The assumption of independence is most often verified based on the design of the experiment and on the good control of experimental conditions rather than via a formal test. If you are still unsure about independence based on the experiment design, ask yourself if one observation is related to another (if one observation has an impact on another) within each group or between the groups themselves. If not, it is most likely that you have independent samples. If observations between samples (forming the different groups to be compared) are dependent (for example, if three measurements have been collected on the **same individuals** as it is often the case in medical studies when measuring a metric (i) before, (ii) during and (iii) after a treatment), the Friedman test should be preferred in order to take into account the dependency between the samples.

Regarding the homoscedasticity (i.e., equality of the variances): As long as you use the Kruskal-Wallis test to, *in fine*, compare groups, homoscedasticity is not required. If you wish to compare medians, the Kruskal-Wallis test requires homoscedasticity.^[See more information about the difference in this [article](https://influentialpoints.com/Training/Kruskal-Wallis_ANOVA_use_and_misuse.htm){target="_blank"}.]

In our example, independence is assumed and we do not need to compare medians (we are only interested in comparing groups), so we can proceed to how to do the test in R. Note that the normality assumption may or may not hold, but for this article we assume it is *not* satisfied.

### In R

The Kruskal-Wallis test in R can be done with the `kruskal.test()` function:

```R
kruskal.test(flipper_length_mm ~ species,
  data = dat)
```


    
    	Kruskal-Wallis rank sum test
    
    data:  flipper_length_mm by species
    Kruskal-Wallis chi-squared = 244.89, df = 2, p-value < 2.2e-16
    
The most important result in this output is the [*p*-value]

### Interpretations

Based on the Kruskal-Wallis test, we reject the null hypothesis and we conclude that at least one species is different in terms of flippers length (*p*-value < 0.001).

(*For the sake of illustration*, if the *p*-value was larger than the significance level $\alpha = 0.05$: we cannot reject the null hypothesis so we cannot reject the hypothesis that the 3 considered species of penguins are equal in terms of flippers length.)


### Post-hoc tests

We have just showed that at least one species is different from the others in terms of flippers length. Nonetheless, here comes the limitations of the Kruskal-Wallis test: it does not say which group(s) is(are) different from the others.

To know this, we need to use other types of test, referred as post-hoc tests (in Latin, "after this", so after obtaining statistically significant Kruskal-Wallis results) or multiple pairwise-comparison tests. For the interested reader, a more detailed explanation of post-hoc tests can be found [here](/blog/anova-in-r/#post-hoc-test).

The most common post-hoc tests after a significant Kruskal-Wallis test are:

- Dunn test
- Conover test
- Nemenyi test
- Pairwise Wilcoxont test

The Dunn test being the most common one, here is how to do it in R.^[Note that there are other *p*-value adjustment methods. See `?dunnTest` for more options.]

### Dunn test

```R
library(FSA)

dunnTest(flipper_length_mm ~ species,
              data=dat,
              method="holm")
```

    Warning message:
    "package 'FSA' was built under R version 4.3.3"
    ## FSA v0.9.5. See citation('FSA') if used in publication.
    ## Run fishR() for related website and fishR('IFAR') for related book.
    
    Warning message:
    "Some rows deleted from 'x' and 'g' because missing data."
    Dunn (1964) Kruskal-Wallis multiple comparison
    
      p-values adjusted with the Holm method.
    
    
    


              Comparison          Z      P.unadj        P.adj
    1 Adelie - Chinstrap  -3.629336 2.841509e-04 2.841509e-04
    2    Adelie - Gentoo -15.476612 4.990733e-54 1.497220e-53
    3 Chinstrap - Gentoo  -8.931938 4.186100e-19 8.372200e-19



It is the last column (the adjusted *p*-values, adjusted for multiple comparisons) that is of interest. These *p*-values should be compared to your desired [significance level](/blog/student-s-t-test-in-r-and-by-hand-how-to-compare-two-groups-under-different-scenarios/#a-note-on-p-value-and-significance-level-alpha) (usually 5%).

Based on the output, we conclude that:

- Adelie and Chinstrap differ significantly (p < 0.001)
- Adelie and Gentoo differ significantly (p < 0.001)
- Chinstrap and Gentoo differ significantly (p < 0.001)

Therefore, based on the Dunn test, we can now conclude that **all 3 species differ in terms of flipper length**.


### Combination of statistical results and plot

A very good alternative for performing a Kruskal-Wallis and the post-hoc tests in R is with the `ggbetweenstats()` function from the `{ggstatsplot}` package:


```R
library(ggstatsplot)
```

    Warning message:
    "package 'ggstatsplot' was built under R version 4.3.3"
    You can cite this package as:
         Patil, I. (2021). Visualizations with statistical details: The 'ggstatsplot' approach.
         Journal of Open Source Software, 6(61), 3167, doi:10.21105/joss.03167
    
    


```R
ggbetweenstats(
  data = dat,
  x = species,
  y = flipper_length_mm,
  type = "nonparametric", # ANOVA or Kruskal-Wallis
  plot.type = "box",
  pairwise.comparisons = TRUE,
  pairwise.display = "significant",
  centrality.plotting = FALSE,
  bf.message = FALSE
)
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/01112020/output_10_0.png)
    
This method has the advantage that all necessary statistical results are displayed directly on the plot.

The results of the Kruskal-Wallis test are shown in the subtitle above the plot (the *p*-value is after `p =`). Moreover, the results of the post-hoc test are displayed between each group via accolades, and the boxplots allow to visualize the distribution for each species.

## Summary

In this post, we reviewed the aim and hypotheses of the Kruskal-Wallis test and its underlying assumptions. We then showed how to do the test in R and how to interpret the results.

We also showed the most common post-hoc test after a significant Kruskal-Wallis test---the Dunn test.

Last but not least, we presented a concise way to present both the data by group and all the statistical results on the same plot.

Thanks for reading.

As always, if you have a question or a suggestion related to the topic covered in this article, please add it as a comment so other readers can benefit from the discussion.

### References

- https://statistics.laerd.com/spss-tutorials/kruskal-wallis-h-test-using-spss-statistics.php