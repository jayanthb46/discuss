+++
title = "PCA analysis in R with explanation"
date = 2023-09-24
draft = false
tags = ["R"]
categories = []
+++

## Principal Component Analysis (PCA)

## Introduction

Principal Component Analysis (PCA) is a dimensionality reduction technique used in statistics and machine learning to transform high-dimensional data into a lower-dimensional form while preserving as much of the original variance as possible. PCA is a widely used method for data preprocessing, visualization, and feature selection. It can also help in reducing noise and simplifying complex data structures.

## PCA analysis in R with explanation

```R
library('tidyverse')
library('corrr')
library("ggcorrplot")
library("FactoMineR")
library("factoextra")
library("dplyr")
```


```R
data <- read.csv("Countries_protein.csv", header=T,row.names=1)
str(data)
```

    'data.frame':	25 obs. of  9 variables:
     $ RedMeat  : num  10.1 8.9 13.5 7.8 9.7 10.6 8.4 9.5 18 10.2 ...
     $ WhiteMeat: num  1.4 14 9.3 6 11.4 10.8 11.6 4.9 9.9 3 ...
     $ Eggs     : num  0.5 4.3 4.1 1.6 2.8 3.7 3.7 2.7 3.3 2.8 ...
     $ Milk     : num  8.9 19.9 17.5 8.3 12.5 25 11.1 33.7 19.5 17.6 ...
     $ Fish     : num  0.2 2.1 4.5 1.2 2 9.9 5.4 5.8 5.7 5.9 ...
     $ Cereals  : num  42.3 28 26.6 56.7 34.3 21.9 24.6 26.3 28.1 41.7 ...
     $ Starch   : num  0.6 3.6 5.7 1.1 5 4.8 6.5 5.1 4.8 2.2 ...
     $ Nuts     : num  5.5 1.3 2.1 3.7 1.1 0.7 0.8 1 2.4 7.8 ...
     $ Fr.Veg   : num  1.7 4.3 4 4.2 4 2.4 3.6 1.4 6.5 6.5 ...
    


```R
head(data)
```


<table class="dataframe">
<caption>A data.frame: 6 × 9</caption>
<thead>
	<tr><th></th><th scope=col>RedMeat</th><th scope=col>WhiteMeat</th><th scope=col>Eggs</th><th scope=col>Milk</th><th scope=col>Fish</th><th scope=col>Cereals</th><th scope=col>Starch</th><th scope=col>Nuts</th><th scope=col>Fr.Veg</th></tr>
	<tr><th></th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>Albania</th><td>10.1</td><td> 1.4</td><td>0.5</td><td> 8.9</td><td>0.2</td><td>42.3</td><td>0.6</td><td>5.5</td><td>1.7</td></tr>
	<tr><th scope=row>Austria</th><td> 8.9</td><td>14.0</td><td>4.3</td><td>19.9</td><td>2.1</td><td>28.0</td><td>3.6</td><td>1.3</td><td>4.3</td></tr>
	<tr><th scope=row>Belgium</th><td>13.5</td><td> 9.3</td><td>4.1</td><td>17.5</td><td>4.5</td><td>26.6</td><td>5.7</td><td>2.1</td><td>4.0</td></tr>
	<tr><th scope=row>Bulgaria</th><td> 7.8</td><td> 6.0</td><td>1.6</td><td> 8.3</td><td>1.2</td><td>56.7</td><td>1.1</td><td>3.7</td><td>4.2</td></tr>
	<tr><th scope=row>Czechoslovakia</th><td> 9.7</td><td>11.4</td><td>2.8</td><td>12.5</td><td>2.0</td><td>34.3</td><td>5.0</td><td>1.1</td><td>4.0</td></tr>
	<tr><th scope=row>Denmark</th><td>10.6</td><td>10.8</td><td>3.7</td><td>25.0</td><td>9.9</td><td>21.9</td><td>4.8</td><td>0.7</td><td>2.4</td></tr>
</tbody>
</table>



### Step 1: Normalize Data

The Normalization is an important step for PCA.Normalization ensures data between different variables is comparable.
First, you typically standardize your data by subtracting the mean and dividing by the standard deviation of each feature. This ensures that each feature has a mean of zero and a standard deviation of one, which helps in the analysis.

```R
scaled.data <- scale(data)
```


```R
head(scaled.data)
```


<table class="dataframe">
<caption>A matrix: 6 × 9 of type dbl</caption>
<thead>
	<tr><th></th><th scope=col>RedMeat</th><th scope=col>WhiteMeat</th><th scope=col>Eggs</th><th scope=col>Milk</th><th scope=col>Fish</th><th scope=col>Cereals</th><th scope=col>Starch</th><th scope=col>Nuts</th><th scope=col>Fr.Veg</th></tr>
</thead>
<tbody>
	<tr><th scope=row>Albania</th><td> 0.08126490</td><td>-1.7584889</td><td>-2.1796385</td><td>-1.15573814</td><td>-1.20028213</td><td> 0.9159176</td><td>-2.2495772</td><td> 1.2227536</td><td>-1.35040507</td></tr>
	<tr><th scope=row>Austria</th><td>-0.27725673</td><td> 1.6523731</td><td> 1.2204544</td><td> 0.39237676</td><td>-0.64187467</td><td>-0.3870690</td><td>-0.4136872</td><td>-0.8923886</td><td> 0.09091397</td></tr>
	<tr><th scope=row>Belgium</th><td> 1.09707621</td><td> 0.3800675</td><td> 1.0415022</td><td> 0.05460623</td><td> 0.06348211</td><td>-0.5146342</td><td> 0.8714358</td><td>-0.4895043</td><td>-0.07539207</td></tr>
	<tr><th scope=row>Bulgaria</th><td>-0.60590157</td><td>-0.5132535</td><td>-1.1954011</td><td>-1.24018077</td><td>-0.90638347</td><td> 2.2280161</td><td>-1.9435955</td><td> 0.3162641</td><td> 0.03547862</td></tr>
	<tr><th scope=row>Czechoslovakia</th><td>-0.03824231</td><td> 0.9485445</td><td>-0.1216875</td><td>-0.64908235</td><td>-0.67126454</td><td> 0.1869740</td><td> 0.4430614</td><td>-0.9931096</td><td>-0.07539207</td></tr>
	<tr><th scope=row>Denmark</th><td> 0.23064892</td><td> 0.7861225</td><td> 0.6835976</td><td> 1.11013912</td><td> 1.65053488</td><td>-0.9428885</td><td> 0.3206688</td><td>-1.1945517</td><td>-0.96235764</td></tr>
</tbody>
</table>



### Step 2: Correlation

PCA calculates the covariance matrix of the standardized data. This matrix represents the relationships between the features and how they vary together. The diagonal elements of the covariance matrix represent the variance of each individual feature, while the off-diagonal elements represent the covariances between pairs of features.
Covariance is nothing but a measure of correlation. Therefore we use cor function in R.


```R
corr_matrix <- cor(scaled.data)
ggcorrplot(corr_matrix)
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/P6/output_8_0.png)
    



```R

```
The result of the correlation matrix can be interpreted as follow: 

The higher the value, the most positively correlated the two variables are.
The closer the value to -1, the most negatively correlated they are.
### Step3  PCA Calculation
Eigenvalue Decomposition: PCA then performs eigenvalue decomposition on the covariance matrix. This process yields a set of eigenvalues and corresponding eigenvectors. The eigenvalues represent the amount of variance explained by each principal component, and the eigenvectors represent the directions (principal components) along which the data varies the most.


```R
data.pca <- princomp(corr_matrix)
summary(data.pca)
```


    Importance of components:
                              Comp.1    Comp.2     Comp.3    Comp.4     Comp.5
    Standard deviation     1.2785390 0.5187769 0.37155811 0.2854996 0.15175548
    Proportion of Variance 0.7550709 0.1243143 0.06376953 0.0376505 0.01063772
    Cumulative Proportion  0.7550709 0.8793852 0.94315474 0.9808052 0.99144296
                                Comp.6      Comp.7       Comp.8 Comp.9
    Standard deviation     0.100089157 0.084613737 0.0367140593      0
    Proportion of Variance 0.004627366 0.003307056 0.0006226222      0
    Cumulative Proportion  0.996070322 0.999377378 1.0000000000      1



```R
data.pca
```


    Call:
    princomp(x = corr_matrix)
    
    Standard deviations:
        Comp.1     Comp.2     Comp.3     Comp.4     Comp.5     Comp.6     Comp.7 
    1.27853902 0.51877690 0.37155811 0.28549959 0.15175548 0.10008916 0.08461374 
        Comp.8     Comp.9 
    0.03671406 0.00000000 
    
     9  variables and  9 observations.


### Step4 Principal Component Selection

You can choose a subset of the principal components based on the eigenvalues. Typically, you select the top N principal components that explain the most variance. This reduces the dimensionality of your data from the original number of features to N.


```R
fviz_eig(data.pca, addlabels = TRUE)
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/P6/output_15_0.png)
    

we notice that nine principal components have been generated (comp.1 to comp.9), which also correspond to the number of variables in the data.

Each component explains a percentage of the total variance in the data set. In the Cumulative Proportion section, the first principal component explains almost 77% of the total variance. This implies that almost two-thirds of the data in the set of 9 variables can be represented by just the first principal component. The second one explains 12.08% of the total variance. 

The cumulative proportion of Comp.1 and Comp.2 explains nearly 89% of the total variance. This means that the first two principal components can accurately represent the data. 

Finally, you project your original data onto the selected principal components to obtain a lower-dimensional representation of your data. This new representation retains most of the original data's variance and can be used for various purposes, such as visualization, clustering, or classification.
PCA is widely used in various fields, including image processing, feature extraction, data compression, and noise reduction.

```R

```


```R
data.pca$loadings[, 1:2]
```


<table class="dataframe">
<caption>A matrix: 9 × 2 of type dbl</caption>
<thead>
	<tr><th></th><th scope=col>Comp.1</th><th scope=col>Comp.2</th></tr>
</thead>
<tbody>
	<tr><th scope=row>RedMeat</th><td> 0.2902937</td><td> 0.09510150</td></tr>
	<tr><th scope=row>WhiteMeat</th><td> 0.3131864</td><td> 0.26392299</td></tr>
	<tr><th scope=row>Eggs</th><td> 0.4131713</td><td> 0.07419838</td></tr>
	<tr><th scope=row>Milk</th><td> 0.3852342</td><td> 0.15393922</td></tr>
	<tr><th scope=row>Fish</th><td> 0.1165053</td><td>-0.69780771</td></tr>
	<tr><th scope=row>Cereals</th><td>-0.4342505</td><td> 0.27531862</td></tr>
	<tr><th scope=row>Starch</th><td> 0.2796041</td><td>-0.36379635</td></tr>
	<tr><th scope=row>Nuts</th><td>-0.4400339</td><td>-0.07686551</td></tr>
	<tr><th scope=row>Fr.Veg</th><td>-0.1567571</td><td>-0.43715631</td></tr>
</tbody>
</table>



### Step 5 Generating Scatter Plot


```R
df_pca <- as.data.frame(data.pca$scores)
```


```R
head(df_pca)
```


<table class="dataframe">
<caption>A data.frame: 6 × 9</caption>
<thead>
	<tr><th></th><th scope=col>Comp.1</th><th scope=col>Comp.2</th><th scope=col>Comp.3</th><th scope=col>Comp.4</th><th scope=col>Comp.5</th><th scope=col>Comp.6</th><th scope=col>Comp.7</th><th scope=col>Comp.8</th><th scope=col>Comp.9</th></tr>
	<tr><th></th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>RedMeat</th><td> 0.8229423</td><td> 0.2489508</td><td> 0.479894369</td><td> 0.41992105</td><td> 0.16249784</td><td> 0.14633466</td><td> 0.02207508</td><td> 0.01521311</td><td>-1.561251e-15</td></tr>
	<tr><th scope=row>WhiteMeat</th><td> 0.8731350</td><td> 0.5413876</td><td>-0.641035392</td><td>-0.06856873</td><td>-0.15658528</td><td> 0.07509367</td><td> 0.07115939</td><td> 0.03807497</td><td> 2.050443e-15</td></tr>
	<tr><th scope=row>Eggs</th><td> 1.3176912</td><td> 0.2014501</td><td>-0.113249199</td><td> 0.21284317</td><td> 0.02509813</td><td>-0.15112470</td><td> 0.06945838</td><td>-0.06523676</td><td>-6.938894e-18</td></tr>
	<tr><th scope=row>Milk</th><td> 1.1517273</td><td> 0.3674206</td><td> 0.480474716</td><td>-0.22886204</td><td>-0.15680672</td><td>-0.07500824</td><td>-0.13380743</td><td> 0.01806214</td><td> 7.285839e-16</td></tr>
	<tr><th scope=row>Fish</th><td> 0.1263054</td><td>-0.9612448</td><td> 0.304644574</td><td>-0.25360565</td><td>-0.15367744</td><td> 0.07196139</td><td> 0.09766257</td><td>-0.01726125</td><td>-1.752071e-16</td></tr>
	<tr><th scope=row>Cereals</th><td>-2.1140099</td><td> 0.6266042</td><td>-0.008336584</td><td>-0.27303559</td><td> 0.04622351</td><td> 0.08628872</td><td>-0.02900992</td><td>-0.05111715</td><td>-1.819725e-15</td></tr>
</tbody>
</table>




```R
df_pca1 <- tibble::rownames_to_column(df_pca, "ID") 
```


```R

```


```R
df1 <- read.table('metadata.txt', header=T)
```


```R
head(df1)
```


<table class="dataframe">
<caption>A data.frame: 6 × 2</caption>
<thead>
	<tr><th></th><th scope=col>ID</th><th scope=col>ID1</th></tr>
	<tr><th></th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>1</th><td>RedMeat  </td><td>Group1</td></tr>
	<tr><th scope=row>2</th><td>WhiteMeat</td><td>Group1</td></tr>
	<tr><th scope=row>3</th><td>Eggs     </td><td>Group1</td></tr>
	<tr><th scope=row>4</th><td>Milk     </td><td>Group2</td></tr>
	<tr><th scope=row>5</th><td>Fish     </td><td>Group1</td></tr>
	<tr><th scope=row>6</th><td>Cereals  </td><td>Group2</td></tr>
</tbody>
</table>




```R
df_pca2 <- merge(df1,df_pca1, by='ID')
```


```R
df_pca2
```


<table class="dataframe">
<caption>A data.frame: 9 × 11</caption>
<thead>
	<tr><th scope=col>ID</th><th scope=col>ID1</th><th scope=col>Comp.1</th><th scope=col>Comp.2</th><th scope=col>Comp.3</th><th scope=col>Comp.4</th><th scope=col>Comp.5</th><th scope=col>Comp.6</th><th scope=col>Comp.7</th><th scope=col>Comp.8</th><th scope=col>Comp.9</th></tr>
	<tr><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>Cereals  </td><td>Group2</td><td>-2.1140099</td><td> 0.62660419</td><td>-0.008336584</td><td>-0.27303559</td><td> 0.04622351</td><td> 0.08628872</td><td>-0.02900992</td><td>-0.051117148</td><td>-1.819725e-15</td></tr>
	<tr><td>Eggs     </td><td>Group1</td><td> 1.3176912</td><td> 0.20145005</td><td>-0.113249199</td><td> 0.21284317</td><td> 0.02509813</td><td>-0.15112470</td><td> 0.06945838</td><td>-0.065236762</td><td>-6.938894e-18</td></tr>
	<tr><td>Fish     </td><td>Group1</td><td> 0.1263054</td><td>-0.96124480</td><td> 0.304644574</td><td>-0.25360565</td><td>-0.15367744</td><td> 0.07196139</td><td> 0.09766257</td><td>-0.017261248</td><td>-1.752071e-16</td></tr>
	<tr><td>Fr.Veg   </td><td>Group2</td><td>-0.8826502</td><td>-0.59634741</td><td>-0.365705158</td><td> 0.43787386</td><td>-0.10367959</td><td> 0.01484096</td><td>-0.13002049</td><td>-0.006462412</td><td>-3.053113e-16</td></tr>
	<tr><td>Milk     </td><td>Group2</td><td> 1.1517273</td><td> 0.36742056</td><td> 0.480474716</td><td>-0.22886204</td><td>-0.15680672</td><td>-0.07500824</td><td>-0.13380743</td><td> 0.018062142</td><td> 7.285839e-16</td></tr>
	<tr><td>Nuts     </td><td>Group2</td><td>-2.0805423</td><td> 0.03844348</td><td> 0.178266077</td><td> 0.09337287</td><td> 0.03087628</td><td>-0.14592375</td><td> 0.08082861</td><td> 0.051002198</td><td> 1.800643e-15</td></tr>
	<tr><td>RedMeat  </td><td>Group1</td><td> 0.8229423</td><td> 0.24895079</td><td> 0.479894369</td><td> 0.41992105</td><td> 0.16249784</td><td> 0.14633466</td><td> 0.02207508</td><td> 0.015213114</td><td>-1.561251e-15</td></tr>
	<tr><td>Starch   </td><td>Group2</td><td> 0.7854012</td><td>-0.46666446</td><td>-0.314953403</td><td>-0.33993894</td><td> 0.30605325</td><td>-0.02246272</td><td>-0.04834618</td><td> 0.017725147</td><td>-6.565928e-16</td></tr>
	<tr><td>WhiteMeat</td><td>Group1</td><td> 0.8731350</td><td> 0.54138761</td><td>-0.641035392</td><td>-0.06856873</td><td>-0.15658528</td><td> 0.07509367</td><td> 0.07115939</td><td> 0.038074968</td><td> 2.050443e-15</td></tr>
</tbody>
</table>




```R
ggplot(data = df_pca2, 
       aes(x = Comp.1, 
           y = Comp.2, color=ID)) +
  geom_point(size=10) + scale_color_manual(values = c("#FFFFCC", "#330000", "#994C00","#0080FF","#FF66FF","#000000","#E0E0E0","#660033","#009999")) + theme_bw() + xlab("PC1 (75.5%)") + 
  ylab("PC2 (12.4%)")

 
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/P6/output_28_0.png)
    


Some key points to remember about PCA are:
-	PCA is a linear transformation technique, meaning it finds linear combinations of the original features.
-	It is unsupervised and does not rely on class labels.
-	The principal components are orthogonal to each other, meaning they are uncorrelated.
-	The first principal component captures the most variance, the second captures the second most, and so on.
-	PCA can help in identifying patterns and reducing redundancy in data.
-	
Keep in mind that while PCA is a powerful technique for dimensionality reduction, it may not always be the best choice, especially when non-linear relationships exist in the data. In such cases, nonlinear dimensionality reduction techniques like t-SNE or autoencoders may be more appropriate.

## References
- https://uw.pressbooks.pub/appliedmultivariatestatistics/chapter/pca/
