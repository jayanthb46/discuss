+++
title = "Multivariate Analysis of Variance (MANOVA)"
date = 2023-10-29
draft = false
tags = ["R"]
categories = []
+++

## Multivariate Analysis of Variance (MANOVA)

### Introduction

In the world of statistics and data analysis, researchers and analysts often find themselves dealing with complex datasets where multiple dependent variables interact with one or more independent variables. When faced with such intricate scenarios, traditional statistical methods like ANOVA (Analysis of Variance) may fall short. This is where MANOVA steps in, offering a comprehensive solution to analyze and understand the relationships between multiple dependent variables and one or more independent variables. In this blog, we'll delve into the world of MANOVA, exploring its concepts, applications, and significance in statistical analysis.

### Understanding MANOVA

MANOVA, or Multivariate Analysis of Variance, is a multivariate statistical technique used to assess whether there are statistically significant differences between the means of three or more groups while simultaneously considering multiple dependent variables. Unlike ANOVA, which deals with a single dependent variable, MANOVA allows researchers to examine how multiple dependent variables respond to changes in one or more independent variables.

### Key Components of MANOVA

Dependent Variables: MANOVA is designed for situations where you have two or more dependent variables. These variables are typically continuous and are used to measure the impact of the independent variable(s).

Independent Variables: The independent variable(s) categorize the subjects into different groups. For example, it could be the effect of different teaching methods on student performance, with teaching methods being the independent variable.

Null Hypothesis: Like other statistical tests, MANOVA starts with a null hypothesis. It assumes that there are no significant differences between group means.

Alternative Hypothesis: The alternative hypothesis suggests that at least one group's means differ significantly from the others.

Test Statistic: MANOVA uses various test statistics, including Wilks' Lambda, Pillai's Trace, Hotelling-Lawley Trace, and Roy's Largest Root, to assess the significance of differences between groups.

### Applications of MANOVA

Scientific Research: MANOVA is widely used in scientific research to analyze how multiple dependent variables are affected by one or more independent variables. It's applied in various fields, such as psychology, biology, and medicine, to study the effects of different treatments, interventions, or conditions.


### Significance of MANOVA

Enhanced Analysis: MANOVA provides a more comprehensive understanding of complex data by considering the relationships between multiple dependent variables and independent variables simultaneously.

Efficient Data Reduction: Rather than conducting separate ANOVAs for each dependent variable, MANOVA condenses the information into a single analysis, reducing the risk of Type I errors and saving time.

Reduced Experiment-wise Error: MANOVA reduces the overall error rate compared to conducting multiple univariate analyses, making it a more powerful technique when multiple dependent variables are involved.

Better Interpretation: It helps researchers discern how changes in the independent variables affect the dependent variables collectively, providing a holistic view of the data.

### Implementation in R

```R
library(tidyverse)
```

    
```R
df=read.csv('manova_data.txt', sep='\t', header=TRUE, check.names=FALSE)
```


```R
head(df)
```


<table class="dataframe">
<caption>A data.frame: 6 × 3</caption>
<thead>
	<tr><th></th><th scope=col>School</th><th scope=col>Math_score</th><th scope=col>History_score</th></tr>
	<tr><th></th><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><th scope=row>1</th><td>A</td><td>20</td><td>0.70</td></tr>
	<tr><th scope=row>2</th><td>A</td><td>22</td><td>0.80</td></tr>
	<tr><th scope=row>3</th><td>A</td><td>24</td><td>0.95</td></tr>
	<tr><th scope=row>4</th><td>A</td><td>18</td><td>0.60</td></tr>
	<tr><th scope=row>5</th><td>A</td><td>20</td><td>0.74</td></tr>
	<tr><th scope=row>6</th><td>A</td><td>20</td><td>0.76</td></tr>
</tbody>
</table>




```R
#df$"Math score" <- as.numeric(df$"Math score")
#df$"History score" <- as.numeric(df$"History score")
```


```R
sapply(df, class)    
```


<style>
.dl-inline {width: auto; margin:0; padding: 0}
.dl-inline>dt, .dl-inline>dd {float: none; width: auto; display: inline-block}
.dl-inline>dt::after {content: ":\0020"; padding-right: .5ex}
.dl-inline>dt:not(:first-of-type) {padding-left: .5ex}
</style><dl class=dl-inline><dt>School</dt><dd>'character'</dd><dt>Math_score</dt><dd>'numeric'</dd><dt>History_score</dt><dd>'numeric'</dd></dl>




```R
df %>% group_by(School) %>%  summarise(n = n(), mean = mean(History_score), sd = sd(History_score))
```


<table class="dataframe">
<caption>A tibble: 4 × 4</caption>
<thead>
	<tr><th scope=col>School</th><th scope=col>n</th><th scope=col>mean</th><th scope=col>sd</th></tr>
	<tr><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>A</td><td>10</td><td>0.784</td><td>0.12130769</td></tr>
	<tr><td>B</td><td>10</td><td>0.608</td><td>0.09681598</td></tr>
	<tr><td>C</td><td>10</td><td>0.272</td><td>0.14327906</td></tr>
	<tr><td>D</td><td>10</td><td>0.474</td><td>0.09453982</td></tr>
</tbody>
</table>




```R
df %>% group_by(School) %>%  summarise(n = n(), mean = mean(Math_score), sd = sd(Math_score))
```


<table class="dataframe">
<caption>A tibble: 4 × 4</caption>
<thead>
	<tr><th scope=col>School</th><th scope=col>n</th><th scope=col>mean</th><th scope=col>sd</th></tr>
	<tr><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;int&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>A</td><td>10</td><td>18.90</td><td>2.923088</td></tr>
	<tr><td>B</td><td>10</td><td>16.54</td><td>1.920185</td></tr>
	<tr><td>C</td><td>10</td><td> 3.05</td><td>1.039498</td></tr>
	<tr><td>D</td><td>10</td><td> 9.35</td><td>2.106735</td></tr>
</tbody>
</table>




```R
library(gridExtra)
p1 <- ggplot(df, aes(x = School, y = Math_score, fill = School)) + geom_boxplot() + geom_jitter(width = 0.2) + theme(legend.position="top") +  labs(y="Math score")
p2 <- ggplot(df, aes(x = School, y = History_score, fill = School)) + geom_boxplot() + geom_jitter(width = 0.2) + theme(legend.position="top") + labs(y="History score")
grid.arrange(p1, p2, ncol=2)
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/output_7_0.png)
    



```R
dp_vars <- cbind(df$Math_score, df$History_score)
```


```R
fit <- manova(dp_vars ~ School, data = df)
```


```R
summary(fit)
```


              Df Pillai approx F num Df den Df    Pr(>F)    
    School     3 1.0365   12.909      6     72 7.575e-10 ***
    Residuals 36                                            
    ---
    Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1



```R
library(effectsize)
effectsize::eta_squared(fit)
```

    Warning message:
    "package 'effectsize' was built under R version 4.2.3"
    


<table class="dataframe">
<caption>A effectsize_anova: 1 × 5</caption>
<thead>
	<tr><th scope=col>Parameter</th><th scope=col>Eta2_partial</th><th scope=col>CI</th><th scope=col>CI_low</th><th scope=col>CI_high</th></tr>
	<tr><th scope=col>&lt;chr&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th><th scope=col>&lt;dbl&gt;</th></tr>
</thead>
<tbody>
	<tr><td>School</td><td>0.5182515</td><td>0.95</td><td>0.3618079</td><td>1</td></tr>
</tbody>
</table>



Pillai’s trace is used as a test statistic in MANOVA and MANCOVA. This is a positive valued statistic ranging from 0 to 1. Increasing values means that effects are contributing more to the model; you should reject the null hypothesis for large values

The Pillai’s Trace test statistics is statistically significant [Pillai’s Trace = 1.03, F(6, 72) = 12.90, p < 0.001] and indicates that School type [A.B,C or D] has a statistically significant association with both combined Marks obtained by the students.

### Conclusion

MANOVA or Multivariate Analysis of Variance helps researchers explore complex relationship of variables within datasets. It helps in reducing errors when analysing multifaceted complex data from complex scientific experiments.
MANOVA opens the door to a deeper understanding of the data you're working with, ultimately enabling more informed decisions and discoveries
