+++
title = "Molecular Docking with Autodock vina"
date = 2024-12-15
draft = false
tags = ["cheminformatics"]
categories = []
+++


## Introduction to Molecular Docking with Vina
Molecular docking is a computational technique that predicts the preferred orientation of one molecule to a second when bound to each other to form a stable complex. This technique is widely used in computational drug discovery to predict the binding affinities of small molecules (ligands) to target proteins (receptors). By simulating the binding process, researchers can identify potential drug candidates with high affinity and specificity for their target proteins.

AutoDock Vina is a popular open-source software tool for molecular docking. It offers a balance between speed and accuracy, making it a valuable tool for virtual screening and lead optimization. Vina employs a sophisticated scoring function to evaluate the binding affinity of different ligand poses within the receptor's binding site.

This introduction will provide a basic overview of molecular docking principles and guide you through the essential steps of using AutoDock Vina for virtual screening and lead optimization.

Steps involved in docking are discussed below
    - Finding and saving a protein structure
    - Visualizing the protein structure
    - Protein structure preparation
    - Ligand Preparation
    - Defining a Ligand Box
    - Docking Ligands with AutoDock Vina
    - Visualizing Docking Results


### Finding and saving a protein structure
To select an appropriate protein for our docking study, we can utilize the PDB Search API . This time, we'll use the EC number, but the second half of our query will include our ligand name.

```python
from rcsbsearchapi.search import TextQuery
from rcsbsearchapi import rcsb_attributes as attrs

ECnumber = "3.4.21.4"    

q1 = attrs.rcsb_polymer_entity.rcsb_ec_lineage.id == ECnumber  # looking for trypsins
q2 = TextQuery("13U")

query = q1 & q2               # combining the two queries into one

results = list(query())
print(results)
```

    ['2ZQ2']

We found the PDB ID of a trypsin with our ligand of interest bound. Our PDB ID is 2zq2, which we saved as a variable in the cell above.

We will now build a directory called protein_structures to store this.

```python
pdb_id = results[0].lower() # Get the PDB ID and convert to lowercase
print(pdb_id)
```

    2zq2



```python
import os # for making directories
import requests

# make a directory for pdb files
protein_directory = "protein_structures"
os.makedirs(protein_directory, exist_ok=True)

pdb_request = requests.get(f"https://files.rcsb.org/download/{pdb_id}.pdb")
pdb_request.status_code
```
200

```python
with open(f"{protein_directory}/{pdb_id}.pdb", "w+") as f:
    f.write(pdb_request.text)
```

### Visualizing the protein structure
Before we begin working with our molecule, let's look at its structure. We will first process our PDB with a package called MDAnalysis, then view it using NGLView.

MDAnalysis is a Python module for processing molecular dynamics trajectories and other 3D-structured molecular data. A "Universe" is the central object of MDAnalysis, and it corresponds to a molecular system. We may load a PDB file into MDAnalysis and then do operations such as measuring distances inside our structure or isolating specific regions.

```python
import MDAnalysis as mda

# Load into MDA universe
u = mda.Universe(f"{protein_directory}/{pdb_id}.pdb")
u
```

```python
import nglview as nv
view = nv.show_mdanalysis(u)
view
```

![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/images/Docking/Screenshot%20from%202024-12-15%2019-35-01.png)

We will create separate variables for the protein and ligand. We can select all protein residues in MDAnalysis using the word "protein" in the select_atoms function. Then, we will select our ligand using resname 13U. This corresponds to the residue name in the PDB we downloaded. We can also select waters in the structure by using "resname HOH".

```python
protein = u.select_atoms("protein")
ligand = u.select_atoms("resname 13U")
water = u.select_atoms("resname HOH")

water
```
<AtomGroup with 269 atoms>



After picking components of our structure, we may add them separately to an NGLView display. In the cell below, we see the protein's surface area colored by hydrophobicity. The crystal structure's waters are represented as spacefill, while the ligand is shown as a ball and stick.

```python
view = nv.show_mdanalysis(protein)
view.clear_representations()
view.add_representation("surface", colorScheme="hydrophobicity")
lig_view = view.add_component(ligand)
lig_view.add_representation("ball+stick")
water_view = view.add_component(water)
water_view.add_representation("spacefill")
view
```
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/images/Docking/Screenshot%20from%202024-12-15%2019-35-23.png)


If you rotate this structure so that you are looking at the bottom, you will be able to see our 13U ligand bound. Upon viewing this structure, you will notice that our ligand seems to appear twice. If you open the PDB file to investigate, you will see the following in the ligand section:

```python
view = nv.show_mdanalysis(ligand)
view.add_representation("ball+stick",colorScheme="hydrophobicity")
view
```

![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/images/Docking/Screenshot%20from%202024-12-15%2019-43-50.png)

```python
protein.write(f"{protein_directory}/protein_{pdb_id}.pdb")
```

    /home/ab/.local/lib/python3.11/site-packages/MDAnalysis/coordinates/PDB.py:1153: UserWarning: Found no information for attr: 'formalcharges' Using default value of '0'
      warnings.warn("Found no information for attr: '{}'"

To perform a docking calculation, we will have to isolate the protein. This starting structure for our protein contains extra molecules like ligands and water. You will also notice from examining our visualization that our structure does not include hydrogen atoms. If you were to examine the PDB file, you would also see that there are some missing atoms and some of our protein residues have alternate locations marked just like thie ligand.

For docking, we will want to remove all of these extra molecules and only keep the protein. We will also want to remove any alternate locations of residues. We will use MDAnalysis to remove these extra molecules and save our starting protein structure as a new file.

### Protein structure preparation
Now that we've isolated our protein, we will want to ensure that we've correctly added hydrogen and fixed any missing atoms.

For fixing our protein, we will use a specialized program called PDB2PQR that is made for working with biomolecules like proteins. The advantage of using PDB2PQR is that it will check our protein for missing atoms and multiple occupancy in the protein, and it will pick positions and add missing atoms.We will use the command-line interface of this PDB2PQR. This means that you would usually type the command below into your terminal You can run command line commands in the Jupyter notebook by putting a ! in front of the command.

```python
! pdb2pqr --ff AMBER --pdb-output=protein_structures/protein_h.pdb --pH=7.4 protein_structures/protein_2zq2.pdb protein_structures/protein_2zq2.pqr --whitespace
```

    INFO:PDB2PQR v3.6.2: biomolecular structure conversion software.
    INFO:Please cite:  Jurrus E, et al.  Improvements to the APBS biomolecular solvation software suite.  Protein Sci 27 112-128 (2018).
    INFO:Please cite:  Dolinsky TJ, et al.  PDB2PQR: expanding and upgrading automated preparation of biomolecular structures for molecular simulations. Nucleic Acids Res 35 W522-W525 (2007).
    INFO:Checking and transforming input arguments.
    INFO:Loading topology files.
    INFO:Loading molecule: protein_structures/protein_2zq2.pdb
    ERROR:Error parsing line: invalid literal for int() with base 10: ''
    ERROR:<REMARK     2>
    ERROR:Truncating remaining errors for record type:REMARK
    WARNING:Warning: protein_structures/protein_2zq2.pdb is a non-standard PDB file.
    
    ERROR:['REMARK']
    INFO:Setting up molecule.
    INFO:Created biomolecule object with 223 residues and 1625 atoms.
    WARNING:Multiple occupancies found: N in SER A 61.
    WARNING:Multiple occupancies found: CA in SER A 61.
    WARNING:Multiple occupancies found: C in SER A 61.
    WARNING:Multiple occupancies found: O in SER A 61.
    WARNING:Multiple occupancies found: CB in SER A 61.
    WARNING:Multiple occupancies found: OG in SER A 61.
    WARNING:Multiple occupancies found in SER A 61. At least one of the instances is being ignored.
    WARNING:Multiple occupancies found: N in SER A 113.
    WARNING:Multiple occupancies found: CA in SER A 113.
    WARNING:Multiple occupancies found: C in SER A 113.
    WARNING:Multiple occupancies found: O in SER A 113.
    WARNING:Multiple occupancies found: CB in SER A 113.
    WARNING:Multiple occupancies found: OG in SER A 113.
    WARNING:Multiple occupancies found in SER A 113. At least one of the instances is being ignored.
    WARNING:Multiple occupancies found: N in SER A 122.
    WARNING:Multiple occupancies found: CA in SER A 122.
    WARNING:Multiple occupancies found: C in SER A 122.
    WARNING:Multiple occupancies found: O in SER A 122.
    WARNING:Multiple occupancies found: CB in SER A 122.
    WARNING:Multiple occupancies found: OG in SER A 122.
    WARNING:Multiple occupancies found in SER A 122. At least one of the instances is being ignored.
    WARNING:Multiple occupancies found: N in SER A 167.
    WARNING:Multiple occupancies found: CA in SER A 167.
    WARNING:Multiple occupancies found: C in SER A 167.
    WARNING:Multiple occupancies found: O in SER A 167.
    WARNING:Multiple occupancies found: CB in SER A 167.
    WARNING:Multiple occupancies found: OG in SER A 167.
    WARNING:Multiple occupancies found in SER A 167. At least one of the instances is being ignored.
    WARNING:Multiple occupancies found: N in SER A 170.
    WARNING:Multiple occupancies found: CA in SER A 170.
    WARNING:Multiple occupancies found: C in SER A 170.
    WARNING:Multiple occupancies found: O in SER A 170.
    WARNING:Multiple occupancies found: CB in SER A 170.
    WARNING:Multiple occupancies found: OG in SER A 170.
    WARNING:Multiple occupancies found in SER A 170. At least one of the instances is being ignored.
    WARNING:Multiple occupancies found: N in SER A 236.
    WARNING:Multiple occupancies found: CA in SER A 236.
    WARNING:Multiple occupancies found: C in SER A 236.
    WARNING:Multiple occupancies found: O in SER A 236.
    WARNING:Multiple occupancies found: CB in SER A 236.
    WARNING:Multiple occupancies found: OG in SER A 236.
    WARNING:Multiple occupancies found in SER A 236. At least one of the instances is being ignored.
    WARNING:Multiple occupancies found: N in GLN A 240.
    WARNING:Multiple occupancies found: CA in GLN A 240.
    WARNING:Multiple occupancies found: C in GLN A 240.
    WARNING:Multiple occupancies found: O in GLN A 240.
    WARNING:Multiple occupancies found: CB in GLN A 240.
    WARNING:Multiple occupancies found: CG in GLN A 240.
    WARNING:Multiple occupancies found: CD in GLN A 240.
    WARNING:Multiple occupancies found: OE1 in GLN A 240.
    WARNING:Multiple occupancies found: NE2 in GLN A 240.
    WARNING:Multiple occupancies found in GLN A 240. At least one of the instances is being ignored.
    INFO:Setting termini states for biomolecule chains.
    INFO:Loading forcefield.
    INFO:Loading hydrogen topology definitions.
    WARNING:Missing atom CG in residue LYS A 222
    WARNING:Missing atom CD in residue LYS A 222
    WARNING:Missing atom CE in residue LYS A 222
    WARNING:Missing atom NZ in residue LYS A 222
    WARNING:Missing atom CG in residue LYS A 222
    WARNING:Missing atom CD in residue LYS A 222
    WARNING:Missing atom CE in residue LYS A 222
    WARNING:Missing atom NZ in residue LYS A 222
    INFO:Attempting to repair 4 missing atoms in biomolecule.
    WARNING:Missing atom CG in residue LYS A 222
    WARNING:Missing atom CD in residue LYS A 222
    WARNING:Missing atom CE in residue LYS A 222
    WARNING:Missing atom NZ in residue LYS A 222
    INFO:Added atom CG to residue LYS A 222 at coordinates 30.628, -3.449, -0.010
    INFO:Added atom CD to residue LYS A 222 at coordinates 32.074, -3.541, -0.453
    INFO:Added atom CE to residue LYS A 222 at coordinates 32.755, -2.198, -0.512
    INFO:Added atom NZ to residue LYS A 222 at coordinates 34.167, -2.339, -0.950
    INFO:Updating disulfide bridges.
    INFO:Debumping biomolecule.
    INFO:Adding hydrogens to biomolecule.
    INFO:Debumping biomolecule (again).
    INFO:Optimizing hydrogen bonds
    INFO:Applying force field to biomolecule states.
    INFO:Regenerating headers.
    INFO:Regenerating PDB lines.
    WARNING:Ignoring 390 header lines in output.
    WARNING:Ignoring 390 header lines in output.

The PDB2PQR program outputs two files, a PDB file and a PQR file. The PDB file is similar to PDB files we have worked with before, except that it contains hydrogens. The PQR file is another molecular file format that is similar to a PDB, but contains information about atomic radii and atomic charges.

For use with AutoDock Vina, we need our protein file to be in the "PDBQT" format. PDBQT is a specialized file format used by AutoDock Vina and other AutoDock tools. Like the PQR format, the PDBQT format can also contain partial charges. We will load our PQR file and use MDAnalysis to write a PDBQT file.

```python
# make a directory for pdb files
pdbqt_directory = "pdbqt"
os.makedirs(pdbqt_directory, exist_ok=True)

u = mda.Universe(f"{protein_directory}/protein_{pdb_id}.pqr")
u.atoms.write(f"{pdbqt_directory}/{pdb_id}.pdbqt")
```

    /home/ab/.local/lib/python3.11/site-packages/MDAnalysis/coordinates/PDBQT.py:296: UserWarning: Supplied AtomGroup was missing the following attributes: altLocs, occupancies, tempfactors. These will be written with default values. 
      warnings.warn(

The PDBQT file generated by MDAnalysis includes two lines at the start of the structure that AutoDock Vina doesn't accept. These lines start with "TITLE" and "CRYST1". To resolve this, the following cell replaces these lines with "REMARK", which is acceptable to AutoDock Vina.

You might have also just chosen to use a different software to write your PDBQT. OpenBabel is a popular choice. However, we are using MDAnalysis here for consistency with the rest of the blog and to limit the number of libraries we are using.

```python
# Read in the just-written PDBQT file, replace text, and write back
with open(f"{pdbqt_directory}/{pdb_id}.pdbqt", 'r') as file:
    file_content = file.read()

# Replace 'TITLE' and 'CRYST1' with 'REMARK'
file_content = file_content.replace('TITLE', 'REMARK').replace('CRYST1', 'REMARK')

# Write the modified content back to the file
with open(f"{pdbqt_directory}/{pdb_id}.pdbqt", 'w') as file:
    file.write(file_content)
```

### Ligand Preparation
When preparing small molecule PDBQT files, you could have also chosen to use MDAnalysis or other tools. However, we are going to use a special program for small molecules and docking called meeko. We choose to use meeko for our ligands because it will allow us to more easily visualize our results later. Note that when using meeko, ligands should have hydrogens added already.

```python
# make a directory for pdb files
import requests
url = 'https://files.rcsb.org/ligands/download/13U_ideal.sdf'


response = requests.get(url)
file_Path = f"{protein_directory}/13U_ideal.sdf"

if response.status_code == 200:
    with open(file_Path, 'wb') as file:
        file.write(response.content)
    print('File downloaded successfully')
else:
    print('Failed to download file')
```

    File downloaded successfully


```python
!mk_prepare_ligand.py -i protein_structures/13U_ideal.sdf -o protein_structures/13U.pdbqt
```

    [RDKit] WARNING:[17:12:42] Warning: molecule is tagged as 2D, but at least one Z coordinate is not zero. Marking the mol as 3D.
    Input molecules processed: 1, skipped: 0
    PDBQT files written: 1
    PDBQT files not written due to error: 0
    Input molecules with errors: 0


 ### Defining a Ligand Box

When we attach our ligands to our proteins, we'll need to specify the binding pocket. Here is where the software will try to bind our ligand. In rare circumstances, individuals may utilize software to locate pockets in proteins and determine binding sites. However, as we saw in the above figure 2, the structure obtained from the PDB already had a ligand bonded. To define our binding box, we will use the position of the bound ligand in our original structure.The approach we will take in this blog is to find the center_of_geometry of our ligand to define the center of our binding pocket. Then we will consider the space around the ligand to be our box.

```python
original_structure = mda.Universe("protein_structures/2zq2.pdb")
ligand_mda = original_structure.select_atoms("resname 13U")

# Get the center of the ligand as the "pocket center"
pocket_center = ligand_mda.center_of_geometry()
print(pocket_center)
```

    [18.3068333  -8.06558332 10.638     ]

After defining the pocket center, we will define our ligand box. One simple approach to this is to subtract the min and max of the ligand positions in each dimension. In order to allow for ligand flexibility and potential interactions with nearby residues, we will add an additional five angstroms to each side of our box

```python
# compute min and max coordinates of the ligand
# take the ligand box to be the difference between the max and min in each direction.
ligand_box = ligand_mda.positions.max(axis=0) - ligand_mda.positions.min(axis=0) + 5
ligand_box
```

    array([14.161999, 17.002   , 13.903   ], dtype=float32)


The pocket_center and ligand_box variables are NumPy arrays. However, AutoDock Vina expects them to be lists. We convert them to lists in the cell below.

```python
pocket_center = pocket_center.tolist()
ligand_box = ligand_box.tolist()
```

### Docking Ligands with AutoDock Vina
Now that we have PDBQT files of our protein and ligand and have defined our docking box, we are ready to perform the actual docking. Before docking, we will make a directory to store our results.

```python
# make a directory to store our results
import os

pdb_id = "2zq2"
ligand = "13U"

os.makedirs("docking_results", exist_ok=True)
```
We will dock using the AutoDock Vina Python API. First, we import Vina from vina. We start docking with the line v = Vina(sf_name="vina"). This creates a docking calculation, v, and sets the scoring function to the vina scoring function.

Vina (vina): vina is an empirical scoring function. Binding energy is predicted as the sum of pairwise atomic interactions. It includes terms for hydrogen bonds, hydrophobic interactions, and steric clashes. The parameters for this scoring function were empirically derived from fitting data available in the PDBbind database. You can read more in the original publication, or in the Vinardo paper

```python
from vina import Vina
v = Vina(sf_name="vina")
```


```python
%%time
v.set_receptor(f"{pdbqt_directory}/{pdb_id}.pdbqt")
v.set_ligand_from_file(f"{pdbqt_directory}/13U.pdbqt")
v.compute_vina_maps(center=pocket_center, box_size=ligand_box)
v.dock(exhaustiveness=5, n_poses=5)
```

    Computing Vina grid ... done.
    CPU times: user 2min 36s, sys: 407 ms, total: 2min 37s
    Wall time: 37.6 s
    Performing docking (random seed: 1815444084) ... 
    0%   10   20   30   40   50   60   70   80   90   100%
    |----|----|----|----|----|----|----|----|----|----|
    ***************************************************


    WARNING: At low exhaustiveness, it may be impossible to utilize all CPUs.

After the dock function, we can write the poses that were calculated to a file. Note that the output format from AutoDock Vina is a PDBQT file. Autodock vina completed docking analysis in 37.6 seconds.

```python
v.write_poses(f"docking_results/{ligand}.pdbqt", n_poses=5, overwrite=True)
```
We can see the energies of the calculated poses by calling energies on the docking calculation variable. According to the Vina documentaiton, the rows correspond to the poses, while columns correspond to different energy types. The types of energies in the columns are ["total", "inter", "intra", "torsions", "intra best pose"]. The number of columns and the types of energies they represent depend on the scoring function you are using. You can see more information in the docs for AutoDock Vina.

```python
v.energies()
```

    array([[ -5.483, -10.451,   0.05 ,   4.968,   0.05 ],
           [ -5.207,  -8.839,  -1.036,   4.718,   0.05 ],
           [ -4.892,  -9.232,  -0.044,   4.433,   0.05 ],
           [ -4.664,  -8.241,  -0.598,   4.226,   0.05 ],
           [ -4.624,  -8.431,  -0.334,   4.19 ,   0.05 ]])


The cell below creates a pandas dataframe and saves the energies as a comma-separated-value (CSV) file.

```python
import pandas as pd


# These are the columns for the types of energies according to AutoDock Vina docs.
column_names = ["total", "inter", "intra", "torsions", "intra best pose"]

df = pd.DataFrame(v.energies(), columns=column_names)
df.head()
```

<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>total</th>
      <th>inter</th>
      <th>intra</th>
      <th>torsions</th>
      <th>intra best pose</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>-5.483</td>
      <td>-10.451</td>
      <td>0.050</td>
      <td>4.968</td>
      <td>0.05</td>
    </tr>
    <tr>
      <th>1</th>
      <td>-5.207</td>
      <td>-8.839</td>
      <td>-1.036</td>
      <td>4.718</td>
      <td>0.05</td>
    </tr>
    <tr>
      <th>2</th>
      <td>-4.892</td>
      <td>-9.232</td>
      <td>-0.044</td>
      <td>4.433</td>
      <td>0.05</td>
    </tr>
    <tr>
      <th>3</th>
      <td>-4.664</td>
      <td>-8.241</td>
      <td>-0.598</td>
      <td>4.226</td>
      <td>0.05</td>
    </tr>
    <tr>
      <th>4</th>
      <td>-4.624</td>
      <td>-8.431</td>
      <td>-0.334</td>
      <td>4.190</td>
      <td>0.05</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Save the calculated energies from docking to a CSV file
df.to_csv("docking_results/13U_energies.csv", index=False)
```

### Visualizing Docking Results
After performing the docking simulation and saving the energies, you might wish to visualize the poses. When visualizing results from molecular docking, scientists often visually inspect the 3D docked structure as well as a 2D representation called an interaction map. We can ues a software called ProLIF (Protein-Ligand Interaction Fingerprints) to make and view these maps in the Jupyter notebook.

In the step above, we wrote the poses to the file docking_results/13U.pdbqt. AutoDock Vina only writes in this file, but in order to visualize your results, we need a more standard format. We will use meeko again to convert our poses to an SDF. Note that meeko will only convert pdbqt files if it prepared the input docking files, which is one reason we used it in the previous notebook.

```python
! mk_export.py docking_results/13U.pdbqt -s docking_results/13U.sdf
```
After converting to SDF, we can again visualize our results with ProLIF. ProLIF requires that molecules be loaded in and has functions to load molecules in several ways. We will use MDAnalysis for loading our proteins to ProLIF and sdf_supplier to load the SDFs we converted in the previous step.

```python
import prolif as plf
import MDAnalysis as mda

pdb_id = "2zq2"

protein = mda.Universe(f"protein_structures/protein_h.pdb")
```


```python
protein_plf = plf.Molecule.from_mda(protein)
poses_plf = plf.sdf_supplier("docking_results/13U.sdf")
```

    /home/ab/.local/lib/python3.11/site-packages/MDAnalysis/converters/RDKit.py:473: UserWarning: No `bonds` attribute in this AtomGroup. Guessing bonds based on atoms coordinates
      warnings.warn(



```python
protein_plf = plf.Molecule.from_mda(protein)
poses_plf = plf.sdf_supplier("docking_results/13U.sdf")
```


```python
fp = plf.Fingerprint(count=True)
```


```python
# run on your poses
fp.run_from_iterable(poses_plf, protein_plf)
```



```python
pose_index=1
```
To analyze the interactions of the ligand and protein we create a molecular fingerprint object. By default, ProLIF will calculate nine types of interactions: 'Hydrophobic', 'HBAcceptor', 'HBDonor', 'Cationic', 'Anionic', 'CationPi', 'PiCation', 'PiStacking', 'VdWContact'

```python
fp.plot_lignetwork(poses_plf[pose_index])
```


![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/images/Docking/Screenshot%20from%202024-12-15%2019-44-15.png)



```python
view = fp.plot_3d(
    poses_plf[pose_index], protein_plf, frame=pose_index, display_all=False
)
view
```

![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/images/Docking/Screenshot%20from%202024-12-15%2019-44-43.png)

### References
- https://userguide.mdanalysis.org/stable/index.html
- https://autodock-vina.readthedocs.io/en/latest/docking_python.html
- https://github.com/AngelRuizMoreno/Jupyter_Dock