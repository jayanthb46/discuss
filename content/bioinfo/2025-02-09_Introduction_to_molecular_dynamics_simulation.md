+++
title = "Introduction to Molecular Dynamics simulation"
date = 2025-02-09
draft = false
tags = ["cheminformatics"]
categories = []
+++


## Introduction to Molecular Dynamics simulation 
Molecular dynamics (MD) simulations have become an indispensable tool in the study of protein-ligand interactions. These simulations offer a dynamic view of how ligands bind to proteins, revealing crucial details about the binding process, affinity, and stability of the complex. Here's an introduction to the key aspects of MD in this context:

### What are Protein-Ligand Interactions?

Proteins and ligands are two types of molecules that interact with each other in a variety of biological processes. Proteins are large biomolecules that perform a wide range of functions in living organisms, while ligands are smaller molecules that bind to proteins to trigger a specific biological response. Examples of protein-ligand interactions include:

- Enzyme-substrate binding: Enzymes are proteins that catalyze biochemical reactions, and substrates are the molecules that bind to enzymes to initiate these reactions.

- Drug-target binding: Drugs are ligands that bind to specific protein targets in the body to produce a therapeutic effect.

- Hormone-receptor binding: Hormones are ligands that bind to specific protein receptors on the surface of cells to regulate various physiological processes.

### Why Study Protein-Ligand Interactions?

Understanding protein-ligand interactions is crucial for many reasons, including:

- Drug discovery: MD simulations can help identify potential drug candidates, predict their binding affinity to target proteins, and optimize their interactions for improved efficacy and safety.

- Enzyme engineering: MD simulations can be used to study enzyme-substrate interactions and design enzymes with improved catalytic activity or specificity.

- Understanding biological processes: MD simulations can provide insights into the molecular mechanisms underlying various biological processes, such as signal transduction, gene regulation, and immune response.

### How Does MD Help?

MD simulations provide a dynamic picture of protein-ligand interactions at the atomic level. By simulating the movements of atoms and molecules over time, MD can reveal:

- Binding pose: How the ligand orients itself within the protein's binding site.
- Binding affinity: The strength of the interaction between the ligand and the protein.
- Binding kinetics: The rate at which the ligand associates and dissociates from the protein.
- Conformational changes: How the protein and ligand change shape upon binding.
- Key interactions: The specific amino acid residues in the protein that interact with the ligand.

### Steps Involved in MD Simulation of Protein-Ligand Systems

#### System Preparation:

- Obtain the 3D structure of the protein, often from the Protein Data Bank (PDB).
- Prepare the ligand molecule, including assigning appropriate charges and parameters.
- Combine the protein and ligand, placing the ligand in the binding site.
- Solvate the system, surrounding the protein-ligand complex with water molecules.
- Add ions to neutralize the system, if necessary.

#### Minimization and Equilibration:

Energy minimization to remove bad contacts and optimize the structure.
Gradual heating to bring the system to the desired temperature.
Equilibration to ensure the system is stable and ready for production runs.

#### Production MD:

Run the simulation for a sufficient time to capture the dynamics of the protein-ligand interaction.
Collect data on the positions and velocities of all atoms at regular intervals.

#### Analysis:

Analyze the trajectory data to understand the binding pose, affinity, kinetics, and other properties of the protein-ligand interaction.
Visualize the simulation results to gain insights into the dynamic behavior of the system. Analysis of the MD is beyond the scope of this article . It will be discussed in next week's post

#### Advantages of MD Simulations

- Atomic-level detail: Provides a detailed view of the interactions between the protein and ligand.
- Dynamic information: Captures the dynamic behavior of the system, including conformational changes and binding/unbinding events.
- Predictive power: Can be used to predict binding affinities and identify key interactions.

#### Limitations of MD Simulations

- Computational cost: MD simulations can be computationally expensive, especially for large systems and long timescales.
- Force field accuracy: The accuracy of MD simulations depends on the quality of the force fields used to describe the interactions between atoms.
- System size: MD simulations are limited by the size of the systems that can be simulated, although this is constantly improving with advances in computing power.

### Introduction to 2RAP

2RAP is one of the smallest protein complex with GTP. Please refer to pdb page(https://www.rcsb.org/structure/2RAP) for 
more details. It is a small G-Protein from Humans expressed in E.coli

#### Python implementation of Protein ligand complex using 2RAP protein 

```python
import copy
from pathlib import Path
import nglview
import requests
from IPython.display import display
import numpy as np
from rdkit import Chem
from rdkit.Chem import Draw
from rdkit.Chem import AllChem
import mdtraj as md
import pdbfixer
import openmm as mm
import openmm.app as app
from openmm import unit
from openff.toolkit.topology import Molecule, Topology
from openmmforcefields.generators import GAFFTemplateGenerator
import MDAnalysis as mda
```


```python
DATA="//home//ab//protein_structures//"
```


```python
from datetime import datetime
t1 = datetime.now()
print(t1)
```

    2025-02-09 19:25:54.004278



```python
pdbid = "2RAP"
ligand_name = "GTP"
pdb_path = DATA + f"{pdbid}.pdb"
pdb_url = f"https://files.rcsb.org/download/{pdbid}.pdb"
```


```python
r = requests.get(pdb_url)
r.raise_for_status()
with open(pdb_path, "wb") as f:
    f.write(r.content)
```


```python
downloaded_pdb="//home//ab//protein_structures/2RAP.pdb"
```


```python
view = nglview.show_structure_file(downloaded_pdb)
```


```python
# Show protein
view = nglview.show_structure_file(downloaded_pdb)
```


```python
view
```

![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/_posts/images/09_FEB_2025/p1.png)


### Protein preparation using PDBFixer

A proper and complete system is an essential component of effective simulation. Crystallographic structures obtained from the Protein Data Bank frequently lack atoms, particularly hydrogens, and may have non-standard residues. In this tutorial, we will use the Python library PDBFixer to prepare the protein structure. However, PDBFixer does not handle co-crystallized ligands well, therefore they will be generated separately.


```python
def prepare_protein(pdb_file, ph=7.0):
    
    """
    Use pdbfixer to prepare the protein from a PDB file. Hetero atoms such as ligands are
    removed and non-standard residues replaced. Missing atoms to existing residues are added.
    Missing residues are ignored by default, but can be included.

    Parameters
    ----------
    pdb_file: pathlib.Path or str
        PDB file containing the system to simulate.
  
    ph: float, optional
        pH value used to determine protonation state of residues

    Returns
    -------
    corrector: pdbfixer.pdbfixer.PDBFixer
        Prepared protein system.
    """
    
    corrector = pdbfixer.PDBFixer(str(pdb_file))
    corrector.removeHeterogens()  # co-crystallized ligands are unknown to PDBFixer
    corrector.findMissingResidues()  # identify missing residues, needed for identification of missing atoms
    corrector.findNonstandardResidues()  # find non-standard residue
    corrector.replaceNonstandardResidues()  # replace non-standard residues with standard one
    corrector.findMissingAtoms()  # find missing heavy atoms
    corrector.addMissingAtoms()  # add missing atoms and residues
    corrector.addMissingHydrogens(ph)  # add missing hydrogens
    return corrector
```


```python
# prepare protein and build missing residues
prepared_protein = prepare_protein(pdb_path)
```


```python
app.PDBFile.writeFile(prepared_protein.topology, prepared_protein.positions, open('//home//ab//protein_structures//corrected_2RAP.pdb', 'w'))
```

p1 = '//home//ab//protein_structures//corrected_2RAP.pdb'

```python
view = nglview.show_structure_file(p1)
```


```python
view
```

![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/_posts/images/09_FEB_2025/p2.png)


After preparing the protein, we focus on the ligand. Again, we need to add hydrogens while also ensuring that the bond ordering are accurately allocated, as certain PDB files may contain errors. We use the Python module RDKit, which is an open-source cheminformatics library. The isomeric SMILES strings for each co-crystallized ligand are available in their respective PDB entries. The ligand associated with PDB ID 2RAP is known as GTP.

```python
def prepare_ligand(pdb_file, resname, smiles, depict=True):
    """
    Prepare a ligand from a PDB file via adding hydrogens and assigning bond orders. A depiction
    of the ligand before and after preparation is rendered in 2D to allow an inspection of the
    results. 
    
    Parameters
    ----------
    pdb_file: pathlib.PosixPath
       PDB file containing the ligand of interest.
    resname: str
        Three character residue name of the ligand.
    smiles : str
        SMILES string of the ligand informing about correct protonation and bond orders.
    depict: bool, optional
        show a 2D representation of the ligand

    Returns
    -------
    prepared_ligand: rdkit.Chem.rdchem.Mol
        Prepared ligand.
    """
    # split molecule
    rdkit_mol = Chem.MolFromPDBFile(str(pdb_file))
    rdkit_mol_split = Chem.rdmolops.SplitMolByPDBResidues(rdkit_mol)

    # extract the ligand and remove any already present hydrogens
    ligand = rdkit_mol_split[resname]
    ligand = Chem.RemoveHs(ligand)

    # assign bond orders from template
    reference_mol = Chem.MolFromSmiles(smiles)
    prepared_ligand = AllChem.AssignBondOrdersFromTemplate(reference_mol, ligand)
    prepared_ligand.AddConformer(ligand.GetConformer(0))

    # protonate ligand
    prepared_ligand = Chem.rdmolops.AddHs(prepared_ligand, addCoords=True)
    prepared_ligand = Chem.MolFromMolBlock(Chem.MolToMolBlock(prepared_ligand))

    # 2D depiction
    if depict:
        ligand_2d = copy.deepcopy(ligand)
        prepared_ligand_2d = copy.deepcopy(prepared_ligand)
        AllChem.Compute2DCoords(ligand_2d)
        AllChem.Compute2DCoords(prepared_ligand_2d)
        display(
            Draw.MolsToGridImage(
                [ligand_2d, prepared_ligand_2d], molsPerRow=2, legends=["original", "prepared"]
            )
        )

    # return ligand
    return prepared_ligand

```


```python
smiles = "NC1=Nc2n(cnc2C(=O)N1)[CH]3O[CH](CO[P](O)(=O)O[P](O)(=O)O[P](O)(O)=O)[CH](O)[CH]3O"
rdkit_ligand = prepare_ligand(pdb_path, ligand_name, smiles)
```

    [19:25:55] WARNING: More than one matching pattern found - picking one
    



    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/_posts/images/09_FEB_2025/output_15_1.png)
    



```python
def rdkit_to_openmm(rdkit_mol, name="LIG"):
    
    """
    Convert an RDKit molecule to an OpenMM molecule.
    

    Parameters
    ----------
    rdkit_mol: rdkit.Chem.rdchem.Mol
        RDKit molecule to convert.
    name: str
        Molecule name.

    Returns
    -------
    omm_molecule: openmm.app.Modeller
        OpenMM modeller object holding the molecule of interest.
    """
    
    # convert RDKit to OpenFF
    OFF_mol = Molecule.from_rdkit(rdkit_mol)

    # add name for molecule
    OFF_mol.name = name

    # add names for atoms
    atom_counter_dict = {}
    for OFF_atom, rdkit_atom in zip(OFF_mol.atoms, rdkit_mol.GetAtoms()):
        atom = rdkit_atom.GetSymbol()
        if atom in atom_counter_dict.keys():
            atom_counter_dict[atom] += 1
        else:
            atom_counter_dict[atom] = 1
        OFF_atom.name = atom + str(atom_counter_dict[atom])

    # convert from OpenFF to OpenMM
    OFF_mol_topology = OFF_mol.to_topology()
    mol_topology = OFF_mol_topology.to_openmm()
    mol_positions = OFF_mol.conformers[0]

    # convert units from Ångström to nanometers
    # since OpenMM works in nm
    mol_positions = mol_positions.to("nanometers")

    # combine topology and positions in modeller object
    OMM_mol = app.Modeller(mol_topology, mol_positions)

    return OMM_mol
```


```python
OMM_ligand = rdkit_to_openmm(rdkit_ligand, ligand_name)
```

### Merge protein and ligand
The following stage is merging the prepared protein and ligand structures using the Python program MDTraj. MDTraj can handle the created protein, which is currently a PDBFixer molecule with a topology and atom locations that are identical to and interchangeable with those of OpenMM Modeller. The ligand, on the other hand, requires many conversions because it is currently an RDKit molecule.


```python
def merge_protein_and_ligand(protein, ligand):
    """
    Merge two OpenMM objects.

    Parameters
    ----------
    protein: pdbfixer.pdbfixer.PDBFixer
        Protein to merge.
    ligand: openmm.app.Modeller
        Ligand to merge.

    Returns
    -------
    complex_topology: openmm.app.topology.Topology
        The merged topology.
    complex_positions: openmm.unit.quantity.Quantity
        The merged positions.
    """
    # combine topologies
    md_protein_topology = md.Topology.from_openmm(protein.topology)  # using mdtraj for protein top
    md_ligand_topology = md.Topology.from_openmm(ligand.topology)  # using mdtraj for ligand top
    md_complex_topology = md_protein_topology.join(md_ligand_topology)  # add them together
    complex_topology = md_complex_topology.to_openmm()

    # combine positions
    total_atoms = len(protein.positions) + len(ligand.positions)

    # create an array for storing all atom positions as tupels containing a value and a unit
    # called OpenMM Quantities
    complex_positions = unit.Quantity(np.zeros([total_atoms, 3]), unit=unit.nanometers)
    complex_positions[: len(protein.positions)] = protein.positions  # add protein positions
    complex_positions[len(protein.positions) :] = ligand.positions  # add ligand positions

    return complex_topology, complex_positions
```


```python
complex_topology, complex_positions = merge_protein_and_ligand(prepared_protein, OMM_ligand)
```

    /home/ab/.conda/envs/myenv/lib/python3.12/site-packages/openmm/unit/quantity.py:753: UnitStrippedWarning: The unit of the quantity is stripped when downcasting to ndarray.
      self._value[key] = value / self.unit



```python
print("Complex topology has", complex_topology.getNumAtoms(), "atoms.")
```

    Complex topology has 2746 atoms.


### Force field¶
Common force fields, such as AMBER, include parameters for amino acids, nucleic acids, water, and ions, and typically provide multiple options based on your goal. We utilize the amber14-all.xml force field file that comes with OpenMM and contains parameters for proteins, DNA, RNA, and lipids.

Ligand parameters, however, are not included. To produce these parameters, we can utilize the General AMBER ForceField (GAFF), which is included in the Python module OpenMM Forcefields. 

```python
def generate_forcefield(
    rdkit_mol=None, protein_ff="amber14-all.xml", solvent_ff="amber14/tip3pfb.xml"
):
    """
    Generate an OpenMM Forcefield object and register a small molecule.

    Parameters
    ----------
    rdkit_mol: rdkit.Chem.rdchem.Mol
        Small molecule to register in the force field.
    protein_ff: string
        Name of the force field.
    solvent_ff: string
        Name of the solvent force field.

    Returns
    -------
    forcefield: openmm.app.Forcefield
        Forcefield with registered small molecule.
    """
    forcefield = app.ForceField(protein_ff, solvent_ff)

    if rdkit_mol is not None:
        gaff = GAFFTemplateGenerator(
            molecules=Molecule.from_rdkit(rdkit_mol, allow_undefined_stereo=True)
        )
        forcefield.registerTemplateGenerator(gaff.generator)

    return forcefield
```


```python
forcefield = generate_forcefield(rdkit_ligand)
```
The function above generate_forcefield first converts the RDKit molecule into an OpenMM Molecule object using Molecule.from_rdkit. The allow_undefined_stereo=True flag allows the conversion of molecules with undefined stereochemistry (useful for some molecules that may not have defined stereochemistry).

It then creates a GAFFTemplateGenerator object from the RDKit molecule. The GAFF (Generalized Amber Force Field) template generator is used to add small molecules to the force field with the appropriate parameters for bonding, charges, etc.
Finally, it registers the GAFF template generator with the OpenMM ForceField, allowing the force field to include parameters for the small molecule during the simulation.

```python
modeller = app.Modeller(complex_topology, complex_positions)
modeller.addSolvent(forcefield, padding=1.0 * unit.nanometers, ionicStrength=0.15 * unit.molar)
```

### System for MD simulation

Using our configured force field, we can now utilize the OpenMM Modeller class to establish the molecular dynamics (MD) environment, which includes a simulation box that holds the complex and is filled with a solvent. The standard solvent employed is water, along with a specific concentration of ions. The box's dimensions can be determined through several methods. We set the size by incorporating padding, resulting in a cubic box whose dimensions are based on the largest dimension of the complex.


```python
system = forcefield.createSystem(modeller.topology, nonbondedMethod=app.PME)
integrator = mm.LangevinIntegrator(
    300 * unit.kelvin, 1.0 / unit.picoseconds, 2.0 * unit.femtoseconds
)
simulation = app.Simulation(modeller.topology, system, integrator)
simulation.context.setPositions(modeller.positions)
```
The function `forcefield.createSystem()` function creates a system using a force field, which defines the mathematical models for interactions (like bonds, angles, van der Waals forces, etc.) between atoms in the system. This function uses the topology parameters generated above and also accepts parameters for non bonded atoms. PME (Particle Mesh Ewald) is a method to efficiently compute long-range electrostatic interactions, which are essential for large systems or systems with periodic boundary conditions.This specifies the method for handling nonbonded interactions, such as electrostatic forces, during the simulation.


`integrator = mm.LangevinIntegrator(300 * unit.kelvin, 1.0 / unit.picoseconds, 2.0 * unit.femtoseconds)`

LangevinIntegrator: This is a class used for integrating the equations of motion in molecular dynamics simulations using the Langevin equation. The Langevin equation introduces friction and random noise to the system, simulating a thermostat (to control temperature) and maintaining the system at a desired temperature.
The parameters passed to the LangevinIntegrator are:

300 * unit.kelvin: This sets the temperature of the system to 300 K, which is typical for simulations of biological systems at room temperature.

1.0 / unit.picoseconds: This represents the friction coefficient (in inverse time units), with a value of 1 ps⁻¹, which controls the damping effect (viscous drag) on the particles. It is related to the time scale at which the system relaxes to equilibrium.

2.0 * unit.femtoseconds: This sets the time step for the integrator to 2 femtoseconds (fs), which is a common choice in molecular dynamics to ensure the simulation progresses without numerical instability.


### Perform MD Analysis

Now that everything is set up, we can perform the simulation. We need to set starting positions and minimize the energy of the system to get a low energy starting configuration, which is important to decrease the chance of simulation failures due to severe atom clashes. The energy minimized system is saved.
Once the minimization has finished, we can perform the MD simulation.


```python
simulation.minimizeEnergy()
```


```python
pq = pdb_path+"2RAP_topology.pdb"
```


```python
with open(pq, "w") as pdb_file:
    app.PDBFile.writeFile(
        simulation.topology,
        simulation.context.getState(getPositions=True, enforcePeriodicBox=True).getPositions(),
        file=pdb_file,
        keepIds=True,
    )
```


```python
pq1 = pdb_path+"2RAP_trajectory.xtc"
```


```python
import sys
```

We will simulate only 10 picco seconds(ps) of molecular dynamics corresponding to 1 step of 2 femto seconds each. We save molecular “snapshots” every 2 femto seconds, for a total of 10 frames.

```python
steps = 10  # corresponds to 20 fs
write_interval = 1  # write every 2 fs
log_interval = 1  # log progress to stdout every 2 fs
simulation.reporters.append(
    md.reporters.XTCReporter(file=pq1, reportInterval=write_interval)
)
simulation.reporters.append(
    app.StateDataReporter(
        sys.stdout,
        log_interval,
        step=True,
        potentialEnergy=True,
        temperature=True,
        progress=True,
        remainingTime=True,
        speed=True,
        totalSteps=steps,
        separator="\t",
    )
)
```
Then the simulation is performed by taking the steps defined before.

```python
simulation.context.setVelocitiesToTemperature(300 * unit.kelvin)
simulation.step(steps)  # perform the simulation
```

    #"Progress (%)"	"Step"	"Potential Energy (kJ/mole)"	"Temperature (K)"	"Speed (ns/day)"	"Time Remaining"
    10.0%	1	-441529.31611102127	301.5097776415006	0	--
    20.0%	2	-435509.6149680956	267.53918097067225	2.12	0:00
    30.0%	3	-428021.7799656409	240.57558934199153	1.81	0:00
    40.0%	4	-420530.2380666369	198.18200001529456	1.7	0:00
    50.0%	5	-418321.4414802611	178.38793791036878	1.63	0:00
    60.0%	6	-414445.80218456977	166.25923712839457	1.57	0:00
    70.0%	7	-413161.98587629275	150.10639758921053	1.51	0:00
    80.0%	8	-414648.98732863046	154.8126973986732	1.46	0:00
    90.0%	9	-415199.94775046076	159.2717018732298	1.42	0:00
    100.0%	10	-417514.6675399916	165.90535273536392	1.38	0:00



```python
from datetime import datetime
t2 = datetime.now()
print(t2)
```

    2025-02-09 19:36:21.315608



```python
t = md.load(pq1, top=pq)
```


```python
print(t)
```

    <mdtraj.Trajectory with 10 frames, 23646 atoms, 7177 residues, and unitcells>

### Conclusion

MD simulations are a powerful tool for studying protein-ligand interactions. They provide valuable insights into the binding process, which is essential for drug discovery, enzyme engineering, and understanding biological processes. Despite some limitations, MD simulations continue to advance and play an increasingly important role in our understanding of the molecular world.

