+++
title = "Hamming Distance"
date = 2022-01-02
draft = false
tags = ["Algorithms"]
categories = []
+++

## Hamming Distance 
Hamming Distance is used to measure/count nucleotide differences in DNA/RNA. This is very useful when we are searching for a particular pattern such as kmer/motif in a genome with specified number mutations or with exact match.Hamming Distance between 2 equal length of strings of symbols is the number of positions at which corresponding symbols are different.
Hamming Distance between ATCGATCG and ATCCATGG is 2.


```python
ATCGATCG
ATCCATGG
   *  *
```

positions of stars indicate mismatches. There are 2 mismatches between strings ATCGATCG and ATCCATGG , therefore hamming distance is 2.

```python
dna_str_1 = "TTCGATCCATTG"
dna_str_2 = "ATCAATCGATCG"
```

### Loop based Method 
Loop based method iterates bases in 2 strings ,increment the position of difference by 1.

```python
# Loop approach
def h_d_loop(str_1, str_2):
    h_distance = 0
    for position in range(len(str_1)):
        if str_1[position] != str_2[position]:
            h_distance += 1
    return h_distance
```

### Set based method
The set data structure for 2 strings are created after enumeration. The positions at which 2 sets differ are identified to compute hamming distance.

```python
def h_d_set(str_1, str_2):
    nucleotide_set_1 = set([(x, y) for x, y in enumerate(str_1)])
    nucleotide_set_2 = set([(x, y) for x, y in enumerate(str_2)])

    return len(nucleotide_set_1.difference(nucleotide_set_2))
```

### Zip based Approach
The Zip based approach iterates 2 strings like a loop to identify positions which are different between 2 strings to calculate hamming distance.

```python
# Zip Approach
def h_d_zip(str_1, str_2):
    return len([(n1, n2) for n1, n2 in zip(str_1, str_2) if n1 != n2])
```


```python
print("Loop Hamming Distance: ", end='')
print(h_d_loop(dna_str_1, dna_str_2))
```

Loop Hamming Distance: 4
    


```python
print("Set Hamming Distance: ", end='')
print(h_d_set(dna_str_1, dna_str_2))
```

Set Hamming Distance: 4
    


```python
print("Zip Hamming Distance: ", end='')
print(h_d_zip(dna_str_1, dna_str_2))
```

Zip Hamming Distance: 4
    

### References
- https://en.wikipedia.org/wiki/Hamming_distance
