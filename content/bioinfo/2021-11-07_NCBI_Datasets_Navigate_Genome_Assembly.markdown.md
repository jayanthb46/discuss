+++
title = "NCBI Datasets Navigate Genome Assembly"
date = 2021-11-07
draft = false
tags = ["NCBI Datasets"]
categories = []
+++

## NCBI Datasets Navigate Genome Assembly

The `ncbi.datasets` python library can be used to query NCBI datasets and navigate through the results quickly within python. 

### Getting started

First, let's import the python modules we'll use.  Be sure you have installed ncbi-datasets api using pip python package manager


```python
import sys
import zipfile
import pandas as pd
from pprint import pprint
from datetime import datetime
from collections import defaultdict, Counter
from IPython.display import display

import matplotlib.pyplot as plt
plt.style.use('ggplot')

try:
    import ncbi.datasets
except ImportError:
    print('ncbi.datasets module not found. To install, run `pip install ncbi-datasets-pylib`.')
```

## Genome summaries

Genome summaries include all the metadata you'll need, and can be accessed in four ways:
1. accession: an NCBI Assembly accession
2. organism: an organism or a taxonomical group name 
3. taxid: using an NCBI Taxonomy identifier, at any level.
4. BioProject: using an NCBI BioProject accession

First, we'll need an api object specific to retrieving assembly descriptors.  To see all the possible API instances, [visit the documentation on GitHub](https://github.com/ncbi/datasets/tree/master/client_docs/python#documentation-for-api-endpoints)

For retrieving Genome Assemblies we used GenomeApi


```python
## start an api_instance 
api_instance = ncbi.datasets.GenomeApi(ncbi.datasets.ApiClient())
```

### Genome summaries by accession

Let's start with the simplest case.  Say you already know the NCBI Assembly accession, for example, for the latest human reference genome assembly (GRCh38), `GCF_000001405.39`.  Using the `assembly_descriptors_by_accessions()` method, we'll get back a `V1AssemblyDatasetDescriptors` object ([documented here](https://github.com/ncbi/datasets/blob/master/client_docs/python/docs/V1AssemblyDatasetDescriptors.md)) (a mouthful we know).  The various fields in the response are attributes of the object.

Let's see this in practice.


```python
assembly_accessions = ['GCF_000001405.39'] ## needs to be a full accession.version

genome_summary = api_instance.assembly_descriptors_by_accessions(assembly_accessions, page_size=1)

type(genome_summary)

```




    ncbi.datasets.openapi.models.v1alpha1_assembly_metadata.V1alpha1AssemblyMetadata



`genome_summary` contains metadata about the genome assembly and the total count of results in JSON format.


```python
print(f"Number of assemblies: {genome_summary.total_count}" )
```

    Number of assemblies: 1



```python
## print other information 
for assembly in map(lambda d: d.assembly, genome_summary.assemblies):
    print(
        assembly.assembly_accession,
        assembly.assembly_level,
        len(assembly.chromosomes),
        assembly.submission_date,
        sep='\t')
```

    GCF_000001405.39	Chromosome	26	2019-02-28



```python
print(genome_summary)
```

    {'assemblies': [{'assembly': {'annotation_metadata': {'file': [{'estimated_size': '50932266',
                                                                    'type': 'GENOME_GFF'},
                                                                   {'estimated_size': '1315687838',
                                                                    'type': 'GENOME_GBFF'},
                                                                   {'estimated_size': '119000227',
                                                                    'type': 'RNA_FASTA'},
                                                                   {'estimated_size': '26333641',
                                                                    'type': 'PROT_FASTA'},
                                                                   {'estimated_size': '43986696',
                                                                    'type': 'GENOME_GTF'},
                                                                   {'estimated_size': '26452904',
                                                                    'type': 'CDS_FASTA'}],
                                                          'name': 'NCBI Annotation '
                                                                  'Release '
                                                                  '109.20210514',
                                                          'release_date': '2021-05-14',
                                                          'release_number': '109.20210514',
                                                          'report_url': 'https://www.ncbi.nlm.nih.gov/genome/annotation_euk/Homo_sapiens/109.20210514/',
                                                          'source': 'NCBI',
                                                          'stats': {'gene_counts': {'protein_coding': 19527,
                                                                                    'total': 54585}}},
                                  'assembly_accession': 'GCF_000001405.39',
                                  'assembly_category': 'reference genome',
                                  'assembly_level': 'Chromosome',
                                  'bioproject_lineages': [{'bioprojects': [{'accession': 'PRJNA31257',
                                                                            'parent_accession': None,
                                                                            'parent_accessions': None,
                                                                            'title': 'The '
                                                                                     'Human '
                                                                                     'Genome '
                                                                                     'Project, '
                                                                                     'currently '
                                                                                     'maintained '
                                                                                     'by '
                                                                                     'the '
                                                                                     'Genome '
                                                                                     'Reference '
                                                                                     'Consortium '
                                                                                     '(GRC)'}]}],
                                  'biosample_accession': None,
                                  'chromosomes': ["{'name': '1', 'length': "
                                                  "'248956422', "
                                                  "'accession_version': "
                                                  "'NC_000001.11', 'gc_count': "
                                                  "'103993629'}",
                                                  "{'name': '2', 'length': "
                                                  "'242193529', "
                                                  "'accession_version': "
                                                  "'NC_000002.12', 'gc_count': "
                                                  "'101405001'}",
                                                  "{'name': '3', 'length': "
                                                  "'198295559', "
                                                  "'accession_version': "
                                                  "'NC_000003.12', 'gc_count': "
                                                  "'91982922'}",
                                                  "{'name': '4', 'length': "
                                                  "'190214555', "
                                                  "'accession_version': "
                                                  "'NC_000004.12', 'gc_count': "
                                                  "'77057701'}",
                                                  "{'name': '5', 'length': "
                                                  "'181538259', "
                                                  "'accession_version': "
                                                  "'NC_000005.10', 'gc_count': "
                                                  "'93753282'}",
                                                  "{'name': '6', 'length': "
                                                  "'170805979', "
                                                  "'accession_version': "
                                                  "'NC_000006.12', 'gc_count': "
                                                  "'68907366'}",
                                                  "{'name': '7', 'length': "
                                                  "'159345973', "
                                                  "'accession_version': "
                                                  "'NC_000007.14', 'gc_count': "
                                                  "'70693857'}",
                                                  "{'name': '8', 'length': "
                                                  "'145138636', "
                                                  "'accession_version': "
                                                  "'NC_000008.11', 'gc_count': "
                                                  "'76166139'}",
                                                  "{'name': '9', 'length': "
                                                  "'138394717', "
                                                  "'accession_version': "
                                                  "'NC_000009.12', 'gc_count': "
                                                  "'53057782'}",
                                                  "{'name': '10', 'length': "
                                                  "'133797422', "
                                                  "'accession_version': "
                                                  "'NC_000010.11', 'gc_count': "
                                                  "'59342366'}",
                                                  "{'name': '11', 'length': "
                                                  "'135086622', "
                                                  "'accession_version': "
                                                  "'NC_000011.10', 'gc_count': "
                                                  "'70266136'}",
                                                  "{'name': '12', 'length': "
                                                  "'133275309', "
                                                  "'accession_version': "
                                                  "'NC_000012.12', 'gc_count': "
                                                  "'58038673'}",
                                                  "{'name': '13', 'length': "
                                                  "'114364328', "
                                                  "'accession_version': "
                                                  "'NC_000013.11', 'gc_count': "
                                                  "'38619357'}",
                                                  "{'name': '14', 'length': "
                                                  "'107043718', "
                                                  "'accession_version': "
                                                  "'NC_000014.9', 'gc_count': "
                                                  "'46277352'}",
                                                  "{'name': '15', 'length': "
                                                  "'101991189', "
                                                  "'accession_version': "
                                                  "'NC_000015.10', 'gc_count': "
                                                  "'46948716'}",
                                                  "{'name': '16', 'length': "
                                                  "'90338345', "
                                                  "'accession_version': "
                                                  "'NC_000016.10', 'gc_count': "
                                                  "'48970946'}",
                                                  "{'name': '17', 'length': "
                                                  "'83257441', "
                                                  "'accession_version': "
                                                  "'NC_000017.11', 'gc_count': "
                                                  "'46944406'}",
                                                  "{'name': '18', 'length': "
                                                  "'80373285', "
                                                  "'accession_version': "
                                                  "'NC_000018.10', 'gc_count': "
                                                  "'40613699'}",
                                                  "{'name': '19', 'length': "
                                                  "'58617616', "
                                                  "'accession_version': "
                                                  "'NC_000019.10', 'gc_count': "
                                                  "'32693605'}",
                                                  "{'name': '20', 'length': "
                                                  "'64444167', "
                                                  "'accession_version': "
                                                  "'NC_000020.11', 'gc_count': "
                                                  "'28406612'}",
                                                  "{'name': '21', 'length': "
                                                  "'46709983', "
                                                  "'accession_version': "
                                                  "'NC_000021.9', 'gc_count': "
                                                  "'18981353'}",
                                                  "{'name': '22', 'length': "
                                                  "'50818468', "
                                                  "'accession_version': "
                                                  "'NC_000022.11', 'gc_count': "
                                                  "'20769235'}",
                                                  "{'name': 'X', 'length': "
                                                  "'156040895', "
                                                  "'accession_version': "
                                                  "'NC_000023.11', 'gc_count': "
                                                  "'67807309'}",
                                                  "{'name': 'Y', 'length': "
                                                  "'57227415', "
                                                  "'accession_version': "
                                                  "'NC_000024.10', 'gc_count': "
                                                  "'10979775'}",
                                                  "{'name': 'Un', 'length': "
                                                  "'11436572', 'gc_count': "
                                                  "'1749548'}",
                                                  "{'name': 'MT', 'length': "
                                                  "'16569', 'accession_version': "
                                                  "'NC_012920.1', 'gc_count': "
                                                  "'7350'}"],
                                  'contig_n50': 57879411,
                                  'display_name': 'GRCh38.p13',
                                  'estimated_size': '2414751949',
                                  'org': {'assembly_count': None,
                                          'assembly_counts': {'node': 894,
                                                              'subtree': 894},
                                          'blast_node': None,
                                          'breed': None,
                                          'children': None,
                                          'common_name': 'human',
                                          'counts': None,
                                          'cultivar': None,
                                          'ecotype': None,
                                          'icon': None,
                                          'isolate': None,
                                          'key': '9606',
                                          'max_ord': None,
                                          'merged': None,
                                          'merged_tax_ids': None,
                                          'min_ord': None,
                                          'parent_tax_id': '9605',
                                          'rank': 'SPECIES',
                                          'sci_name': 'Homo sapiens',
                                          'search_text': None,
                                          'sex': None,
                                          'strain': None,
                                          'tax_id': '9606',
                                          'title': 'human',
                                          'weight': None},
                                  'paired_assembly_accession': 'GCA_000001405.28',
                                  'seq_length': '3099706404',
                                  'submission_date': '2019-02-28',
                                  'submitter': 'Genome Reference Consortium'},
                     'messages': None}],
     'messages': None,
     'next_page_token': None,
     'total_count': 1}


### Genome summaries by organism or tax group name 

Now let's say you only know the name of the organism for which you want to retrieve assembly information.


```python
## a few examples to try 
# tax_name = 'mammals'
# tax_name = 'birds'
# tax_name = 'butterflies'
tax_name = 'primates'

genome_summary = api_instance.assembly_descriptors_by_taxon(
    taxon=tax_name,
    page_size=1000)
print(f"Number of assemblies in the group '{tax_name}': {genome_summary.total_count}")
```

    Number of assemblies in the group 'primates': 1043


#### Assemblies organized by GenBank/RefSeq
Now we can analyze the results and organize by GenBank and RefSeq, and make a nice tabular output and pie-chart of the results.


```python
assm_counter = Counter()
for assembly in map(lambda d: d.assembly, genome_summary.assemblies):
    if assembly.assembly_accession[:3] == 'GCA':
        assm_counter['GenBank'] += 1
    elif assembly.assembly_accession[:3] == 'GCF':
        assm_counter['RefSeq'] += 1
    
print(assm_counter)
```

    Counter({'GenBank': 965, 'RefSeq': 35})



```python
df = pd.DataFrame.from_dict(assm_counter, orient='index', columns=['count'])
display(df)
df.plot(kind='pie', y='count', figsize=(6,6), title='Assemblies by type')
```


<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>count</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>GenBank</th>
      <td>965</td>
    </tr>
    <tr>
      <th>RefSeq</th>
      <td>35</td>
    </tr>
  </tbody>
</table>
</div>





    <matplotlib.axes._subplots.AxesSubplot at 0x7f5b42ef6a00>




![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/Nov7_2021/output_15_2.png)


#### Assemblies organized by assembly level
Alternatively, we can organize by the assembly level (scaffold, contig or choromosome).


```python
assm_level = Counter()
for assembly in map(lambda d: d.assembly, genome_summary.assemblies):
    assm_level[assembly.assembly_level] += 1
    
print(assm_level)
```

    Counter({'Scaffold': 828, 'Contig': 103, 'Chromosome': 68, 'Complete Genome': 1})



```python
df = pd.DataFrame.from_dict(assm_level, orient='index', columns=['count'])
display(df)
df.plot(kind='pie', y='count', figsize=(6,6), title='Assemblies by level',)
```


<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>count</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>Scaffold</th>
      <td>828</td>
    </tr>
    <tr>
      <th>Chromosome</th>
      <td>68</td>
    </tr>
    <tr>
      <th>Contig</th>
      <td>103</td>
    </tr>
    <tr>
      <th>Complete Genome</th>
      <td>1</td>
    </tr>
  </tbody>
</table>
</div>





    <matplotlib.axes._subplots.AxesSubplot at 0x7f5b42fd4190>




![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/Nov7_2021/output_18_2.png)


#### Assemblies grouped and counted by annotation release number

All RefSeq assemblies are annotated and each annotation release is numbered, starting from 100. A quick way to check if the latest annotation is the first time an assembly for that organism was annotated is to check the annotation release number. Anything above 100 can be interpreted to have been through multiple annotations. 

For example, in the analysis shown below, the human assembly has an annotation release number 109 indicating that a human assembly was annotated multiple times. On the other hand, the silvery gibbon assembly has an annotation release number of 100 indicating that this is the first time an assembly from this organism was annotated. 


```python
## out of the 28 RefSeq assemblies, how many have been annotated more than once? 
annot_counter = Counter()
for assembly in map(lambda d: d.assembly, genome_summary.assemblies):
    if assembly.assembly_accession.startswith('GCF') and assembly.annotation_metadata and assembly.annotation_metadata.release_number:
        rel = int(assembly.annotation_metadata.release_number.split('.')[0])
        annot_counter[rel] += 1
pprint(annot_counter)
```

    Counter({101: 7, 100: 4, 105: 3, 102: 3, 103: 3, 104: 2, 109: 1})



```python
df = pd.DataFrame.from_dict(annot_counter, orient='index', columns=['count']).sort_index()
df.plot(kind='bar', y='count', figsize=(9,6))
```




    <matplotlib.axes._subplots.AxesSubplot at 0x7f5b43133b20>




![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/Nov7_2021/output_21_1.png)


#### Assemblies organized by annotation release number


```python
annot_list = defaultdict(list)
for assembly in map(lambda d: d.assembly, genome_summary.assemblies):
    if assembly.assembly_accession.startswith('GCF') and assembly.annotation_metadata:
        rel = assembly.annotation_metadata.release_number
        annot_list[rel].append(assembly.assembly_accession + ' ' + assembly.org.title)
pprint(annot_list)
```

    defaultdict(<class 'list'>,
                {None: ['GCF_000151905.2 western lowland gorilla',
                        'GCF_000306695.2 human',
                        'GCF_000772875.2 Rhesus monkey',
                        'GCF_000146795.2 northern white-cheeked gibbon'],
                 '100': ['GCF_000955945.1 sooty mangabey',
                         'GCF_000951035.1 Colobus angolensis palliatus',
                         'GCF_009828535.1 silvery gibbon',
                         'GCF_000951045.1 drill'],
                 '101': ["GCF_000952055.2 Ma's night monkey",
                         'GCF_000164805.1 Philippine tarsier',
                         'GCF_001604975.1 Panamanian white-faced capuchin',
                         'GCF_009828535.2 silvery gibbon',
                         'GCF_000364345.1 crab-eating macaque',
                         'GCF_000956065.1 pig-tailed macaque',
                         'GCF_000165445.2 gray mouse lemur'],
                 '102': ['GCF_015252025.1 green monkey',
                         'GCF_008122165.1 western lowland gorilla',
                         'GCF_000181295.1 small-eared galago'],
                 '103': ['GCF_003339765.1 Rhesus monkey',
                         'GCF_006542625.1 northern white-cheeked gibbon',
                         'GCF_000258655.2 pygmy chimpanzee'],
                 '104': ['GCF_000004665.1 white-tufted-ear marmoset',
                         'GCF_013052645.1 pygmy chimpanzee'],
                 '105': ['GCF_009663435.1 white-tufted-ear marmoset',
                         'GCF_002880755.1 chimpanzee'],
                 '105.20201022': ['GCF_000001405.25 human'],
                 '109.20210514': ['GCF_000001405.39 human']})


### Genomes summaries by taxid
Finally, you may want to query for genome assemblies by NCBI Taxonomy ID.  


```python
# taxid = '8782' ## birds
# taxid = '7898' ## ray-finned fish
taxid = '37572' ## butterflies

genome_summary = api_instance.assembly_descriptors_by_taxon(
    taxon=taxid,
    page_size=100,
    filters_assembly_source='refseq')
```


```python
print(f"Number of assemblies: {genome_summary.total_count}")
```

    Number of assemblies: 11



```python
## count number of GenBank and RefSeq assemblies
## all are RefSeq assemblies because 'filters_refseq_only=True' above
assm_counter = Counter()
for assembly in map(lambda d: d.assembly, genome_summary.assemblies):
    assm_counter[assembly.assembly_accession[:3]] += 1
    
print(assm_counter)
```

    Counter({'GCF': 11})


#### Group assemblies by annotation date


```python
annot_rel_dates = Counter()
for assembly in map(lambda d: d.assembly, genome_summary.assemblies):
    if not assembly.annotation_metadata:
        continue
    rel_year = datetime.strptime(assembly.annotation_metadata.release_date, '%Y-%M-%d').year
    annot_rel_dates[rel_year] += 1
pprint(sorted(annot_rel_dates.items()))
```

    [(2015, 3), (2017, 1), (2018, 2), (2020, 3), (2021, 2)]



```python
df = pd.DataFrame.from_dict(annot_rel_dates, orient='index', columns=['count']).sort_index()
df.plot(kind='barh', y='count', figsize=(9,6))
```




    <matplotlib.axes._subplots.AxesSubplot at 0x7f5b42be07c0>




![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/Nov7_2021/output_30_1.png)



```python
genome_table = {}
for assembly in map(lambda d: d.assembly, genome_summary.assemblies):
    if not assembly.annotation_metadata:
        continue
    n_chr = len(assembly.chromosomes) if assembly.assembly_level == 'Chromosome' else None
    genome_table[assembly.assembly_accession] = {
        'assm_name': assembly.display_name,
        'annot_rel_date': assembly.annotation_metadata.release_date,
        'annot_rel_num': assembly.annotation_metadata.release_number,
        'assm_level': assembly.assembly_level,
        'num_chromosomes': n_chr,
        'contig_n50': assembly.contig_n50,
        'seq_length': assembly.seq_length,
        'submission_date': assembly.submission_date }
df = pd.DataFrame.from_dict(genome_table, orient='columns')
display(df)
```


<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>GCF_905147365.1</th>
      <th>GCF_900239965.1</th>
      <th>GCF_009731565.1</th>
      <th>GCF_902806685.1</th>
      <th>GCF_001298355.1</th>
      <th>GCF_000836215.1</th>
      <th>GCF_000836235.1</th>
      <th>GCF_905163445.1</th>
      <th>GCF_001856805.1</th>
      <th>GCF_002938995.1</th>
      <th>GCF_012273895.1</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>assm_name</th>
      <td>ilAriAges1.1</td>
      <td>Bicyclus_anynana_v1.2</td>
      <td>Dplex_v4</td>
      <td>iAphHyp1.1</td>
      <td>Pap_ma_1.0</td>
      <td>Ppol_1.0</td>
      <td>Pxut_1.0</td>
      <td>ilParAegt1.1</td>
      <td>P_rapae_3842_assembly_v2</td>
      <td>ASM293899v1</td>
      <td>Zerene_cesonia_1.1</td>
    </tr>
    <tr>
      <th>annot_rel_date</th>
      <td>2021-06-24</td>
      <td>2018-02-16</td>
      <td>2020-02-24</td>
      <td>2020-12-18</td>
      <td>2015-10-28</td>
      <td>2015-07-30</td>
      <td>2015-07-31</td>
      <td>2021-03-05</td>
      <td>2017-08-08</td>
      <td>2018-10-03</td>
      <td>2020-12-23</td>
    </tr>
    <tr>
      <th>annot_rel_num</th>
      <td>100</td>
      <td>100</td>
      <td>100</td>
      <td>100.20201218</td>
      <td>100</td>
      <td>100</td>
      <td>100</td>
      <td>100</td>
      <td>100</td>
      <td>100</td>
      <td>100</td>
    </tr>
    <tr>
      <th>assm_level</th>
      <td>Chromosome</td>
      <td>Scaffold</td>
      <td>Chromosome</td>
      <td>Chromosome</td>
      <td>Scaffold</td>
      <td>Scaffold</td>
      <td>Scaffold</td>
      <td>Chromosome</td>
      <td>Scaffold</td>
      <td>Scaffold</td>
      <td>Chromosome</td>
    </tr>
    <tr>
      <th>num_chromosomes</th>
      <td>24</td>
      <td>None</td>
      <td>31</td>
      <td>30</td>
      <td>None</td>
      <td>None</td>
      <td>None</td>
      <td>30</td>
      <td>None</td>
      <td>None</td>
      <td>30</td>
    </tr>
    <tr>
      <th>contig_n50</th>
      <td>17774651</td>
      <td>78697</td>
      <td>108026</td>
      <td>2012761</td>
      <td>92238</td>
      <td>47768</td>
      <td>128246</td>
      <td>19375358</td>
      <td>54957</td>
      <td>254123</td>
      <td>43484</td>
    </tr>
    <tr>
      <th>seq_length</th>
      <td>435265205</td>
      <td>475399557</td>
      <td>248676414</td>
      <td>408137179</td>
      <td>278421261</td>
      <td>227005758</td>
      <td>243890167</td>
      <td>516571003</td>
      <td>245871251</td>
      <td>357124929</td>
      <td>266405969</td>
    </tr>
    <tr>
      <th>submission_date</th>
      <td>2021-01-28</td>
      <td>2018-01-02</td>
      <td>2019-12-11</td>
      <td>2020-02-22</td>
      <td>2015-09-28</td>
      <td>2015-02-02</td>
      <td>2015-02-02</td>
      <td>2021-01-28</td>
      <td>2016-10-16</td>
      <td>2018-02-23</td>
      <td>2020-10-02</td>
    </tr>
  </tbody>
</table>
</div>


## Genome assembly downloads
So far, we have looked at interacting with genome summaries, which describe the essential metadata for genome assemblies. In addition to metadata, the Datasets API can be used to download a genome dataset consisting of genome, transcript, and protein sequences in FASTA format, as well as annotation data in gff3, gtf, and GenBank flat file formats.

To illustrate, let's start by downloading a genome dataset including mitochondrial genome sequence and all protein sequences for the latest human genome assembly, GRCh38.


```python
assembly_accessions = ['GCF_000001405.39']
chromosomes = ['MT']
exclude_sequence = False
include_annotation_type = ['PROT_FASTA']

api_response = api_instance.download_assembly_package(
    assembly_accessions,
    chromosomes=chromosomes,
    exclude_sequence=exclude_sequence,
    include_annotation_type=include_annotation_type,
    # Because we are streaming back the results to disk, 
    # we should defer reading/decoding the response
    _preload_content=False
)

with open('human_assembly.zip', 'wb') as f:
    f.write(api_response.data)

```

Now we'll unzip the downloaded zip archive. All data is contained in ncbi_dataset/data. Data that is specific to the human reference genome, GRCh38, is contained within a subdirectory named with that assembly accession, GCF_000001405.39. 
The data directory contains five files:
 1. The assembly data report contains assembly information like sequence names,  NCBI accessions, UCSC-style chromosome names, and annotation statistics (gene counts). Note that this file is directly under the data directory and not in the subdirectory named with the assembly accession. When the genome dataset contains data for multiple assemblies, genome assembly metadata for all of these assemblies is contained in the `assembly_data_report.jsonl` file
 2. The sequence report (`sequence_report.jsonl`) contains a list of the sequences that comprise the GRCh38 assembly
 3. The nucleotide sequence in FASTA (nucleotide) format for the one "chromosome" we requested: `chrMT.fna`
 4. All protein sequences in FASTA (amino acid) format: `protein.faa`
 5. And finally, a dataset catalog file (`dataset_catalog.json`) that describes the contents of the archive, to aid in programmatic access.
Read more about the contents in the [download assembly command
 section of the documentation](https://www.ncbi.nlm.nih.gov/datasets/docs/command-line-assembly/).  


```python
!unzip -v human_assembly.zip
```

    Archive:  human_assembly.zip
     Length   Method    Size  Cmpr    Date    Time   CRC-32   Name
    --------  ------  ------- ---- ---------- ----- --------  ----
        1599  Defl:N      770  52% 2021-11-07 02:36 8fff0de4  README.md
        1462  Defl:N      778  47% 2021-11-07 02:36 c0d678b4  ncbi_dataset/data/assembly_data_report.jsonl
       16834  Defl:N     5379  68% 2021-11-07 02:36 932c3ae8  ncbi_dataset/data/GCF_000001405.39/chrMT.fna
    86751124  Defl:N 26333623  70% 2021-11-07 02:52 951c4924  ncbi_dataset/data/GCF_000001405.39/protein.faa
         228  Defl:N      173  24% 2021-11-07 02:53 2d8e085c  ncbi_dataset/data/GCF_000001405.39/sequence_report.jsonl
         520  Defl:N      230  56% 2021-11-07 02:53 cc604b6d  ncbi_dataset/data/dataset_catalog.json
    --------          -------  ---                            -------
    86771767         26340953  70%                            6 files


## Using genome summary data to request genome datasets

When you need to download a genome dataset for a particular taxonomic group, you'll need to first get the list of genome assembly accessions, then you can query by accession to download the data that you're interested in.

In this example, we'll download a genome dataset for a list of bird RefSeq genomes annotated in 2020.

1. Fetch a list of RefSeq assembly accessions for all rodent genomes using `assembly_descriptors_by_taxid` 
2. Filter assemblies that were annotated in 2020
3. Download data, but in this case, retrieve a dehydrated zip archive that can be rehydrated later to obtain the sequence data itself.


```python
genome_summary = api_instance.assembly_descriptors_by_taxon(
    taxon='9989', ## Rodents taxid
    page_size=1000,
    filters_assembly_source='refseq')

print(f'Number of assemblies: {genome_summary.total_count}')
```

    Number of assemblies: 46



```python
annots_by_year = Counter()
for assembly in map(lambda d: d.assembly, genome_summary.assemblies):
    if assembly.annotation_metadata:
        annot_year = int(assembly.annotation_metadata.release_date.split('-')[0])
        annots_by_year[annot_year] += 1
    else:
        print(f'No annotation for {assembly.assembly_accession}')
    
print(f'Assemblies grouped by year of annotation')
pprint(sorted(annots_by_year.items()))
```

    No annotation for GCF_000230445.1
    No annotation for GCF_000002165.2
    Assemblies grouped by year of annotation
    [(2013, 1),
     (2015, 3),
     (2016, 5),
     (2017, 5),
     (2018, 5),
     (2019, 7),
     (2020, 11),
     (2021, 7)]



```python
rodents_annotated_in_2020_accs = []
for assembly in map(lambda d: d.assembly, genome_summary.assemblies):
    if not assembly.annotation_metadata:
        continue
    annot_year = int(assembly.annotation_metadata.release_date.split('-')[0])
    if annot_year == 2020:
        rodents_annotated_in_2020_accs.append(assembly.assembly_accession)
        
print('Rodent assemblies that were annotated in 2020:')
print(f'{", ".join(rodents_annotated_in_2020_accs)}')
```

    Rodent assemblies that were annotated in 2020:
    GCF_011762505.1, GCF_903992535.1, GCF_000223135.1, GCF_003668045.3, GCF_012274545.1, GCF_003676075.2, GCF_000001635.27, GCF_000001635.26, GCF_903995425.1, GCF_004664715.2, GCF_011064425.1



```python
assm_table = {}
for assembly in map(lambda d: d.assembly, genome_summary.assemblies):
    if not assembly.annotation_metadata:
        continue

    annot_year = int(assembly.annotation_metadata.release_date.split('-')[0])
    if annot_year == 2019:
        n_chr = len(assembly.chromosomes) if assembly.assembly_level == 'Chromosome' else None
        assm_table[assembly.assembly_accession] = {
            'assm_name': assembly.display_name,
            'org_name': assembly.org.title,
            'sci_name': assembly.org.sci_name,
            'annot_rel_date': assembly.annotation_metadata.release_date,
            'annot_rel_num': assembly.annotation_metadata.release_number,
            'assm_level': assembly.assembly_level,
            'num_chromosomes': n_chr,
            'contig_n50': assembly.contig_n50,
            'seq_length': assembly.seq_length,
            'submission_date': assembly.submission_date }
df = pd.DataFrame.from_dict(assm_table, orient='columns')
display(df)
```


<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>GCF_004785775.1</th>
      <th>GCF_003676075.1</th>
      <th>GCF_008632895.1</th>
      <th>GCF_900094665.1</th>
      <th>GCF_900095145.1</th>
      <th>GCF_000622305.1</th>
      <th>GCF_004664715.1</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>assm_name</th>
      <td>NIH_TR_1.0</td>
      <td>ASM367607v1</td>
      <td>UCSF_Mcou_1</td>
      <td>CAROLI_EIJ_v1.1</td>
      <td>PAHARI_EIJ_v1.1</td>
      <td>S.galili_v1.0</td>
      <td>Pero_0.1</td>
    </tr>
    <tr>
      <th>org_name</th>
      <td>Grammomys surdaster</td>
      <td>yellow-bellied marmot</td>
      <td>southern multimammate mouse</td>
      <td>Ryukyu mouse</td>
      <td>shrew mouse</td>
      <td>Upper Galilee mountains blind mole rat</td>
      <td>white-footed mouse</td>
    </tr>
    <tr>
      <th>sci_name</th>
      <td>Grammomys surdaster</td>
      <td>Marmota flaviventris</td>
      <td>Mastomys coucha</td>
      <td>Mus caroli</td>
      <td>Mus pahari</td>
      <td>Nannospalax galili</td>
      <td>Peromyscus leucopus</td>
    </tr>
    <tr>
      <th>annot_rel_date</th>
      <td>2019-04-18</td>
      <td>2019-01-25</td>
      <td>2019-10-18</td>
      <td>2019-06-07</td>
      <td>2019-06-14</td>
      <td>2019-06-05</td>
      <td>2019-04-18</td>
    </tr>
    <tr>
      <th>annot_rel_num</th>
      <td>100</td>
      <td>100</td>
      <td>100</td>
      <td>101</td>
      <td>101</td>
      <td>102</td>
      <td>100</td>
    </tr>
    <tr>
      <th>assm_level</th>
      <td>Scaffold</td>
      <td>Scaffold</td>
      <td>Chromosome</td>
      <td>Chromosome</td>
      <td>Chromosome</td>
      <td>Scaffold</td>
      <td>Scaffold</td>
    </tr>
    <tr>
      <th>num_chromosomes</th>
      <td>None</td>
      <td>None</td>
      <td>4</td>
      <td>22</td>
      <td>25</td>
      <td>None</td>
      <td>None</td>
    </tr>
    <tr>
      <th>contig_n50</th>
      <td>51731</td>
      <td>130359</td>
      <td>30483</td>
      <td>30917</td>
      <td>29465</td>
      <td>30353</td>
      <td>4102727</td>
    </tr>
    <tr>
      <th>seq_length</th>
      <td>2412664998</td>
      <td>2582142248</td>
      <td>2507168619</td>
      <td>2553112587</td>
      <td>2475012951</td>
      <td>3061408210</td>
      <td>2474055010</td>
    </tr>
    <tr>
      <th>submission_date</th>
      <td>2019-04-12</td>
      <td>2018-10-22</td>
      <td>2019-09-24</td>
      <td>2017-04-28</td>
      <td>2017-04-28</td>
      <td>2014-06-05</td>
      <td>2019-04-05</td>
    </tr>
  </tbody>
</table>
</div>


#### Download package for selected assemblies

For the assemblies collected above, download a dehydrated data package (hydrated=DATA_REPORT_ONLY).  This will only contain the data report, and defer collection of nucleotide and protein sequence data until rehydration.


```python
%%time

print(f'Download a dehydrated package for {rodents_annotated_in_2020_accs}, with the ability to rehydrate with the CLI later on.')
api_response = api_instance.download_assembly_package(
    rodents_annotated_in_2020_accs,
    exclude_sequence=True,
    hydrated='DATA_REPORT_ONLY',
    _preload_content=False )

zipfile_name = 'rodent_genomes.zip'
with open(zipfile_name, 'wb') as f:
    f.write(api_response.data)

print('Download complete')
```

    Download a dehydrated package for ['GCF_011762505.1', 'GCF_903992535.1', 'GCF_000223135.1', 'GCF_003668045.3', 'GCF_012274545.1', 'GCF_003676075.2', 'GCF_000001635.27', 'GCF_000001635.26', 'GCF_903995425.1', 'GCF_004664715.2', 'GCF_011064425.1'], with the ability to rehydrate with the CLI later on.
    Download complete
    CPU times: user 3.28 ms, sys: 37.4 ms, total: 40.6 ms
    Wall time: 474 ms



```python
!unzip -v {zipfile_name}
```

    Archive:  rodent_genomes.zip
     Length   Method    Size  Cmpr    Date    Time   CRC-32   Name
    --------  ------  ------- ---- ---------- ----- --------  ----
        1599  Defl:N      770  52% 2021-11-07 02:53 8fff0de4  README.md
       22026  Defl:N     5021  77% 2021-11-07 02:53 d6e694b2  ncbi_dataset/data/assembly_data_report.jsonl
        2009  Defl:N      396  80% 2021-11-07 02:53 d7494f5c  ncbi_dataset/fetch.txt
        1939  Defl:N      292  85% 2021-11-07 02:53 2d014679  ncbi_dataset/data/dataset_catalog.json
    --------          -------  ---                            -------
       27573             6479  77%                            4 files


#### Rehydrate data package
To rehydrate, use the [NCBI Datasets command-line application](https://www.ncbi.nlm.nih.gov/datasets/docs/command-line-start/). For example, the following commands illustrate the process for Linux 
```
curl -o datasets 'https://ftp.ncbi.nlm.nih.gov/pub/datasets/command-line/LATEST/linux-amd64/datasets'  
chmod +x datasets
# specify the directory that contains the extracted zip archive after the directory flag
./datasets rehydrate --directory rodent_genomes/
```

### References
- https://www.ncbi.nlm.nih.gov/datasets/docs/v1/languages/python/api/ncbi.datasets/
