+++
title = "Reading Fasta  format in Python"
date = 2021-02-28
draft = false
tags = ["Reading Fasta format in Python"]
categories = ['File operations']
+++

## Reading Fasta format in Python

Reading or manipulating Fasta is one of the essential task in Bioinformatics data Analysis. Python offers an elegant efficient way of accomplishing this task through itertools.groupby

### Introduction to itertools.groupby

Iterators are objects representing stream of data. It is very usefull for grouping related data points in memory efficient fashion. Iterators are implemented in python built in module itertools.

#### itertools groupby example


```python
companies = [{'country': 'India', 'company': 'Flipkart'}, {'country': 'India', 'company': 'Myntra'}, {'country': 'India', 'company': 'Paytm'}, {'country': 'USA', 'company': 'Apple'}, {'country': 'USA', 'company': 'Facebook'}, {'country': 'Japan', 'company': 'Canon'}, {'country': 'Japan', 'company': 'Pixela'}]
```


```python
companies
```




    [{'country': 'India', 'company': 'Flipkart'},
     {'country': 'India', 'company': 'Myntra'},
     {'country': 'India', 'company': 'Paytm'},
     {'country': 'USA', 'company': 'Apple'},
     {'country': 'USA', 'company': 'Facebook'},
     {'country': 'Japan', 'company': 'Canon'},
     {'country': 'Japan', 'company': 'Pixela'}]




```python
from itertools import groupby
```


```python
gr = groupby(companies, key=lambda each: each['country'])
```


```python
for country_name, country_companies in gr:
    print("----")
    print(country_name)
    for company_detail in country_companies:
        print("--", company_detail['company'])
        print("----")
```

    ----
    India
    -- Flipkart
    ----
    -- Myntra
    ----
    -- Paytm
    ----
    ----
    USA
    -- Apple
    ----
    -- Facebook
    ----
    ----
    Japan
    -- Canon
    ----
    -- Pixela
    ----

From the above example we understand job of itertools groupby is to group together the items in iterable that have the same key.
In the above example companies with common country(key) are grouped together.In the context of fasta itertools.groupby can be used to group fasta header and sequence as separate groups.

```python
fasta_file = open('Test.fa','r')
```


```python
 iterator = (i[1] for i in groupby(fasta_file, lambda line: line[0] == ">"))
```
The iterator stores the iter object for all the fasta headers. Then next function coupled with for loop to iterate over fasta headers and sequences sequentially.

```python
headers = []
fasta_dict = {}
for fasta_header in iterator:
    header_line = fasta_header.__next__()[1:].strip()
    seq = "".join(s.strip() for s in iterator.__next__())
    headers.append(header_line)
    fasta_dict[header_line] = seq
```


```python
print("Total fasta sequence in Test:",'\t',len(headers))
```

    Total fasta sequence in Test: 	 6



```python
for i in fasta_dict:
    print(fasta_dict)
```

    {'YP_008530238.1 hypothetical protein SAOUHSC_00381a [Staphylococcus aureus subsp. aureus NCTC 8325]': 'MVPEEKGSITLSKEAAIIFAIAKFKPFKNRIKNNPQKTNPFLKLHENKKS', 'YP_008530239.1 membrane protein [Staphylococcus aureus subsp. aureus NCTC 8325]': 'MKQKKSKNIFWVFSILAVVFLVLFSFAVGASNVPMMILTFILLVATFGIGFTTKKKYRENDWL', 'YP_008530240.1 hypothetical protein SAOUHSC_1307a [Staphylococcus aureus subsp. aureus NCTC 8325]': 'MEQIKLKTFTAETLELLEKNINAFLSSEEATNLKLVNITIKEIEERTFPNNEEEFNAILTLSVNK', 'YP_008530241.1 large-conductance mechanosensitive channel [Staphylococcus aureus subsp. aureus NCTC 8325]': 'MLKEFKEFALKGNVLDLAIAVVMGAAFNKIISSLVENIIMPLIGKIFGSVDFAKEWSFWGIKYGLFIQSVIDFIIIAFALFIFVKIANTLMKKEEAEEEAVVEENVVLLTEIRDLLREKK', 'YP_008530242.1 membrane protein [Staphylococcus aureus subsp. aureus NCTC 8325]': 'MSILTIILIALLVILLFRVGLSILRFLIYVGLVLLCIYLGYQGLIWLLDFFQINSGFLPHFQFNN', 'YP_008530243.1 30S ribosomal protein S10 [Staphylococcus aureus subsp. aureus NCTC 8325]': 'MAKQKIRIRLKAYDHRVIDQSAEKIVETAKRSGADVSGPIPLPTEKSVYTIIRAVHKYKDSREQFEQRTHKRLIDIVNPTPKTVDALMGLNLPSGVDIEIKL'}
    
### Random access to Fasta

Iterating through dictionary can be used to randomly extract sequence based on the header.

```python
X = 'YP_008530238.1 hypothetical protein SAOUHSC_00381a [Staphylococcus aureus subsp. aureus NCTC 8325]'
if X in fasta_dict:
    print(fasta_dict[X])
```

    MVPEEKGSITLSKEAAIIFAIAKFKPFKNRIKNNPQKTNPFLKLHENKKS

### References

- https://pythonforbiologists.com/working-with-files
- https://docs.python.org/3/library/itertools.html

