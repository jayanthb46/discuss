+++
title = "Introduction to PDBFixer using NGLView"
date = 2025-02-23
draft = false
tags = ["cheminformatics"]
categories = []
+++

![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/_posts/images/23_02_2025/52428936.png)
## Introduction to PDBFixer using NGLView

 Before you can run a MD simulation, you need to prepare your protein structure. This can be a time-consuming and error-prone process, especially if you are working with a large or complex protein.

Fortunately, there is a tool called PDBFixer that can help you automate this process. PDBFixer is a free and open-source software package that can be used to fix common problems with Protein Data Bank (PDB) files, such as missing atoms, missing residues, and improper formatting.

### What can PDBFixer do?

PDBFixer can perform a variety of tasks, including:

- Adding missing atoms
- Adding missing residues
- Converting non-standard residues to standard residues
- Removing alternate conformations
- Removing unwanted chains
- Removing unwanted ligands
- Adding solvent
- Adding membranes

### Pdbfixer illustration using NGLView in python


```python
from pdbfixer import PDBFixer
from nglview import show_text, NGLWidget
from typing import Literal
from io import StringIO
from openmm.app import PDBFile
from itertools import cycle
```

```python
def fixer_to_string(fixer):
    with StringIO() as file:
        PDBFile.writeFile(fixer.topology, fixer.positions, file)
        pdb_string = file.getvalue()
    return pdb_string
    

def set_representations(
    widget: NGLWidget,
    representations: Literal['defaults', 'cartoononly', 'linesonly', 'licoriceonly', 'withunitcell', None]='defaults', 
):
    widget.clear_representations()
    if representations in ['defaults', 'withunitcell'] :
        widget.add_representation('cartoon', colorScheme='atomindex', selection='protein')
        widget.add_representation('spacefill', selection='not protein and not water')
        widget.add_representation('licorice', selection='water')
        if representations == 'withunitcell':
            widget.add_representation('unitcell')
    elif representations == 'cartoononly':        
        widget.add_representation('cartoon', colorScheme='atomindex', selection='protein')
    elif representations == 'linesonly':        
        widget.add_representation('line')
    elif representations == 'licoriceonly':        
        widget.add_representation('licorice')
    elif representations is not None:
        raise ValueError(f"Unknown representations {representations}")

    return widget

def show_fixer(
    fixer: PDBFixer, 
    representations: Literal['defaults', 'cartoononly']='defaults', 
    *args, 
    **kwargs
):
    widget = show_text(fixer_to_string(fixer), *args, **kwargs)
    set_representations(widget, representations)
    return widget
```


```python
def check_missing_residues(fixer) -> tuple[NGLWidget, str, str]:
    widget = show_fixer(fixer, 'cartoononly')
    
    colors = cycle(['red', 'green', 'blue', 'purple', 'yellow', 'grey', 'orange', 'black'])

    # Header for table
    print(f"{'Color': <10} {'Chain': <8} {'Residue': <8} Loop")
    print('-' * 80)
    
    missing_loops_selection = []
    missing_resis_selection = []
    added_resis = 0
    for color, ((chainindex, resindex), to_insert) in zip(colors, fixer.missingResidues.items()):
        chainid = [*fixer.topology.chains()][chainindex].id
    
        # Display the ends of the missing loops
        print(f"{color: <10} {chainid: <8} {resindex: <8} [{', '.join(to_insert)}]")
        widget.add_representation(
            "spacefill", 
            selection=f"({resindex} or {resindex+1}) and :{chainid}.CA",
            color=color,
        )
    
        # Build a selection string for the residues we're about to add
        loop_start = resindex + added_resis + 1
        loop_end = resindex + added_resis + len(to_insert)

        missing_loops_selection.append(f"{loop_start}-{loop_end}:{chainid}")
        
        added_resis += len(to_insert)
        
    missing_loops_selection = " or ".join(missing_loops_selection)
    
    return widget, missing_loops_selection
```


```python
pdbid = "5ap1"
```


```python
import urllib.request

urllib.request.urlretrieve(
    f"https://files.rcsb.org/download/{pdbid}.pdb", f"{pdbid}.pdb"
)
```




    ('5ap1.pdb', <http.client.HTTPMessage at 0x7d84ec356850>)




```python
fixer = PDBFixer(filename=f"{pdbid}.pdb")
```


```python
show_fixer(fixer)
```


    NGLWidget()

![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/_posts/images/23_02_2025/Screenshot%20from%202025-02-23%2017-01-18.png)

Red dots are crystallographic water oxygens. Spheres are non-polymers, cartoons are protein.

```python
fixer.findMissingResidues()

widget, missing_loops_selection = check_missing_residues(fixer)
widget
```

    Color      Chain    Residue  Loop
    --------------------------------------------------------------------------------
    red        A        0        [MET, HIS, HIS, HIS, HIS, HIS, HIS, SER, SER, GLY, VAL, ASP, LEU, GLY, THR, GLU, ASN, LEU, TYR]
    green      A        167      [SER, GLN]
    blue       A        184      [SER, ARG, GLU, ASN, GLY, LYS, SER]
    purple     A        272      [HIS, PRO, VAL, ASN, GLN, MET, ALA, LYS, GLY, THR, THR, GLU, GLU]



    NGLWidget()

![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/_posts/images/23_02_2025/Screenshot%20from%202025-02-23%2017-01-54.png)

The missing loops will be added to the model alongside the other missing heavy atoms in a later step.

### Standardizing non-standard residues


```python
from pprint import pprint

fixer.findNonstandardResidues()
```


```python
pprint(fixer.nonstandardResidues)
```

    [(<Residue 160 (TPO) of chain 0>, 'THR'),
     (<Residue 161 (TPO) of chain 0>, 'THR'),
     (<Residue 162 (SEP) of chain 0>, 'SER'),
     (<Residue 169 (TPO) of chain 0>, 'THR')]

fixer.findNonstandardResidues() stores the residues it finds in fixer.nonstandardResidues, a list of tuples defining which residues should be replaced with corresponding amino acid

```python
nsaa_selections = [
    f"{residue.index+1}:{residue.chain.id}"
    for residue, target in fixer.nonstandardResidues
]

widget = show_fixer(fixer, "cartoononly")
widget.add_representation(
    "licorice",
    selection=" or ".join(nsaa_selections),
    radius=0.25,
)
widget
```


    NGLWidget()

![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/_posts/images/23_02_2025/Screenshot%20from%202025-02-23%2017-02-47.png)

Remove atoms in non-standard residues that are not present in the target residue. For example, remove selenium from selenomethionine and replace it with sulfur. Modelled tiny molecules are difficult to protonate unambiguously and are frequently non-biologically relevant crystallization artifacts, therefore they are also deleted. Licorice displays the remaining atoms of nonstandard residues

```python
fixer.replaceNonstandardResidues()

widget = show_fixer(fixer, "cartoononly")
widget.add_representation(
    "licorice",
    selection=" or ".join(nsaa_selections),
    radius=0.25,
)
widget
```


    NGLWidget()

![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/_posts/images/23_02_2025/Screenshot%20from%202025-02-23%2017-03-16.png)

### Removing non-biopolymer components


```python
fixer.removeHeterogens(keepWater=True)

show_fixer(fixer)
```


    NGLWidget()

![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/_posts/images/23_02_2025/Screenshot%20from%202025-02-23%2017-03-57.png)

Doing this step earlier would eliminate non-standard residues before they could be corrected. All atoms that are not part of a normal DNA, RNA, or protein residue are deleted.

### Check disulfide bonds


```python
disulfides = [
    bond for bond in fixer.topology.bonds() 
    if bond.atom1.element.symbol == 'S' and bond.atom2.element.symbol == 'S'
]
print(disulfides)

w = show_fixer(fixer, 'cartoononly')
w.add_licorice(selection='CYS CYX', radius=0.5)
w
```

    NGLWidget()

![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/_posts/images/23_02_2025/Screenshot%20from%202025-02-23%2017-05-12.png)

### Restoring missing heavy atoms


```python
from itertools import chain

fixer.findMissingAtoms()
pprint(fixer.missingAtoms)
pprint(fixer.missingTerminals)

missing_atom_selections = [
    f"{residue.index+1}:{residue.chain.id}"
    for residue in chain(fixer.missingAtoms, fixer.missingTerminals)
]

widget = show_fixer(fixer, "cartoononly")
widget.add_representation(
    "licorice",
    selection=" or ".join(missing_atom_selections),
    radius=0.25,
)
widget
```

    {<Residue 32 (LYS) of chain 0>: [<Atom 3 (CG) of chain 0 residue 0 (LYS)>,
                                     <Atom 4 (CD) of chain 0 residue 0 (LYS)>,
                                     <Atom 5 (CE) of chain 0 residue 0 (LYS)>,
                                     <Atom 6 (NZ) of chain 0 residue 0 (LYS)>],
     <Residue 44 (GLU) of chain 0>: [<Atom 3 (CG) of chain 0 residue 0 (GLU)>,
                                     <Atom 4 (CD) of chain 0 residue 0 (GLU)>,
                                     <Atom 5 (OE1) of chain 0 residue 0 (GLU)>,
                                     <Atom 6 (OE2) of chain 0 residue 0 (GLU)>],
     <Residue 48 (GLN) of chain 0>: [<Atom 3 (CG) of chain 0 residue 0 (GLN)>,
                                     <Atom 4 (CD) of chain 0 residue 0 (GLN)>,
                                     <Atom 5 (OE1) of chain 0 residue 0 (GLN)>,
                                     <Atom 6 (NE2) of chain 0 residue 0 (GLN)>],
     <Residue 99 (LYS) of chain 0>: [<Atom 5 (CE) of chain 0 residue 0 (LYS)>,
                                     <Atom 6 (NZ) of chain 0 residue 0 (LYS)>],
     <Residue 100 (LYS) of chain 0>: [<Atom 3 (CG) of chain 0 residue 0 (LYS)>,
                                      <Atom 4 (CD) of chain 0 residue 0 (LYS)>,
                                      <Atom 5 (CE) of chain 0 residue 0 (LYS)>,
                                      <Atom 6 (NZ) of chain 0 residue 0 (LYS)>],
     <Residue 102 (LYS) of chain 0>: [<Atom 3 (CG) of chain 0 residue 0 (LYS)>,
                                      <Atom 4 (CD) of chain 0 residue 0 (LYS)>,
                                      <Atom 5 (CE) of chain 0 residue 0 (LYS)>,
                                      <Atom 6 (NZ) of chain 0 residue 0 (LYS)>],
     <Residue 253 (LYS) of chain 0>: [<Atom 3 (CG) of chain 0 residue 0 (LYS)>,
                                      <Atom 4 (CD) of chain 0 residue 0 (LYS)>,
                                      <Atom 5 (CE) of chain 0 residue 0 (LYS)>,
                                      <Atom 6 (NZ) of chain 0 residue 0 (LYS)>],
     <Residue 245 (LYS) of chain 0>: [<Atom 5 (CE) of chain 0 residue 0 (LYS)>,
                                      <Atom 6 (NZ) of chain 0 residue 0 (LYS)>],
     <Residue 238 (LYS) of chain 0>: [<Atom 3 (CG) of chain 0 residue 0 (LYS)>,
                                      <Atom 4 (CD) of chain 0 residue 0 (LYS)>,
                                      <Atom 5 (CE) of chain 0 residue 0 (LYS)>,
                                      <Atom 6 (NZ) of chain 0 residue 0 (LYS)>],
     <Residue 214 (ILE) of chain 0>: [<Atom 3 (CG2) of chain 0 residue 0 (ILE)>,
                                      <Atom 4 (CG1) of chain 0 residue 0 (ILE)>,
                                      <Atom 5 (CD1) of chain 0 residue 0 (ILE)>],
     <Residue 218 (SER) of chain 0>: [<Atom 3 (OG) of chain 0 residue 0 (SER)>],
     <Residue 221 (HIS) of chain 0>: [<Atom 3 (CG) of chain 0 residue 0 (HIS)>,
                                      <Atom 4 (ND1) of chain 0 residue 0 (HIS)>,
                                      <Atom 5 (CE1) of chain 0 residue 0 (HIS)>,
                                      <Atom 6 (NE2) of chain 0 residue 0 (HIS)>,
                                      <Atom 7 (CD2) of chain 0 residue 0 (HIS)>],
     <Residue 224 (ILE) of chain 0>: [<Atom 3 (CG2) of chain 0 residue 0 (ILE)>,
                                      <Atom 4 (CG1) of chain 0 residue 0 (ILE)>,
                                      <Atom 5 (CD1) of chain 0 residue 0 (ILE)>],
     <Residue 227 (ASN) of chain 0>: [<Atom 3 (CG) of chain 0 residue 0 (ASN)>,
                                      <Atom 4 (OD1) of chain 0 residue 0 (ASN)>,
                                      <Atom 5 (ND2) of chain 0 residue 0 (ASN)>],
     <Residue 229 (GLU) of chain 0>: [<Atom 4 (CD) of chain 0 residue 0 (GLU)>,
                                      <Atom 5 (OE1) of chain 0 residue 0 (GLU)>,
                                      <Atom 6 (OE2) of chain 0 residue 0 (GLU)>],
     <Residue 182 (SER) of chain 0>: [<Atom 3 (OG) of chain 0 residue 0 (SER)>],
     <Residue 183 (SER) of chain 0>: [<Atom 3 (OG) of chain 0 residue 0 (SER)>],
     <Residue 184 (LYS) of chain 0>: [<Atom 3 (CG) of chain 0 residue 0 (LYS)>,
                                      <Atom 4 (CD) of chain 0 residue 0 (LYS)>,
                                      <Atom 5 (CE) of chain 0 residue 0 (LYS)>,
                                      <Atom 6 (NZ) of chain 0 residue 0 (LYS)>],
     <Residue 185 (SER) of chain 0>: [<Atom 3 (OG) of chain 0 residue 0 (SER)>],
     <Residue 186 (LYS) of chain 0>: [<Atom 3 (CG) of chain 0 residue 0 (LYS)>,
                                      <Atom 4 (CD) of chain 0 residue 0 (LYS)>,
                                      <Atom 5 (CE) of chain 0 residue 0 (LYS)>,
                                      <Atom 6 (NZ) of chain 0 residue 0 (LYS)>],
     <Residue 179 (LYS) of chain 0>: [<Atom 3 (CG) of chain 0 residue 0 (LYS)>,
                                      <Atom 4 (CD) of chain 0 residue 0 (LYS)>,
                                      <Atom 5 (CE) of chain 0 residue 0 (LYS)>,
                                      <Atom 6 (NZ) of chain 0 residue 0 (LYS)>],
     <Residue 167 (VAL) of chain 0>: [<Atom 3 (CG1) of chain 0 residue 0 (VAL)>,
                                      <Atom 4 (CG2) of chain 0 residue 0 (VAL)>],
     <Residue 166 (ASP) of chain 0>: [<Atom 3 (CG) of chain 0 residue 0 (ASP)>,
                                      <Atom 4 (OD1) of chain 0 residue 0 (ASP)>,
                                      <Atom 5 (OD2) of chain 0 residue 0 (ASP)>],
     <Residue 165 (LYS) of chain 0>: [<Atom 3 (CG) of chain 0 residue 0 (LYS)>,
                                      <Atom 4 (CD) of chain 0 residue 0 (LYS)>,
                                      <Atom 5 (CE) of chain 0 residue 0 (LYS)>,
                                      <Atom 6 (NZ) of chain 0 residue 0 (LYS)>],
     <Residue 163 (VAL) of chain 0>: [<Atom 3 (CG1) of chain 0 residue 0 (VAL)>,
                                      <Atom 4 (CG2) of chain 0 residue 0 (VAL)>],
     <Residue 157 (GLN) of chain 0>: [<Atom 4 (CD) of chain 0 residue 0 (GLN)>,
                                      <Atom 5 (OE1) of chain 0 residue 0 (GLN)>,
                                      <Atom 6 (NE2) of chain 0 residue 0 (GLN)>],
     <Residue 154 (ASN) of chain 0>: [<Atom 3 (CG) of chain 0 residue 0 (ASN)>,
                                      <Atom 4 (OD1) of chain 0 residue 0 (ASN)>,
                                      <Atom 5 (ND2) of chain 0 residue 0 (ASN)>],
     <Residue 142 (ASP) of chain 0>: [<Atom 3 (CG) of chain 0 residue 0 (ASP)>,
                                      <Atom 4 (OD1) of chain 0 residue 0 (ASP)>,
                                      <Atom 5 (OD2) of chain 0 residue 0 (ASP)>],
     <Residue 107 (TRP) of chain 0>: [<Atom 3 (CG) of chain 0 residue 0 (TRP)>,
                                      <Atom 4 (CD1) of chain 0 residue 0 (TRP)>,
                                      <Atom 5 (NE1) of chain 0 residue 0 (TRP)>,
                                      <Atom 6 (CE2) of chain 0 residue 0 (TRP)>,
                                      <Atom 7 (CZ2) of chain 0 residue 0 (TRP)>,
                                      <Atom 8 (CH2) of chain 0 residue 0 (TRP)>,
                                      <Atom 9 (CZ3) of chain 0 residue 0 (TRP)>,
                                      <Atom 10 (CE3) of chain 0 residue 0 (TRP)>,
                                      <Atom 11 (CD2) of chain 0 residue 0 (TRP)>],
     <Residue 271 (THR) of chain 0>: [<Atom 3 (CG2) of chain 0 residue 0 (THR)>,
                                      <Atom 4 (OG1) of chain 0 residue 0 (THR)>],
     <Residue 0 (PHE) of chain 0>: [<Atom 3 (CG) of chain 0 residue 0 (PHE)>,
                                    <Atom 4 (CD1) of chain 0 residue 0 (PHE)>,
                                    <Atom 5 (CE1) of chain 0 residue 0 (PHE)>,
                                    <Atom 6 (CZ) of chain 0 residue 0 (PHE)>,
                                    <Atom 7 (CE2) of chain 0 residue 0 (PHE)>,
                                    <Atom 8 (CD2) of chain 0 residue 0 (PHE)>],
     <Residue 1 (GLN) of chain 0>: [<Atom 3 (CG) of chain 0 residue 0 (GLN)>,
                                    <Atom 4 (CD) of chain 0 residue 0 (GLN)>,
                                    <Atom 5 (OE1) of chain 0 residue 0 (GLN)>,
                                    <Atom 6 (NE2) of chain 0 residue 0 (GLN)>],
     <Residue 8 (ARG) of chain 0>: [<Atom 6 (CZ) of chain 0 residue 0 (ARG)>,
                                    <Atom 7 (NH1) of chain 0 residue 0 (ARG)>,
                                    <Atom 8 (NH2) of chain 0 residue 0 (ARG)>],
     <Residue 14 (LYS) of chain 0>: [<Atom 5 (CE) of chain 0 residue 0 (LYS)>,
                                     <Atom 6 (NZ) of chain 0 residue 0 (LYS)>],
     <Residue 30 (GLU) of chain 0>: [<Atom 3 (CG) of chain 0 residue 0 (GLU)>,
                                     <Atom 4 (CD) of chain 0 residue 0 (GLU)>,
                                     <Atom 5 (OE1) of chain 0 residue 0 (GLU)>,
                                     <Atom 6 (OE2) of chain 0 residue 0 (GLU)>],
     <Residue 31 (LYS) of chain 0>: [<Atom 3 (CG) of chain 0 residue 0 (LYS)>,
                                     <Atom 4 (CD) of chain 0 residue 0 (LYS)>,
                                     <Atom 5 (CE) of chain 0 residue 0 (LYS)>,
                                     <Atom 6 (NZ) of chain 0 residue 0 (LYS)>]}
    {}



    NGLWidget()

![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/_posts/images/23_02_2025/Screenshot%20from%202025-02-23%2017-05-47.png)

Add missing atoms from non-standard or incomplete residues. Residues for which this step has added atoms are in licorice.

```python
fixer.addMissingAtoms()

widget = show_fixer(fixer, "cartoononly")
widget.add_representation(
    "licorice",
    selection=" or ".join(missing_atom_selections),
    radius=0.25,
)
widget.add_representation(
    "backbone", selection=missing_loops_selection, radius=1, color="green"
)
widget.add_representation(
    "ball+stick", selection="ACE NME", radius=0.25
)
widget
```


    NGLWidget()

![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/_posts/images/23_02_2025/Screenshot%20from%202025-02-23%2017-06-37.png)


### Automate all the steps above


```python
# # If you don't want to check what's happening at every step,
# # all of the above can be done at once like this:
def prepare_protein(pdb_file, ph=7.0):
    
    """
    Use pdbfixer to prepare the protein from a PDB file. Hetero atoms such as ligands are
    removed and non-standard residues replaced. Missing atoms to existing residues are added.
    Missing residues are ignored by default, but can be included.

    Parameters
    ----------
    pdb_file: pathlib.Path or str
        PDB file containing the system to simulate.
  
    ph: float, optional
        pH value used to determine protonation state of residues

    Returns
    -------
    corrector: pdbfixer.pdbfixer.PDBFixer
        Prepared protein system.
    """
    
    corrector = PDBFixer(str(pdb_file))
    corrector.removeHeterogens()  # co-crystallized ligands are unknown to PDBFixer
    corrector.findMissingResidues()  # identify missing residues, needed for identification of missing atoms
    corrector.findNonstandardResidues()  # find non-standard residue
    corrector.replaceNonstandardResidues()  # replace non-standard residues with standard one
    corrector.findMissingAtoms()  # find missing heavy atoms
    corrector.addMissingAtoms()  # add missing atoms and residues
    corrector.addMissingHydrogens(ph)  # add missing hydrogens
    return corrector
```


```python
DATA="//home//ab//protein_structures//"
pdbid = "2RAP"
ligand_name = "GTP"
pdb_path = DATA + f"{pdbid}.pdb"
pdb_url = f"https://files.rcsb.org/download/{pdbid}.pdb"
```


```python
# prepare protein and build missing residues
prepared_protein = prepare_protein(pdb_path)
```


```python
show_fixer(prepared_protein)
```


    NGLWidget()

![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/_posts/images/23_02_2025/Screenshot%20from%202025-02-23%2017-07-14.png)

### References
- https://github.com/openmm/pdbfixer
- https://github.com/nglviewer/nglview