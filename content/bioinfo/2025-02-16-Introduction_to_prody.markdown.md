+++
title = "Introduction to prody"
date = 2025-02-16
draft = false
tags = ["cheminformatics"]
categories = []
+++

## Introduction to prody


This blog aims to teach basic data structures and functions in ProDy. 

First, we need to import required packages:


```python
from prody import *
from numpy import *
from matplotlib.pyplot import *
%matplotlib inline
confProDy(auto_show=False)
confProDy(auto_secondary=True)
```

    @> ProDy is configured: auto_show=False
    @> ProDy is configured: auto_secondary=True


These import commands will load numpy, matplotlib, and ProDy into the memory. `confProDy` is used to modify the default behaviors of ProDy. Here we turned off `auto_show` so that the plots can be made in the same figure, and we turn on `auto_secondary` to parse the secondary structure information whenever we load a PDB into ProDy. See [here](http://prody.csb.pitt.edu/manual/reference/prody.html?highlight=confprody#prody.confProDy) for a complete list of behaviors that can be changed by this function. This function only needs to be called once, and the setting will be remembered by ProDy.

### Loading PDB files and visualization

ProDy comes with many functions that can be used to fetch data from [Protein Data Bank](https://www.rcsb.org/).

The standard way to do this is with `parsePDB`, which will download a PDB file if needed and load it into a variable of a special data type called an `AtomGroup`:


```python
p38 = parsePDB('1p38')
p38
```

    @> Connecting wwPDB FTP server RCSB PDB (USA).
    @> Downloading PDB files via FTP failed, trying HTTP.
    @> 1p38 downloaded (1p38.pdb.gz)
    @> PDB download via HTTP completed (1 downloaded, 0 failed).
    @> 2962 atoms and 1 coordinate set(s) were parsed in 0.05s.
    @> Secondary structures were assigned to 188 residues.





    <AtomGroup: 1p38 (2962 atoms)>



To visualize the structure, we do the following:


```python
showProtein(p38);
legend();
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/images/Prody/16_02_2025/output_9_0.png)
    


If you would like to display the 3D structure using other packages or your own code, you can get the 3D coordinates via the `getCoords` method, which returns a NumPy ndarray:


```python
coords = p38.getCoords()
```


```python
coords.shape
```




    (2962, 3)



We can also visualise the contact map as follows:


```python
showContactMap(p38.ca);
```

    @> Kirchhoff was built in 0.01s.
    @> WARNING matplotlib 3.6 and later are not compatible with interactive matrices
    @> WARNING matplotlib 3.6 and later are not compatible with interactive matrices



    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/images/Prody/16_02_2025/output_14_1.png)
    


An `AtomGroup` is essentially a collection of protein atoms. Each atom can be indexed/queried/found by the following way:


```python
p38[10]
```




    <Atom: CA from 1p38 (index 10)>



This will give you the **$11^{th}$** atom from `p38`, noting that Python index starts from **0**. We can also examine the spatial location of this atom by querying the coordinates, which we can also use to highlight this atom in a plot.


```python
p38[10].getCoords()
```




    array([24.179,  4.807, 21.907])




```python
showProtein(p38);
ax3d = gca()
x, y, z = p38[10].getCoords()
ax3d.plot([x], [y], [z], 'bo', markersize=20);
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/images/Prody/16_02_2025/output_19_0.png)
    


We could select a chain, e.g. chain A, of the protein by indexing using its identifier, as follows:


```python
p38['A']
```




    <Chain: A from 1p38 (480 residues, 2962 atoms)>




```python
p38['A'].getSequence()
```




    'ERPTFYRQELNKTIWEVPERYQNLSPVGSGAYGSVCAAFDTKTGHRVAVKKLSRPFQSIIHAKRTYRELRLLKHMKHENVIGLLDVFTPARSLEEFNDVYLVTHLMGADLNNIVKCQKLTDDHVQFLIYQILRGLKYIHSADIIHRDLKPSNLAVNEDCELKILDFGLARHTDDEMTGYVATRWYRAPEIMLNWMHYNQTVDIWSVGCIMAELLTGRTLFPGTDHIDQLKLILRLVGTPGAELLKKISSESARNYIQSLAQMPKMNFANVFIGANPLAVDLLEKMLVLDSDKRITAAQALAHAYFAQYHDPDDEPVADPYDQSFESRDLLIDEWKSLTYDEVISFVPPPLD'



In many cases, it is more convenient to examine the structure with **[residue numbers](https://pdb101.rcsb.org/learn/guide-to-understanding-pdb-data/primary-sequences-and-the-pdb-format)**, and `AtomGroup` supports indexing with a chain ID and a residue number:


```python
p38[10].getResnum()
```




    5




```python
p38['A', 5]
```




    <Residue: ARG 5 from Chain A from 1p38 (11 atoms)>



This will give you the residue with the residue number of atom 10, which is an arginine in `p38`. Please note the difference between this line and the previous one. 


```python
p38['A', 5].getNames()
```




    array(['N', 'CA', 'C', 'O', 'CB', 'CG', 'CD', 'NE', 'CZ', 'NH1', 'NH2'],
          dtype='<U6')



Note that some ProDy objects may not support indexing using a chain identifier or a residue number. In such cases, we can first obtain a hierarchical view of the object:


```python
hv = p38.getHierView()
```

And then use `HierView` to index with a chain identifier and residue number as it will always be supported:


```python
hv['A', 5]
```




    <Residue: ARG 5 from Chain A from 1p38 (11 atoms)>



### Retrieving data from an AtomGroup

Many properties of the protein can be acquired by functions named like "getxxx". For instance, we can obtain the B-factors by:


```python
betas = p38.getBetas()
betas.shape
```




    (2962,)



In this way, we can obtain the B-factor for every single atom. However, in some cases, we only need to know the B-factors of alpha-carbons. We have a shortcut for this:


```python
p38.ca
```




    <Selection: 'ca' from 1p38 (351 atoms)>




```python
betas = p38.ca.getBetas()
betas.shape
```




    (351,)




```python
plot(betas);
ylabel('B-factor');
xlabel('Residue index');
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/images/Prody/16_02_2025/output_38_0.png)
    


If we would like to use residue numbers in the PDB, instead of the indices as the x-axis of the plot, it would be much more convenient to use the ProDy plotting function, `showAtomicLines`.


```python
showAtomicLines(betas, atoms=p38.ca);
ylabel('B-factor');
xlabel('Residue number');
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/images/Prody/16_02_2025/output_40_0.png)
    


We can also obtain the secondary structure information as an array:


```python
p38.ca.getSecstrs()
```




    array(['C', 'C', 'C', 'C', 'E', 'E', 'E', 'E', 'E', 'E', 'C', 'C', 'E',
           'E', 'E', 'E', 'E', 'E', 'C', 'C', 'C', 'C', 'C', 'C', 'E', 'E',
           'E', 'E', 'C', 'C', 'C', 'C', 'E', 'E', 'E', 'E', 'E', 'E', 'E',
           'C', 'C', 'C', 'C', 'C', 'C', 'E', 'E', 'E', 'E', 'E', 'E', 'E',
           'C', 'C', 'C', 'C', 'C', 'C', 'H', 'H', 'H', 'H', 'H', 'H', 'H',
           'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'C', 'C', 'C', 'C',
           'C', 'C', 'C', 'C', 'C', 'C', 'E', 'E', 'E', 'C', 'C', 'C', 'C',
           'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'E', 'E', 'E', 'E', 'E',
           'C', 'C', 'C', 'C', 'C', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'C',
           'C', 'C', 'C', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H',
           'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'C', 'C',
           'C', 'C', 'C', 'C', 'C', 'C', 'G', 'G', 'G', 'E', 'E', 'E', 'C',
           'C', 'C', 'C', 'C', 'E', 'E', 'E', 'C', 'C', 'C', 'C', 'C', 'C',
           'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'H', 'H',
           'H', 'H', 'H', 'C', 'C', 'H', 'H', 'H', 'H', 'C', 'C', 'C', 'C',
           'C', 'C', 'C', 'G', 'G', 'G', 'G', 'G', 'G', 'G', 'G', 'G', 'G',
           'G', 'G', 'G', 'G', 'G', 'G', 'G', 'C', 'C', 'C', 'C', 'C', 'C',
           'C', 'C', 'C', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H',
           'H', 'H', 'C', 'C', 'C', 'C', 'H', 'H', 'H', 'H', 'H', 'H', 'C',
           'C', 'C', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'C', 'C',
           'C', 'C', 'C', 'C', 'C', 'C', 'G', 'G', 'G', 'C', 'C', 'C', 'C',
           'C', 'C', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'C',
           'C', 'C', 'C', 'G', 'G', 'G', 'C', 'C', 'C', 'H', 'H', 'H', 'H',
           'H', 'C', 'C', 'G', 'G', 'G', 'C', 'C', 'C', 'C', 'C', 'G', 'G',
           'G', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'G', 'G', 'G',
           'G', 'C', 'C', 'C', 'C', 'H', 'H', 'H', 'H', 'H', 'H', 'H', 'H',
           'H', 'H', 'H', 'H', 'H', 'C', 'C', 'C', 'C', 'C', 'C', 'C', 'C'],
          dtype='<U1')



To make it easier to read, we can convert the array into a string using the Python's built-in function, `join` :


```python
''.join(p38.ca.getSecstrs())
```




    'CCCCEEEEEECCEEEEEECCCCCCEEEECCCCEEEEEEECCCCCCEEEEEEECCCCCCHHHHHHHHHHHHHHHHCCCCCCCCCCEEECCCCCCCCCCCCEEEEECCCCCHHHHHHHCCCCHHHHHHHHHHHHHHHHHHHHHCCCCCCCCGGGEEECCCCCEEECCCCCCCCCCCCCCCCCHHHHHCCHHHHCCCCCCCGGGGGGGGGGGGGGGGGCCCCCCCCCHHHHHHHHHHHHCCCCHHHHHHCCCHHHHHHHHHCCCCCCCCGGGCCCCCCHHHHHHHHHHCCCCGGGCCCHHHHHCCGGGCCCCCGGGCCCCCCCCCGGGGCCCCHHHHHHHHHHHHHCCCCCCCC'



`C` is for coil, `H` for alpha helix, `I` for pi helix, `G` for 3-10 helix, and `E` for beta strand (sheet). 

To get a complete list of "get" functions, you can type `p38.get<TAB>`. We provide a cell for doing this here:


```python
p38
```




    <AtomGroup: 1p38 (2962 atoms)>



The [measure](http://prody.csb.pitt.edu/manual/reference/measure/index.html?highlight=measure#module-prody.measure) module contains various additional functions for calculations for structural properties. For example, you can calculate the phi angle of the 11th residue:


```python
calcPhi(p38['A', 10])
```




    -115.5351427673999



Note that the residue at the N-terminus or C-terminus does not have a Phi or Psi angle, respectively. 

If we calculate the Phi and Psi angle for every non-terminal residue, we can obtain a [Ramachandran plot](https://en.wikipedia.org/wiki/Ramachandran_plot) for a protein. An example of Ramachandran plot for human [PCNA](https://en.wikipedia.org/wiki/Proliferating_cell_nuclear_antigen) is shown below:
<img src="https://upload.wikimedia.org/wikipedia/commons/4/43/1axc_PCNA_ProCheck_Rama.jpg" align="bottom" width="300" height="300">

Three favored regions are shown in red -- **upper left: beta sheet; center left: alpha helix; center right: left-handed helix**. Each blue data point corresponds to the two dihedrals of a residue. We will reproduce this plot for ubiquitin (only the points).


```python
chain = p38['A']
Phi = []; Psi = []; c = []
for res in chain.iterResidues():
    try:
        phi = calcPhi(res)
        psi = calcPsi(res)
    except:
        continue
    else:
        Phi.append(phi)
        Psi.append(psi)
        if res.getResname() == 'GLY':
            c.append('black')
        else:
            secstr = res.getSecstrs()[0]
            if secstr == 'H':
                c.append('red')
            elif secstr == 'G':
                c.append('darkred')
            elif secstr == 'E':
                c.append('blue')
            else:
                c.append('grey')
```

In the above code, we use an exception handler to exclude the terminal residues from the calculation.


```python
scatter(Phi, Psi, c=c, s=10);
xlabel('Phi (degree)');
ylabel('Psi (degree)');
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/images/Prody/16_02_2025/output_52_0.png)
    


### Selection

In theory you could retrieve any set of atoms by indexing the `AtomGroup`, but it would be cumbersome to do so. To make it more convienient, ProDy provides VMD-like syntax for selecting atoms. Here lists a few common selection strings. For a more complete tutorial on selection, please see [here](http://prody.csb.pitt.edu/tutorials/prody_tutorial/selection.html).


```python
ca = p38.select('calpha')
ca
```




    <Selection: 'calpha' from 1p38 (351 atoms)>




```python
bb = p38.select('backbone')
bb
```




    <Selection: 'backbone' from 1p38 (1404 atoms)>



We could also perform some simple selections right when the structure is being parsed. For example, we can specify that we would like to obtain only alpha-carbons of chain A of p38 as follows:


```python
chainA_ca = parsePDB('1p38', chain='A', subset='ca')
```

    @> Connecting wwPDB FTP server RCSB PDB (USA).
    @> Downloading PDB files via FTP failed, trying HTTP.
    @> 1p38 downloaded (1p38.pdb.gz)
    @> PDB download via HTTP completed (1 downloaded, 0 failed).
    @> 351 atoms and 1 coordinate set(s) were parsed in 0.01s.
    @> Secondary structures were assigned to 188 residues.


We could find the chain A using selection (as an alternative to the indexing method shown above):


```python
chA = p38.select('calpha and chain A')
chA
```




    <Selection: 'calpha and chain A' from 1p38 (351 atoms)>



Selection also works for finding a single residue or multiple residues:


```python
res = p38.ca.select('chain A and resnum 10')
res.getResnums()
```




    array([10])




```python
res = p38.ca.select('chain A and resnum 10 11 12')
res.getResnums()
```




    array([10, 11, 12])




```python
head = p38.ca.select('resnum < 50')
head.numAtoms()
```




    46



We can also select a range of residues as follows:


```python
fragment = p38.ca.select('resnum 50 to 100')
```

If we have data associated to the full length of the protein, we can slice the data using the `sliceAtomicData`:


```python
subbetas = sliceAtomicData(betas, atoms=p38.ca, select=fragment)
```

We can visualize the data of this range using `showAtomicLines`:


```python
showAtomicLines(subbetas, atoms=fragment);
xlabel('Residue number');
ylabel('B factor');
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/images/Prody/16_02_2025/output_70_0.png)
    


Or highlight the subset in the plot of the whole protein:


```python
showAtomicLines(betas, atoms=p38.ca, overlay=True);
showAtomicLines(subbetas, atoms=fragment, overlay=True);
xlabel('Residue number');
ylabel('B factor');
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/images/Prody/16_02_2025/output_72_0.png)
    


Selection also allows us to extract particular amino acid types:


```python
args = p38.ca.select('resname ARG')
args
```




    <Selection: '(resname ARG) and (ca)' from 1p38 (19 atoms)>



Again, combined with `sliceAtomicData` and `showAtomicLines`, we can highlight these residues in the plot of the whole protein:


```python
argbetas = sliceAtomicData(betas, atoms=p38.ca, select=args)
showAtomicLines(betas, atoms=p38.ca, overlay=True);
showAtomicLines(argbetas, atoms=args, linespec='r*', overlay=True);
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/images/Prody/16_02_2025/output_76_0.png)
    



```python
argbetas = sliceAtomicData(betas, atoms=p38.ca, select=args)
showAtomicLines(betas, atoms=p38.ca, overlay=True);
showAtomicLines(argbetas, atoms=args, linespec='r*', overlay=True);
xlabel('Residue number');
ylabel('B factor');
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/images/Prody/16_02_2025/output_77_0.png)
    


### Compare and align structures

You can also compare different structures using some of the methods in proteins module. Let’s parse another p38 MAP kinase structure. 


```python
bound = parsePDB('1zz2')
```

    @> Connecting wwPDB FTP server RCSB PDB (USA).
    @> Downloading PDB files via FTP failed, trying HTTP.
    @> 1zz2 downloaded (1zz2.pdb.gz)
    @> PDB download via HTTP completed (1 downloaded, 0 failed).
    @> 2872 atoms and 1 coordinate set(s) were parsed in 0.03s.
    @> Secondary structures were assigned to 220 residues.


You can find similar chains in structure 1p38 and 1zz2 using the `matchChains` function


```python
results = matchChains(p38, bound)
results[0]
```

    (<AtomMap: Chain A from 1p38 -> Chain A from 1zz2 from 1p38 (337 atoms)>,
     <AtomMap: Chain A from 1zz2 -> Chain A from 1p38 from 1zz2 (337 atoms)>,
     99.5475113122172,
     92.08333333333333)



In Python, a tuple (or any indexable objects) can be unpacked as follows:


```python
apo_chA, bnd_chA, seqid, overlap = results[0]

```

The first two terms are the mapping of the proteins to each other,


```python
apo_chA
```




    <AtomMap: Chain A from 1p38 -> Chain A from 1zz2 from 1p38 (337 atoms)>




```python
bnd_chA
```




    <AtomMap: Chain A from 1zz2 -> Chain A from 1p38 from 1zz2 (337 atoms)>



the third term is the sequence identity,


```python
seqid
```




    99.5475113122172



and the forth term is the sequence coverage or overlap:


```python
overlap
```




    92.08333333333333



If we calculate RMSD right now, we will obtain the value for the unsuperposed proteins:


```python
calcRMSD(bnd_chA, apo_chA)
```




    72.93023086946586




```python
showProtein(bnd_chA);
showProtein(apo_chA);
legend();
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/images/Prody/16_02_2025/output_94_0.png)
    


After superposition, the RMSD will be much improved,


```python
bnd_chA, transformation = superpose(bnd_chA, apo_chA)
calcRMSD(bnd_chA, apo_chA)
```

    @> WARNING mobile is an AtomMap instance, consider assign weights=mobile.getFlags("mapped") if there are dummy atoms in mobile
    @> WARNING target is an AtomMap instance, consider assign weights=target.getFlags("mapped") if there are dummy atoms in target





    1.8628014908695514




```python
showProtein(bnd_chA);
showProtein(apo_chA);
legend();
```

    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/images/Prody/16_02_2025/output_97_0.png)
    


We can also visualize the superposition of the full proteins as the transform matrix is applied to the entire structure:


```python
showProtein(p38);
showProtein(bound);
legend();
```


    
![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/images/Prody/16_02_2025/output_99_0.png)
    


# Advanced Visualization

Using `matplotlib`, we only obtained a very simple linear representation of proteins. ProDy also supports a more sophisticated way of visualizing proteins in 3D via [py3Dmol](http://3dmol.csb.pitt.edu/):


```python
import py3Dmol
showProtein(p38)
```

![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/refs/heads/master/images/Prody/16_02_2025/Screenshot%20from%202025-02-16%2016-43-34.png)

The limitation is that `py3Dmol` only works in an iPython notebook. You can always write out the protein to a PDB file and visualize it in an external program:


```python
writePDB('bound_aligned.pdb', bnd_chA)
```

### References

- http://www.bahargroup.org/prody/manual/reference/index.html