+++
title = "Research updates Bioinformatics January 2021"
showthedate = false
draft = false
tags = ["Research"]
categories = []
+++

## Date : February 7 2021

## Research updates Bioinformatics January 2021

In this article, we summarize the main findings made in bioinformatics this month.

### Denovo Variants and role in autism.

Autism or Autism spectrum disorder is a broad term used to describe group of neurodevelopmental disorders.  Predominant symptoms of ASD include difficulties with social interaction and communication.

New research published in nature journal describes the role of denovo mutations in ASD.

The authors have identified tandem repeats length varying from 1 to 20 bases as the main source of denovo mutation. 

 The authors have developed a bioinformatics pipeline to identify tandem repat mutations and prioritize these mutations from sequencing data. As a result, the authors have identified significant genome-wide tandem repeat mutations in the probands. 
 
The authors have developed MonSTR (https://github.com/gymreklab/STRDenovoTools) for identifying tandem repeat mutations and  SISTR (https://github.com/BonnieCSE/SISTR) for prioritizing variants .(Mitra, I., Huang, B., Mousavi, N. et al). 

### H2V A new database of human responsive genes & proteins for SARS & MERS.

H2V is a comprahensive database for  response genes and proteins in humans for SARS and MERS. This database is a collection of all human genes and proteins that respond to SARS-CoV and MERS. The information is collected from published journal articles including transcriptomic and proteomic studies surveying coronavirus infection. H2V database is freely accessible at http://www.datasj.com:40090/h2v/.( Zhou, N., Bao, J. & Ning, Y. et.al).

### A new webserver to evaluate predicted protein distances.

A new webserver called DISTEVAL is developed that evaluates the predicted protein distances. DISTEVAL assesses the predicted inter-residue contacts and distances both quantitatively and qualitatively. It can evaluate the protein distances even without the 3D structure of a protein. DISTEVAL web server is freely accessible at http://deep.cs.umsl.edu/disteval/. (Adhikari, B., Shrestha, B., Bernardini, M. et al.). 

### Novel web application for phylogenetic analysis.

 A new web application called P*R*O*P is introduced. This application generates genetic differences considering the effect of gaps. It helps in detecting important genetic differences in an evolutionary process. Users can easily upload sequence and perform phylogenetic analysis on the website. P*R*O*P is available at https://www.rs.tus.ac.jp/bioinformatics/prop. The source code is available on https://github.com/TUS-Satolab/PROP. (Nishimaki, T., Sato, K. et.al).

### Novel annotation pipeline for microbial genomes.

MicrobeAnnotator is a new functional annotation pipeline for microbial genomes. Novel feature of this pipeline is integration of KEGG orthology . This pipeline combines results from the reference protein databases such as KEGG Orthology, Gene Ontology, Interpro, Pfam, and Enzyme Commission. MicrobeAnnotator is platform-independent, written in Python, and is freely accessible on https://github.com/cruizperez/MicrobeAnnotator. (Ruiz-Perez, C.A., Conrad, R.E. & Konstantinidis, K.T. et.al).

### Pcirc novel circular RNA prediction in plants.

Circular RNA  is a single stranded RNA with closed loop structure. This tool is designed to predict novel circular RNA in plants. Tool employs random forest classifier to classify sequences based on  kmer sequence , length of ORF and splice-junction sequence information. Tool is implemented in python using scikit-learn library, and is freely accessible on https://github.com/Lilab-SNNU/Pcirc.(Yin, Shuwei, et al.). 

### References

- Mitra, I., Huang, B., Mousavi, N. et al. (2021). Patterns of de novo tandem repeat mutations and their role in autism. Nature 589, 246–250.

- Zhou, N., Bao, J. & Ning, Y. (2021). H2V: a database of human genes and proteins that respond to SARS-CoV-2, SARS-CoV, and MERS-CoV infection. BMC Bioinformatics 22, 18.

- Adhikari, B., Shrestha, B., Bernardini, M. et al. (2021). DISTEVAL: a web server for evaluating predicted protein distances. BMC Bioinformatics 22, 8.

- Nishimaki, T., Sato, K. (2021). P*R*O*P: a web application to perform phylogenetic analysis considering the effect of gaps. BMC Bioinformatics 22, 36.

- Ruiz-Perez, C.A., Conrad, R.E. & Konstantinidis, K.T. (2021). MicrobeAnnotator: a user-friendly, comprehensive functional annotation pipeline for microbial genomes. BMC Bioinformatics 22, 11.

- Yin, Shuwei, et al. "PCirc: random forest-based plant circRNA identification software." BMC bioinformatics 22.1 (2021): 1-14.
