+++
title = "Reading Fasta in R"
date = 2021-03-07
draft = false
tags = ["Reading Fasta in R"]
categories = []
+++



## Reading Fasta in R

Reading fasta is one of the essential tasks in bioinformatics.
In today's blog we explore operations on fasta file using seqinR package.

### Installing seqinR

seqinR is a popular package in bioconductor. It can be installed throgh

#### install package with bioconductor’s package manager
BiocManager::install(c("GenomicFeatures", "AnnotationDbi","seqinr"));


#### load package

```R
library("seqinr")
```
In this example we use fasta sequence from mirbase database. 
we have downloaded mature mirna from mirbase (ftp://mirbase.org/pub/mirbase/CURRENT/mature.fa.gz)

read.fasta command can be used to load fasta sequence simillar to read.csv for reading .csv files.

```R
mirna <- read.fasta('mature.fa')
```


```R
head(mirna)
```


    $`cel-let-7-5p`
     [1] "u" "g" "a" "g" "g" "u" "a" "g" "u" "a" "g" "g" "u" "u" "g" "u" "a" "u" "a"
    [20] "g" "u" "u"
    attr(,"name")
    [1] "cel-let-7-5p"
    attr(,"Annot")
    [1] ">cel-let-7-5p MIMAT0000001 Caenorhabditis elegans let-7-5p"
    attr(,"class")
    [1] "SeqFastadna"
    
    $`cel-let-7-3p`
     [1] "c" "u" "a" "u" "g" "c" "a" "a" "u" "u" "u" "u" "c" "u" "a" "c" "c" "u" "u"
    [20] "a" "c" "c"
    attr(,"name")
    [1] "cel-let-7-3p"
    attr(,"Annot")
    [1] ">cel-let-7-3p MIMAT0015091 Caenorhabditis elegans let-7-3p"
    attr(,"class")
    [1] "SeqFastadna"
    
    $`cel-lin-4-5p`
     [1] "u" "c" "c" "c" "u" "g" "a" "g" "a" "c" "c" "u" "c" "a" "a" "g" "u" "g" "u"
    [20] "g" "a"
    attr(,"name")
    [1] "cel-lin-4-5p"
    attr(,"Annot")
    [1] ">cel-lin-4-5p MIMAT0000002 Caenorhabditis elegans lin-4-5p"
    attr(,"class")
    [1] "SeqFastadna"
    
    $`cel-lin-4-3p`
     [1] "a" "c" "a" "c" "c" "u" "g" "g" "g" "c" "u" "c" "u" "c" "c" "g" "g" "g" "u"
    [20] "a" "c" "c"
    attr(,"name")
    [1] "cel-lin-4-3p"
    attr(,"Annot")
    [1] ">cel-lin-4-3p MIMAT0015092 Caenorhabditis elegans lin-4-3p"
    attr(,"class")
    [1] "SeqFastadna"
    
    $`cel-miR-1-5p`
     [1] "c" "a" "u" "a" "c" "u" "u" "c" "c" "u" "u" "a" "c" "a" "u" "g" "c" "c" "c"
    [20] "a" "u" "a"
    attr(,"name")
    [1] "cel-miR-1-5p"
    attr(,"Annot")
    [1] ">cel-miR-1-5p MIMAT0020301 Caenorhabditis elegans miR-1-5p"
    attr(,"class")
    [1] "SeqFastadna"
    
    $`cel-miR-1-3p`
     [1] "u" "g" "g" "a" "a" "u" "g" "u" "a" "a" "a" "g" "a" "a" "g" "u" "a" "u" "g"
    [20] "u" "a"
    attr(,"name")
    [1] "cel-miR-1-3p"
    attr(,"Annot")
    [1] ">cel-miR-1-3p MIMAT0000003 Caenorhabditis elegans miR-1-3p"
    attr(,"class")
    [1] "SeqFastadna"


### count total mirna in the file

```R
length(mirna)
```


48885

A total of 48885 sequences present in the fasta file. 

### Extract fasta record by index

```R
seq1 <- mirna[[1]]
```


```R
seq1
```


     [1] "u" "g" "a" "g" "g" "u" "a" "g" "u" "a" "g" "g" "u" "u" "g" "u" "a" "u" "a"
    [20] "g" "u" "u"
    attr(,"name")
    [1] "cel-let-7-5p"
    attr(,"Annot")
    [1] ">cel-let-7-5p MIMAT0000001 Caenorhabditis elegans let-7-5p"
    attr(,"class")
    [1] "SeqFastadna"

### Calculate nucleotide composition


```R
count(seq1,1)
```


    
    a c g t 
    5 0 8 0 


### Calculate di-nucleotide frequency


```R
count(seq1,2)
```


    
    aa ac ag at ca cc cg ct ga gc gg gt ta tc tg tt 
     0  0  4  0  0  0  0  0  1  0  2  0  0  0  0  0 


### Calculate GC percentage

```R
GC(seq1)
```

0.615384615384615

### Extract Fasta headers

getAnnot extracts fasta headers 

```R
annotation <- getAnnot(seq1)
```


```R
annotation
```


'cel-let-7-5p MIMAT0000001 Caenorhabditis elegans let-7-5p'



```R
annotation <- getAnnot(mirna)
```


```R
head(annotation)
```


<ol>
	<li>'cel-let-7-5p MIMAT0000001 Caenorhabditis elegans let-7-5p'</li>
	<li>'cel-let-7-3p MIMAT0015091 Caenorhabditis elegans let-7-3p'</li>
	<li>'cel-lin-4-5p MIMAT0000002 Caenorhabditis elegans lin-4-5p'</li>
	<li>'cel-lin-4-3p MIMAT0015092 Caenorhabditis elegans lin-4-3p'</li>
	<li>'cel-miR-1-5p MIMAT0020301 Caenorhabditis elegans miR-1-5p'</li>
	<li>'cel-miR-1-3p MIMAT0000003 Caenorhabditis elegans miR-1-3p'</li>
</ol>

### Subset human mirna from the file

we use grep function to extract  index of all human mirna headers from annotation(all the headers)


```R
human_mirna <- grep("Homo",annotation,ignore.case=T)
```


```R
head(human_mirna)
```


<style>
.list-inline {list-style: none; margin:0; padding: 0}
.list-inline>li {display: inline-block}
.list-inline>li:not(:last-child)::after {content: "\00b7"; padding: 0 .5ex}
</style>
<ol class=list-inline><li>113</li><li>114</li><li>115</li><li>116</li><li>117</li><li>118</li></ol>


#### Count total human miRNA

```R
length(human_mirna)
```

2656

A total of 2656 human mirna are identified by index



```R
mirna_human <- mirna[human_mirna]
```
#### Exploratory analysis on first human mirna

```R
X <- mirna_human[[1]]
```


```R
X
```


     [1] "u" "g" "a" "g" "g" "u" "a" "g" "u" "a" "g" "g" "u" "u" "g" "u" "a" "u" "a"
    [20] "g" "u" "u"
    attr(,"name")
    [1] "hsa-let-7a-5p"
    attr(,"Annot")
    [1] ">hsa-let-7a-5p MIMAT0000062 Homo sapiens let-7a-5p"
    attr(,"class")
    [1] "SeqFastadna"



```R
length(X)
```


22

Length of human mirna is 22 bp.

### Frequency Distribution of human miRNA

```R
freq_distribution <- table(getLength(mirna_human))
```


```R
barplot(freq_distribution, xlab="miRNA lengths", ylab="Frequency")
```


![png](https://gitlab.com/jayanthb46/discuss/-/raw/master/docs/img/output_25_0.png)

### Write human mirna to file

write.fasta function is used to write fasta sequence to file


```R
mirna_human_sequence <- getSequence(mirna_human)
mirna_human_annotation <- getAnnot(mirna_human)
write.fasta(mirna_human_sequence,mirna_human_annotation,'hsa_mirna.fa')
```


### References
- https://seqinr.r-forge.r-project.org/seqinr_3_1-5.pdf
- http://finzi.psych.upenn.edu/R/library/seqinr/html/read.fasta.html
- https://sites.google.com/site/rhandout/seqinr-package
