+++
title = "Six frame translation of DNA"
date = 2024-08-04
draft = false
tags = ["Bioinformatics and Genomics"]
categories = []
+++

# Six frame translation of DNA

- Steps involved in six frame translation of DNA:
- Transcription: DNA is transcribed into messenger RNA (mRNA) in the nucleus. This step involves copying the DNA sequence of a gene into a complementary RNA sequence.

DNA: 3'-ATG CCG TAA GGC-5'
mRNA: 5'-UAC GGC AUU CCG-3'

RNA Processing: In eukaryotic cells, the mRNA undergoes processing, which includes adding a 5' cap, a poly-A tail, and splicing out introns (non-coding regions).

Processed mRNA: 5'-CAP UAC GGC AUU CCG-3'-Poly-A

Translation Initiation: The processed mRNA exits the nucleus and enters the cytoplasm. The small ribosomal subunit binds to the mRNA at the start codon (AUG), and the initiator tRNA (carrying methionine) pairs with the start codon.

mRNA Codons: AUG | GGC | AUU | CCG
tRNA Anticodons: UAC | CCG | UAA | GGC

Elongation: The large ribosomal subunit binds, and the ribosome moves along the mRNA, adding amino acids to the growing polypeptide chain according to the mRNA sequence. Each tRNA molecule delivers its amino acid to the ribosome, where peptide bonds form between adjacent amino acids.

Polypeptide Chain: Met - Gly - Ile - Pro

Termination: When the ribosome encounters a stop codon on the mRNA (e.g., UAA), translation stops, and the newly synthesized polypeptide chain is released.

Stop Codon: UAA

### Python implementation of six frame translation


```python
from itertools import groupby
```


```python
d = {}
```
create a dictionary to store fasta sequences

```python
fasta_file = 'test1.fna'
```
create a fasta file called test1.fna

```python
fh = open(fasta_file, 'r')

faiter = (x[1] for x in groupby(fh, lambda line: line[0] == ">"))
for header in faiter:
    headerStr = str(next(header).strip('\n').split(' ')[0])
    long_name = headerStr.strip().replace('>', '')
    name = long_name.split()[0]
    seq = "".join(s.strip() for s in next(faiter))
    d[headerStr] = seq
```


```python
for i in d:
    print(i,'\t', len(d[i]), '\t', d[i])
```

    >lcl|AF144300.1_cds_AAD51922.1_1 	 320 	 ATGGAAAGAATAAAAGAACTAAGAGATCTAATGTCGCAGTCCCGCACTCGCGAGATACTAACAAAAACCACTGTGGATCATATGGCCATAATCAAGAAATACACATCAGGAAGACAAGAGAAGAACCCTGCTCTCAGAATGAAATGGATGATGGCAATGAAATATCCAATCACAGCAGACAAGAGAATAATGGAGATGATTCCTGAAAGGAATGAGCAAGGACAAACGCTTTGGAGCAAGACAAATGATGCTGGGTCGGACAGAGTGATGGTGTCTCCCCTAGCTGTAACTTGGTGGAACAGGAATGGGCCGACAACAAG

fasta file is read into dictionary called d. The sequence is displayed above.

```python
def rev_seq(seq):
    trans=[]
    for i in seq:
        if i=='A':
            trans.append('T')
        elif i=='C':
            trans.append('G')
        elif i=='G':
            trans.append('C')
        elif i=='T':
            trans.append('A')
        else:
            trans.append(i)
    trans=''.join(trans)
    seq_rev= trans[::-1]
    return seq_rev
```
Sequence is subjected to reverse complement to extract 3 frames in the opposite strand of DNA.

```python
for i in d:
    x = d[i]
    print('original sequence:', x)
    x1 = rev_seq(x)
    print('sequence after Reverse complement:',x1)
```

    original sequence: ATGGAAAGAATAAAAGAACTAAGAGATCTAATGTCGCAGTCCCGCACTCGCGAGATACTAACAAAAACCACTGTGGATCATATGGCCATAATCAAGAAATACACATCAGGAAGACAAGAGAAGAACCCTGCTCTCAGAATGAAATGGATGATGGCAATGAAATATCCAATCACAGCAGACAAGAGAATAATGGAGATGATTCCTGAAAGGAATGAGCAAGGACAAACGCTTTGGAGCAAGACAAATGATGCTGGGTCGGACAGAGTGATGGTGTCTCCCCTAGCTGTAACTTGGTGGAACAGGAATGGGCCGACAACAAG
    sequence after Reverse complement: CTTGTTGTCGGCCCATTCCTGTTCCACCAAGTTACAGCTAGGGGAGACACCATCACTCTGTCCGACCCAGCATCATTTGTCTTGCTCCAAAGCGTTTGTCCTTGCTCATTCCTTTCAGGAATCATCTCCATTATTCTCTTGTCTGCTGTGATTGGATATTTCATTGCCATCATCCATTTCATTCTGAGAGCAGGGTTCTTCTCTTGTCTTCCTGATGTGTATTTCTTGATTATGGCCATATGATCCACAGTGGTTTTTGTTAGTATCTCGCGAGTGCGGGACTGCGACATTAGATCTCTTAGTTCTTTTATTCTTTCCAT

The Genetic code is obtained from NCBI and stored in dictionary. The dna sequence is subjected to slicing in the window of 3 bases which represents triplet codon . The corresponding amino acid for each triplet codon is extracted to generate translated protein.

```python
def swap_dna(dnastring):
    table = {
        'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
        'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
        'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
        'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
        'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
        'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
        'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
        'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
        'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
        'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
        'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
        'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
        'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
        'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
        'TAC':'Y', 'TAT':'Y', 'TAA':'_', 'TAG':'_',
        'TGC':'C', 'TGT':'C', 'TGA':'_', 'TGG':'W',
        }
    protein = []
    end = len(dnastring) - (len(dnastring) %3) - 1
    for i in range(0,end,3):
        codon = dnastring[i:i+3]
        if codon in table:
            aminoacid = table[codon]
            protein.append(aminoacid)
        else:
            protein.append("N")
    return "".join(protein)
```


```python
def frame_id(seq):
    '''
    Usage: frame_id(dictionary['key'])
    frame_id(sequences['>Seq1'])
    six_frames = frame_id(sequences['>Seq1'])
'''
    frames = {'+1':[],'+2':[],'+3':[],'-1':[],'-2':[],'-3':[]}
    seq_rev = rev_seq(seq)
    for j in range(0,3):
        temp = ''.join([seq[j::]])
        temp_rev = ''.join([seq_rev[j::]])
        seq_trans = swap_dna(temp)
        seq_rev_trans = swap_dna(temp_rev)
        if j==0:
            frames['+1']=seq_trans
            frames['-1']=seq_rev_trans
        if j==1:
            frames['+2']=seq_trans
            frames['-2']=seq_rev_trans
        if j==2:
            frames['+3']=seq_trans
            frames['-3']=seq_rev_trans

    return frames
```


```python

```


```python
## Generates all the frames for all the sequences

def gen_frames(dictionary):
    all_dict = {}
    for key, value in dictionary.items():
        all_dict[key] = frame_id(dictionary[key])

    return all_dict
```


```python
x = gen_frames(d)
for key, value in x.items():
    print(key)
    for f in value:
        print(f)
        print(value[f])
```

    >lcl|AF144300.1_cds_AAD51922.1_1
    +1
    MERIKELRDLMSQSRTREILTKTTVDHMAIIKKYTSGRQEKNPALRMKWMMAMKYPITADKRIMEMIPERNEQGQTLWSKTNDAGSDRVMVSPLAVTWWNRNGPTT
    +2
    WKE_KN_EI_CRSPALARY_QKPLWIIWP_SRNTHQEDKRRTLLSE_NG_WQ_NIQSQQTRE_WR_FLKGMSKDKRFGARQMMLGRTE_WCLP_L_LGGTGMGRQQ
    +3
    GKNKRTKRSNVAVPHSRDTNKNHCGSYGHNQEIHIRKTREEPCSQNEMDDGNEISNHSRQENNGDDS_KE_ARTNALEQDK_CWVGQSDGVSPSCNLVEQEWADNK
    -1
    LVVGPFLFHQVTARGDTITLSDPASFVLLQSVCPCSFLSGIISIILLSAVIGYFIAIIHFILRAGFFSCLPDVYFLIMAI_STVVFVSISRVRDCDIRSLSSFILS
    -2
    LLSAHSCSTKLQLGETPSLCPTQHHLSCSKAFVLAHSFQESSPLFSCLL_LDISLPSSISF_EQGSSLVFLMCIS_LWPYDPQWFLLVSRECGTATLDLLVLLFFP
    -3
    CCRPIPVPPSYS_GRHHHSVRPSIICLAPKRLSLLIPFRNHLHYSLVCCDWIFHCHHPFHSESRVLLLSS_CVFLDYGHMIHSGFC_YLASAGLRH_IS_FFYSFH

+1, +2, +3 represents sequence in the first strand of DNA. -1, -2, -3 represents sequence in the opposite strand of DNA.

## References
- https://www.geeksforgeeks.org/dna-protein-python-3/