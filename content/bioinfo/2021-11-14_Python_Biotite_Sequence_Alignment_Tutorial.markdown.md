+++
title = "Python Biotite Sequence Alignment Tutorial"
date = 2021-11-14
draft = false
tags = ["Python Biotite"]
categories = []
+++

## Python Biotite Sequence Alignment Tutorial


```python
import matplotlib.pyplot as plt
import biotite.sequence as seq
import biotite.sequence.align as align
import biotite.sequence.io.fasta as fasta
import biotite.database.entrez as entrez
import biotite.sequence.graphics as graphics
```


Biotite provides NCBI entrez interface. In today's blog we use NCBI entrez interface to download HLA protein sequence from Human and Mouse.


```python
fasta_file = fasta.FastaFile.read(entrez.fetch_single_file(
    ["CAK22320.1", "AAH11215.1"], None, "protein", "fasta"
))
```

fasta_file biotite object can be iterated as dictionary in python. 
In this tutorial we extract human and mouse sequence from dictionary using `ProteinSequence` function provided by
biotite seq.


```python
print(dict(fasta_file.items()))
```

    {'CAK22320.1 HLA, partial [Homo sapiens]': 'SHSMRYFYTAVSRPSRGEPHFIAVGYVDDTQFVRFDSDAASPRGEPRAPWVEQEGPEYWDRETQKYKRQAQTDRVNLRKLRGYYNQSEAGSHTLQRMYGCDLGPDGRLLRGYDQSAYDGKDYIALNEDLRSWTAADTAAQITQRKWEAAREAEQWRAYLEGECVEWLRRYLENGKETLQRAEHPKTHVTHHPVSDHEATLRCWALGFYPAEITLTWQRDGEDQTQDTELVETRPAGDGTFQKWAAVVVPSGEEQRYTCHVQHEGLPEPLTLRW', 'AAH11215.1 H2-Q10 protein [Mus musculus]': 'MGAMAPRTLLLLLAAALAPTQTQAGSHSMRYFETSVSRPGLGEPRFIIVGYVDDTQFVRFDSDAETPRMEPRAPWMEQEGPEYWERETQRAKGNEQSFRVSLRTLLGYYNQSESGSHTIQWMYGCKVGSDGRFLRGYLQYAYDGRDYIALNEDLKTWTAADVAAIITRRKWEQAGAAEYYRAYLEAECVEWLLRYLELGKETLLRTDPPKTHVTHHPGSEGDVTLRCWALGFYPADITLTWQLNGEELTQDMELVETRPAGDGTFQKWASVVVPLGKEQNYTCHVYHEGLPEPLTLRWEPPPSTDSIMSHIADLLWPSLKLWWYL'}



```python
for name, sequence in fasta_file.items():
    if "CAK22320.1" in name:
        human_seq = seq.ProteinSequence(sequence)
    elif "AAH11215.1" in name:
        mouse_seq = seq.ProteinSequence(sequence)
```


```python
print(human_seq)
```

    SHSMRYFYTAVSRPSRGEPHFIAVGYVDDTQFVRFDSDAASPRGEPRAPWVEQEGPEYWDRETQKYKRQAQTDRVNLRKLRGYYNQSEAGSHTLQRMYGCDLGPDGRLLRGYDQSAYDGKDYIALNEDLRSWTAADTAAQITQRKWEAAREAEQWRAYLEGECVEWLRRYLENGKETLQRAEHPKTHVTHHPVSDHEATLRCWALGFYPAEITLTWQRDGEDQTQDTELVETRPAGDGTFQKWAAVVVPSGEEQRYTCHVQHEGLPEPLTLRW



```python
matrix = align.SubstitutionMatrix.std_protein_matrix()
```


```python
print(matrix)
```

        A   C   D   E   F   G   H   I   K   L   M   N   P   Q   R   S   T   V   W   Y   B   Z   X   *
    A   4   0  -2  -1  -2   0  -2  -1  -1  -1  -1  -2  -1  -1  -1   1   0   0  -3  -2  -2  -1   0  -4
    C   0   9  -3  -4  -2  -3  -3  -1  -3  -1  -1  -3  -3  -3  -3  -1  -1  -1  -2  -2  -3  -3  -2  -4
    D  -2  -3   6   2  -3  -1  -1  -3  -1  -4  -3   1  -1   0  -2   0  -1  -3  -4  -3   4   1  -1  -4
    E  -1  -4   2   5  -3  -2   0  -3   1  -3  -2   0  -1   2   0   0  -1  -2  -3  -2   1   4  -1  -4
    F  -2  -2  -3  -3   6  -3  -1   0  -3   0   0  -3  -4  -3  -3  -2  -2  -1   1   3  -3  -3  -1  -4
    G   0  -3  -1  -2  -3   6  -2  -4  -2  -4  -3   0  -2  -2  -2   0  -2  -3  -2  -3  -1  -2  -1  -4
    H  -2  -3  -1   0  -1  -2   8  -3  -1  -3  -2   1  -2   0   0  -1  -2  -3  -2   2   0   0  -1  -4
    I  -1  -1  -3  -3   0  -4  -3   4  -3   2   1  -3  -3  -3  -3  -2  -1   3  -3  -1  -3  -3  -1  -4
    K  -1  -3  -1   1  -3  -2  -1  -3   5  -2  -1   0  -1   1   2   0  -1  -2  -3  -2   0   1  -1  -4
    L  -1  -1  -4  -3   0  -4  -3   2  -2   4   2  -3  -3  -2  -2  -2  -1   1  -2  -1  -4  -3  -1  -4
    M  -1  -1  -3  -2   0  -3  -2   1  -1   2   5  -2  -2   0  -1  -1  -1   1  -1  -1  -3  -1  -1  -4
    N  -2  -3   1   0  -3   0   1  -3   0  -3  -2   6  -2   0   0   1   0  -3  -4  -2   3   0  -1  -4
    P  -1  -3  -1  -1  -4  -2  -2  -3  -1  -3  -2  -2   7  -1  -2  -1  -1  -2  -4  -3  -2  -1  -2  -4
    Q  -1  -3   0   2  -3  -2   0  -3   1  -2   0   0  -1   5   1   0  -1  -2  -2  -1   0   3  -1  -4
    R  -1  -3  -2   0  -3  -2   0  -3   2  -2  -1   0  -2   1   5  -1  -1  -3  -3  -2  -1   0  -1  -4
    S   1  -1   0   0  -2   0  -1  -2   0  -2  -1   1  -1   0  -1   4   1  -2  -3  -2   0   0   0  -4
    T   0  -1  -1  -1  -2  -2  -2  -1  -1  -1  -1   0  -1  -1  -1   1   5   0  -2  -2  -1  -1   0  -4
    V   0  -1  -3  -2  -1  -3  -3   3  -2   1   1  -3  -2  -2  -3  -2   0   4  -3  -1  -3  -2  -1  -4
    W  -3  -2  -4  -3   1  -2  -2  -3  -3  -2  -1  -4  -4  -2  -3  -3  -2  -3  11   2  -4  -3  -2  -4
    Y  -2  -2  -3  -2   3  -3   2  -1  -2  -1  -1  -2  -3  -1  -2  -2  -2  -1   2   7  -3  -2  -1  -4
    B  -2  -3   4   1  -3  -1   0  -3   0  -4  -3   3  -2   0  -1   0  -1  -3  -4  -3   4   1  -1  -4
    Z  -1  -3   1   4  -3  -2   0  -3   1  -3  -1   0  -1   3   0   0  -1  -2  -3  -2   1   4  -1  -4
    X   0  -2  -1  -1  -1  -1  -1  -1  -1  -1  -1  -1  -2  -1  -1   0   0  -1  -2  -1  -1  -1  -1  -4
    *  -4  -4  -4  -4  -4  -4  -4  -4  -4  -4  -4  -4  -4  -4  -4  -4  -4  -4  -4  -4  -4  -4  -4   1

Perform an optimal alignment of two sequences based on a dynamic programming algorithm. 
Biotite provides align_optimal function which is an implementation of Needleman-Wunsch algorithm and Smith–Waterman algorithm.

```python
alignments = align.align_optimal(human_seq, mouse_seq, matrix,
                                 gap_penalty=(-10, -1), terminal_penalty=False)
```
alignments can iterated using typical for loop to visualize alignments.

```python
for x in alignments:
    print(x)
```

    -------------------------SHSMRYFYTAVSRPSRGEPHFIAVGYVDDTQFVRFDSDAASPRGE
    MGAMAPRTLLLLLAAALAPTQTQAGSHSMRYFETSVSRPGLGEPRFIIVGYVDDTQFVRFDSDAETPRME
    
    PRAPWVEQEGPEYWDRETQKYKRQAQTDRVNLRKLRGYYNQSEAGSHTLQRMYGCDLGPDGRLLRGYDQS
    PRAPWMEQEGPEYWERETQRAKGNEQSFRVSLRTLLGYYNQSESGSHTIQWMYGCKVGSDGRFLRGYLQY
    
    AYDGKDYIALNEDLRSWTAADTAAQITQRKWEAAREAEQWRAYLEGECVEWLRRYLENGKETLQRAEHPK
    AYDGRDYIALNEDLKTWTAADVAAIITRRKWEQAGAAEYYRAYLEAECVEWLLRYLELGKETLLRTDPPK
    
    THVTHHPVSDHEATLRCWALGFYPAEITLTWQRDGEDQTQDTELVETRPAGDGTFQKWAAVVVPSGEEQR
    THVTHHPGSEGDVTLRCWALGFYPADITLTWQLNGEELTQDMELVETRPAGDGTFQKWASVVVPLGKEQN
    
    YTCHVQHEGLPEPLTLRW---------------------------
    YTCHVYHEGLPEPLTLRWEPPPSTDSIMSHIADLLWPSLKLWWYL

Alignments can be visualized using matplotlib library provided in python.

```python
fig = plt.figure(figsize=(8.0, 2.5))
ax = fig.add_subplot(111)
graphics.plot_alignment_similarity_based(
    ax, alignments[0], matrix=matrix, labels=["Human", "Mouse"],
    show_numbers=True, show_line_position=True
)
fig.tight_layout()

plt.show()
```


![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/Nov_14_2021/output_14_0.png)


### Reference
- https://www.biotite-python.org/apidoc/biotite.sequence.io.fasta.html.