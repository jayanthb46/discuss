+++
title = "Python Biotite Multiple Sequence Alignment Tutorial"
date = 2021-12-12
draft = false
tags = ["Python Biotite"]
categories = []
+++


### Python Biotite Multiple Sequence Alignment Tutorial
RBD (receptor-binding domain) of the S1 subunit of the spike (S) protein from different highly pathogenic human virus, severe acute respiratory syndrome coronavirus 2 (SARS-cov2) used as example for analysis.

Variants of concern (VOC) such as Delta , Beta and newly discovered omicron RBD sequences is used in the example analysis performed below.

align_multiple() function provided by Biotite used for Multiple sequence Alignment analysis of RBD S1 subunit.

```python
import numpy as np
import matplotlib.pyplot as plt
import biotite.sequence as seq
import biotite.sequence.io.fasta as fasta
import biotite.sequence.align as align
import biotite.sequence.graphics as graphics
import biotite.database.entrez as entrez
```


```python
fasta_file = fasta.FastaFile.read("//home//ab//Desktop//rbd.aa.fasta")
```


```python
ids = []
sequences = []
```


```python
for header, seq_str in fasta_file.items():
    # Extract the UniProt Entry name from header
    identifier = header.split("|")[0].split()[0]
    ids.append(identifier)
    sequences.append(seq.ProteinSequence(seq_str))
```


```python
matrix = align.SubstitutionMatrix.std_protein_matrix()
alignment, order, tree, distances = align.align_multiple(
    sequences, matrix, gap_penalty=(-10,-1), terminal_penalty=False
)
```


```python
alignment = alignment[:, order]
ids = [ids[i] for i in order]
```


```python
fig = plt.figure(figsize=(8.0, 20.0))
ax = fig.add_subplot(111)
graphics.plot_alignment_type_based(
    ax, alignment, labels=ids, show_numbers=True, spacing=2.0
)
fig.tight_layout()

plt.show()
```


![png](https://raw.githubusercontent.com/balakuntlaJayanth/Stats/master/images/Dec12_2021/output_8_0.png)

### References
- https://www.nature.com/articles/s41586-020-2180-5